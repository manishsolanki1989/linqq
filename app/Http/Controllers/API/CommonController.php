<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Notification;
use App\Officer;
use App\OfficerToken;
use App\User;
use App\UserToken;
use DB;
use DateTime;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use Validator;

class CommonController extends Controller
{

    public function validate_request(Request $request, $rules = null)
    {
        if (!empty($rules)) {
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->error_message('403', $error[0], $request->path());
            } else {
                return true;
            }
        }
    }

    public function upload_image(Request $request,$file_name=null,$dirName = null, $resizeDirName = null)
    {
        $image = ($request->file($file_name));// dd($image->getClientOriginalExtension());
        if (!empty($image)) {
            $dirName       = !empty($dirName) ? $dirName : "profile";
            $resizeDirName = !empty($resizeDirName) ? $resizeDirName : "profile_resize";
            $extension     = $image->getClientOriginalExtension(); // getting image
            $fileName      = date("YmdHis") . mt_rand(100000, 999999) . '.' . $extension;
            $path          = public_path($dirName . '/' . $fileName);
            $pathResize    = public_path($resizeDirName . '/' . $fileName);
            Image::make($image->getRealPath())->save($path);
            Image::make($image->getRealPath())->resize(300, 300)->save($pathResize);
            return $fileName;
        } else {
            $this->error_message("Image is required.", "upload_image");
        }
    }

    public function upload_file(Request $request, $file_name = null,$folder_name = null)
    {
        if (!empty($file_name)) {
            $user_file = ($request->file($file_name));
            if (!empty($user_file)) {
                $folder_name     = !empty($folder_name) ? $folder_name : "attachment";
                $destinationPath = public_path() . '/' . $folder_name . '/';
                $extension       = $user_file->getClientOriginalExtension(); // getting file
                $fileName        = date("YmdHis") . mt_rand(100000, 999999) . '.' . $extension;
                $user_file->move($destinationPath, $fileName);
                return $fileName;
            } else {
                $this->error_message("File is required.", "upload_file");
            }
        } else {
            $this->error_message("File  name is required.", "upload_file");
        }
    }

    public function is_file($folder_name = null, $file_name = null)
    {
        $path = "";

        if (!empty($folder_name) && !empty($file_name)) {
            $file_path = public_path() . '/' . $folder_name . '/' . $file_name; 
            if (file_exists($file_path)) {
                $path      = url("/public") . "/" . $folder_name . '/' . $file_name;
            }
        }
        return $path;
    }

    public function is_filetemp($folder_name = null, $file_name = null)
    {
        $path = "";

        if (!empty($folder_name) && !empty($file_name)) {
            $file_path = public_path() . '/' . $folder_name . '/' . $file_name; 
            if (file_exists($file_path)) {
                
                $path      = url("/public") . "/" . $folder_name . '/' . $file_name;
           
            } else {

                 $path      = url("/public").'/img/default_pic3.png';
            }  
        } else {
        
                $path      = url("/public") . "/img/default_pic3.png";

        }
        return $path;
    }

    public function save_user_token(Request $request)
    {
        $tokenKey   = str_random(50);
        $lang       = !empty($request->language) ? $request->language : 'ENG';
        $matchThese = array('user_id' => $request->user_id);
        $saveThese  = array('device_type' => $request->device_type, 'device_id' => $request->device_id, 'token' => $tokenKey, 'language' => $lang);
        $token      = UserToken::updateOrCreate($matchThese, $saveThese);
        return $token;
    }

    

    public function check_token($tokenId = null)
    {
        $token = UserToken::where('token', '=', $tokenId)->first();
        if (!empty($token)) {
            $user = User::where("id", "=", $token->user_id)->first();
            if (!empty($user)) {
                if ($user->status == "INACTIVE") {
                    $this->error_message("403","Your account is deactivated.", "check_token");
                } else {
                    return $user->id;
                }
            } else {
                $this->error_message("403","Your account is deleted.", "check_token");
            }
        } else {
            $this->error_message("401","Please login.", "check_token");
        }
    }

    public function android_push($deviceToken = null, $message = null, $type = null,$badge = null,$batch = array())
    {
        try{
        $this->autoRender = false;
        $this->layout     = false;
        $url              = 'https://android.googleapis.com/gcm/send';
        $message          = array("message" => $message, "type" => $type, "batch" => $batch,'badge'=>$badge,'sound' => 'default');
        $registatoin_ids  = array($deviceToken);
        $fields           = array('registration_ids' => $registatoin_ids, 'data' => $message);
        //$GOOGLE_API_KEY   = "AIzaSyCT1hAiELV9ogaDsrPfyTlBJ9ocI5jRqE0";
        $GOOGLE_API_KEY   = "AIzaSyBQzgxWDDe2u_UEk8VKbC5Bg6CUfrVTIUY";
        $headers          = array(
            'Authorization: key=' . $GOOGLE_API_KEY,
            'Content-Type: application/json',
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === false) {
            //die('Curl failed: ' . curl_error($ch));
        } else {
            //print_r("success");die;
        }
        curl_close($ch);

    }
    catch (\Exception $e) {

    }

    }

    public function iphone_push($deviceToken = null, $message = null, $type = null,$badge=null,$batch = array())
    {
        try{
        $deviceToken      = $deviceToken; //"01FE59D85A1F62728541988192F92AF840D098F89254D607D6D7B97F250D12BB";//
        $passphrase       = '123456';
        $Text             = $message;
        $this->autoRender = false; 
        $this->layout     = false;
        $basePath         = public_path() . '/pem/pushcert.pem';
        if (empty($batch)) {
            $batch = array("test" => "test");
        }
        if (file_exists($basePath)) {
            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', $basePath);
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client(
                'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx
            );
            if (!$fp) {
                exit("Failed to connect: $err $errstr" . PHP_EOL);
            }
            //$body['aps'] = array('alert' => array("body" => $Text, 'type' => $type,'badge'=>$badge,'batch' => $batch),'sound' => 'default');
            $body['aps'] = array('alert' => array("body" => $Text, 'type' => $type),'badge'=>$badge,'sound' => 'default');
            
            $body['bad'] = $batch;
            $payload     = json_encode($body);
            $msg         = chr(0) . pack('n', 32) . pack("H*", $deviceToken) . pack('n', strlen($payload)) . $payload;
            $result      = fwrite($fp, $msg, strlen($msg)); 
            if (!$result) {
               /* echo 'Message not delivered' . PHP_EOL;
                echo $result;
                echo "Failure";*/
            } else {
                // var_dump($result);
                //print_r($msg);

                //echo "success";die;
            }
            fclose($fp);

        }

    }catch (\Exception $e) {}

    }  


     public function android_pushh($deviceToken = null, $message = null, $type = null,$badge = null,$batch = array())
    {
        $this->autoRender = false;
        $this->layout     = false;
        $url              = 'https://android.googleapis.com/gcm/send';
        $message          = array("message" => $badge['sender_name'].' sent you a message.', "type" => $type, "batch" => $batch,'badge'=>$badge,'sound' => 'default');
        $registatoin_ids  = array($deviceToken); 
        $fields           = array('registration_ids' => $registatoin_ids, 'data' => $message);
          //$GOOGLE_API_KEY   = "AIzaSyCT1hAiELV9ogaDsrPfyTlBJ9ocI5jRqE0";
        $GOOGLE_API_KEY   = "AIzaSyBQzgxWDDe2u_UEk8VKbC5Bg6CUfrVTIUY";
        $headers          = array(
            'Authorization: key=' . $GOOGLE_API_KEY,
            'Content-Type: application/json',
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === false) {
            //die('Curl failed: ' . curl_error($ch));
        } else {
            //print_r("success");die;
        }
        curl_close($ch);
    }


    public function iphone_pushh($deviceToken = null, $message = null, $type = null,$badge=null,$count = null,$content_available=null)
    {
        $deviceToken      = $deviceToken; //"01FE59D85A1F62728541988192F92AF840D098F89254D607D6D7B97F250D12BB";//
        $passphrase       = '123456';
        $Text             = $message;
        $this->autoRender = false; 
        $this->layout     = false;
        $basePath         = public_path() . '/pem/pushcert.pem';
        // if (empty($batch)) {
        //     $batch = array("test" => "test");
        // }
        if (file_exists($basePath)) {
            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', $basePath);
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client(
                'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx
            );
            if (!$fp) {
                exit("Failed to connect: $err $errstr" . PHP_EOL);
            }
           // $body['aps'] = array('alert' => array("body" =>$badge['sender_name'].' sent you a message.', 'type' => $type,'badge'=>$badge,'batch' => $batch),'sound' => 'default');
           
             $body['aps'] = array('alert' => array("body" =>$badge['sender_name'].' sent you a message.', 'type' => $type),'badge'=>$count,'sound' => 'default','content-available'=>$content_available);
             $body['bad']=$badge;
              
              //$body['aps'] = array('alert' => $badge['sender_name'].' sent you a message.','type'=$type,'badge'=>$badge,,'sound' => 'default');  

            $payload     = json_encode($body);
            $msg         = chr(0) . pack('n', 32) . pack("H*", $deviceToken) . pack('n', strlen($payload)) . $payload;
            $result      = fwrite($fp, $msg, strlen($msg)); 
            
            if (!$result) {
               /* echo 'Message not delivered' . PHP_EOL;
                echo $result;
                echo "Failure";*/
            } else {
                // var_dump($result);
                //print_r($msg);

                //echo "success";die;
            }
            fclose($fp);
        }
    }

    public function user_device_id($userId = null)
    {
        if (!empty($userId)) {
            $token = User::where('id', '=', $userId)
                ->where('device_id', '<>', "")
                ->first();
            if (count($token)) {
                return $token;
            } else {
                return false;
            }
        }
    }

    public function save_notification($user_type = null, $user_id = null, $officer_id = null, $request_id = null, $officer_request_id = null, $type = null, $message = null)
    {
        $notificationObj                     = new Notification;
        $notificationObj->user_type          = $user_type;
        $notificationObj->user_id            = $user_id;
        $notificationObj->officer_id         = $officer_id;
        $notificationObj->request_id         = $request_id;
        $notificationObj->officer_request_id = $officer_request_id;
        $notificationObj->type               = $type;
        $notificationObj->message            = $message;
        $notificationObj->date               = date("Y-m-d");
        $notificationObj->time               = date("H:i:s");
        if ($notificationObj->save()) {
            return true;
        } else {
            return false;
        }
    }

    public function error_message($status_code = null, $message = null, $requestKey = null)
    {
        $status_code = !empty($status_code)?$status_code:"403";
        exit(response()->json(
            ["status_code"=>$status_code, 'status' => 'FAILURE', "message" => $message, "requestKey" => $requestKey]
        )->getContent());
    }


    public function set_empty($data = array()){
        foreach ($data as $key => $value) { 
            if (empty($value)) {
                $data->$key = "";
            }
        }
        return $data;
    }

    public function set_blank($data = null)
    {
        if (!empty($data)) {
            $data = json_decode(json_encode($data));
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
        }
        return $data;
    }

    public function set_data($status_code = null, $message = null, $requestKey = null, $responseKey = null, $data = null)
    {
        if (!empty($data)) {
            $response = array();
            if (count($data) > 1) {
                foreach ($data as $value) {
                    $response[] = $this->set_blank($value);
                }
            } else {
                $response = $this->set_blank($data);
            } 
             
            

                
                 exit(response()->json(
                [
                    "status_code"=>$status_code,
                    'status'     => 'SUCCESS',
                    $responseKey => $response,
                    "message"    => $message,
                    "requestKey" => $requestKey,
                ]
            )->getContent());

           

           
        } else if (!empty($responseKey)) {
            exit(response()->json(
                [
                    "status_code"=>$status_code,
                    'status'     => 'SUCCESS',
                    $responseKey => array(),
                    "message"    => $message,
                    "requestKey" => $requestKey,
                ]
            )->getContent());
        } else {
            exit(response()->json(
                [
                    "status_code"=>$status_code,
                    'status'     => 'SUCCESS',
                    "message"    => $message,
                    "requestKey" => $requestKey,
                ]
            )->getContent());
        }
    }  
  


    public function set_datadeep($status_code = null, $message = null, $requestKey = null, $responseKey = null, $data = null,$count=null,$tot=null,$tstatus=null,$cdd=null) 
    {
        if (!empty($data)) {
            $response = array();
            if (count($data) > 1) {
                foreach ($data as $value) {
                    $response[] = $this->set_blank($value);
                }
            } else {
                $response = $this->set_blank($data);
            } 
             
            if($count >= 0){  
 

                 exit(response()->json(
                [
                    "status_code" =>$status_code,
                    'status'      => 'SUCCESS',
                    $responseKey  => $response,
                    "message"     => $message,
                    "requestKey"  => $requestKey,
                    "badge"       => $count,
                    "total_rec"   => $tot, 
                    'timer_status'=> $tstatus,
                    'card_count'=> $cdd,
                ]
            )->getContent());
         
      
          } else {

                
                 exit(response()->json(
                [
                    "status_codee"=>$status_code,
                    'status'     => 'SUCCESS',
                    $responseKey => $response,
                    "message"    => $message,
                    "requestKey" => $requestKey,
                ]
            )->getContent());

            } 

           
        } else if (!empty($responseKey)) {
            exit(response()->json(
                [
                    "status_code"=>$status_code,
                    'status'     => 'SUCCESS',
                    $responseKey => array(),
                    "message"    => $message,
                    "requestKey" => $requestKey,
                    "timer_status" => $tstatus,
                    "badge"       => $count,
                    "total_rec"   => $tot, 
					 'card_count'=> $cdd,
                ]
            )->getContent());
        } else {
            exit(response()->json(
                [
                    "status_code"=>$status_code,
                    'status'     => 'SUCCESS',
                    "message"    => $message,
                    "requestKey" => $requestKey,
                ]
            )->getContent());
        }
    }  




    public function get_date_formate($currentDate = '', $pastDate = '')
    {
        if (!empty($pastDate) && !empty($currentDate)) {
            $pastDate    = new DateTime($pastDate);
            $currentDate = new DateTime($currentDate);
            $interval    = $pastDate->diff($currentDate);
            $date        = "";
            if ($interval->format("%a") != "0") {
                $date = $interval->format("%a Days Ago");
            }
            if ($interval->format("%m") != "0") {
                $date = $interval->format("%m Month");
            }
            if ($interval->format("%Y") != "0") {
                $date = $interval->format("%Y Year");
            }
            if ($interval->format("%a") == "1") {
                $date = "Yesterday";
            }
            if ($interval->format("%a") == "0") {
                $date = "Today";
            }
            return $date;
        } else {
            return false;
        }
    }

    public function get_distancee($lat, $long, $lats, $longs)
    {
        //Get latitude and longitude from geo data
        $latitudeFrom  = $lat;
        $longitudeFrom = $long;
        $latitudeTo    = $lats;
        $longitudeTo   = $longs;

        //Calculate distance from latitude and longitude
        $theta = $longitudeFrom - $longitudeTo;
        $dist  = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) + cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
        $dist  = acos($dist);
        $dist  = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        //$unit = strtoupper($unit);
        $dis = number_format((float) $miles * 1.609344, 2, '.', '');
        return $dis;
    }

    public function time_string($datetime, $full = false)
    {
        $now  = new DateTime;
        $ago  = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) {
            $string = array_slice($string, 0, 1);
        }

        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public function time_micro($micro=null){
        if (!empty($micro)) {
            $mt = explode(' ', microtime($micro));
            return ((int)$mt[1]) * 1000000 + ((int)round($mt[0] * 1000000));
        }
    }

    public function get_code()
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res   = "";
        for ($i = 0; $i < 10; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        return $res;
    }

   
}
