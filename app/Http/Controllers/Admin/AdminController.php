<?php

namespace App\Http\Controllers\Admin;

use App\Advertisement;
use App\Http\Controllers\Controller;
use App\Industry;
use App\Interest;
use App\User;
use App\Degree;
use App\AdvertisementCount;
use App\MessageTemplate;
use App\UserReport;
use App\RequestTemplate;
use App\UserNotification;
use App\UserEducation;
use App\Company;
use App\Designation;
use App\UserFavourite;
use App\UserInterest;
use App\UserNetworking;
use App\UserOrganisation;
use App\LocationRadius;
use App\Card;
use App\UserProfileAdd;
use App\Networking;
use App\Favourite;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use Mail;
use Redirect;
use Session;
use View;
use Crypt;
use Edujugon\PushNotification\PushNotification;


class AdminController extends Controller
{

    public function index(Request $request)
    {

    }
    
    
    public function push_notification_instance_creation_without_argument_set_gcm_as_service()
    {
        $push = new PushNotification();

        $this->assertInstanceOf('Edujugon\PushNotification\Gcm', $push->service);
    }



    

public function send_push_to_iphone_user($devicetokens,$msg=null,$type=null,$badge=null,$batch=array())
{
    
       
        try{
        $push = new PushNotification('apn');
        $push->setMessage([
                    'aps' => [
                        'alert' => [
                            'type' => "admin",
                            'body'  => $msg,
                            
                        ],
                        'sound' => 'default',
                        'badge' =>$badge
                    ],
                    'extraPayLoad' => [
                'badge' =>$badge,
            ]
                ])
            ->setDevicesToken([$devicetokens])
            ->send();
            
         //    print_r($push->feedback);die;
        }
        catch (Exception $e) {  }
    
}



public function send_push_to_android_user($devicetokens,$msg=null,$type=null,$badge=null,$batch=array())
{
      
   
        try{
        $push = new PushNotification('gcm');
        $push->setMessage([
        'registration_ids'=>['data' => [
                'message'=>$msg,
                'sound' => 'default',
                'type'  =>'admin',
                'batch' =>$batch,
                'badge' =>$badge
                ]
                ]
        ])->setDevicesToken([$devicetokens])->send();

        }
       catch (Exception $e) { }
}


public function save_notification($sender_id, $receiver_id, $type, $message)
    
    { 
        try {
            $create              = new UserNotification;
            $create->sender_id   = $sender_id;
            $create->receiver_id = $receiver_id;
            $create->type        = $type;
            $create->message     = $message;
            $create->save();
            return $create->id; 
        } catch (Exception $e) {
          
            //$this->obj->error_message('500', "Internal error.", $request->path());
       
        }
   }


public function send_notification(Request $request)
{
    
   if(!empty($_POST['userids']))
   {
        $u_id = explode(',',$_POST['userids']);
        $devicetoken['android'] = User::select('device_id', 'id')->whereIn('id',$u_id)->where('device_type','android')->where('device_id','!=','')->get()->toArray();
        $devicetoken['iphone']= User::select('device_id', 'id')->whereIn('id',$u_id)->where('device_type','iphone')->where('device_id','!=','')->get()->toArray();
       
      
        
        $msg = $_POST['message'];

            if(!empty($devicetoken['android']))
            {
                foreach ($devicetoken['android'] as $value) {

                            $not_id = $this->save_notification("38",$value['id'],"admin",$msg); 
                            
                            $batch=array();
                            $batch['type']        =  "admin";
                            $batch['sender_id']   =  "38";
                            $batch['receiver_id'] =  $value['id']; 
                            $batch['message']     = $msg;
                            $batch['not_id']      =  (string)$not_id;
                          
                           $ncount = UserNotification::where('receiver_id',$batch['receiver_id'])->where('status','Unread')->count();

                   $android_divice_id[0] =$value['device_id'];
                   $this->send_push_to_android_user($android_divice_id,$msg,"admin",$ncount,$batch);
                }
             
            }
            
            if(!empty($devicetoken['iphone']))
            {
                foreach ($devicetoken['iphone'] as $value) {

                    $not_id = $this->save_notification("38",$value['id'],"admin",$msg); 
                    
                     $batch=array();
                    $batch['type']        =  "admin";
                    $batch['sender_id']   =  "38";
                    $batch['receiver_id'] =  $value['id']; 
                    $batch['message']     = $msg;
                    $batch['not_id']      =  (string)$not_id;
                  
                   $ncount = UserNotification::where('receiver_id',$batch['receiver_id'])->where('status','Unread')->count();

                    $iphone_divice_id[0] =$value['device_id'];
                    $this->send_push_to_iphone_user($iphone_divice_id,$msg,"admin",$ncount,$batch);
                }
            }
           
          
            
            
            $request->session()->flash('alert-success', 'Send successfully.');
            return redirect('/admin/data_list_notification');
           
            
      //$devicetoken  }
       
        
      
   }
   else
   {
    
        $devicetoken['android'] = User::select('device_id', 'id')->where('device_type','android')->where('device_id','!=','')->get()->toArray();
        $devicetoken['iphone']= User::select('device_id', 'id')->where('device_type','iphone')->where('device_id','!=','')->get()->toArray();
       
      
        $query = "SELECT device_id,device_type FROM users";
        $users = DB::select($query); 
       
            
            $msg = $_POST['message'];

            if(!empty($devicetoken['android']))
            {
                foreach ($devicetoken['android'] as $value) {

                            $not_id = $this->save_notification("38",$value['id'],"admin",$msg); 
                            
                            $batch=array();
                            $batch['type']        =  "admin";
                            $batch['sender_id']   =  "38";
                            $batch['receiver_id'] =  $value['id']; 
                            $batch['message']     = $msg;
                            $batch['not_id']      =  (string)$not_id;
                          
                           $ncount = UserNotification::where('receiver_id',$batch['receiver_id'])->where('status','Unread')->count();

                   $android_divice_id[0] =$value['device_id'];
                   $this->send_push_to_android_user($android_divice_id,$msg,"admin",$ncount,$batch);
                }
             
            }
            
            if(!empty($devicetoken['iphone']))
            {
                foreach ($devicetoken['iphone'] as $value) {

                    $not_id = $this->save_notification("38",$value['id'],"admin",$msg); 
                    
                     $batch=array();
                    $batch['type']        =  "admin";
                    $batch['sender_id']   =  "38";
                    $batch['receiver_id'] =  $value['id']; 
                    $batch['message']     = $msg;
                    $batch['not_id']      =  (string)$not_id;
                  
                   $ncount = UserNotification::where('receiver_id',$batch['receiver_id'])->where('status','Unread')->count();

                    $iphone_divice_id[0] =$value['device_id'];
                    $this->send_push_to_iphone_user($iphone_divice_id,$msg,"admin",$ncount,$batch);
                }
            }
           
           
            
            
            $request->session()->flash('alert-success', 'Send successfully.');
            return redirect('/admin/data_list_notification');
           
            
       // }
        
        
   }
   // print_r($_POST);die;
}





public function testPush(Request $request)
{




        $devicetoken= "32955BB526A6EABD1B6BF68F7E9C1AD017A6FF004E7FCD5FE82383FBC2D6BE90";
        
        $push = new PushNotification('apn');

        $push->setMessage([
                    'aps' => [
                        'alert' => [
                            'type' => "admin",
                            'body'  => "asdfasdfads"
                        ],
                        'sound' => 'default'
                    ]
                ])
            ->setDevicesToken([$devicetoken])
            ->send();


        if(isset($push->feedback->error)){
            ////\Log::info('ERRO notificação ios - ' . $push->feedback->error);
        } else {
            // \Log::info('Enviado com sucesso - ' . $devicetoken);
        }

       //  $this->assertInstanceOf('stdClass', $push->getFeedback());

}




    public function login(Request $request)
    {
        if ((Auth::user())) {
            return redirect()->intended(route('admins.user_list'));
        } else {
            if ($request->isMethod('post')) {
                $rule = $this->validate($request, [
                    'email'    => 'required|email|max:190',
                    'password' => 'required|max:190',
                ]);

                if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_admin' => '1'], $request->remember)) {
                     return redirect('admin/user_list');
                } else {
                    $request->session()->flash('alert-danger', 'Wrong Credentials ');
                    return redirect('/admin/login');
                }
            }
            return view('admins.login');
        }

    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/admin/login');
    }

   public function user_list(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $obj   = new CommonAdminController;
            $users = User::where('is_admin', '0')->orderBy('id', 'DESC')->get();
            foreach ($users as $user) {
                $user->image      = $obj->is_file("profile_resize", $user->image);
                $user->created_on = date("dFY", strtotime($user->created_at));
            }
            return view('admins.user_list', compact('users'));
        }
    }
   
   
     public function data_list(Request $request)
   
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $obj   = new CommonAdminController;
            $interests = Interest::orderBy('name','asc')->get();
            $networkings = Networking::orderBy('name','asc')->get();
            $favourites = Favourite::orderBy('name','asc')->get();
            $industries = Industry::orderBy('name','asc')->get();
            $degrees = Degree::orderBy('name','asc')->get();
            $fields       = " users.id AS user_id, users.name,users.created_at,users.phone,users.gender, users.location, users.designation, users.organisation, users.image ";
            $condition    = " users.is_admin = '0' ";
            $total_user = User::where('is_admin', '0')->orderBy('id', 'DESC')->count();
            $count_name = "";
            $count_location = "";
            $count_networking = "";
            $count_gender = "";
            $count_industry = "";
            $count_interest = "";
            $count_favourite = "";
            $count_gender = "";
            $count_degree = "";
            $label="";

            if (!empty($request->name)) {
                $condition .= " AND ( users.name LIKE '%$request->name%' OR users.designation LIKE '%$request->name%' OR users. organisation LIKE '%$request->name%' ) ";
                $count_name = User::where('name','LIKE',"%$request->name%")->count();
                $count_name = round(($count_name*100)/$total_user);   
                $label .= ' / Name';
            }

            if (!empty($request->location)) {
                $condition .= " AND users.location LIKE '%$request->location%' ";
                $count_location = User::where('location','LIKE',"%$request->location%")->count();
                $count_location = round(($count_location*100)/$total_user);
                $label .= ' / Location';
            }
            if (!empty($request->networking_id)) {

                $condition .= " AND user_networkings.networking_id = $request->networking_id ";
                $count_networking = UserNetworking::where('networking_id',$request->networking_id)->count();
                $count_networking = round(($count_networking*100)/$total_user);
                 $label .= ' / Networking';
             }
            if (!empty($request->interest_id)) {
                $condition .= " AND user_interests.interest_id = $request->interest_id ";
                $count_interest = UserInterest::where('interest_id',$request->interest_id)->count();
                $count_interest = round(($count_interest*100)/$total_user);
                 $label .= ' / Interest';
            }
            if (!empty($request->favourite_id)) {
                $condition .= " AND user_favourites.favourite_id = $request->favourite_id  ";
                $count_favourite = UserFavourite::where('favourite_id',$request->favourite_id)->count();
                $count_favourite = round(($count_favourite*100)/$total_user);
                $label .= ' / Favourite Way To Meet';
            }
            if (!empty($request->gender)) {
                $condition .= " AND users.gender =  '$request->gender' ";
                $count_gender = User::where('gender',$request->gender)->count();
                $count_gender = round(($count_gender*100)/$total_user);
                $label .= ' / Gender';
            }
           
            if (!empty($request->industry_id)) {
                $condition .= " AND users.industry_id =  '$request->industry_id' ";
                $count_industry = User::where('industry_id',$request->industry_id)->count();
                $count_industry = round(($count_industry*100)/$total_user);
                $label .= ' / Industry';
            }
            
           if (!empty($request->degree)) {
           
                $condition .= " AND user_educations.degree =  '$request->degree' ";
                $count_degree = UserEducation::where('degree',$request->degree)->count();
                $count_degree = round(($count_degree*100)/$total_user);
                $label .= ' / Degree';
            } 
             
             $label= trim(trim($label),'/'); 
             //echo $label;die;

            $query = "SELECT $fields FROM users
            LEFT JOIN user_interests ON users.id = user_interests.user_id
            LEFT JOIN user_networkings ON users.id = user_networkings.user_id
            LEFT JOIN user_favourites ON users.id = user_favourites.user_id
            LEFT JOIN user_educations ON users.id = user_educations.user_id
            WHERE $condition GROUP BY user_id ";
            
            $users = DB::select($query); 
            //echo "<pre>"; print_r($users);die; 

            foreach ($users as $user) {
                $user->image      = $obj->is_file("profile_resize", $user->image);
                $user->created_on = date("dFY", strtotime($user->created_at));

            }
           
            $search_user = count($users);
            $total_user = round(($search_user*100)/$total_user);
            //echo $total_user;die;
            
            return view('admins.data_list', compact('users','interests','networkings','favourites','total_user','industries','degrees','count_gender','count_location','count_name','count_industry','count_interest','count_degree','count_favourite','count_networking','label'));
         } 
    }   



    public function data_list_notification(Request $request)
   
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $obj   = new CommonAdminController;
            $interests = Interest::orderBy('name','asc')->get();
            $networkings = Networking::orderBy('name','asc')->get();
            $favourites = Favourite::orderBy('name','asc')->get();
            $industries = Industry::orderBy('name','asc')->get();
            $degrees = Degree::orderBy('name','asc')->get();
            $fields       = " users.id AS user_id, users.name,users.created_at,users.phone,users.gender, users.location, users.designation, users.organisation, users.image ";
            $condition    = " users.is_admin = '0' ";
            $total_user = User::where('is_admin', '0')->orderBy('id', 'DESC')->count();
            $count_name = "";
            $count_location = "";
            $count_networking = "";
            $count_gender = "";
            $count_industry = "";
            $count_interest = "";
            $count_favourite = "";
            $count_gender = "";
            $count_degree = "";
            $label="";

            if (!empty($request->name)) {
                $condition .= " AND ( users.name LIKE '%$request->name%' OR users.designation LIKE '%$request->name%' OR users. organisation LIKE '%$request->name%' ) ";
                $count_name = User::where('name','LIKE',"%$request->name%")->count();
                $count_name = round(($count_name*100)/$total_user);   
                $label .= ' / Name';
            }

            if (!empty($request->location)) {
                $condition .= " AND users.location LIKE '%$request->location%' ";
                $count_location = User::where('location','LIKE',"%$request->location%")->count();
                $count_location = round(($count_location*100)/$total_user);
                $label .= ' / Location';
            }
            if (!empty($request->networking_id)) {

                $condition .= " AND user_networkings.networking_id = $request->networking_id ";
                $count_networking = UserNetworking::where('networking_id',$request->networking_id)->count();
                $count_networking = round(($count_networking*100)/$total_user);
                 $label .= ' / Networking';
             }
            if (!empty($request->interest_id)) {
                $condition .= " AND user_interests.interest_id = $request->interest_id ";
                $count_interest = UserInterest::where('interest_id',$request->interest_id)->count();
                $count_interest = round(($count_interest*100)/$total_user);
                 $label .= ' / Interest';
            }
            if (!empty($request->favourite_id)) {
                $condition .= " AND user_favourites.favourite_id = $request->favourite_id  ";
                $count_favourite = UserFavourite::where('favourite_id',$request->favourite_id)->count();
                $count_favourite = round(($count_favourite*100)/$total_user);
                $label .= ' / Favourite Way To Meet';
            }
            if (!empty($request->gender)) {
                $condition .= " AND users.gender =  '$request->gender' ";
                $count_gender = User::where('gender',$request->gender)->count();
                $count_gender = round(($count_gender*100)/$total_user);
                $label .= ' / Gender';
            }
           
            if (!empty($request->industry_id)) {
                $condition .= " AND users.industry_id =  '$request->industry_id' ";
                $count_industry = User::where('industry_id',$request->industry_id)->count();
                $count_industry = round(($count_industry*100)/$total_user);
                $label .= ' / Industry';
            }
            
           if (!empty($request->degree)) {
           
                $condition .= " AND user_educations.degree =  '$request->degree' ";
                $count_degree = UserEducation::where('degree',$request->degree)->count();
                $count_degree = round(($count_degree*100)/$total_user);
                $label .= ' / Degree';
            } 
             
             $label= trim(trim($label),'/'); 
             //echo $label;die;

            $query = "SELECT $fields FROM users
            LEFT JOIN user_interests ON users.id = user_interests.user_id
            LEFT JOIN user_networkings ON users.id = user_networkings.user_id
            LEFT JOIN user_favourites ON users.id = user_favourites.user_id
            LEFT JOIN user_educations ON users.id = user_educations.user_id
            WHERE $condition GROUP BY user_id ";
            
            $users = DB::select($query); 
            //echo "<pre>"; print_r($users);die; 

            foreach ($users as $user) {
                $user->image      = $obj->is_file("profile_resize", $user->image);
                $user->created_on = date("dFY", strtotime($user->created_at));

            }
           
            $search_user = count($users);
            $total_user = round(($search_user*100)/$total_user);
            //echo $total_user;die;
            
            return view('admins.data_list_notification', compact('users','interests','networkings','favourites','total_user','industries','degrees','count_gender','count_location','count_name','count_industry','count_interest','count_degree','count_favourite','count_networking','label'));
         } 
    }

    
    public function view_user(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $user_id = $request->id;
            $user_id = Crypt::decrypt($user_id);
            $user = User::where('id',$user_id)->first();
            if (!empty($user)) {
                $obj   = new CommonAdminController;
                $user->image      = $obj->is_file("profile", $user->image);
                $query = "SELECT networkings.name FROM user_networkings LEFT JOIN networkings ON user_networkings.networking_id=networkings.id WHERE user_networkings.user_id = $user_id";
                $networkings = DB::select($query);
                $user->networkings = $networkings;

                $query = "SELECT * FROM user_educations WHERE user_educations.user_id = $user_id";
                $educations = DB::select($query);
                $user->educations = $educations;

                $query = "SELECT * FROM user_organisations WHERE user_id = $user_id";
                $organisations = DB::select($query);
                $user->organisations = $organisations;

                if(!empty($user->industry_id)){
                 $query = "SELECT * FROM industries WHERE id = $user->industry_id";
                
                $industry = DB::select($query);
                $user->industry = $industry;
                }

                $query = "SELECT networkings.name FROM user_networkings LEFT JOIN networkings ON user_networkings.networking_id=networkings.id WHERE user_networkings.user_id = $user_id";
                $networkings = DB::select($query);
                $user->networkings = $networkings;

                $query = "SELECT favourites.name FROM user_favourites LEFT JOIN favourites ON user_favourites.favourite_id=favourites.id WHERE user_favourites.user_id = $user_id";
                $favourites = DB::select($query);
                $user->favourites = $favourites;

                $query = "SELECT interests.name FROM user_interests LEFT JOIN interests ON user_interests.interest_id=interests.id WHERE user_interests.user_id = $user_id";
                $interests = DB::select($query);
                $user->interests = $interests;

                return view('admins.view_user', compact('user'));
            }else{
                $request->session()->flash('alert-danger', 'No user found.');
                return redirect('/admin/view_user');
            }
        }
    }

    public function add_profile_add(Request $request)
    { 

        if(!empty($_POST['company']))
          {
            $company_arr = explode(',', $_POST['company']);
            $check_exits = Company::orderBy('name','asc')->pluck('name')->toArray();

           // $inserted_data = array_diff($check_exits,$company_arr);
            if(!empty($company_arr))
            {
            foreach ($company_arr as $name) {

                $company       = new Company;
                $company->name = ucwords($name);
               
               if(!in_array($company->name,$check_exits)){ 
                    $company->save();
               }
                    
                }
            } 

            echo "1";

          }


          if(!empty($_POST['designation']))
          {
            $designation_arr = explode(',', $_POST['designation']);
            $check_exits = Designation::orderBy('name','asc')->pluck('name')->toArray();

            if(!empty($designation_arr))
            {
            foreach ($designation_arr as $name) {

                $designation       = new Designation;
                $designation->name = ucwords($name);
               
               if(!in_array($designation->name,$check_exits)){ 
                    $designation->save();
               }
                    
                }
            } 

            echo "1";

          }





    }

    public function interest_list(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $obj       = new CommonAdminController;
            $interests = Interest::all();
            return view('admins.interest_list', compact('interests'));
        }
    }

    public function degree_list(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $obj       = new CommonAdminController;
            $degrees = Degree::all();
            return view('admins.degree_list', compact('degrees'));
        }
    }

    public function industry_list(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $obj       = new CommonAdminController;
            $interests = Industry::orderBy('id', "DESC")->get();
            return view('admins.industry_list', compact('interests'));
        }
    }


    public function company_list(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $obj       = new CommonAdminController;
            $company = Company::orderBy('id', "DESC")->get();
            $company_add = UserProfileAdd::where('type','company')->orderBy('name', "asc")->get();
            return view('admins.company_list', compact('company','company_add'));
        }
    }


    public function designation_list(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $obj       = new CommonAdminController;
            $designation = Designation::orderBy('id', "DESC")->get();
            $designation_add = UserProfileAdd::where('type','designation')->orderBy('name', "asc")->get();
            return view('admins.designation_list', compact('designation','designation_add'));
        }
    }

    public function advertisement_list(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $ads = Advertisement::all();
            return view('admins.advertisement_list', compact('ads'));
        }
    }

    public function message_template_list(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $messages = MessageTemplate::all();
            return view('admins.message_template_list', compact('messages'));
        }
    }

    public function request_template_list(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $messages = RequestTemplate::all();
            return view('admins.request_template_list', compact('messages'));
        }
    }

    public function user_report_list(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $obj   = new CommonAdminController;
            $reports = UserReport::all();
            foreach ($reports as $value) {
                $user = User::where('id',$value->user_id)->first();
                $value->name = $user->name;
                $value->phone = $user->phone;
                $value->image = $obj->is_file('profile_resize',$user->image);
            }
            return view('admins.user_report_list', compact('reports'));
        }
    }

    public function add_interest(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $rule = $this->validate($request, [
                'name' => 'required|max:190',
            ]);
            $interest       = new Interest;
            $interest->name = ucwords($request->name);
            $interest->save();
            return redirect('/admin/interest_list');
        }
    }

    public function add_degree(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $rule = $this->validate($request, [
                'name' => 'required|max:190',
            ]);
            $degrees       = new Degree;
            $degrees->name = ucwords($request->name);
            $degrees->save();
            return redirect('/admin/degree_list');
        }
    }

    public function add_advertisement(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $rule = $this->validate($request, [
                'title'       => 'required|max:190',
                'description' => 'required',
                'url'         => 'required|max:190',
                'image'       => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
            ]);
            $obj = new CommonAdminController;
            if (!empty($request->id)) {
                $ads = Advertisement::findOrFail($request->id);
            } else {
                $ads = new Advertisement;
                if (empty($request->image)) {
                    $request->session()->flash('alert-danger', 'The image is required.');
                    return redirect('/admin/advertisement_list');
                }
            }
            $ads->title       = ucwords($request->title);
            $ads->url       = $request->url;
            $ads->description = $request->description;
            if (!empty($request->image)) {
                $ads->image = $obj->upload_file($request, "", "image");
            }

            if ($ads->save()) {
                $request->session()->flash('alert-success', 'Save successfully.');
            } else {
                $request->session()->flash('alert-danger', 'Internal error.');
            }
            return redirect('/admin/advertisement_list');
        }
    }

    public function add_message_template(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $rule = $this->validate($request, [
                'message'       => 'required|max:230',
                
            ]);
            if (!empty($request->id)) {
                $template = MessageTemplate::findOrFail($request->id);
            } else {
                $template = new MessageTemplate;
            }
            $template->message       = $request->message;
            if ($template->save()) {
                $request->session()->flash('alert-success', 'Save successfully.');
            } else {
                $request->session()->flash('alert-danger', 'Internal error.');
            }
            return redirect('/admin/message_template_list');
        }
    }

    public function add_request_template(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $rule = $this->validate($request, [
                'message'       => 'required|max:230',
                
            ]);
            if (!empty($request->id)) {
                $template = RequestTemplate::findOrFail($request->id);
            } else {
                $template = new RequestTemplate;
            }
            $template->message       = $request->message;
            if ($template->save()) {
                $request->session()->flash('alert-success', 'Save successfully.');
            } else {
                $request->session()->flash('alert-danger', 'Internal error.');
            }
            
            return redirect('/admin/request_template_list');
        }
    }

    public function upload_interest(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {

            $csv_mimetypes = array(
                'text/csv',
                'application/csv',
            );

            if (in_array($_FILES['upload']['type'], $csv_mimetypes)) {
                $file   = $_FILES['upload']['tmp_name'];
                $handle = fopen($file, "r");
                while (($filesop = fgetcsv($handle, 1000, ",")) !== false) {
                    $data = !empty($filesop[0]) ? $filesop[0] : "";
                    if (!empty($data)) {
                        $data        = ucwords($data);
                        $getInterest = Interest::where('name', $data)->first();
                        if (empty($getInterest)) {
                            $interest       = new Interest;
                            $interest->name = $data;
                            $interest->save();
                        }
                    }
                }
                $request->session()->flash('alert-success', 'Uploaded successfully.');
                return redirect('/admin/interest_list');
            } else {
                $request->session()->flash('alert-danger', 'The file must be a type of: csv.');
                return redirect('/admin/interest_list');
            }

        }
    }

     public function upload_degree(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {

            $csv_mimetypes = array(
                'text/csv',
                'application/csv',
               /* 'text/comma-separated-values',
                'application/excel',
                'application/vnd.ms-excel',
                'application/vnd.msexcel',
                'application/octet-stream',*/
            );

            if (in_array($_FILES['upload']['type'], $csv_mimetypes)) {
                $file   = $_FILES['upload']['tmp_name'];
                $handle = fopen($file, "r");
                while (($filesop = fgetcsv($handle, 1000, ",")) !== false) {
                    $data = !empty($filesop[0]) ? $filesop[0] : "";
                    if (!empty($data)) {
                        $data        = ucwords($data);
                        $getDegree = Degree::where('name', $data)->first();
                        if (empty($getDegree)) {
                            $degree       = new Degree;
                            $degree->name = $data;
                            $degree->save();
                        }
                    }
                }
                $request->session()->flash('alert-success', 'Uploaded successfully.');
                return redirect('/admin/degree_list');
            } else {
                $request->session()->flash('alert-danger', 'The file must be a type of: csv.');
                return redirect('/admin/degree_list');
            }

        }
    }


public function upload_company(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {

            $csv_mimetypes = array(
                'text/csv',
                'application/csv',
            );

            if (in_array($_FILES['upload']['type'], $csv_mimetypes)) {
                $file   = $_FILES['upload']['tmp_name'];
                $handle = fopen($file, "r");
                while (($filesop = fgetcsv($handle, 1000, ",")) !== false) {
                    $data = !empty($filesop[0]) ? $filesop[0] : "";
                    if (!empty($data)) {
                        $data        = ucwords($data);
                        $getCompany = Company::where('name', $data)->first();
                        if (empty($getCompany)) {
                            $company       = new Company;
                            $company->name = $data;
                            $company->save();
                        }
                    }
                }
                $request->session()->flash('alert-success', 'Uploaded successfully.');
                return redirect('/admin/company_list');
            } else {
                $request->session()->flash('alert-danger', 'The file must be a type of: csv.');
                return redirect('/admin/company_list');
            }

        }
    }


public function upload_designation(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {

            $csv_mimetypes = array(
                'text/csv',
                'application/csv',
            );

            if (in_array($_FILES['upload']['type'], $csv_mimetypes)) {
                $file   = $_FILES['upload']['tmp_name'];
                $handle = fopen($file, "r");
                while (($filesop = fgetcsv($handle, 1000, ",")) !== false) {
                    $data = !empty($filesop[0]) ? $filesop[0] : "";
                    if (!empty($data)) {
                        $data        = ucwords($data);
                        $getDesignation = Designation::where('name', $data)->first();
                        if (empty($getDesignation)) {
                            $designation       = new Company;
                            $designation->name = $data;
                            $designation->save();
                        }
                    }
                }
                $request->session()->flash('alert-success', 'Uploaded successfully.');
                return redirect('/admin/designation_list');
            } else {
                $request->session()->flash('alert-danger', 'The file must be a type of: csv.');
                return redirect('/admin/designation_list');
            }

        }
    }



    public function upload_industry(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {

            $csv_mimetypes = array(
                'text/csv',
                'application/csv',
            );

            if (in_array($_FILES['upload']['type'], $csv_mimetypes)) {
                $file   = $_FILES['upload']['tmp_name'];
                $handle = fopen($file, "r");
                while (($filesop = fgetcsv($handle, 1000, ",")) !== false) {
                    $data = !empty($filesop[0]) ? $filesop[0] : "";
                    if (!empty($data)) {
                        $data        = ucwords($data);
                        $getIndustry = Industry::where('name', $data)->first();
                        if (empty($getIndustry)) {
                            $industry       = new Industry;
                            $industry->name = $data;
                            $industry->save();
                        }
                    }
                }
                $request->session()->flash('alert-success', 'Uploaded successfully.');
                return redirect('/admin/industry_list');
            } else {
                $request->session()->flash('alert-danger', 'The file must be a type of: csv.');
                return redirect('/admin/industry_list');
            }

        }
    }

    public function add_industry(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $rule = $this->validate($request, [
                'name' => 'required|max:190',
            ]);
            $obj            = new CommonAdminController;
            $interest       = new Industry;
            $interest->name = $request->name;
            $interest->save();
            return redirect('/admin/industry_list');
        }
    }


     public function add_company(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $rule = $this->validate($request, [
                'name' => 'required|max:190',
            ]);
            $obj            = new CommonAdminController;
            $company       = new Company;
            $company->name = $request->name;
            $company->save();
            return redirect('/admin/company_list');
        }
    }



    public function add_designation(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $rule = $this->validate($request, [
                'name' => 'required|max:190',
            ]);
            $obj            = new CommonAdminController;
            $designation       = new Designation;
            $designation->name = $request->name;
            $designation->save();
            return redirect('/admin/designation_list');
        }
    }


    public function profile(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $admin = User::where("is_admin", "1")->first();
            if ($request->isMethod('post')) {
                $rule = $this->validate($request, [
                    'email'    => 'required|email|max:190',
                    'password' => 'required|max:190',
                    'name'     => 'required|max:190',
                    'image'    => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
                ]);
                $obj = new CommonAdminController;
                if (!empty($request->file('image'))) {
                    if (file_exists(public_path('profile' . '/' . $admin->image))) {
                        @unlink(public_path('profile' . '/' . $admin->image));
                    }
                    if (file_exists(public_path('profile_resize' . '/' . $admin->image))) {
                        @unlink(public_path('profile_resize' . '/' . $admin->image));
                    }
                    $image = $obj->upload_image($request);
                } else {
                    $image = $admin->image;
                }

                $update = array(
                    'email'             => $request->email,
                    'password'          => bcrypt($request->password),
                    'original_password' => $request->password,
                    'name'              => $request->name,
                    'image'             => $image,
                );
                if (User::where('id', $admin->id)->update($update)) {
                    $request->session()->flash('alert-success', 'Updated successfullys.');
                    return redirect('/admin/profile');
                } else {
                    $request->session()->flash('alert-danger', 'Internal error .');
                }
            } else {
                return view('admins.profile', compact('admin'));
            }
        }
    }

    public function forgot_password(Request $request)
    {
        try {
            if (Auth::user()) {
                return redirect()->intended(route('admins.user_list'));
            } elseif ($request->isMethod('post')) {
                $this->validate($request, [
                    'email' => 'required|email',
                ]);
                $create = User::where('is_admin', '1')->where('email', $request->email)->first();
                if (!empty($create)) {
                    $password                  = mt_rand(10000, 99999);
                    $create->password          = Hash::make($password);
                    $create->original_password = $password;
                    if ($create->save()) {
                        $email   = $create->email;
                        $subject = "New Password";
                        Mail::send('emails.forgot_password', ['newPassword' => $password, 'email' => $create->email], function ($message) use ($email, $subject) {
                            $message->to($email)->subject($subject);
                        });
                        $request->session()->flash('alert-success', 'Password send to your mail');
                        return redirect('/admin/login');
                    } else {
                        $request->session()->flash('alert-success', 'Internal Server Error');
                    }
                } else {
                    $request->session()->flash('alert-danger', 'Not a valid user.');
                }
                return Redirect::back();
            } else {
                return view('admins.forgot_password');
            }

        } catch (Exception $e) {

        }
    }

    public function common_delete(Request $request)
    {
        try {
            if (DB::table($request->table)->where('id', $request->id)->delete()) {
                echo true;
            } else {
                echo false;
            }
            die;
        } catch (\Exception $e) {
            print_r($e);die; 
        }
    }

    public function add_ads_count(Request $request){
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $rule = $this->validate($request, [
                'count' => 'required|max:190',
            ]);
            $create       = AdvertisementCount::find(1);
            $create->count = $request->count;
            $create->save();
            return redirect('/admin/advertisement_list');
        }
    }

    public function add_radius(Request $request){
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $rule = $this->validate($request, [
                'count' => 'required|max:190',
            ]);
            $create       = LocationRadius::find(1);
            $create->radius = $request->count;
            $create->save();
            return redirect('/admin/user_list');
        }
    }
 
    public function add_count(Request $request){
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $rule = $this->validate($request, [
                'c_count' => 'required|max:190',
            ]);
            $create       = Card::find(1);
            $create->count = $request->c_count;
            $create->save();
            return redirect('/admin/user_list');
        }
    }


    
    public function common_get(Request $request)
    {
        try {
            $data = DB::table($request->table)->where('id', $request->id)->first();
            if (!empty($data)) {

                if ($request->table == "advertisements") {
                    $img         = $data->image;
                    $data->image = url("public/ads/$img");
                }
                $data = json_encode($data);
                print_r($data);die;
            } else {
                echo false;
            }
            die;
        } catch (\Exception $e) {
            print_r($e);die;
        }
    }

    public function card_get(Request $request)
    {
        try {
            $data = DB::table($request->table)->where('id', $request->id)->first(); 
            if (!empty($data)) {

                if ($request->table == "cards") {
                    //$img         = $data->image;
                    //$data->image = url("public/ads/$img");
                }
                $data = json_encode($data);
                print_r($data);die;
            } else {
                echo false;
            }
            die;
        } catch (\Exception $e) {
            print_r($e);die;
        }
    }
}
