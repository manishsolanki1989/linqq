<?php

namespace App\Http\Controllers\API;
use App\Advertisement;
use App\AdvertisementCount;
use App\Degree;
use App\Http\Controllers\Controller;
use App\Industry;
use App\Interest;
use App\Company;
use App\Designation;
use App\PreviousEducation;
use App\Message;
use App\MessageTemplate;
use App\Networking;
use App\Feedback;
use App\OauthAccessToken;
use App\RequestTemplate;
use App\User;
use App\UserEducation;
use App\UserFavourite;
use App\UserInterest;
use App\UserMessageTemplate;
use App\UserNetworking;
use App\UserNotification;
use App\UserOrganisation;
use App\UserReport; 
use App\UserRequest;
use App\LocationRadius;
use App\IgnoreUser;
use App\UserProfileAdd;
use App\Invite;
use App\Card;  
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Twilio\Rest\Client;
use Validator;
use Mail;  
use SendGrid;



class UserController extends Controller
{ 

   public function __construct(CommonController $obj)
   
   {   
    $this->obj = new CommonController;
  
   } 



   function geocode_by_address()
   {

     $condition    = " location != ' ' AND (lat = '0.00' OR lat = '')";
    $query = "SELECT * FROM users WHERE $condition ";
    $users = DB::select($query);
    //echo $response3 = json_encode($users);

    if(!empty($users)){
        foreach ($users as $value) {
        $address = $value->location;
        $formattedAddr = str_replace(' ','+',$address);
        //Send request and receive json data by address
        $region = "INDIA";


        $geocodeFromAddr = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$formattedAddr&key=AIzaSyBeFlXb12q35S8SrqFmlx_gDKPmXhgmKsk&region=$region");

       // $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false&key=AIzaSyBQzgxWDDe2u_UEk8VKbC5Bg6CUfrVTIUY');
        $output = json_decode($geocodeFromAddr);
       
         if(!empty($output)){

             //Get latitude and longitute from json data
        $data['latitude']  = $output->results[0]->geometry->location->lat; 
        $data['longitude'] = $output->results[0]->geometry->location->lng;
        //Return latitude and longitude of the given address


        $user_update_lat_long = User::where('id', $value->id)->update(array("lat"=>$data['latitude'],"log"=>$data['longitude']));
           // return $data;
        }else{
            return false;
        }




        }
   

    }

   }

  
  public function about()
  {
    return view('service.about');
  }

 public function push_notification_instance_creation_without_argument_set_gcm_as_service()
    {
        $push = new PushNotification();

        $this->assertInstanceOf('Edujugon\PushNotification\Gcm', $push->service);
    }


public function testPush(Request $request)
{

$push = new PushNotification('fcm');
$push->setMessage(['message'=>'Hello World'])
                ->setApiKey('AIzaSyBQzgxWDDe2u_UEk8VKbC5Bg6CUfrVTIUY')
                ->setDevicesToken(['eT09enlZedo:APA91bE5UCfw6gY82E6bhhlx9Gp5z8srae2JXk55xUNencbSiGwaEM2E_JOqQsuu1zbljsGZepU3p1Mf6vcZB69dv9fvPqZylGqJOUK89UX3TvDFSqZo8aDolZ6wzzctmjLb4wZu3sGb'])
                ->setConfig(['dry_run' => false]);

        $push = $push->send();
        //print_r($push->getFeedback());

       //  $this->assertInstanceOf('stdClass', $push->getFeedback());

}





  public function login(Request $request)
   { 
        try {
            
            $rules = array(
                'phone'       => 'required|numeric',
                'password'    => 'required|max:190',
                'device_id'   => 'required|max:190',
                'device_type' => 'required|max:190',
                'lat'         =>  '',
                'log'         =>  '',
            );
            
           if($this->obj->validate_request($request, $rules)) {
               
               $get = User::where('phone',request('phone'))->first();
              
              if(!empty($get)) {


               if(Auth::attempt(['phone' => request('phone'), 'password' => request('password')])) {
                    
                    $oldtoken = User::where('device_id',$request->device_id)->first(); 
                    if(!empty($oldtoken)){
                         User::where('id', $oldtoken->id)->update(['device_id' =>'']);
                    } 
                    //User::where('id', $user_id)->update(['device_id' =>'','device_type'=>'']);
                    
                    $auth = Auth::user();
                    $user = User::where('id', $auth->id)
                        ->update(array('device_type' => $request->device_type, 'device_id' => $request->device_id));
                   
                   $oauth = OauthAccessToken::where('user_id', $auth->id);
                   
                    if(!empty($oauth)) {
                        
                         $oauth->delete();
                     }   
                  
                    $user             = User::find($auth->id);
                    $user->user_id    = (string) $user->id;
                    $user->card_count = $this->ads_count();
                    $user->total_user = $this->total_user($user->user_id);
                    $user->token      = 'Bearer ' . $user->createToken(request('phone'))->accessToken;
                    $user->image      = $this->obj->is_file("profile", $user->image);
                    $user->lat        = !empty($request->lat)?$request->lat:"01.00"; 
                    $user->log        = !empty($request->log)?$request->log:"02.00";
                    $ccount           =  Message::where('receiver_id',$user->user_id)->where('status','Unread')->count(); 
                    $user->total_chatcount = $ccount;

                    //manish code
                    $check_user_interest = UserInterest::where('user_id',$user->id)->first();
                    $user->headline_status = !empty($user->headline)?"yes":"no";
                    $user->interest_status = (count($check_user_interest)>0)?"yes":"no";
                    $user->additional_info_status = !empty($user->designation)?"yes":"no";
                    //manish code

                    
                     if(!empty($request->lat)){
                    User::where('id', $user->id)->update(['lat' =>$request->lat,'log'=>$request->log]);
                         }
                    $this->obj->set_data("200", "Login successfully", $request->path(), "response", $user);
             
                } else {
               
                    $this->obj->error_message('401', "Invalid Mobile Number or Password!", $request->path());
        
                } 
            } else{

                $this->obj->error_message('401', "User does not exist.", $request->path());

            }
        }

        
        } catch (Exception $e) {

            $this->obj->error_message('500', "Internal error.", $request->path());
        
         } 
   } 
   
 

  public function total_user($userId = null)
    { 
        if (!empty($userId)) {  
             
                $getUser = User::where('id',$userId)->first();
                $location = $getUser->location;
                //AND location LIKE '%$location%'
                $condition    = " is_admin != '1' AND id != $userId AND location LIKE '%$location%' ";
                $user_request = UserRequest::where('sender_id', $userId)->pluck('receiver_id');
                $remove_user = "";
                if (count($user_request)) {
                    $remove_id = implode(",", $user_request->toArray());
                    $remove_user .= $remove_id;
                }
                $user_reject = UserRequest::where('receiver_id', $userId)->where('reject_status','1')->pluck('receiver_id');
                if (count($user_reject)) {
                    $remove_id = implode(",", $user_reject->toArray());
                    $remove_user .= $remove_id;
                }
                if (!empty($remove_user)) {
                    $condition .= " AND users.id NOT IN ($remove_user)";
                }
                $query = "SELECT * FROM users WHERE $condition ";
                $users = DB::select($query);
                
            return (string)count($users);
        }
    }  
 

    public function total_userr($userId = null,$lat=null,$long=null)
    {  
        if (!empty($userId)) { 
             
                $getUser = User::where('id',$userId)->first();
                $location = $getUser->location;
                //AND location LIKE '%$location%'
                $condition    = " is_admin != '1' AND id != $userId AND location LIKE '%$location%' ";
                $user_request = UserRequest::where('sender_id', $userId)->pluck('receiver_id');
                $remove_user = "";
                if (count($user_request)) {
                    $remove_id = implode(",", $user_request->toArray());
                    $remove_user .= $remove_id;
                }
                $user_reject = UserRequest::where('receiver_id', $userId)->where('reject_status','1')->pluck('receiver_id');
                if (count($user_reject)) {
                    $remove_id = implode(",", $user_reject->toArray());
                    $remove_user .= $remove_id;
                }
                if (!empty($remove_user)) {
                    $condition .= " AND users.id NOT IN ($remove_user)";
                }
                $query = "SELECT * FROM users WHERE $condition ";
                $users = DB::select($query);
                
            return (string)count($users);
        }
    }  
    
    public function ads_count()
    {
        $cdd          = Card::where('id',1)->first();    
        $ads_count = AdvertisementCount::select('count')->first();
       // echo $ads_count->count;die;
        if($ads_count->count>0)
        {
            $count = $ads_count->count;
        }
        else
        {
            $count =   ($cdd->count!=0)?$cdd->count:'20';
        }
        return $count;

        //  $count     = isset($ads_count->count>0)?$ads_count->count:($cdd->count!=0)?$cdd->count:'20';
        //echo $count;die;

    }

    public function card_count(Request $request)
    {
        $user_id   = Auth::user()->id;
        $ads_count = AdvertisementCount::select('count')->first();
        $this->obj->set_data("200", "Phone number is in record.", $request->path(), 'request', $ads_count);

    }

    public function unique_user(Request $request)
    { 
        try {
            $rules = array(
                'phone' => 'required|numeric|unique:users',
            );

            if ($this->obj->validate_request($request, $rules)) {
                $this->obj->set_data("200", "No record.", $request->path());
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function unique_user_otp(Request $request)
    {
        try {
            $rules = array(
                'phone' => 'required|numeric',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user = User::where('phone', $request->phone)->first();
                if (!empty($user)) {
                    $this->obj->set_data("200", "Phone number is in record.", $request->path());
                } else {
                    $this->obj->error_message('404', "No record.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function signup(Request $request)
    {
        try { 

             $rules = array(
                'name'         => 'required|max:190',
                'phone'        => 'required|numeric|unique:users',
                //'gender'       => 'required|max:190',
                'designation'  => '',
                'organisation' => '',
                'password'     => 'required|max:190',
                'device_id'    => 'required|max:190',
                'device_type'  => 'required|max:190',
                'lat'         =>  '',
                'log'         =>  '',
                'image'        => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
            );
 
           if($this->obj->validate_request($request, $rules)) {
                $createUser                    = new User;
                $createUser->original_password = $request->password;
                $createUser->phone             = $request->phone;
                $createUser->designation       = !empty($request->designation)?$request->designation:"";
                $createUser->organisation      = !empty($request->organisation)?$request->organisation:"";
                $createUser->device_id         = $request->device_id;
                $createUser->device_type       = $request->device_type;
                $createUser->password          = bcrypt($request->password);
                $createUser->name              = $request->name;
                $createUser->lat        = !empty($request->lat)?$request->lat:"0.00";
                $createUser->log        = !empty($request->log)?$request->log:"0.00";
                if (!empty($request->location)) {
                    $createUser->location = $request->location;
                }
                if (!empty($request->image)) {
                    $createUser->image = $this->obj->upload_image($request, 'image');
                }
              //  print_r($createUser);die;

                if(!empty($request->organisation)){
                $check_exits_company = Company::where('name',$request->organisation)->first();
                	if(empty($check_exits_company))
                	{
                		$designation1       = new UserProfileAdd;
              			$designation1->name = ucwords($request->organisation);
              			$designation1->type = "company";
	               		$designation1->save();
                	}
                }


                	if(!empty($request->designation)){
                	$check_exits_designation = Designation::where('name',$request->designation)->first();
                	if(empty($check_exits_designation))
                	{
                		$designation2       = new UserProfileAdd;
              			$designation2->name = ucwords($request->designation);
              			$designation2->type = "designation";
	               		$designation2->save();
                	}
                }


                if ($createUser->save()) {
                    $createUser->user_id    = (string) $createUser->id;
                    $createUser->token      = 'Bearer ' . $createUser->createToken('signup')->accessToken;
                    $createUser->image      = $this->obj->is_file("profile", $createUser->image);
                    $createUser->card_count = $this->ads_count();
                    $createUser->total_user = $this->total_user($createUser->id);
                    $this->obj->set_data("200", "Signup successfully", $request->path(), "response", $createUser);
                } else {
                    $this->obj->error_message('500', "Internal error.", $request->path());
                }
            }
        } catch (Exception $e) {
        
            $this->obj->error_message('500', "Internal error.", $request->path());
        
        }
   }

    public function linkdin_signup(Request $request)
    {
        try { 
         
         $rules = array(
                'name'                  => 'required|max:190',
                'linkedin_id'           => '',
                'facebook_id'           => '',
                'gmail_id'              => '',
                'signup_type'           => 'required|max:190',
                'device_id'             => 'required|max:190',
                'device_type'           => 'required|max:190',
                'designation'           => '',
                'organisation'          => '',
                'previous_designation'  => '',
                'previous_organisation' => '',
                'headline'              => 'max:300',
                'location'              => 'max:190',
                'image'                 => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
                'lat'                   => '',
                'log'                   => '',
            );

            if ($this->obj->validate_request($request, $rules)) {
                $flag       = 0;

                if($request->signup_type=="linkedin"){
                $createUser = User::where('linkedin_id', $request->linkedin_id)->first();
                 $createUser2 = User::where('linkedin_id', $request->linkedin_id)->first();
                }
                elseif($request->signup_type=="facebook")
                {
                    $createUser = User::where('facebook_id', $request->facebook_id)->first();
                    $createUser2 = User::where('facebook_id', $request->facebook_id)->first();
                }
                else{
                     $createUser = User::where('gmail_id', $request->gmail_id)->first();
                     $createUser2 = User::where('gmail_id', $request->gmail_id)->first();
                }
              
                if(empty($createUser)) {

                
                    $flag       = 1;
                    $createUser = new User;
               
                if (!empty($request->designation)) {
                    $createUser->designation = $request->designation;
                }
                if (!empty($request->organisation)) {
                    $createUser->organisation = $request->organisation;
                }
                if (!empty($request->headline)) {
                    $createUser->headline = $request->headline;
                }
                
                if (!empty($request->location)) {
                    $createUser->location = $request->location;
                }
                
                if (!empty($request->email)) { 
                    $createUser->email = $request->email; 
                }
 

                $createUser->device_id   = $request->device_id;
                $createUser->device_type = $request->device_type;
                $createUser->linkedin_id = !empty($request->linkedin_id)?$request->linkedin_id:"";
                $createUser->facebook_id = !empty($request->facebook_id)?$request->facebook_id:"";
                $createUser->gmail_id    = !empty($request->gmail_id)?$request->gmail_id:"";
                $createUser->name        = $request->name;
                $createUser->lat        = !empty($request->lat)?$request->lat:"0.00";
                $createUser->log        = !empty($request->log)?$request->log:"0.00";


                 


                if (!empty($request->image)) {
                
                    $createUser->image = $this->obj->upload_image($request, 'image');
                } 

                if (!empty($request->industry)) {
                    $industry = Industry::where('name', ucwords($request->industry))->first();
                    if (empty($industry)) {
                        $industry       = new Industry;
                        $industry->name = ucwords($request->industry);
                        $industry->save();
                    }
                    $createUser->industry_id = $industry->id;
                 }  

                $nwuser=1; 


             

          } else {  
               

             
                $createUser->email = $request->email; 
                $createUser->device_id   = $request->device_id;
                $createUser->device_type = $request->device_type;
                $createUser->linkedin_id = !empty($request->linkedin_id)?$request->linkedin_id:"";
                $createUser->facebook_id = !empty($request->facebook_id)?$request->facebook_id:"";
                $createUser->gmail_id    = !empty($request->gmail_id)?$request->gmail_id:"";
                //$createUser->name        = $request->name;
                $createUser->lat        = !empty($request->lat)?$request->lat:"0.00";
                $createUser->log        = !empty($request->log)?$request->log:"0.00";

                //  if (!empty($request->location)) {
                //     $createUser->location = $request->location;
                // }





                 $oauth = OauthAccessToken::where('user_id',$createUser->id);
                    if (!empty($oauth)) {
                        
                          $oauth->delete();
                       } 
                       $nwuser = 0;
        }



               if ($createUser->save()) {   
 
                 $createUser->first_user = "no";
                 
                    if ($flag == 1) {
                        $createUser->first_user = "yes";
                        if (!empty($request->previous_designation) && !empty($request->previous_organisation)) {
                            $org          = new UserOrganisation;
                            $org->title   = $request->previous_designation;
                            $org->name    = $request->previous_organisation;
                            $org->user_id = $createUser->id;
                            $org->save();
                        }
                   } 
                     
                    $createUser->user_id            = (string) $createUser->id;
                    $createUser->token              = 'Bearer ' . $createUser->createToken('signup')->accessToken;
                    $createUser->image              = $this->obj->is_file("profile", $createUser->image);
                    $createUser->organisation_name  = !empty($request->organisation) ? $request->organisation : "";
                    $createUser->organisation_title = !empty($request->designation) ? $request->designation : "";
                    $createUser->card_count         = $this->ads_count();
                    $createUser->total_user         = $this->total_user($createUser->id);
                    $ccount           =  Message::where('receiver_id',$createUser->id)->where('status','Unread')->count();
                    $createUser->total_chatcount = $ccount;   


                    

                   
                  if($nwuser == 1) {



                    $uid = $createUser->id;
                    $user = User::where('id',$uid)->first(); 

         //if(!empty($user->email) && ($user->notification == "ON")) { 
$mssg= '
<!DOCTYPE html>    
<html>
   <head>
      <title>Push Email</title>
      <link rel="shortcut icon" href="favicon.ico">
      <style type="text/css">
         table[name="blk_permission"], table[name="blk_footer"] {display:none;} 
      </style>
      
      <style type="text/css">   
          @import url("https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900")
          body{font-family: "Lato", sans-serif !important;}
         /*** BMEMBF Start ***/    
         [name=bmeMainBody]{/* min-height:1000px; */}    
         @media only screen and (max-width: 480px){table.blk, table.tblText, .bmeHolder, .bmeHolder1, table.bmeMainColumn{width:100% !important;} }        
         @media only screen and (max-width: 480px){.bmeImageCard table.bmeCaptionTable td.tblCell{padding:0px 20px 20px 20px !important;} }        
         @media only screen and (max-width: 480px){.bmeImageCard table.bmeCaptionTable.bmeCaptionTableMobileTop td.tblCell{padding:20px 20px 0 20px !important;} }        
         @media only screen and (max-width: 480px){table.bmeCaptionTable td.tblCell{padding:10px !important;} }        
         @media only screen and (max-width: 480px){table.tblGtr{ padding-bottom:20px !important;} }        
         @media only screen and (max-width: 480px){td.blk_container, .blk_parent, .bmeLeftColumn, .bmeRightColumn, .bmeColumn1, .bmeColumn2, .bmeColumn3, .bmeBody{display:table !important;max-width:600px !important;width:100% !important;} }        
         @media only screen and (max-width: 480px){table.container-table, .bmeheadertext, .container-table { width: 95% !important; } }        
         @media only screen and (max-width: 480px){.mobile-footer, .mobile-footer a{ font-size: 13px !important; line-height: 18px !important; } .mobile-footer{ text-align: center !important; } table.share-tbl { padding-bottom: 15px; width: 100% !important; } table.share-tbl td { display: block !important; text-align: center !important; width: 100% !important; } }        
         @media only screen and (max-width: 480px){td.bmeShareTD, td.bmeSocialTD{width: 100% !important; } }        
         @media only screen and (max-width: 480px){td.tdBoxedTextBorder{width: auto !important;}}    
         @media only screen and (max-width: 480px){table.blk, table[name=tblText], .bmeHolder, .bmeHolder1, table[name=bmeMainColumn]{width:100% !important;} }    
         @media only screen and (max-width: 480px){.bmeImageCard table.bmeCaptionTable td[name=tblCell]{padding:0px 20px 20px 20px !important;} }    
         @media only screen and (max-width: 480px){.bmeImageCard table.bmeCaptionTable.bmeCaptionTableMobileTop td[name=tblCell]{padding:20px 20px 0 20px !important;} }    
         @media only screen and (max-width: 480px){table.bmeCaptionTable td[name=tblCell]{padding:10px !important;} }    
         @media only screen and (max-width: 480px){table[name=tblGtr]{ padding-bottom:20px !important;} }    
         @media only screen and (max-width: 480px){td.blk_container, .blk_parent, [name=bmeLeftColumn], [name=bmeRightColumn], [name=bmeColumn1], [name=bmeColumn2], [name=bmeColumn3], [name=bmeBody]{display:table !important;max-width:600px !important;width:100% !important;} }    
         @media only screen and (max-width: 480px){table[class=container-table], .bmeheadertext, .container-table { width: 95% !important; } }    
         @media only screen and (max-width: 480px){.mobile-footer, .mobile-footer a{ font-size: 13px !important; line-height: 18px !important; } .mobile-footer{ text-align: center !important; } table[class="share-tbl"] { padding-bottom: 15px; width: 100% !important; } table[class="share-tbl"] td { display: block !important; text-align: center !important; width: 100% !important; } }    
         @media only screen and (max-width: 480px){td[name=bmeShareTD], td[name=bmeSocialTD]{width: 100% !important; } }    
         @media only screen and (max-width: 480px){td[name=tdBoxedTextBorder]{width: auto !important;}}    
         @media only screen and (max-width: 480px){.bmeImageCard table.bmeImageTable{height: auto !important; width:100% !important; padding:20px !important;clear:both; float:left !important; border-collapse: separate;} }    
         @media only screen and (max-width: 480px){.bmeMblInline table.bmeImageTable{height: auto !important; width:100% !important; padding:10px !important;clear:both;} }    
         @media only screen and (max-width: 480px){.bmeMblInline table.bmeCaptionTable{width:100% !important; clear:both;} }    
         @media only screen and (max-width: 480px){table.bmeImageTable{height: auto !important; width:100% !important; padding:10px !important;clear:both; } }    
         @media only screen and (max-width: 480px){table.bmeCaptionTable{width:100% !important;  clear:both;} }    
         @media only screen and (max-width: 480px){table.bmeImageContainer{width:100% !important; clear:both; float:left !important;} }    
         @media only screen and (max-width: 480px){table.bmeImageTable td{padding:0px !important; height: auto; } }    
         @media only screen and (max-width: 480px){td.bmeImageContainerRow{padding:0px !important;}}    
         @media only screen and (max-width: 480px){img.mobile-img-large{width:100% !important; height:auto !important;} }    
         @media only screen and (max-width: 480px){img.bmeRSSImage{max-width:320px; height:auto !important;}}    
         @media only screen and (min-width: 640px){img.bmeRSSImage{max-width:600px !important; height:auto !important;} }    
         @media only screen and (max-width: 480px){.trMargin img{height:10px;} }    
         @media only screen and (max-width: 480px){div.bmefooter, div.bmeheader{ display:block !important;} }    
         @media only screen and (max-width: 480px){.tdPart{ width:100% !important; clear:both; float:left !important; } }    
         @media only screen and (max-width: 480px){table.blk_parent1, table.tblPart {width: 100% !important; } }    
         @media only screen and (max-width: 480px){.tblLine{min-width: 100% !important;}}     
         @media only screen and (max-width: 480px){.bmeMblCenter img { margin: 0 auto; } }       
         @media only screen and (max-width: 480px){.bmeMblCenter, .bmeMblCenter div, .bmeMblCenter span  { text-align: center !important; text-align: -webkit-center !important; } }    
         @media only screen and (max-width: 480px){.bmeNoBr br, .bmeImageGutterRow, .bmeMblStackCenter .bmeShareItem .tdMblHide { display: none !important; } }    
         @media only screen and (max-width: 480px){.bmeMblInline table.bmeImageTable, .bmeMblInline table.bmeCaptionTable, td.bmeMblInline { clear: none !important; width:50% !important; } }    
         @media only screen and (max-width: 480px){.bmeMblInlineHide, .bmeShareItem .trMargin { display: none !important; } }    
         @media only screen and (max-width: 480px){.bmeMblInline table.bmeImageTable img, .bmeMblShareCenter.tblContainer.mblSocialContain, .bmeMblFollowCenter.tblContainer.mblSocialContain{width: 100% !important; } }    
         @media only screen and (max-width: 480px){.bmeMblStack> .bmeShareItem{width: 100% !important; clear: both !important;} }    
         @media only screen and (max-width: 480px){.bmeShareItem{padding-top: 10px !important;} }    
         @media only screen and (max-width: 480px){.tdPart.bmeMblStackCenter, .bmeMblStackCenter .bmeFollowItemIcon {padding:0px !important; text-align: center !important;} }    
         @media only screen and (max-width: 480px){.bmeMblStackCenter> .bmeShareItem{width: 100% !important;} }    
         @media only screen and (max-width: 480px){ td.bmeMblCenter {border: 0 none transparent !important;}}    
         @media only screen and (max-width: 480px){.bmeLinkTable.tdPart td{padding-left:0px !important; padding-right:0px !important; border:0px none transparent !important;padding-bottom:15px !important;height: auto !important;}}    
         @media only screen and (max-width: 480px){.tdMblHide{width:10px !important;} }    
         @media only screen and (max-width: 480px){.bmeShareItemBtn{display:table !important;}}    
         @media only screen and (max-width: 480px){.bmeMblStack td {text-align: left !important;}}    
         @media only screen and (max-width: 480px){.bmeMblStack .bmeFollowItem{clear:both !important; padding-top: 10px !important;}}    
         @media only screen and (max-width: 480px){.bmeMblStackCenter .bmeFollowItemText{padding-left: 5px !important;}}    
         @media only screen and (max-width: 480px){.bmeMblStackCenter .bmeFollowItem{clear:both !important;align-self:center; float:none !important; padding-top:10px;margin: 0 auto;}}    
         @media only screen and (max-width: 480px){    
         .tdPart> table{width:100% !important;}    
         }    
         @media only screen and (max-width: 480px){.tdPart>table.bmeLinkContainer{ width:auto !important; }}    
         @media only screen and (max-width: 480px){.tdPart.mblStackCenter>table.bmeLinkContainer{ width:100% !important;}}     
         .blk_parent:first-child, .blk_parent{float:left;}    
         .blk_parent:last-child{float:right;}    
         /*** BMEMBF END ***/    
         table[name="bmeMainBody"], body {background-color:#eaeaea;}    
         td[name="bmePreHeader"] {background-color:#00ae54;}    
         td[name="bmeHeader"] {background:#ffffff;background-color:#00ae54;}    
         td[name="bmeBody"], table[name="bmeBody"] {background-color:#ffffff;}    
         td[name="bmePreFooter"] {background-color:#ffffff;}    
         td[name="bmeFooter"] {background-color:#ffffff;}    
         td[name="tblCell"], .blk {font-family:initial;font-weight:normal;font-size:initial;}    
         table[name="blk_blank"] td[name="tblCell"] {font-family: "Lato", sans-serif;font-size:14px;}    
         [name=bmeMainContentParent] {border-color:#666666;border-width:0px;border-style:none;border-radius:0px;border-collapse:separate;border-spacing:0px;overflow:hidden;}    
         [name=bmeMainColumnParent] {border-color:transparent;border-width:0px;border-style:none;border-radius:0px;}    
         [name=bmeMainColumn] {border-color:transparent;border-width:0px;border-style:none;border-radius:0px;border-collapse:separate;border-spacing:0px;}    
         [name=bmeMainContent] {border-color:transparent;border-width:0px;border-style:none;border-radius:0px;border-collapse:separate;border-spacing:0px;}    
      </style>
   </head>
   <body marginheight=0 marginwidth=0 topmargin=0 leftmargin=0 style="height: 100% !important; margin: 0; padding: 0; width: 100% !important;min-width: 100%;">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" name="bmeMainBody" style="background-color: #eaeaea;" bgcolor="#eaeaea">
         <tbody>
            <tr>
               <td width="100%" valign="middle" align="center">
                  <table cellspacing="0" cellpadding="0" border="0" name="bmeMainColumnParentTable">
                     <tbody>
                        <tr>
                           <td name="bmeMainColumnParent" style="border: 0px none transparent; border-radius: 0px; border-collapse: separate;">
                              <table name="bmeMainColumn" class="bmeHolder bmeMainColumn" style="max-width: 600px; border-radius: 0px; border-collapse: separate; border-spacing: 0px; overflow: visible;" cellspacing="0" cellpadding="0" border="0" align="center">
                                 <tbody>
                                    <tr>
                                       <td width="100%" class="blk_container bmeHolder" name="bmePreHeader" valign="top" align="center" style="color: rgb(102, 102, 102); border: 0px none transparent; background-color: rgb(0, 174, 84);" bgcolor="#00ae54"></td>
                                    </tr>
                                    <tr>
                                       <td width="100%" class="bmeHolder" valign="top" align="center" name="bmeMainContentParent" style="border: 0px none rgb(102, 102, 102); border-radius: 0px; border-collapse: separate; border-spacing: 0px; overflow: hidden;">
                                          <table name="bmeMainContent" style="border-radius: 0px; border-collapse: separate; border-spacing: 0px; border: 0px none transparent;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                             <tbody>
                                                <tr>
                                                   <td width="100%" class="blk_container bmeHolder bmeBody" name="bmeBody" valign="top" align="center" style="color: rgb(56, 56, 56); border: 0px none transparent; background-color: rgb(255, 255, 255);" bgcolor="#ffffff">
                                                      <div id="dv_15" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table width="600" cellspacing="0" cellpadding="0" border="0" class="blk" name="blk_divider" style="">
                                                            <tbody>
                                                               <tr>
                                                                  <td class="tblCellMain" style="padding: 10px 0px;">
                                                                     <table class="tblLine" cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top-width: 0px; border-top-style: none; min-width: 1px;">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td><span></span></td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                      <div id="dv_11" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table width="600" cellspacing="0" cellpadding="0" border="0" class="blk" name="blk_image">
                                                            <tbody>
                                                               <tr>
                                                                  <td>
                                                                     <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td align="center" class="bmeImage" style="border-collapse: collapse; padding: 20px;"><img    
                                                                                 src="'.url("/public/img/logo_5.png").'" width="52" style="max-width: 150px; display: block; width: 150px;" alt="" border="0"></td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                      <div id="dv_17" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table width="600" cellspacing="0" cellpadding="0" border="0" class="blk" name="blk_text">
                                                            <tbody>
                                                               <tr>
                                                                  <td>
                                                                     <table cellpadding="0" cellspacing="0" border="0" width="100%" class="bmeContainerRow">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td class="tdPart" valign="top" align="center">
                                                                                 <table cellspacing="0" cellpadding="0" border="0" width="600" name="tblText" style="float:left; background-color:transparent;" align="left" class="tblText">
                                                                                    <tbody>
                                                                                       <tr>
                                                                                          <td valign="top" align="left" name="tblCell" style="padding: 10px 20px; font-family: "Lato", sans-serif; font-size: 14px; font-weight: 400; color: rgb(56, 56, 56); text-align: left;" class="tblCell">
                         <div style="line-height: 150%; display:inline-block;text-align: left; color: #3e81c6 !important;font-weight:bold;"><span  style="display:inline-block;font-size: 18px;
    font-family: "Lato", sans-serif;
    color: #3e81c6 !important;
    line-height: 150%;">Hi '.$user->name.',</span>  <br> <br> 
                                                                                              <span  style="font-size: 18px; display:inline-block;
    font-family: "Lato", sans-serif;
    color: #3e81c6;
    line-height: 150%;">Welcome to LINQQ,</span>                                                                                           
                                                                                               <br><br>
                                                                                                 <a style="font-size: 16px; font-family: "Lato", sans-serif; color: #464646 !important; line-height: 150%;font-weight: 600;"><strong style="color:#000000;">We are privileged to have you on board, so lets undergo
the journey to success together.</strong></a><br>
                                                                                             </div>
                                                                                          </td>
                                                                                       </tr>
                                                                                    </tbody>
                                                                                 </table> 
                                                                              </td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                      
                                                      
                                                     
                                                      
                                                   </td>
                                                </tr>
                                        
                                                <tr>
                                                   <td width="100%" class="blk_container bmeHolder" name="bmePreFooter" valign="top" align="center" style="color: rgb(56, 56, 56); border: 0px none transparent; background-color: rgb(255, 255, 255);" bgcolor="#ffffff">
                                                      <div id="dv_5" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table width="600" cellspacing="0" cellpadding="0" border="0" class="blk" name="blk_text">
                                                            <tbody>
                                                               <tr>
                                                                  <td>
                                                                     <table cellpadding="0" cellspacing="0" border="0" width="100%" class="bmeContainerRow">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td class="tdPart" valign="top" align="center">
                                                                                 <table cellspacing="0" cellpadding="0" border="0" width="600" name="tblText" style="float:left; background-color:transparent;" align="left" class="tblText">
                                                                                    <tbody>
                                                                                       <tr>
                                                                                          <td valign="top" align="left" name="tblCell" style="padding: 10px 20px; font-family: "Lato", sans-serif; font-size: 14px; font-weight: 400; color: rgb(56, 56, 56); text-align: left;" class="tblCell">
                                                                                             <div style="line-height: 200%;color: #3e81c6 !important;
    font-weight: bold;"><span style="display:inline-block;font-size: 14px;
   font-family: "Lato", sans-serif;
    color: #3879bd;
    line-height: 200%;
        background: #f6f6f8 !important;
    font-weight: 900;"><span  style="font-size: 14px;
    font-family: "Lato", sans-serif;display:inline-block;
    color: #3e81c6 !important;
    font-weight: bold;
    line-height: 150%;">HAPPY NETWORKING !</span></span></div>
                                                                                          </td>
                                                                                       </tr>
                                                                                    </tbody>
                                                                                 </table>
                                                                              </td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                       <br>
                                                       <br>

                                                         <!----------------------------------------------------->
                                                      
                                                      <div id="dv_8" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table cellspacing="0" cellpadding="0" border="0" id="bllbbbb"  width="1000"  align="center" class="blk" name="blk_divider" style="background: #f6f6f8;padding: 20px 10px;    background: #f6f6f8;
    padding: 20px 10px;
    margin: 0 auto;
    width: 600px;">   
    
    
                                                            <tbody>
                                                               <tr>
                                                                  <td>
                                                                     <table cellpadding="0" cellspacing="0" border="0" class="bmeContainerRow">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td class="tdPart" valign="top" align="center">
                                                                                 <table cellspacing="0" cellpadding="0" border="0" width="1000" style="    background: #f6f6f8;
    padding: 20px 10px;
    margin: 0 auto;
    width: 600px;" name="tblText" style="" align="center" class="tblText">
                                                                                    <tbody>
                                                                                       <tr>
                                                                                          <td valign="top" align="center" name="tblCell" style="padding: 10px 20px; font-family: "Lato", sans-serif; font-size: 14px; font-weight: 400; color: rgb(56, 56, 56); text-align: center;" class="tblCell">
                                                                                             <div style="text-align: center;margin-bottom: 0px;">
                                                                                                 <span style="font-size: 16px; font-family: "Lato", sans-serif; color: #333; line-height: 150%;"><b><a href="javascript:void();" style="display:inline-block; color:#333 !important;">www.linqq.io</a></b></span>  <br>
                                                                                                 <span style="font-size: 16px; font-family: "Lato", sans-serif; color: #333; line-height: 150%;"><b><a href="javascript:void();" style="display:inline-block; color:#333 !important;">Support@linqq.io</a></b></span> 
                                                                                                <br>
                                                                                             </div>
                                                                                          </td>
                                                                                       </tr>
                                                                                    </tbody>
                                                                                 </table>
                                                                              </td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                            <tbody>
                                                                           <tr>
                                                                              <td class="tdItemContainer" style="margin-top:20px">
                                                                                 <table cellspacing="0" cellpadding="0" border="0" class="mblSocialContain" style="table-layout: auto;     text-align: center;
    margin: auto;">
                                                                                    <tbody>
                                                                                       <tr>
                                                                                          <td valign="top" name="bmeSocialTD" class="bmeSocialTD">
                                                                                             <!--[if gte mso 6]>
                                                                                          </td>
                                                                                          <td align="left" valign="top">
                                                                                             <![endif]-->    
                                                                                             <table cellspacing="0" cellpadding="0" border="0" class="bmeFollowItem" type="facebook" style="float: left; display: block;" align="left">
                                                                                                <tbody>
                                                                                                   <tr>
                                                                                                      <td align="left" class="bmeFollowItemIcon" gutter="20" width="24" style="padding-right:20px;height:20px;">    
                                                                                                         <a href="https://www.facebook.com/Linqq.io/" target="_blank" style="display: inline-block;">
                                                                                                         <img src="'.url("/public/img/facebook.png").'" border="0" width="24" height="24"></a>
                                                                                                      </td>
                                                                                                   </tr>
                                                                                                </tbody>
                                                                                             </table>
                                                                                             <!--[if gte mso 6]>
                                                                                          </td>
                                                                                          <td align="left" valign="top">
                                                                                             <![endif]-->    
                                                                                             <table cellspacing="0" cellpadding="0" border="0" class="bmeFollowItem" type="twitter" style="float: left; display: block;" align="left">
                                                                                                <tbody>
                                                                                                   <tr>
                                                                                                      <td align="left" class="bmeFollowItemIcon" gutter="20" width="24" style="padding-right:20px;height:20px;">    
                                                                                                         <a href="https://www.linkedin.com/company/linqqapp/" target="_blank" style="display: inline-block;"><img src="'.url("/public/img/linkedin.png").'" border="0" width="24" height="24"></a>
                                                                                                      </td>
                                                                                                   </tr>
                                                                                                </tbody>
                                                                                             </table>
                                                                                             <!--[if gte mso 6]>
                                                                                          </td>
                                                                                          <td align="left" valign="top">
                                                                                             <![endif]-->    
                                                                                             <table cellspacing="0" cellpadding="0" border="0" class="bmeFollowItem" type="email" style="float: left; display: block; padding-right: 20px;
}" align="left">
                                                                                                <tbody>
                                                                                                   <tr>
                                                                                                      <td align="left" class="bmeFollowItemIcon" gutter="20" width="24" style="height:20px;">    
                                                                                                         <a href="https://www.instagram.com/linqq.io/" style="display: inline-block;" target="_blank" ><img src="'.url("/public/img/instagram.png").'" alt="Email" style="display: block; max-width: 114px; margin-right: 20px;" border="0" width="24" height="24"></a>
                                                                                                      </td>
                                                                                                   </tr>
                                                                                                </tbody>
                                                                                             </table>  
                                                                                             <table cellspacing="0" cellpadding="0" border="0" class="bmeFollowItem" type="email" style="float: left; display: block;" align="left">
                                                                                                <tbody>
                                                                                                   <tr>
                                                                                                      <td align="left" class="bmeFollowItemIcon" gutter="20" width="24" style="height:20px;">    
                                                                                                         <a href="https://twitter.com/Linqq_io" style="display: inline-block;" target="_blank"><img src="'.url("/public/img/twitter.png").'" alt="Email" style="display: block; max-width: 114px;" border="0" width="24" height="24"></a>
                                                                                                      </td>
                                                                                                   </tr>
                                                                                                </tbody>
                                                                                             </table>
                                                                                          </td>
                                                                                       </tr>
                                                                                    </tbody>
                                                                                 </table>
                                                                              </td>
                                                                           </tr>
                                                                        </tbody>
                                                         </table>
                                                      </div>
                                                      <!----------------------------------------------------->
                                                       
                                                      
                                                      
                                                      
                                                                                                      
                                                      
                                                      
                                                      <div id="dv_9" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table width="600" cellspacing="0" cellpadding="0" border="0" class="blk" name="blk_divider" style="">
                                                            <tbody>
                                                               <tr>
                                                                  <td class="tblCellMain" style="padding: 10px 20px;">
                                                                     <table class="tblLine" cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top-width: 0px; border-top-style: none; min-width: 1px;">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td><span></span></td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                      
                                                      
                                                      <div id="dv_13" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table cellspacing="0" cellpadding="0" border="0" name="blk_divider" width="600" class="blk">
                                                            <tbody>
                                                               <tr>
                                                                  <td style="padding: 10px 0px;" class="tblCellMain">
                                                                     <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-top-width: 0px; border-top-style: none; min-width: 1px;" class="tblLine">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td><span></span></td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                       
                                                       
                                                       
                                          
                                                      
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                    
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
   </body>
</html>
';
//     $mssg='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

//                 <html>
//       <body><div bgcolor="#f4f8fc" marginwidth="0" marginheight="0" style="margin:0;padding:0;background:#f4f8fc">
// <table style="background:#f4f8fc;width:100%!important" width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#f4f8fc"><tbody><tr><td>
//   <table style="min-width:450px!important;max-width:620px!important;width:100%!important;margin:auto" class="m_-1255399300471218352m_-7251330376895672873m_3466839749643232769email-container" width="620" cellspacing="0" cellpadding="0" border="0" align="center">
//    <tbody><tr>
//  <td>
//         <table style="width:100%!important;border-bottom:solid 2px #f1f3f5;background:#ffffff" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
//           <tbody><tr>
//             <td style="font-size:0;line-height:0" width="50%" height="30">&nbsp;</td>
//             <td style="font-size:0;line-height:0" width="50%" height="30">&nbsp;</td>
//           </tr>
//           <tr>
//             <td style="text-align:center" colspan="2" width="100%" valign="center">
//                 <center><a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://www.shapr.net&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNHwi8xTsaT8hvSLKHkXMDqSrj4uAg">
//                 <img src="'.url("/public/img/logo_5.png").'" alt="Linqq" style="display:block" width="126" height="30" border="0" class="CToWUd"></a></center>
//             </td>
//           </tr>
//           <tr>
//             <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
//             <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
//           </tr>
//         </tbody></table>
        
//     </td>
// </tr>
//     <tr>
//     <td>
        
//         <table style="width:100%!important;background:#2f6ca6" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#2F6CA6">
//             <tbody><tr>
//                 <td style="font-size:0;line-height:0" width="50%" height="30">&nbsp;</td>
//                 <td style="font-size:0;line-height:0" width="50%" height="30">&nbsp;</td>
//             </tr>
//             <tr>
//                 <td style="text-align:center;font-family:sans-serif;font-size:13px;color:#ffffff" width="100%" valign="center">

//               </td>
//             </tr>
//             <tr>
//                 <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
//                 <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
//             </tr>
//         </tbody></table>        
//     </td>
//     </tr>
//     <tr>
//   <td> 
    
//     <table style="background:#12283d;width:100%!important" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#12283d">
//       <tbody><tr>
//         <td style="font-size:0;line-height:0" height="46">&nbsp;</td>
//       </tr>
//       <tr>
      
//           <td style="font-family:sans-serif;font-size:16px;color:#ffffff" valign="middle" align="center">
//           Congratulations, '.$user->name.'!
//           </td>
//       </tr>
//       <tr>
//         <td style="font-size:0;line-height:0" height="12">&nbsp;</td>
//       </tr>

//       <tr> 
//         <td style="font-size:0;line-height:0" height="40">&nbsp;</td>
//       </tr>
//       <tr><td width="50" valign="middle" align="center">
          
//         <img alt="Userimage" src="'.$this->obj->is_filetemp("profile", $user->image).'" style="    border-radius: 100%;
//     width: 100px;
//     height: 100px;" class="CToWUd">
//       </td>
//       </tr><tr>
//         <td style="font-size:0;line-height:0" height="20">&nbsp;</td>
//       </tr>
//         <tr>
//         <td style="font-family:sans-serif;font-size:25px;color:#ffffff;padding-right:36px;padding-left:36px" valign="middle" align="center">'.$user->designation.'</td>  
//       </tr>
//       <tr> 
//         <td style="font-size:0;line-height:0" height="10">&nbsp;</td>
//       </tr> 
//        <tr>
//         <td style="font-size:0;line-height:0" height="20">&nbsp;</td>
//       </tr>
//       <tr> 
//         <td style="font-size:0;line-height:0" height="46">&nbsp;</td>
//       </tr> 
//     </tbody></table>
    
//   </td>
// </tr>
       
 

//      <tr>
//         <td>
//               <table style="width:100%!important;background:#e5e5e5" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#E5E5E5">
//                 <tbody><tr>
//                     <td style="font-size:0;line-height:0" width="50%" height="15">&nbsp;</td>
//                     <td style="font-size:0;line-height:0" width="50%" height="15">&nbsp;</td>
//                 </tr>
//                 <tr>
//                     <td style="text-align:left;font-family:sans-serif;font-size:13px;color:#ffffff" width="100%" valign="center">

//                         <table style="width:100%!important" width="100%" cellspacing="0" cellpadding="5" border="0">
//                             <tbody><tr>
//                                 <td width="2%"></td>
                               
//                                 <td style="font-family:sans-serif;font-size:15px;color:#000">You have just joined a global community of professionals who believe that the craziest life opportunities come from meeting new people.<p>&nbsp;</p>

// At Linqq, our goal is to spark meaningful, mutually beneficial conversations that evolve into long-term relationships. I truly believe you will be inspired through your conversations if you approach networking with an open mind, a curious attitude, and an eagerness to help others learn and succeed.
// <p>&nbsp;</p>
// Welcome to Linqq, '.$user->name.'!
// </td>

//                                 <td width="2%"></td>
//                             </tr>
//                         </tbody></table>
//               </td>
//             </tr>
//                 <tr>
//                     <td style="font-size:0;line-height:0" height="15">&nbsp;</td>
//                     <td style="font-size:0;line-height:0" height="15">&nbsp;</td>
//                 </tr>
//             </tbody></table>
            

//         </td>
//     </tr>


//             <tr>
//   <td>
    
//     <table style="border-bottom:solid 1px #e2e9ef;background:#ffffff;width:100%!important" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
//       <tbody><tr>
//         <td colspan="3" style="font-size:0;line-height:0" height="50">&nbsp;</td>
//       </tr>
//       <tr>
//         <td style="text-align:left;padding-left:36px" class="m_-1255399300471218352m_-7251330376895672873m_3466839749643232769content-padded-left" valign="middle">
//           <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://www.shapr.net&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNHwi8xTsaT8hvSLKHkXMDqSrj4uAg"><img src="'.url("/public/img/logo_5.png").'" alt="Linqq" width="84" height="25" border="0" class="CToWUd"></a>
//         </td>

//         <td style="text-align:left;font-size:12px;color:#606c76;font-family:sans-serif;white-space:nowrap" valign="middle">&nbsp;</td>
//         <td style="text-align:right;padding-right:36px" class="m_-1255399300471218352m_-7251330376895672873m_3466839749643232769content-padded-right" valign="middle">
//           <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://itunes.apple.com/us/app/shapr/id859091569?mt%3D8&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNGZz6HUgvOTwHST7283NsGT3JV9DQ"><img src="https://ci6.googleusercontent.com/proxy/0NZeXk4LPNZRWvbEqWEP8ad9Ze3p4l8rjev4cx-KQLkJ7YBZPmcis6JVkC2PbB-OWsd4Fbkb26nJmKJEinWwGLVQx7CBuUk=s0-d-e1-ft#http://ws.shapr.net/front/emails/social-apple.png" alt="Apple" width="32" height="32" border="0" class="CToWUd"></a>&nbsp;
//           <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://play.google.com/store/apps/details?id%3Dcom.shapr&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNGQATgICekqfBhimlyyS9us2poyCw"><img src="https://ci3.googleusercontent.com/proxy/Cmj9Gqo1YmIHxxwdGqHTYQqdu_kmZkIpnp9XKlKXFG2r0dCYwBIpJTgq5ReZuLxPa1B5WDlagqb50R8Dxl3CYQFrJCYbQ1cDww=s0-d-e1-ft#http://ws.shapr.net/front/emails/social-android.png" alt="Android" width="32" height="32" border="0" class="CToWUd"></a>&nbsp;
//           <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://www.facebook.com/LinqqApp&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNH5JT4AAWMS_Gi3tk6N4o_eeNy9Mg"><img src="https://ci3.googleusercontent.com/proxy/46FKFGM-8-H6ucqfv1acrmZUWXrYTX6EIsmG5BZZzeHkE-q8zt1caX-Q4NDnZ-bqHoYSb72_WsA4wz1CVZXP7_sZ-sAJHSEWDIc=s0-d-e1-ft#http://ws.shapr.net/front/emails/social-facebook.png" alt="Facebook" width="32" height="32" border="0" class="CToWUd"></a>&nbsp;
//           <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://twitter.com/weareshapr&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNGDiPxh00M54w_fnmHOJ65UUUr1PA"><img src="https://ci3.googleusercontent.com/proxy/o1cMaAilevsLR04C4mKjru0nXtNp2F1b2nTzUp74lG9dGe2s1rKxOzXse1VP5jxcWqPe26juTgYQLqm3nE97YbSgvf8kD_DGOw=s0-d-e1-ft#http://ws.shapr.net/front/emails/social-twitter.png" alt="Twitter" width="32" height="32" border="0" class="CToWUd"></a>&nbsp;
//           <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://plus.google.com/%2LinqqNet&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNH8LQk6IW7qqCzsK5U4J-0bhKf12Q"><img src="https://ci4.googleusercontent.com/proxy/SMh2etWUm4-OEvKG7SW6H4QbH-VtuqTgb3taS15uHzEjVw9wWotK8h_-98DjdIG8jXUqHUjw5YmAJWV4HQ3VqvNswTnEm2I=s0-d-e1-ft#http://ws.shapr.net/front/emails/social-gplus.png" alt="Google Plus" width="32" height="32" border="0" class="CToWUd"></a>
//         </td>
//       </tr>
//       <tr>
//         <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
//         <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
//         <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
//       </tr>
//     </tbody></table>
    
//   </td>
// </tr>
        
  
//       </tbody></table>  
  
//             </td></tr></tbody></table><div class="yj6qo"></div><div class="adL">
// </div></div></body></html>';   
                      
                    $to      = (string)$user->email; 
                    $subject = 'Welcome to Linqq';  
                    $message =  $mssg;  
                    $headers1 = "From:  info@mobulous.co.in \r\n"; 
                    //$headers1 .= "Reply-To: info@mobulous.co.in \r\n";
                    $headers1 .= "MIME-Version: 1.0 \r\n";
                    $headers1 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                    mail($to, $subject, $message, $headers1);


//  $email = new \SendGrid\Mail\Mail(); 
// $email->setFrom("support@linqqapp.com", "Team Linqq");
// $email->setSubject("Welcome to Linqq");
// $email->addTo($to, "Team Linqq");
// $email->addContent(
//     "text/html", $mssg
// );
// $sendgrid = new \SendGrid("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");
// try {
//     $response = $sendgrid->send($email);
//    // print $response->statusCode() . "\n";
//     //print_r($response->headers());
//     //print $response->body() . "\n";
// } catch (Exception $e) {
//     //echo 'Caught exception: '. $e->getMessage() ."\n";
// }



        //} 
      
      }  
                   
                 //manish code


                    $createUser->check_first_status = (count($createUser2)>0)?"yes":"no"; 

                    $check_user_interest = UserInterest::where('user_id',$createUser->id)->first();
                    $createUser->headline_status = !empty($createUser->headline)?"yes":"no";
                    $createUser->interest_status = (count($check_user_interest)>0)?"yes":"no";
                    $createUser->additional_info_status = !empty($createUser->designation)?"yes":"no";
                    //manish code

                 $this->obj->set_data("200", "Signup successfully", $request->path(), "response", $createUser); 
              
                } else {  

                    $this->obj->error_message('500', "Internal error.", $request->path());
                }
           } 
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
  }  
  


 public function logout(Request $request)
    {

         try {   

            if (Auth::check()) { 
                
                $user_id       = Auth::user()->id;
                User::where('id', $user_id)->update(['device_id' =>'','device_type'=>'']);
                Auth::user()->AauthAcessToken()->delete(); 
                $this->obj->set_data("200", "Logout successfully", $request->path());
            
            } else {
                $this->obj->error_message('500', "Already logout.", $request->path());
            } 

        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
  }    

    public function forgot_password(Request $request)
    {
        try {
            $rules = array(
                'phone'    => 'required|numeric',
                'password' => 'required|max:190',
            );

            if ($this->obj->validate_request($request, $rules)) {
                $user = User::where("phone", $request->phone)->first();
                if (!empty($user)) {
                    $user->original_password = $request->password;
                    $user->password          = bcrypt($request->password);
                    if ($user->save()) {
                        $this->obj->set_data("200", "Changed successfully.", $request->path());
                    } else {
                        $this->obj->error_message('500', "Internal error.", $request->path());
                    }
                } else {
                
                    $this->obj->error_message('500', "Phone number not in record.", $request->path());
                
                } 
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }  

  public function change_password(Request $request)
    { 
        try {
            $rules = array(
                'old_password' => 'required|max:190',
                'new_password' => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id = Auth::user()->id;
                $user    = User::where("id", $user_id)->first();
                if (Hash::check($request->old_password, $user->password)) {
                    $user->original_password = $request->new_password;
                    $user->password          = bcrypt($request->new_password);
                    if ($user->save()) {
                        $this->obj->set_data("200", "Changed successfully", $request->path());
                    } else {
                        $this->obj->error_message('500', "Internal error.", $request->path());
                    }
                } else {
                    $this->obj->error_message('500', "Current password do not match.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function add_message_template(Request $request)
    {
        try {
            $rules = array(
                'message' => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id              = Auth::user()->id;
                $getUserTemplateCount = UserMessageTemplate::where('user_id', $user_id)->count();
                if ($getUserTemplateCount <= 2) {
                    $create          = new UserMessageTemplate;
                    $create->user_id = $user_id;
                    $create->message = ucfirst($request->message);
                    if ($create->save()) {
                        $this->obj->set_data("200", "Saved successfully", $request->path());
                    } else {
                        $this->obj->error_message('500', "Internal error.", $request->path());
                    }
                } else {
                    $this->obj->error_message('500', "Sorry, you can not add more than two template.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
   } 

    public function edit_message_template(Request $request)
    {
        try {
            $rules = array(
                'template_id'         => 'max:190',
                'request_template_id' => 'max:190',
                'message'             => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id      = Auth::user()->id;
                if (!empty($request->request_template_id)) {
                    $updateStatus = UserMessageTemplate::where('request_template_id',$request->request_template_id)->where('user_id',$user_id)->first();
                    if (empty($updateStatus)) {
                        $updateStatus = new UserMessageTemplate;
                    }
                    $updateStatus->user_id = $user_id;
                    $updateStatus->request_template_id = $request->request_template_id;
                    $updateStatus->message = $request->message;
                    $updateStatus->save();
                }else{
                    $updateStatus = UserMessageTemplate::where('id', $request->template_id)->update(array('message' => $request->message));
                }
                if ($updateStatus) {
                    $this->obj->set_data("200", "Updated successfully.", $request->path());
                } else {
                    $this->obj->error_message('500', "Internal error.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function delete_message_template(Request $request)
    {
        try { 
            $rules = array(
                'template_id' => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id      = Auth::user()->id;
                $deleteStatus = UserMessageTemplate::where('id', $request->template_id)->first();
                if (!empty($deleteStatus)) {
                    UserMessageTemplate::where('id', ucfirst($request->template_id))->delete();
                    $this->obj->set_data("200", "Deleted successfully", $request->path());
                } else {
                    $this->obj->error_message('500', "Internal error.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }
 

   public function user_request_template(Request $request)
    {  
        try {  

            $rules = array(
                'user_id' => 'required|max:190',
            ); 
           
            if($this->obj->validate_request($request, $rules)) {
               
                $user_id = Auth::user()->id;
                $user    = User::where('id', $request->user_id)->first();
                if (empty($user)) {
                    $this->obj->error_message('500', "User not found.", $request->path());
                }
                $getUserTemplate = UserMessageTemplate::whereNotNull('request_template_id')->where('id',$user_id)->pluck('request_template_id');
                if (!empty($getUserTemplate)) {
                    $getUserTemplate = $getUserTemplate->toArray();
                    $get = RequestTemplate::select('id AS request_template_id', 'message')->whereNotIn('id',$getUserTemplate)->get();
                }else{
                    $get = RequestTemplate::select('id AS request_template_id', 'message')->get();
                }
                if (count($get)) { 
                    foreach ($get as $value) {
                        $value->message     = str_replace("[name]", $user->name, $value->message);
                        $value->template_id = "";
                    }
                    $uTemplate = UserMessageTemplate::select('id AS template_id', 'message')->where('user_id', $user_id)->get();
                    if (count($uTemplate)) {
                        foreach ($uTemplate as $value) {
                            $value->request_template_id = "";   
                        }
                        $uTemplate = $uTemplate->toArray();
                        $get       = $get->toArray();
                        $get       = array_merge($uTemplate, $get);
                    }
                    $this->obj->set_data("200", "User template.", $request->path(), 'response', $get);
                } else {
                    $this->obj->set_data("404", "No template found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function user_message_template(Request $request)
    {
        try {
            $rules = array(
                'user_id' => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id = Auth::user()->id;
                $user    = User::where('id', $request->user_id)->first();
                if (empty($user)) {
                    $this->obj->error_message('500', "User not found.", $request->path());
                }
                $get = MessageTemplate::select('id AS template_id', 'message')->get();
                if (count($get)) {
                    foreach ($get as $value) {
                        $value->message = str_replace("[name]", $user->name, $value->message);
                    }
                    $this->obj->set_data("200", "User template.", $request->path(), 'response', $get);
                } else {
                    $this->obj->set_data("404", "No template found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function add_organisations(Request $request)
    {
        try {
            $rules = array(
                'title' => 'required|max:190',
                'name'  => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id         = Auth::user()->id;
                $create          = new UserOrganisation;
                $create->user_id = $user_id;
                $create->title   = ucwords($request->title);
                $create->name    = ucwords($request->name);

                $check_exits_company = Company::where('name',$request->name)->first();
                	if(empty($check_exits_company))
                	{
                		$designation1       = new UserProfileAdd;
              			$designation1->name = ucwords($request->name);
              			$designation1->type = "company";
	               		$designation1->save();
                	}

                	$check_exits_designation = Designation::where('name',$request->title)->first();
                	if(empty($check_exits_designation))
                	{
                		$designation2       = new UserProfileAdd;
              			$designation2->name = ucwords($request->title);
              			$designation2->type = "designation";
	               		$designation2->save();
                	}


                if ($create->save()) {
                    $this->obj->set_data("200", "Saved successfully", $request->path());
                } else {
                    $this->obj->error_message('500', "Internal error.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    } 

    public function edit_organisations(Request $request)
    {
        try {
            $rules = array(
                'organisation_id' => 'required|max:190',
                'name'            => 'required|max:190',
                'title'           => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id   = Auth::user()->id;
                $getStatus = UserOrganisation::where('id', $request->organisation_id)->update(array('name' => ucwords($request->name), 'title' => ucwords($request->title)));
               	
               	$check_exits_company = Company::where('name',$request->name)->first();
                	if(empty($check_exits_company))
                	{
                		$designation1       = new UserProfileAdd;
              			$designation1->name = ucwords($request->name);
              			$designation1->type = "company";
	               		$designation1->save();
                	}

                	$check_exits_designation = Designation::where('name',$request->title)->first();
                	if(empty($check_exits_designation))
                	{
                		$designation2       = new UserProfileAdd;
              			$designation2->name = ucwords($request->title);
              			$designation2->type = "designation";
	               		$designation2->save();
                	}

                if ($getStatus) {
                    $this->obj->set_data("200", "Updated successfully", $request->path());
                } else {
                    $this->obj->error_message('500', "Internal error.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function delete_organisations(Request $request)
    {
        try {
            $rules = array(
                'organisation_id' => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id   = Auth::user()->id;
                $getStatus = UserOrganisation::where('id', $request->organisation_id)->first();
                if (!empty($getStatus)) {
                    UserOrganisation::where('id', $request->organisation_id)->delete();
                    $this->obj->set_data("200", "Deleted successfully", $request->path());
                } else {
                    $this->obj->error_message('500', "Internal error.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function user_organisations(Request $request)
    {
        try {
            $rules = array(

            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {
                $user_id = Auth::user()->id;
                $get     = UserOrganisation::select('id AS organisation_id', 'title', 'name')->where("user_id", $user_id)->orderBy('id','DESC')->get();   
                if (count($get)) {
                    $this->obj->set_data("200", "User organisations.", $request->path(), 'response', $get);
                } else {
                    $this->obj->set_data("404", "No organisations found.", $request->path(), 'response');
                } 
            } 
        } catch (Exception $e) {
       
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
   } 


   public function feedback(Request $request)
    {
        try {
            $rules = array(
                'title'     => 'required|max:500',
                'description' => 'required|max:500',
                
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id            = Auth::user()->id;
                $feedback             = new Feedback;
                $feedback->user_id    = $user_id;
                $feedback->feedback_option     = ucwords($request->title);
                $feedback->feedback_description     = ucwords($request->description);
                $feedback->created_at   = date('Y-m-d');
                $feedback->updated_at  = date('Y-m-d');
               
                if ($feedback->save()) {

                    $user = User::where('id',$user_id)->first();
                    $email = !empty($user->email)?$user->email:"N/A";
                    $Phone = !empty($user->phone)?$user->phone:"N/A";
                    // try{
                    //$to      = $user->email; 
                     $to      = "Team@linqqapp.com";
                    // $subject = 'Feedback From Linqq';  
                    $message =  "Hello,<br><br>";  
                    $message .=  "Name : ".$user->name."<br>";
                    $message .=  "Email : ".$email."<br>";
                    $message .=  "Phone : ".$Phone."<br>";
                    $message .=  "Title : ".$feedback->feedback_option."<br>";
                    $message .=  "Feedback : ".$feedback->feedback_description.'<br><br><br>';  
                    $message .= "Thanks<BR>";
                    $message .= $user->name;
                    // $headers1 = "From: info@mobulous.co.in \r\n"; 
                    // //$headers1 .= "Reply-To: info@mobulous.co.in \r\n";
                    // $headers1 .= "MIME-Version: 1.0 \r\n";
                    // $headers1 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                    // mail($to, $subject, $message, $headers1); 
                    // }
                    // catch(Exception $e)
                    // {

                    // } 




$email = new \SendGrid\Mail\Mail(); 
$email->setFrom("support@linqqapp.com", "Team Linqq");
$email->setSubject("Feedback From Linqq");
$email->addTo($to, "Team Linqq");
$email->addContent(
    "text/html", $message
);
$sendgrid = new \SendGrid("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");
try {
    $response = $sendgrid->send($email);
   // print $response->statusCode() . "\n";
    //print_r($response->headers());
    //print $response->body() . "\n";
} catch (Exception $e) {
    echo 'Caught exception: '. $e->getMessage() ."\n";
}  


                    $this->obj->set_data("200", "Saved successfully", $request->path());
                } else {
                    $this->obj->error_message('500', "Internal error.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }


   public function add_education(Request $request)
    {
        try {
            $rules = array(
                'degree'     => 'required|max:190',
                'start_year' => '',
                'end_year'   => '',
                'school'     => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id            = Auth::user()->id;
                $create             = new UserEducation;
                $create->user_id    = $user_id;
                $create->degree     = ucwords($request->degree);
                $create->school     = ucwords($request->school);
                $create->start_year = "";
                $create->end_year   = "";
                if ($create->save()) {
                    $this->obj->set_data("200", "Saved successfully", $request->path());
                } else {
                    $this->obj->error_message('500', "Internal error.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function edit_education(Request $request)
    {
        try {
            $rules = array(
                'degree'       => 'required|max:190',
                'start_year'   => '',
                'end_year'     => '',
                'school'       => 'required|max:190',
                'education_id' => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id   = Auth::user()->id;
                $getStatus = UserEducation::where('id', $request->education_id)->update(array('degree' => ucwords($request->degree), 'school' => ucwords($request->school)));
                if ($getStatus) {
                    $this->obj->set_data("200", "Updated successfully", $request->path());
                } else {
                    $this->obj->error_message('500', "Internal error.", $request->path());
                }
            } 
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function delete_education(Request $request)
    {
        try {
            $rules = array(
                'education_id' => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id   = Auth::user()->id;
                $getStatus = UserEducation::where('id', $request->education_id)->first();
                if (!empty($getStatus)) {
                    UserEducation::where('id', $request->education_id)->delete();
                    $this->obj->set_data("200", "Deleted successfully", $request->path());
                } else {
                    $this->obj->error_message('500', "Internal error.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function user_education(Request $request)
    {
        try {
            $rules = array(

            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {
                $user_id = Auth::user()->id;
                $get     = UserEducation::where("user_id", $user_id)->get();
                if (count($get)) {
                    foreach ($get as $value) {
                        $value->education_id = (string) $value->id;
                    }

                    $this->obj->set_data("200", "List.", $request->path(), 'response', $get);
                } else {
                    $this->obj->set_data("404", "No record found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function add_interest(Request $request)
    {
        try {
            $rules = array(
                'interest_id' => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id = Auth::user()->id;
                $i_data  = UserInterest::where('user_id', $user_id)->where('interest_id', $request->interest_id)->first();
                if (!empty($i_data)) {
                    $this->obj->error_message('500', "You already selected this option.", $request->path());
                }


                $response=array();

                if(!empty($request->interest_id))
                {
                    $i=0;
                    $net_ids = explode(',',$request->interest_id);
                    foreach ($net_ids as $id) {
                        
                         $create              = new UserInterest;
                         $create->user_id     = $user_id;
                         $create->interest_id = $id;
                         $create->save();
                            
                           $response[$i]['id'] =  $id;
                           $response[$i]['status']="true";

                           $i++;
                         }

                    $this->obj->set_data("200", "Saved successfully", $request->path(),'response',$response); 
     
                }

                 else {
                    $this->obj->error_message('500', "Internal error.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function delete_interest(Request $request)
    {
        try {
            $rules = array(
                'interest_id' => 'required|max:190',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {
                $user_id   = Auth::user()->id;
                $getStatus = UserInterest::where('interest_id', $request->interest_id)->where('user_id', $user_id)->first();
               if (!empty($getStatus)) {
                    
                     UserInterest::where('id', $getStatus->id)->delete();
                      
                     $response   = new \stdClass();
                     $response->id= $request->interest_id;
                     $response->status= "true";
                    $this->obj->set_data("200", "Deleted successfully", $request->path(),'response',$response);
                
                } else {
                    $this->obj->error_message('500', "Interest not found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function user_interest(Request $request)
    {
        try {

              $rules = array(

            );
            
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {
                $user_id = Auth::user()->id;
                $query   = "SELECT user_interests.interest_id,interests.name FROM user_interests LEFT JOIN interests ON user_interests.interest_id = interests.id WHERE user_interests.user_id = $user_id";
                $get     = DB::select($query);
                if (count($get)) {

                    if(!empty($get))
                    {
                        foreach ($get as $value) {
                           $value->status = "true";
                        }
                    }

                    $this->obj->set_data("200", "List.", $request->path(), 'response', $get);
                } else {
                    $this->obj->set_data("404", "No record found.", $request->path());
                }
            }
        } catch (Exception $e) {
  
            $this->obj->error_message('500', "Internal error.", $request->path());
  
        }
    
 } 

    public function add_networking(Request $request)
    {
        try {
            $rules = array(
                'networking_id' => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id = Auth::user()->id;
                $n_data  = UserNetworking::where('user_id', $user_id)->where('networking_id', $request->networking_id)->first();
                if (!empty($n_data)) {
                    $this->obj->error_message('500', "You already selected this option.", $request->path());
                }

                $response=array();

                if(!empty($request->networking_id))
                {
                    $i=0;
                    $net_ids = explode(',',$request->networking_id);
                    foreach ($net_ids as $id) {
                        
                        $create                = new UserNetworking;
                        $create->user_id       = $user_id;
                        $create->networking_id = $id;
                        $create->save();
                            
                           $response[$i]['id'] =  $id;
                           $response[$i]['status']="true";

                           $i++;
                         }


                    
                       $this->obj->set_data("200", "Saved successfully", $request->path(),'response',$response); 
               
                     } else {
                    //$this->obj->error_message('500', "Internal error.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function delete_networking(Request $request)
    {
        try {
            $rules = array(
                'networking_id' => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id   = Auth::user()->id;
                $getStatus = UserNetworking::select('id')->where('networking_id', $request->networking_id)->where('user_id', $user_id)->first();
                if (!empty($getStatus)) {
                    UserNetworking::where('id', $getStatus->id)->delete();

                     $response   = new \stdClass();
                     $response->id= $request->networking_id;
                     $response->status= "true";



                    $this->obj->set_data("200", "Deleted successfully", $request->path(),'response',$response);
                } else {
                    $this->obj->set_data("404", "No record found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function user_networking(Request $request)
    {
        try {
            $rules = array(

            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {
                $user_id = Auth::user()->id;
                $get     = UserNetworking::select('networking_id')->where("user_id", $user_id)->get();
                if (count($get)) {

                    if(!empty($get))
                    {
                        foreach ($get as $value) {
                            
                            $value->status = "true";
                        }
                    }



                    $this->obj->set_data("200", "List.", $request->path(), 'response', $get);
                } else {
                    $this->obj->set_data("404", "No record found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function networking_list(Request $request)
    {
        try {
            $rules = array(

            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {
                $user_id = Auth::user()->id;
                $get     = Networking::select('id AS networking_id', 'name')->get();
                if (count($get)) {
                    $this->obj->set_data("200", "List.", $request->path(), 'response', $get);
                } else {
                    $this->obj->set_data("404", "No record found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

   
   public function industry_list(Request $request)
    {
        try {
            $rules = array(

            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {
                $user_id = Auth::user()->id;
                $get     = Industry::select('id AS industry_id', 'name')->get();
                if (count($get)) {
                    $this->obj->set_data("200", "List.", $request->path(), 'response', $get);
                } else {
                    $this->obj->set_data("404", "No record found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

   
   public function degree_list(Request $request)
    {
        try {
            $rules = array(

            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {
                $user_id = Auth::user()->id;
                $get     = Degree::select('id AS degree_id', 'name')->get();
                if (count($get)) {
                    $this->obj->set_data("200", "List.", $request->path(), 'response', $get);
                } else {
                    $this->obj->set_data("404", "No record found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function interest_list(Request $request)
    {
        try {
            $rules = array(

            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {
                $user_id = Auth::user()->id;
                $get     = Interest::select('id AS interest_id', 'name')->get();
                if (count($get)) {
                    $this->obj->set_data("200", "List.", $request->path(), 'response', $get);
                } else {
                    $this->obj->set_data("404", "No record found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }


//manish code

     public function company_and_desig_list(Request $request)
    {
        try {
            $rules = array(

            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {
                $response=array();
               // $user_id = Auth::user()->id;
                $cget     = Company::select('id AS company_id', 'name')->get()->toArray();
                 $dget     = Designation::select('id AS designation_id', 'name')->get()->toArray();
                 $eget     = PreviousEducation::select('id AS education_id', 'name')->get()->toArray();
                 $response['company']=$cget;
                 $response['designation']=$dget;
                  $response['previous_education']=$eget;
                 
                if (count($cget) or count($dget) or count($eget)) {
                   
                  exit(response()->json(
                            [
                                'status'     => 'SUCCESS',
                                "message"    => "List",
                                "requestKey" => $request->path(),
                                "company_and_desig_list"   => $response,
                            ]
                        )->getContent());       

                } else {
                    $this->obj->set_data("404", "No record found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

//manish code

   
    public function add_favourite(Request $request)
    {
        try {
            $rules = array(
                'favourite_id' => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id = Auth::user()->id;
                /*$u_fav = UserFavourite::where('user_id',$user_id)->where('favourite_id',$request->favourite_id)->first();
                if (!empty($u_fav)) {
                $this->obj->error_message('500', "You already selected this option.", $request->path());
                }*/
                $create               = new UserFavourite;
                $create->user_id      = $user_id;
                $create->favourite_id = $request->favourite_id;
                if ($create->save()) {
                    $this->obj->set_data("200", "Saved successfully", $request->path());
                } else {
                    $this->obj->error_message('500', "Internal error.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function delete_favourite(Request $request)
    {
        try {
            $rules = array(
                'favourite_id' => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id   = Auth::user()->id;
                $getStatus = UserFavourite::select('id')->where('favourite_id', $request->favourite_id)->where('user_id', $user_id)->first();
                if (!empty($getStatus)) {
                    UserFavourite::where('id', $getStatus->id)->delete();
                    $this->obj->set_data("200", "Deleted successfully", $request->path());
                } else {
                    $this->obj->set_data("404", "No record found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function user_favourite(Request $request)
    {
        try {
            $rules = array(

            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {
                $user_id = Auth::user()->id;
                $get     = UserFavourite::where("user_id", $user_id)->get();
                if (count($get)) {
                    $this->obj->set_data("200", "List.", $request->path(), 'response', $get);
                } else {
                    $this->obj->set_data("404", "No record found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function profile(Request $request)
    {
        try {
            $user_id           = Auth::user()->id;
            $user              = User::find($user_id);

            $lat = $request->lat;
            $long  = $request->long;
    
            if(!empty($lat)){
            $update_lat_long = User::where("id",$user_id)->update(array("lat"=>$request->lat,"log"=>$request->log)); 
            }           


            $user->image       = $this->obj->is_file("profile", $user->image);
            $user->cover_image = $this->obj->is_file("profile", $user->cover_image);
            unset($user->password, $user->original_password);
            $user_education           = UserEducation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->first();
            $user->degree             = !empty($user_education->degree) ? $user_education->degree : "";
            $user->school             = !empty($user_education->school) ? $user_education->school : "";
            $user->education          = UserEducation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->take(2)->get();
            $user_organisations       = UserOrganisation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->first();
            $user->organisation_name  = !empty($user_organisations->name) ? $user_organisations->name : "";
            $user->organisation_title = !empty($user_organisations->title) ? $user_organisations->title : "";
            $user->organisations       = UserOrganisation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->take(2)->get();
            if (!empty($user->industry_id)) {
                $industry       = Industry::where('id', $user->industry_id)->first();
                $user->industry = !empty($industry->name) ? $industry->name : '';
            } else {
                $user->industry = "";
            }
            $query            = "SELECT favourites.name, favourites.image, user_favourites.favourite_id FROM user_favourites LEFT JOIN favourites ON user_favourites.favourite_id = favourites.id WHERE user_favourites.user_id = $user_id";
            $favourites       = DB::select($query);
            $user->favourites = array();
            if (count($favourites)) {
                foreach ($favourites as $key => $value) {
                    $value->image = url("/public/apps/$value->image");
                }
                $user->favourites = $favourites;
            }

            $query             = "SELECT networkings.name, networkings.image, user_networkings.networking_id FROM user_networkings LEFT JOIN networkings ON user_networkings.networking_id = networkings.id WHERE user_networkings.user_id = $user_id";
            $networkings       = DB::select($query);
            $user->networkings = array();
            if (count($networkings)) {
                foreach ($networkings as $key => $value) {
                    $value->image = url("/public/apps/$value->image");
                }
                $user->networkings = $networkings;
            }
            $user->card_count = $this->ads_count();
            $query            = "SELECT interests.name, user_interests.interest_id FROM user_interests LEFT JOIN interests ON user_interests.interest_id = interests.id WHERE user_interests.user_id = $user_id";
            $interests        = DB::select($query);
            $user->interests  = array();
            if (count($interests)) {
               
                   $user->interests = $interests;
            
            }
            /*echo "<pre>";
            print_r($user);
            die;*/  
            $this->obj->set_data("200", "User profile.", $request->path(), 'response', $user);

        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }

    public function view_profile(Request $request)
    {
        try {
            $rules = array(
                'user_id' => 'required',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $userId = Auth::user()->id;
                $userID = $request->user_id;
                $query  = "SELECT * FROM users WHERE is_admin != '1' AND id != $userId AND id IN ($userID) ORDER BY instr('$userID',id)";

                $users  = DB::select($query);  
                  
                  foreach ($users as $user) {
                  
                    $user_id           = $user->id;
                    $user->user_id     = (string) $user->id;
                    $user->image       = $this->obj->is_file("profile", $user->image);
                    $user->cover_image = $this->obj->is_file("profile", $user->cover_image);
                    unset($user->password, $user->original_password);
                    if (!empty($user->industry_id)) {
                        $industry             = Industry::where('id', $user->industry_id)->first();
                        $user->industry       = !empty($industry->name) ? $industry->name : '';
                        $user->industry_match = "false";
                        if (Auth::user()->industry_id == $user->industry_id) {
                            $user->industry_match = "true";
                        }
                    } else {
                        $user->industry = "";
                    }
                    $user_education           = UserEducation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->first();
                    $user->degree             = !empty($user_education->degree) ? $user_education->degree : "";
                    $user->school             = !empty($user_education->school) ? $user_education->school : "";
                    $user_organisations       = UserOrganisation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->first();
                    $user_organisations       = UserOrganisation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->first();
                    $user->organisation_name  = !empty($user_organisations->name) ? $user_organisations->name : "";
                    $user->organisation_title = !empty($user_organisations->title) ? $user_organisations->title : "";
                    $query                    = "SELECT favourites.name, favourites.image, user_favourites.favourite_id FROM user_favourites LEFT JOIN favourites ON user_favourites.favourite_id = favourites.id WHERE user_favourites.user_id = $user_id";
                    $favourites               = DB::select($query);
                    $user->favourites         = array();
                    if (count($favourites)) {
                        $getFav = UserFavourite::where('user_id', $userId)->pluck('favourite_id');
                        foreach ($favourites as $key => $value) {
                            $value->image = url("/public/apps/$value->image");
                            $value->match = "false";
                            if (count($getFav)) {
                                if (in_array($value->favourite_id, $getFav->toArray())) {
                                    $value->match = "true";
                                }
                            }
                        }
                        $user->favourites = $favourites;
                    }

                    $query             = "SELECT networkings.name, networkings.image, user_networkings.networking_id FROM user_networkings LEFT JOIN networkings ON user_networkings.networking_id = networkings.id WHERE user_networkings.user_id = $user_id";
                    $networkings       = DB::select($query);
                    $user->networkings = array();
                    if (count($networkings)) {
                        $getNet = UserNetworking::where('user_id', $userId)->pluck('networking_id');
                        foreach ($networkings as $key => $value) {
                            $value->image = url("/public/apps/$value->image");
                            $value->match = "false";
                            if (count($getNet)) {
                                if (in_array($value->networking_id, $getNet->toArray())) {
                                    $value->match = "true";
                                }
                            }
                        }
                        $user->networkings = $networkings;
                    }
                    $user->education = UserEducation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->take(2)->get();
                     $user->organisations  = UserOrganisation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->take(2)->get();  
                    $query           = "SELECT interests.name, user_interests.interest_id FROM user_interests LEFT JOIN interests ON user_interests.interest_id = interests.id WHERE user_interests.user_id = $user_id";
                    $interests       = DB::select($query);
                    $user->interests = array();
                    if (count($interests)) {
                        $getInterest = UserInterest::where("user_id", $userId)->pluck('interest_id');
                        foreach ($interests as $value) {
                            $value->match = "false";
                            if (count($getInterest)) {
                                if (in_array($value->interest_id, $getInterest->toArray())) {
                                    $value->match = "true";
                                }
                            }
                        }
                        $user->interests = $interests;
                    }
                } 

                $userID = explode(",", $userID);
                $userID = $userID[0];

                $this->obj->set_data("200", "User profile.", $request->path(), 'response', $users);
            }

        } catch (Exception $e) {

            $this->obj->error_message('500', "Internal error.", $request->path());
        }
   }

    public function get_ads(Request $request)
    {
       try {
              $rules = array(
           );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {
                $ads = Advertisement::inRandomOrder()->first();
                $ads_count = AdvertisementCount::where('id',1)->first();
                if (!empty($ads)) {

                    if($ads_count->count=='0')
                    {
                       
                    $ads = new \stdClass;
                    $ads->id = "";
                     $ads->title = "";
                      $ads->description = "";
                       $ads->image = "";
                        $ads->url = "";
                         $ads->created_at = "";
                          $ads->updated_at = "";

                    $this->obj->set_data("404", "No record found.", $request->path(),'response', $ads);
                    }
                    else
                    {
                        $ads->image = $this->obj->is_file("ads", $ads->image);
                         $this->obj->set_data("200", "List", $request->path(), 'response', $ads); 
                    }


                    
                } else {


                    $ads = new \stdClass;
                    $ads->id = "";
                     $ads->title = "";
                      $ads->description = "";
                       $ads->image = "";
                        $ads->url = "";
                         $ads->created_at = "";
                          $ads->updated_at = "";

                    $this->obj->set_data("404", "No record found.", $request->path(),'response', $ads);
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
 }

    

    public function all_user_profiletest(Request $request)
    {   
        try {       

            $rules = array(
           
                 'offset'         => 'required',
                 'current_lat'   => 'required',
                 'current_long'  => 'required',
           
            ); 
          
            if ($this->obj->validate_request($request, $rules)) {
                
                $userId       = Auth::user()->id;  
                //echo $userId;die; 
                $iguser = IgnoreUser::where('user_id',$userId)->orderBy('id','DESC')->first();
                
                if($iguser){  
                     
                     //echo $iguser->created_at;die;
                     $itime =  $iguser->created_at;
                        
                }
                 
                 $couser = UserRequest::where('sender_id',$userId)->where('reject_status','0')->orderBy('id','DESC')->first();
                
                if($couser){

                    $ctime =  $couser->created_at;                 
                 }
 
            
                 if(!empty($iguser) && !empty($couser)){
                   
                    if($itime > $ctime){ 

                        $ftime = $itime;

                    } else if($ctime > $itime) {
                         
                         $ftime= $ctime;
                    }

                } else if(empty($iguser) && !empty($couser)){ 

                     $ftime = $ctime; 

                } else if(!empty($iguser) && empty($couser)){
                      
                      $ftime = $itime; 
                
                }     
                 
                $curtime = strtotime(date("Y-m-d H:i:s"));
                
                if(!empty($ftime)){ 

                  
                  $fultime   = date('Y-m-d H:i:s',strtotime('+12 hour',strtotime($ftime))); 
                  $cfultime  = strtotime($fultime);
                   
                   
                 if($curtime < $cfultime){      
                  
                     $lmt =  20 - Auth::user()->count;
                      
                  } else {  
                    
                    
                      $lmt = 20;  
    
                   }       
                 
                } else {  
                    
                    $lmt = 20; 
                    User::where('id', $userId)->update(['count' => 0]); 
                    
                    if(!empty($iguser)){ 
                       
                         IgnoreUser::where('user_id', $userId)->delete();
                     
                      }   
               }  

           
                //echo $lmt;die;
               
                $location     =   trim(Auth::user()->location);
                $limit        =   !empty($this->ads_count())?$this->ads_count():"1";
                //echo $limit;die;   
                //$offset       =  ($limit*$request->offset)+$request->offset;
                $offset       =  ($limit*$request->offset);
                //$lat = Auth::user()->lat;
                //$log = Auth::user()->log;
                $lat   = $request->current_lat;
                $log  = $request->current_long; 
                $radius  = LocationRadius::find(1);
                if ($radius->radius > 0) {
                    $distance = $radius->radius; 
               
                } else {   
                   
                   $distance = 1000000000000000000;
               } 

                 $condition    = " is_admin != '1' AND id != $userId";
                //AND  location LIKE '%$location%' 
                
               $user_request = UserRequest::where('sender_id', $userId)->where('connection','ON')->where('reject_status','0')->pluck('receiver_id'); 
                $remove_user = ""; 

                if(count($user_request)) { 
                      
                      $remove_id = implode(",", $user_request->toArray());
                      $remove_user .= $remove_id.',';
                }
                
                $user_reject = UserRequest::where('receiver_id', $userId)->where('reject_status','1')->pluck('receiver_id');
                if (count($user_reject)) { 
                    $remove_id = implode(",", $user_reject->toArray());
                    $remove_user .= $remove_id.',';
                }

                $user_rej = UserRequest::where('receiver_id', $userId)->where('connection','ON')->where('reject_status','0')->pluck('sender_id');
                if (count($user_rej)) {
                   
                    $remove_id = implode(",", $user_rej->toArray());
                    $remove_user .= $remove_id.','; 
                }

                $user_ignore = IgnoreUser::where('user_id',$userId)->pluck('ignore_id');
                if (count($user_ignore)) {
                    $remove_id = implode(",", $user_ignore->toArray());
                    $remove_user .= $remove_id.',';
                }   

               if(!empty($remove_user)) {   
                 
                  $remove_user  = rtrim($remove_user, ',');  
                  $condition .= " AND users.id NOT IN ($remove_user)";
                }   

                /* $igg = UserInterest::where('user_id',$userId)->pluck('interest_id')->toArray();
                
                   if(!empty($igg)){
                   
                    $interest_ids = UserInterest::whereIn('interest_id',$igg)->pluck('user_id')->toArray();
                   
                    $int_ids = implode(',',$interest_ids);   
                    $condition .= " OR users.id IN ($int_ids)"; 
                    
                   }  
               */

                  $condition .=  " HAVING distance <= 10000";
                  $fields     = " * ";   
                

                 
                 
                $d = "(((acos(sin(('$lat'*pi()/180)) * sin((users.lat*pi()/180))+cos(('$lat'*pi()/180)) * cos((users.lat*pi()/180)) * cos((('$log'- users.log)*pi()/180))))*180/pi())*60*1.1515) AS distance"; 
                /*$query = "SELECT *, $d FROM users WHERE $condition ORDER BY users.id DESC LIMIT $limit OFFSET $offset";
                $fquery = "SELECT *, $d FROM users WHERE $condition ORDER BY users.id DESC";*/

                $query = "SELECT *, $d FROM users WHERE $condition ORDER BY distance ASC LIMIT $limit OFFSET $offset";
                $fquery = "SELECT *, $d FROM users WHERE $condition ORDER BY distance ASC"; 
                 
                //echo $query ;die;  
                $users  = DB::select($query);
                $fusers = DB::select($fquery); 
                $tot    = count($fusers);  
               
                //print_r($fusers);die;   
                //echo $tot ;die;
                $ncount     = UserNotification::where('receiver_id', $userId)->where('status','Unread')->count();
                //$user_count = ($this->total_user($userId,$lat,$log))-$offset; 
                //echo $user_count;die;
                //echo $ncount;die;
           
             
             if($tot >= $lmt) {     
                  
                  $flmt  = $lmt;
              
                }  else if($tot < $lmt) {
                  
                   $flmt   = $tot;  
                  
                }  

                //echo $offset; die   
                $user_count = $flmt - $offset;
                $tc=$user_count;
                //echo $flmt;die;
                foreach ($users as $user) { 

                    $user_id           = $user->id;
                    $user->user_number = (string)($user_count);

                    $user_count = $user->user_number - 1;
                    $user->user_id     = (string) $user->id;
                    $user->image       = $this->obj->is_file("profile", $user->image);
                    $user->cover_image = $this->obj->is_file("profile", $user->cover_image);
                    unset($user->password, $user->original_password);
                    if (!empty($user->industry_id)) {
                        $industry             = Industry::where('id', $user->industry_id)->first();
                        $user->industry       = !empty($industry->name) ? $industry->name : '';
                        $user->industry_match = "false";
                        if (Auth::user()->industry_id == $user->industry_id) {
                            $user->industry_match = "true";
                         }

                    } else { 
                         
                          $user->industry = "";
                    }  

                    $user_education           = UserEducation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->first();
                    $user->degree             = !empty($user_education->degree) ? $user_education->degree : "";
                    $user->school             = !empty($user_education->school) ? $user_education->school : "";
                    $user_organisations       = UserOrganisation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->first();
                    
                    $user->organisation_name  = !empty($user_organisations->name) ? $user_organisations->name : "";
                    $user->organisation_title = !empty($user_organisations->title) ? $user_organisations->title : "";
                    $query                    = "SELECT favourites.name, favourites.image, user_favourites.favourite_id FROM user_favourites LEFT JOIN favourites ON user_favourites.favourite_id = favourites.id WHERE user_favourites.user_id = $user_id";
                    $favourites               = DB::select($query);
                    $user->favourites         = array();
                    if (count($favourites)) {
                        $getFav = UserFavourite::where('user_id', $userId)->pluck('favourite_id');
                        foreach ($favourites as $key => $value) {
                            $value->image = url("/public/apps/$value->image");
                            $value->match = "false";
                            if (count($getFav)) {
                                if (in_array($value->favourite_id, $getFav->toArray())) {
                                    $value->match = "true";
                                }
                            }
                        } 

                        $user->favourites = $favourites;
                    } 

                    $query             = "SELECT networkings.name, networkings.image, user_networkings.networking_id FROM user_networkings LEFT JOIN networkings ON user_networkings.networking_id = networkings.id WHERE user_networkings.user_id = $user_id order by id asc";
                    $networkings       = DB::select($query);
                    $user->networkings = array();
                    if (count($networkings)) {
                        $getNet = UserNetworking::where('user_id', $userId)->pluck('networking_id');
                        foreach ($networkings as $key => $value) {
                            $value->image = url("/public/apps/$value->image");
                            $value->match = "false";
                            if (count($getNet)) {
                                if (in_array($value->networking_id, $getNet->toArray())) {
                                    $value->match = "true";
                                }
                            }
                        } 
                        $user->networkings = $networkings;
                    }
                    $user->education = UserEducation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->take(2)->get();
                    $query           = "SELECT interests.name, user_interests.interest_id FROM user_interests LEFT JOIN interests ON user_interests.interest_id = interests.id WHERE user_interests.user_id = $user_id";
                    $interests       = DB::select($query);
                    $user->interests = array();
                    if (count($interests)) {
                        $getInterest = UserInterest::where("user_id", $userId)->pluck('interest_id');
                        foreach ($interests as $value) {
                            $value->match = "false";
                            if (count($getInterest)) {
                                if (in_array($value->interest_id, $getInterest->toArray())) {
                                    $value->match = "true";
                                }
                            } 
                        }

                          $user->interests = $interests;
                    }
                }  
               
              $this->obj->set_datadeep("200", "User profile.", $request->path(), 'response', $users,$ncount,$tc); 
            }

        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
  }     


 public function all_user_profilet(Request $request)
 
 {

     try {     
            $rules = array(
                 
                 'offset'        => 'required',
                 'current_lat'   => 'required',
                 'current_long'  => 'required',
            
            );
             
            if ($this->obj->validate_request($request, $rules)) {
                $userId       = Auth::user()->id;  
                $cdd          = Card::where('id',1)->first();    
                $userlmt      = $cdd->count;

                //echo date("Y-m-d H:i:s");die;
                $curtime = strtotime(date("Y-m-d H:i:s"));
                
                //echo $userId;die;
                $iguser = IgnoreUser::where('user_id',$userId)->orderBy('id','DESC')->first();
                
                if($iguser) {  
                     
                 //echo $iguser->created_at;die;
                 $itime =  $iguser->created_at;    

           
                  $tme   = date('Y-m-d H:i:s',strtotime('+12 hour',strtotime($itime))); 
                  $tmme  = strtotime($tme); 

                 if($curtime >= $tmme) { 
 
                  
                  User::where('id', $userId)->update(['count' => 0]); 
                  IgnoreUser::where('user_id', $userId)->delete();
                  $timer_status = "No";
                     
              } else { 
                 

                $finduser            = User::where('id', $userId)->first(); 
                if($finduser->count >= $userlmt){
                  
                   $timer_status = "Yes";

                }

              }   
         }  
           
        
        $couser = UserRequest::where('sender_id',$userId)->where('connection','OFF')->where('reject_status','0')->orderBy('id','DESC')->first();
                
                if($couser) {
                    
                     $ctime =  $couser->created_at;                 
                }
   
              if(!empty($iguser) && !empty($couser)) {
                   
                    if($itime > $ctime){ 

                        $ftime = $itime; 

                    } else if($ctime > $itime) {
                         
                         $ftime= $ctime;
                    }

                } else if(empty($iguser) && !empty($couser)) { 

                     $ftime = $ctime; 

                } else if(!empty($iguser) && empty($couser)){
                      
                     $ftime = $itime; 
                }      
                 
              if(!empty($ftime)) {   

                  $fultime   = date('Y-m-d H:i:s',strtotime('+12 hour',strtotime($ftime))); 
                  $cfultime  = strtotime($fultime); 
                 

                 if($curtime < $cfultime) {      
                  
                     $lmt =  $userlmt - Auth::user()->count;
                      
                  } else {     
                    
                    $lmt = $userlmt;    

                  }      

               } else {   
 
                    $lmt = $userlmt; 
               }

                //echo $lmt;die;
                $location     =   trim(Auth::user()->location);
                $limit        =   !empty($this->ads_count())?$this->ads_count():"1";
                //echo $limit;die;   
                //$offset       =  ($limit*$request->offset)+$request->offset;
                $offset       =  ($limit*$request->offset);
                //$lat = Auth::user()->lat;
                //$log = Auth::user()->log;
                $lat   = $request->current_lat;
                $log  = $request->current_long; 
                $radius  = LocationRadius::find(1);
                if ($radius->radius > 0) {
                    
                   $distance = $radius->radius; 
                
                }  else  {
                     
                    $distance = 1000000000000;
                }

                $condition    = " is_admin != '1' AND id != $userId";
                //AND  location LIKE '%$location%' 
                //$user_request = UserRequest::where('sender_id', $userId)->where('connection','ON')->where('reject_status','0')->pluck('receiver_id');
                
                $user_request = UserRequest::where('sender_id', $userId)->where('connection','ON')->where('reject_status','0')->pluck('receiver_id');
                $remove_user = ""; 
                $remove_userd="";
                
                if (count($user_request)) {  
                      
                      $remove_id = implode(",", $user_request->toArray());
                      $remove_user .= $remove_id.',';
                } 

                //echo $remove_user;die;
                 
                $user_reject = UserRequest::where('receiver_id', $userId)->where('connection','ON')->where('reject_status','0')->pluck('sender_id');   

                if (count($user_reject)) { 

                    $remove_id = implode(",", $user_reject->toArray());
                    $remove_user .= $remove_id.',';
                }  

                $user_wait = UserRequest::where('sender_id', $userId)->where('connection','OFF')->where('reject_status','0')->pluck('receiver_id');
             
                if (count($user_wait)) { 
                      
                      $remove_id    = implode(",", $user_wait->toArray());  
                      $remove_user .= $remove_id.',';
                } 

                $user_rwait = UserRequest::where('receiver_id', $userId)->where('connection','OFF')->where('reject_status','0')->pluck('sender_id');
                
                if (count($user_rwait)) { 
                      
                      $remove_id = implode(",", $user_rwait->toArray());
                      $remove_user .= $remove_id.',';
                }  
                 
               $user_rej = UserRequest::where('sender_id', $userId)->where('connection','OFF')->where('reject_status','1')->pluck('receiver_id'); 
                if (count($user_rej)) {
                     $remove_id = implode(",", $user_rej->toArray());
                     $remove_user .= $remove_id.',';
                    //$condition .= " AND users.id NOT IN ($remove_id)";   
                } 
                 
                $user_rrej = UserRequest::where('receiver_id', $userId)->where('connection','OFF')->where('reject_status','1')->pluck('sender_id'); 
                if (count($user_rrej)) {

                    $remove_id = implode(",", $user_rrej->toArray());
                    $remove_user .= $remove_id.',';
                    //$condition .= " AND users.id NOT IN ($remove_id)";   
                }    


                $user_ignore = IgnoreUser::where('user_id',$userId)->pluck('ignore_id');
                if (count($user_ignore)) {
                    $remove_id    = implode(",", $user_ignore->toArray());
                    $remove_user .= $remove_id.','; 
                } 


                 $igg = UserInterest::where('user_id',$userId)->pluck('interest_id')->toArray();
                
                   if(!empty($igg)){
                   
                    $interest_ids = UserInterest::whereIn('interest_id',$igg)->pluck('user_id')->toArray();
                   
                    $int_ids = implode(',',$interest_ids);   
                     $remove_userd .= $int_ids;
                     //$//condition .= " AND users.id IN ($int_ids)";
                    
                 }    

               if(!empty($remove_user)) {       
                     
                   $remove_user  = rtrim($remove_user, ','); 
                   $condition   .= " AND users.id NOT IN ($remove_user)";
                   if(!empty($interest_ids))
                   {  

                   $remove_userdf  = rtrim($remove_userd, ','); 
                   $condition   .= " AND users.id NOT IN ($remove_userdf)";

                   }
            
            }    
                 
                 //echo $remove_user;die;
                //if(empty($ftime)){
               //}    
                  $condition .=  " HAVING distance <= $distance";
                  $fields = " * ";     

                  //echo $remove_user;die;
                

                $d = "(((acos(sin(('$lat'*pi()/180)) * sin((users.lat*pi()/180))+cos(('$lat'*pi()/180)) * cos((users.lat*pi()/180)) * cos((('$log'- users.log)*pi()/180))))*180/pi())*60*1.1515) AS distance";
                $query = "SELECT *, $d FROM users WHERE $condition ORDER BY distance ASC LIMIT $limit OFFSET $offset"; 

                $fquery = "SELECT *, $d FROM users WHERE $condition ORDER BY distance ASC"; 
                 
                 //echo $fquery;die; 
                 /*$igg = UserInterest::where('user_id',$userId)->pluck('interest_id')->toArray();
                 dd($igg);*/
                 
                  //echo $fquery ;die; 
                  $conditiond ='';
                  
                     
                     if(!empty($interest_ids)){
                     $conditiond    = " is_admin != '1' AND id != $userId"; 
                     $conditiond .= " AND users.id IN ($int_ids) ";

                     if(!empty($remove_user)) {       
                         $remove_user  = rtrim($remove_user, ',');  
                         //echo $remove_user;die;
                         $conditiond   .= " AND users.id NOT IN ($remove_user)";
                      }  

                     $conditiond .=  " HAVING distance <= 10000"; 
                      $da = "(((acos(sin(('$lat'*pi()/180)) * sin((users.lat*pi()/180))+cos(('$lat'*pi()/180)) * cos((users.lat*pi()/180)) * cos((('$log'- users.log)*pi()/180))))*180/pi())*60*1.1515) AS distance";  
                      $dquery = "SELECT *, $d FROM users WHERE $conditiond ORDER BY distance ASC LIMIT $limit OFFSET $offset";  

                       $dfquery = "SELECT *, $d FROM users WHERE $conditiond ORDER BY distance ASC";  

                       //echo $dfquery;die;
                       
                       $iousers = DB::select($dquery);
                       $iusers = DB::select($dfquery);
                  }    
                   
                  $wousers  = DB::select($query);
                  $wusers  = DB::select($fquery); 
               
                  //echo $int_ids;die; 
                 
                 
                
                 /* print_r(count($iusers));
                  print_r($iusers);
                  
                  $wusers  = DB::select($fquery);
                  print_r(count($wusers)); 
                  print_r($wusers);*/
                 
             
              //echo $dquery;die;

              /* $fusers = DB::select($fquery);
               $tot    = count($fusers);*/
                //print_r($fusers);die;
                //echo $tot ;die; 

                if(!empty($iusers)){ 

                 //echo "1122"; die;    
              
               //$users  = DB::select($dquery); 
              $user = array_merge($iusers,$wusers); 
                /*print_r(count($totusers));
                print_r($totusers);die;*/


                }  else {  

               //$users  = DB::select($query); 
               $user   = $wusers;

            
                }
                
              

                if(!empty($iousers)) { 

                    if(count($iousers) < $limit){ 
                        
                        $rank=count($iousers); 
                        $len = $limit - $rank;  
                        $slice_array=array_slice($wousers,0,$len);
                        $users=array_merge($iousers,$slice_array); 
                      //$users =  array_merge($iousers) 

                    } else { 
                       
                        $users =  $iousers; 
                    }

                } else { 
                    
                    $users =  $wousers;

                }   
              
             //print_r($users);die;
                //usort($users, function($a, $b) { return $a['distance'] - $b['distance']; });

                 
                //$fusers = DB::select($fquery); 
                //print_r($wousers);die;

                 $tot    = count($user);
                 //$tot    = count($fusers);
                 $ncount     = UserNotification::where('receiver_id', $userId)->where('status','Unread')->count();
                //$user_count = ($this->total_user($userId,$lat,$log))-$offset; 
                //echo $ncount;die;
                 //echo $lmt.'Done';
  
                if($tot >= $lmt) {                 
                  
                  $flmt  = $lmt;
              
                }  else if($tot < $lmt) { 
                  
                    $flmt   = $tot;  
                
                }   
                 
                 //echo "Done";die; 
                //echo $offset; die   
                $user_count = $flmt - $offset;
                //echo $user_count;die;
                $tc=$user_count;
                //echo $flmt;die;

               /*  if(empty($users) && $timer_status='No'){ 
                  
                   if (count($user_ignore)) {
                    $add_id    = implode(",", $user_ignore->toArray());
                    $cdn  = " is_admin != '1' AND id != $userId AND user_id IN($add_id)";
                    //$remove_user .= $remove_id.',';  
                  }

                  $qryy = "SELECT * FROM users WHERE $cdn  LIMIT $limit OFFSET $offset"; 
                  $users  = DB::select($qryy);
                 
                 }  
                 
                 $users=!empty($user)?$users:array();*/
                 
                foreach ($users as $user) {      

                    $user_id           = $user->id; 
                    $user->user_number = (string)($user_count);

                    $user_count = $user->user_number - 1;
                    $user->user_id     = (string) $user->id; 
                    $user->image       = $this->obj->is_file("profile", $user->image);
                    $user->cover_image = $this->obj->is_file("profile", $user->cover_image);
                    unset($user->password, $user->original_password);
                    if (!empty($user->industry_id)) {
                        $industry             = Industry::where('id', $user->industry_id)->first();
                        $user->industry       = !empty($industry->name) ? $industry->name : '';
                        $user->industry_match = "false";
                        if (Auth::user()->industry_id == $user->industry_id) {
                            $user->industry_match = "true";
                         } 

                    } else {    
                         
                          $user->industry = "";
                    }  

                    $user_education           = UserEducation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->first();
                    $user->degree             = !empty($user_education->degree) ? $user_education->degree : "";
                    $user->school             = !empty($user_education->school) ? $user_education->school : "";

                    $user_organisations       = UserOrganisation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->first();
                    
                    $user->organisation_name  = !empty($user_organisations->name) ? $user_organisations->name : "";
                    $user->organisation_title = !empty($user_organisations->title) ? $user_organisations->title : "";


                   
                    $query                    = "SELECT favourites.name, favourites.image, user_favourites.favourite_id FROM user_favourites LEFT JOIN favourites ON user_favourites.favourite_id = favourites.id WHERE user_favourites.user_id = $user_id";
                    $favourites               = DB::select($query);
                    $user->favourites         = array();
                    if (count($favourites)) {
                        $getFav = UserFavourite::where('user_id', $userId)->pluck('favourite_id');
                        foreach ($favourites as $key => $value) {
                            $value->image = url("/public/apps/$value->image");
                            $value->match = "false";
                            if (count($getFav)) {
                                if (in_array($value->favourite_id, $getFav->toArray())) {
                                    $value->match = "true";
                                }
                            }
                        }
                        $user->favourites = $favourites;
                    } 

                    $query             = "SELECT networkings.name, networkings.image, user_networkings.networking_id FROM user_networkings LEFT JOIN networkings ON user_networkings.networking_id = networkings.id WHERE user_networkings.user_id = $user_id";
                    $networkings       = DB::select($query);
                    $user->networkings = array();
                    if (count($networkings)) {
                        $getNet = UserNetworking::where('user_id', $userId)->pluck('networking_id');
                        foreach ($networkings as $key => $value) {
                            $value->image = url("/public/apps/$value->image");
                            $value->match = "false";
                            if (count($getNet)) {
                                if (in_array($value->networking_id, $getNet->toArray())) {
                                    $value->match = "true";
                                }
                            }
                        }
                        $user->networkings = $networkings;
                    }

                   $user->education = UserEducation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->take(2)->get();
                   $user->organisations  = UserOrganisation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->take(2)->get();  
                   $query           = "SELECT interests.name, user_interests.interest_id FROM user_interests LEFT JOIN interests ON user_interests.interest_id = interests.id WHERE user_interests.user_id = $user_id";
                  
                    $interests       = DB::select($query);
                    $user->interests = array();
                    if (count($interests)) {
                        $getInterest = UserInterest::where("user_id", $userId)->pluck('interest_id');
                        foreach ($interests as $value) {
                            $value->match = "false";
                            if (count($getInterest)) {
                                if (in_array($value->interest_id, $getInterest->toArray())) {
                                    $value->match = "true";
                                }
                            } 
                        } 
                           
                           $user->interests = $interests;
                    } 
               }  


                
                
               
                $getUser = User::where('id',$userId)->first();
                if($getUser->count >= $userlmt) {
              
                    $users = array();     

                 }    
                 
                 if(!empty($users)){
                    $users = array_filter($users, function($v) { return $v->user_number >= 1; });
                 }
                  
               
                 //print_r($users);die;
                //$final = array_filter($users, function($v) { return $v->user_number >= 20; });  
                 /*$new = array_filter($arr, function ($var) {
    return ($var['name'] == 'CarEnquiry'); 

});*/         
                $tsatus = isset($timer_status)?$timer_status:'No'; 
               $this->obj->set_datadeep("200", "User profile.", $request->path(), 'response', $users,$ncount,$tc,$tsatus); 

              /*} else { 

             $this->obj->set_datadeep("200", "User profile.", $request->path(), 'response', array());

             }  */ 
          }  
        
       } catch (Exception $e) { 

            $this->obj->error_message('500', "Internal error.", $request->path());
      
       } 
 }  

 public function all_user_profile(Request $request)
 
 {

     try {     
            $rules = array(
                 
                 'offset'        => 'required',
                 'current_lat'   => 'required',
                 'current_long'  => 'required',
            
            );
             
            if ($this->obj->validate_request($request, $rules)) {
                $userId       = Auth::user()->id;  
                $cdd          = Card::where('id',1)->first();
                //$adv          = Advertisement::get();    
                $userlmt      = ($cdd->count!=0)?$cdd->count:'20';

                //echo date("Y-m-d H:i:s");die;
                $curtime = strtotime(date("Y-m-d H:i:s"));
                
                //echo $userId;die;
                $iguser = IgnoreUser::where('user_id',$userId)->orderBy('id','DESC')->first();
             //   print_r($iguser);die;
                if($iguser) {  
                    
                        //echo "1";
                 //echo $iguser->created_at;die; 
                 $itime =  $iguser->created_at;     

                
                   $tme   = date('Y-m-d H:i:s',strtotime('+12 hour',strtotime($itime))); 
                   $tmme  = strtotime($tme);

                 if($curtime >= $tmme) { 
                    
                //echo "3";
                   User::where('id', $userId)->update(['count' => 0]); 
                   User::where('id', $userId)->update(['time_status' =>'no']); 
                  //IgnoreUser::where('user_id', $userId)->delete();
                  $timer_status = "No";
                  

                  $count_ognore = IgnoreUser::where('user_id', $userId)->count();
                  $user_request_count_sender = UserRequest::where('sender_id', $userId)->count();
                    $user_request_count_reciever = UserRequest::where('receiver_id', $userId)->count();
                    $total_count = $count_ognore + $user_request_count_sender + $user_request_count_reciever;
                  $total_user22            = User::count();
                   $users_count  = $total_user22 - 2;
                   $total_count;
                   if($users_count==$total_count)
                     {
                        IgnoreUser::where('user_id', $userId)->delete();
                     }
            

                  
                     
              } else { 
                    //echo "4";
                //User::where('id', $userId)->update(['time_status' =>'yes']); 
                $finduser            = User::where('id', $userId)->first();
                 $total_user22            = User::count();
                    $users_count  = $total_user22 - 2;
                   $count_ognore = IgnoreUser::where('user_id', $userId)->count();
                     $user_request_count_sender = UserRequest::where('sender_id', $userId)->count();
                    $user_request_count_reciever = UserRequest::where('receiver_id', $userId)->count();
                    $total_count = $count_ognore + $user_request_count_sender + $user_request_count_reciever;


                 if($users_count==$total_count)
                 {
                  //  IgnoreUser::where('user_id', $userId)->delete();
                  // echo "5";
                    $timer_status = "Yes";
                 }
                 elseif($finduser->count >= $userlmt){
                     //echo "6";
                        $timer_status = "Yes";
                    }
                    else
                    {

                    }


              }   
         }  
           
        
                $couser = UserRequest::where('sender_id',$userId)->where('connection','OFF')->where('reject_status','0')->orderBy('id','DESC')->first();
                
                if($couser) {
                    //echo "2";
                    
                     $ctime =  $couser->created_at;                 
                }
   
              if(!empty($iguser) && !empty($couser)) {
                   
                    if($itime > $ctime){ 

                        $ftime = $itime; 

                    } else if($ctime > $itime) {
                         
                         $ftime= $ctime;
                    }

                } else if(empty($iguser) && !empty($couser)) { 

                     $ftime = $ctime; 

                } else if(!empty($iguser) && empty($couser)){
                      
                     $ftime = $itime; 
                }      
                 
              if(!empty($ftime)) {   

                  $fultime   = date('Y-m-d H:i:s',strtotime('+12 hour',strtotime($ftime))); 
                  $cfultime  = strtotime($fultime); 
                 

                 if($curtime < $cfultime) {   
                    // echo "7";                 
                  
                     $lmt =  $userlmt - Auth::user()->count;

                     $finduser            = User::where('id', $userId)->first();
                     $total_user22            = User::count();
                     $users_count  = $total_user22 - 2;
                     $count_ognore = IgnoreUser::where('user_id', $userId)->count();
                     $user_request_count_sender = UserRequest::where('sender_id', $userId)->count();
                    $user_request_count_reciever = UserRequest::where('receiver_id', $userId)->count();
                    $total_count = $count_ognore + $user_request_count_sender + $user_request_count_reciever;


                 if($users_count==$total_count)
                 {
                   
                    $timer_status = "Yes";
                 }
                 elseif($finduser->count >= $userlmt){
                        $timer_status = "Yes";
                    }
                    else
                    {
                         $timer_status = "Yes";
                    }


                     //$timer_status = "Yes";

                      
                  } else {     
                    
                    User::where('id', $userId)->update(['count' => 0]); 
                    User::where('id', $userId)->update(['time_status' =>'no']); 
                    $lmt = $userlmt; 
                    //$timer_status = "No"; 

                  }      

               } else {   
 
                    $lmt = $userlmt; 
               }

                //echo $lmt;die;
                $location     =   trim(Auth::user()->location);

               // echo $this->ads_count();die;
                $limit        =   !empty($this->ads_count())?$this->ads_count():'20';
                //echo $limit;die;   
                //$offset       =  ($limit*$request->offset)+$request->offset;
                $offset       =  ($limit*$request->offset);
                //$lat = Auth::user()->lat;
                //$log = Auth::user()->log;
                $lat   = $request->current_lat;
                $log  = $request->current_long; 
                $radius  = LocationRadius::find(1);
                if ($radius->radius > 0) {
                    
                   $distance = $radius->radius; 
                
                }  else  {
                     
                    $distance = 1000000000000;
                }

                $condition    = " is_admin != '1' AND id != $userId";
                //AND  location LIKE '%$location%' 
                //$user_request = UserRequest::where('sender_id', $userId)->where('connection','ON')->where('reject_status','0')->pluck('receiver_id');
                
                $user_request = UserRequest::where('sender_id', $userId)->where('connection','ON')->where('reject_status','0')->pluck('receiver_id');
                $remove_user = ""; 
                $remove_userd="";
                
                if (count($user_request)) {  
                      
                      $remove_id = implode(",", $user_request->toArray());
                      $remove_user .= $remove_id.',';
                } 

                //echo $remove_user;die;
                 
                $user_reject = UserRequest::where('receiver_id', $userId)->where('connection','ON')->where('reject_status','0')->pluck('sender_id');   

                if (count($user_reject)) { 

                    $remove_id = implode(",", $user_reject->toArray());
                    $remove_user .= $remove_id.',';
                }  

                $user_wait = UserRequest::where('sender_id', $userId)->where('connection','OFF')->where('reject_status','0')->pluck('receiver_id');
             
                if (count($user_wait)) { 
                      
                      $remove_id    = implode(",", $user_wait->toArray());  
                      $remove_user .= $remove_id.',';
                } 

                $user_rwait = UserRequest::where('receiver_id', $userId)->where('connection','OFF')->where('reject_status','0')->pluck('sender_id');
                
                if (count($user_rwait)) { 
                      
                      $remove_id = implode(",", $user_rwait->toArray());
                      $remove_user .= $remove_id.',';
                }  
                 
               $user_rej = UserRequest::where('sender_id', $userId)->where('connection','OFF')->where('reject_status','1')->pluck('receiver_id'); 
                if (count($user_rej)) {
                     $remove_id = implode(",", $user_rej->toArray());
                     $remove_user .= $remove_id.',';
                    //$condition .= " AND users.id NOT IN ($remove_id)";   
                } 
                 
                $user_rrej = UserRequest::where('receiver_id', $userId)->where('connection','OFF')->where('reject_status','1')->pluck('sender_id'); 
                if (count($user_rrej)) {

                    $remove_id = implode(",", $user_rrej->toArray());
                    $remove_user .= $remove_id.',';
                    //$condition .= " AND users.id NOT IN ($remove_id)";   
                }    


                $user_ignore = IgnoreUser::where('user_id',$userId)->pluck('ignore_id');
                if (count($user_ignore)) {
                    $remove_id    = implode(",", $user_ignore->toArray());
                    $remove_user .= $remove_id.','; 
                } 


                 $igg = UserInterest::where('user_id',$userId)->pluck('interest_id')->toArray();
                
                   if(!empty($igg)){
                   
                    $interest_ids = UserInterest::whereIn('interest_id',$igg)->pluck('user_id')->toArray();
                   
                    $int_ids = implode(',',$interest_ids);   
                     $remove_userd .= $int_ids;
                     //$//condition .= " AND users.id IN ($int_ids)";
                    
                 }    

               if(!empty($remove_user)) {       
                     
                   $remove_user  = rtrim($remove_user, ','); 
                   $condition   .= " AND users.id NOT IN ($remove_user)";
                   if(!empty($interest_ids))
                   {  

                   $remove_userdf  = rtrim($remove_userd, ','); 
                   $condition   .= " AND users.id NOT IN ($remove_userdf)";

                   }
            
            }    
                 
                 //echo $remove_user;die;
                //if(empty($ftime)){
               //}    
                  $condition .=  " HAVING distance <= $distance";
                  $fields = " * ";     

                  //echo $remove_user;die;
                

                $d = "(((acos(sin(('$lat'*pi()/180)) * sin((users.lat*pi()/180))+cos(('$lat'*pi()/180)) * cos((users.lat*pi()/180)) * cos((('$log'- users.log)*pi()/180))))*180/pi())*60*1.1515) AS distance";
                $query = "SELECT *, $d FROM users WHERE $condition ORDER BY distance ASC LIMIT $limit OFFSET $offset"; 

                $fquery = "SELECT *, $d FROM users WHERE $condition ORDER BY distance ASC"; 
                 
                // print_r($fquery);die;
                 //echo $fquery;die; 
                 /*$igg = UserInterest::where('user_id',$userId)->pluck('interest_id')->toArray();
                 dd($igg);*/
                 
                  //echo $fquery ;die; 
                  $conditiond ='';
                  
                     
                     if(!empty($interest_ids)){
                     $conditiond    = " is_admin != '1' AND id != $userId"; 
                     $conditiond .= " AND users.id IN ($int_ids) ";

                     if(!empty($remove_user)) {       
                         $remove_user  = rtrim($remove_user, ',');  
                         //echo $remove_user;die;
                         $conditiond   .= " AND users.id NOT IN ($remove_user)";
                      }  

                     $conditiond .=  " HAVING distance <= 10000"; 
                      $da = "(((acos(sin(('$lat'*pi()/180)) * sin((users.lat*pi()/180))+cos(('$lat'*pi()/180)) * cos((users.lat*pi()/180)) * cos((('$log'- users.log)*pi()/180))))*180/pi())*60*1.1515) AS distance";  
                      $dquery = "SELECT *, $d FROM users WHERE $conditiond ORDER BY distance ASC LIMIT $limit OFFSET $offset";  


                       $dfquery = "SELECT *, $d FROM users WHERE $conditiond ORDER BY distance ASC";  

                       //echo $dfquery;die;
                       
                       $iousers = DB::select($dquery);
                       $iusers = DB::select($dfquery);
                  }    
                   
                  $wousers  = DB::select($query);
                  $wusers  = DB::select($fquery); 
               
                  //echo $int_ids;die; 
                 
                 
                
                 /* print_r(count($iusers));
                  print_r($iusers);
                  
                  $wusers  = DB::select($fquery);
                  print_r(count($wusers)); 
                  print_r($wusers);*/
                 
             
              //echo $dquery;die;

              /* $fusers = DB::select($fquery);
               $tot    = count($fusers);*/
                //print_r($fusers);die;
                //echo $tot ;die; 

                if(!empty($iusers)){ 

               //  echo "1122"; die;    
              
               //$users  = DB::select($dquery); 
              $user = array_merge($iusers,$wusers); 
                /*print_r(count($totusers));
                print_r($totusers);die;*/


                }  else {  


               //$users  = DB::select($query); 
               $user   = $wusers;

            
                }
                

              

                if(!empty($iousers)) { 

                    if(count($iousers) < $limit){ 
                        
                        $rank=count($iousers); 
                        $len = $limit - $rank;  
                        $slice_array=array_slice($wousers,0,$len);
                        $users=array_merge($iousers,$slice_array); 
                      //$users =  array_merge($iousers) 

                    } else { 
                       
                        $users =  $iousers; 
                    }

                } else { 
                    
                    $users =  $wousers;

                }   
              
                
                //usort($users, function($a, $b) { return $a['distance'] - $b['distance']; });

                 
                //$fusers = DB::select($fquery); 
                //print_r($wousers);die;

                 $tot    = count($user);
                 //$tot    = count($fusers);
                 $ncount     = UserNotification::where('receiver_id', $userId)->where('status','Unread')->count();
                //$user_count = ($this->total_user($userId,$lat,$log))-$offset; 
                //echo $ncount;die;
                 //echo $lmt.'Done';
  
                if($tot >= $lmt) {                 
                  
                  $flmt  = $lmt;
              
                }  else if($tot < $lmt) { 
                  
                    $flmt   = $tot;  
                
                }   
                 
                 //echo "Done";die; 
                //echo $offset; die   
                $user_count = $flmt - $offset;
                //echo $user_count;die;
                $tc=$user_count;
                //echo $flmt;die;

               /*  if(empty($users) && $timer_status='No'){ 
                  
                   if (count($user_ignore)) {
                    $add_id    = implode(",", $user_ignore->toArray());
                    $cdn  = " is_admin != '1' AND id != $userId AND user_id IN($add_id)";
                    //$remove_user .= $remove_id.',';  
                  }

                  $qryy = "SELECT * FROM users WHERE $cdn  LIMIT $limit OFFSET $offset"; 
                  $users  = DB::select($qryy);
                 
                 }  
                 
                 $users=!empty($user)?$users:array();*/
                
                 $count = sizeof($users);  
                foreach ($users as $user) {      
        
                    //$user->user_number = (string)$count;  
                    $user_id           = $user->id; 
                    $user->user_number = (string)($user_count);

                   $user_count = $user->user_number - 1;
                    $user->user_id     = (string) $user->id; 
                    $user->image       = $this->obj->is_file("profile", $user->image);
                    $user->cover_image = $this->obj->is_file("profile", $user->cover_image);
                    unset($user->password, $user->original_password);
                    if (!empty($user->industry_id)) {
                        $industry             = Industry::where('id', $user->industry_id)->first();
                        $user->industry       = !empty($industry->name) ? $industry->name : '';
                        $user->industry_match = "false";
                        if (Auth::user()->industry_id == $user->industry_id) {
                            $user->industry_match = "true";
                         } 

                    } else {    
                         
                          $user->industry = "";
                    }  

                    $user_education           = UserEducation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->first();
                    $user->degree             = !empty($user_education->degree) ? $user_education->degree : "";
                    $user->school             = !empty($user_education->school) ? $user_education->school : "";

                    $user_organisations       = UserOrganisation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->first();
                    
                    $user->organisation_name  = !empty($user_organisations->name) ? $user_organisations->name : "";
                    $user->organisation_title = !empty($user_organisations->title) ? $user_organisations->title : "";


                   
                    $query                    = "SELECT favourites.name, favourites.image, user_favourites.favourite_id FROM user_favourites LEFT JOIN favourites ON user_favourites.favourite_id = favourites.id WHERE user_favourites.user_id = $user_id";
                    $favourites               = DB::select($query);
                    $user->favourites         = array();
                    if (count($favourites)) {
                        $getFav = UserFavourite::where('user_id', $userId)->pluck('favourite_id');
                        foreach ($favourites as $key => $value) {
                            $value->image = url("/public/apps/$value->image");
                            $value->match = "false";
                            if (count($getFav)) {
                                if (in_array($value->favourite_id, $getFav->toArray())) {
                                    $value->match = "true";
                                }
                            }
                        }
                        $user->favourites = $favourites;
                    } 

                    $query             = "SELECT networkings.name, networkings.image, user_networkings.networking_id FROM user_networkings LEFT JOIN networkings ON user_networkings.networking_id = networkings.id WHERE user_networkings.user_id = $user_id";
                    $networkings       = DB::select($query);
                    $user->networkings = array();
                    if (count($networkings)) {
                        $getNet = UserNetworking::where('user_id', $userId)->pluck('networking_id');
                        foreach ($networkings as $key => $value) {
                            $value->image = url("/public/apps/$value->image");
                            $value->match = "false";
                            if (count($getNet)) {
                                if (in_array($value->networking_id, $getNet->toArray())) {
                                    $value->match = "true";
                                }
                            }
                        }
                        $user->networkings = $networkings;
                    }

                   $user->education = UserEducation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->take(2)->get();
                   $user->organisations  = UserOrganisation::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->take(2)->get();  
                   $query           = "SELECT interests.name, user_interests.interest_id FROM user_interests LEFT JOIN interests ON user_interests.interest_id = interests.id WHERE user_interests.user_id = $user_id";
                  
                    $interests       = DB::select($query);
                    $user->interests = array();
                    if (count($interests)) {
                        $getInterest = UserInterest::where("user_id", $userId)->pluck('interest_id');
                        foreach ($interests as $value) {
                            $value->match = "false";
                            if (count($getInterest)) {
                                if (in_array($value->interest_id, $getInterest->toArray())) {
                                    $value->match = "true";
                                }
                            } 
                        } 
                           
                           $user->interests = $interests;
                    } 
                    
                    $count--;
               }  


                
                 
              
                $getUser = User::where('id',$userId)->first();
                
                if($getUser->count >= $userlmt) {
                //   echo $userlmt;
             // echo $getUser->count;die;
                    $users = array(); 


                 } 

                
                 if(!empty($users)){
                    $users = array_filter($users, function($v) { return $v->user_number >= 1; });
                 }
                  
                  
                    $finduser2 = User::where('id', $userId)->first(); 
                    
                        
                   if($finduser2->time_status==='yes') {  
                  
                     $users = array();  
                   }
                 
                
                
               //  print_r($users);die;
                //$final = array_filter($users, function($v) { return $v->user_number >= 20; });  
                 /*$new = array_filter($arr, function ($var) {
    return ($var['name'] == 'CarEnquiry'); 

});*/         
                // if(empty($adv) OR empty($users))
                // {
                //     $tsatus = "Yes";
                // }
                // else
                // {   
                    $tsatus = isset($timer_status)?$timer_status:'No';
                //} 

                
                 // print_r($users);die;
        //echo $cdd->count;die;  

               $this->obj->set_datadeep("200", "User profile.", $request->path(), 'response', $users,$ncount,$tc,$tsatus,$cdd->count); 

              /*} else { 

             $this->obj->set_datadeep("200", "User profile.", $request->path(), 'response', array());

             }  */ 
          }  
        
       } catch (Exception $e) { 

            $this->obj->error_message('500', "Internal error.", $request->path());
      
       } 
 }     

 
public function timer_timeleft(Request $request) {
     

     try { 
           $userId = Auth::user()->id;
           $curtime = strtotime(date("Y-m-d H:i:s"));
           $iguser = IgnoreUser::where('user_id',$userId)->orderBy('id','DESC')->first();
            
           if($iguser) {  
                     
                 $itime =  $iguser->created_at;    
           }   
                
          User::where('id',$userId)->update(array('time_status'=>'yes'));       
            

          $couser = UserRequest::where('sender_id',$userId)->where('connection','OFF')->where('reject_status','0')->orderBy('id','DESC')->first();
                
                if($couser) {
                    
                     $ctime =  $couser->created_at;                 
                }
   
              if(!empty($iguser) && !empty($couser)) {
                   
                    if($itime > $ctime){ 

                        $ftime = $itime; 

                    } else if($ctime > $itime) {
                         
                         $ftime= $ctime;
                    }

                } else if(empty($iguser) && !empty($couser)) { 

                     $ftime = $ctime; 

                } else if(!empty($iguser) && empty($couser)){
                      
                     $ftime = $itime; 
                }      
                 
                //echo $ftime; die;
            

             if(!empty($ftime)) {   
                
                  $fultime   = date('Y-m-d H:i:s',strtotime('+12 hour',strtotime($ftime))); 
                  $cfultime  = strtotime($fultime); 
                   
                   
                 if($curtime < $cfultime){       
                  
                     $time['time_left'] =   $cfultime - $curtime;
                      
                  } else {      
                    
                   $time['time_left'] = 43200;    

                        }      

             } else {     
 
                   $time['time_left'] = 43200;
            } 
        
        $this->obj->set_data("200", "Time Left.", $request->path(),'response',$time);
          //echo $time;die;
        //$this->obj->set_data("200", "Timer left Time.", $request->path(), 'response', $time); 

         
         } catch(Exception $e){
           
          $this->obj->error_message('500', "Internal error.", $request->path());

         }

 }  

 public function ignore_user(Request $request) { 
 
      try { 
             
             $rules = array(
                'user_id' => 'required|max:190',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {
                $user_id = Auth::user()->id;
                $ignore = new IgnoreUser;
                $ignore->user_id = $user_id;
                $ignore->ignore_id = $request->user_id;
                
                if($ignore->save()) {
                     $ucount = User::where('id',$user_id)->first();
                     User::where('id', $user_id)->update(['count' => $ucount->count+1]);
                    $this->obj->set_data("200", "Save successfully.", $request->path());
                } else {
                    $this->obj->error_message('404', "Internal error.", $request->path());
                }
            } 

          } catch (Exception $e) {
          
            $this->obj->error_message('500', "Internal error.", $request->path());
          
          }
   } 

 public function update_location(Request $request){
        try {
            $rules = array(
                'lat' => 'required|max:190',
                'log' => 'required|max:190',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {
                $user_id = Auth::user()->id;
                $user = User::find($user_id);
                $user->lat = $request->lat;
                $user->log = $request->log;
                if ($user->save()) {
                    $this->obj->set_data("200", "Save successfully.", $request->path());
                } else {
                    $this->obj->error_message('404', "Internal error.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
   } 



     public function edit_profile(Request $request)
    {
        try {
            $rules = array(
                'name'           => 'max:190',
                'gender'         => 'max:190',
                'designation'    => 'max:190',
                'organisation'   => 'max:190',
                'headline'       => 'max:300',
                'industry_id'    => 'max:190',
                'linkdin_link'   => 'max:190',
                'website_link'   => 'max:190',
                'twitter_link'   => 'max:190',
                'instagram_link' => 'max:190',
                'location'       => 'max:190',
                'image'          => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
                'cover_image'    => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id = Auth::user()->id;
                $user    = User::find($user_id);


                if (!empty($request->name)) {
                    $user->name = ucwords($request->name);
                }

                if (!empty($request->email)) {
                    $chechkemail = DB::select("SELECT * FROM `users` WHERE `email`='".$request->email."' and `id` != '".$user_id."'");

                    if(empty($chechkemail)){
                         $user->email = $request->email;
                    } else {
                        $this->obj->error_message('500', "email already exist", $request->path());
                    }
                   
                }

                if (!empty($request->gender)) {
                    $user->gender = $request->gender;
                }
                if (!empty($request->designation)) {
                	$user->designation = $request->designation;
                }
                if (!empty($request->organisation)) {
                    $user->organisation = $request->organisation;
                    
                }
                if (!empty($request->headline)) {
                    $user->headline = $request->headline;
                }
                if (!empty($request->industry_id)) {
                    $user->industry_id = $request->industry_id;
                }
                if (!empty($request->location)) {
                    $user->location = $request->location;
                }
                if (!empty($request->linkdin_link)) {
                    $user->linkdin_link = $request->linkdin_link;
                }
                if (!empty($request->website_link)) { 
                    $user->website_link = $request->website_link;
                }
                if (!empty($request->twitter_link)) {
                    $user->twitter_link = $request->twitter_link;
                }
                if (!empty($request->instagram_link)) {
                    $user->instagram_link = $request->instagram_link;
                }
                if (!empty($request->image)) {
                    $user->image = $this->obj->upload_image($request, 'image');
                }
                if (!empty($request->cover_image)) {
                    $user->cover_image = $this->obj->upload_image($request, 'cover_image');
                }

                    // $user->lat = $user->lat;
                    // $user->log = $user->log;


                if($user->save()) {
                    $this->obj->set_data("200", "Saved successfully.", $request->path());
                } else {
                    $this->obj->error_message('500', "Internal error.", $request->path());
                }
            }
       } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
       } 
}

   public function save_notification($sender_id, $receiver_id, $type, $message)
    
    { 
        try {
            $create              = new UserNotification;
            $create->sender_id   = $sender_id;
            $create->receiver_id = $receiver_id;
            $create->type        = $type;
            $create->message     = $message;
            $create->save();
            return $create->id; 
        } catch (Exception $e) {
          
            $this->obj->error_message('500', "Internal error.", $request->path());
       
        }
   }

  public function send_request(Request $request)
    {  
        try { 

            $rules = array(
                'receiver_id' => 'required|max:190',
                'message'     => 'max:190',
            );  

            if ($this->obj->validate_request($request, $rules)) {
                $user_id = Auth::user()->id;
                $user    = User::where('id', $request->receiver_id)->first();  
                if (!empty($user)) {
                    $checkRequest = UserRequest::where('sender_id', $user_id)->where('receiver_id', $request->receiver_id)->where('connection','ON')->first(); 
                     $checkRequestt = UserRequest::where('sender_id', $user_id)->where('receiver_id', $request->receiver_id)->where('connection','OFF')->first();  
                    if (!empty($checkRequest) OR !empty($checkRequestt)) {
                        $this->obj->error_message('500', "You already sent the request.", $request->path());
                   
                 } else {   

                        $create              = new UserRequest;
                        $create->receiver_id = $request->receiver_id;
                        $create->sender_id   = $user_id;
                        $create->message     = Auth::user()->name . " sent you request.";
                        if (!empty($request->message)) {
                            $create->message = $request->message;
                        }

                        if ($create->save()) {    
                        
                         //$ruser    = User::where('id', $request->receiver_id)->first();  
            if(!empty($user->email) && ($user->notification == "ON")) { 

           $mssg='<html>
   <head>
      <title>Push Email</title>
      <link rel="shortcut icon" href="favicon.ico">
      <style type="text/css">
         table[name="blk_permission"], table[name="blk_footer"] {display:none;} 
      </style>
      <meta name="googlebot" content="noindex" />
      <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"/>
      <link rel="stylesheet" href="/style/dhtmlwindow.css" type="text/css" />
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900" type="text/css" />
      <script type="text/javascript" src="/script/dhtmlwindow.js">
         /***********************************************
         * DHTML Window Widget- © Dynamic Drive (www.dynamicdrive.com)
         * This notice must stay intact for legal use.
         * Visit www.dynamicdrive.com for full source code
         ***********************************************/
      </script>
      <link rel="stylesheet" href="/style/modal.css" type="text/css" />
      
      <meta content="width=device-width, initial-scale=1.0" name="viewport">
      <style type="text/css">   
          @import url("https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900")
          body{font-family: "Lato", sans-serif !important;}
         /*** BMEMBF Start ***/    
         [name=bmeMainBody]{/* min-height:1000px; */}    
         @media only screen and (max-width: 480px){table.blk, table.tblText, .bmeHolder, .bmeHolder1, table.bmeMainColumn{width:100% !important;} }        
         @media only screen and (max-width: 480px){.bmeImageCard table.bmeCaptionTable td.tblCell{padding:0px 20px 20px 20px !important;} }        
         @media only screen and (max-width: 480px){.bmeImageCard table.bmeCaptionTable.bmeCaptionTableMobileTop td.tblCell{padding:20px 20px 0 20px !important;} }        
         @media only screen and (max-width: 480px){table.bmeCaptionTable td.tblCell{padding:10px !important;} }        
         @media only screen and (max-width: 480px){table.tblGtr{ padding-bottom:20px !important;} }        
         @media only screen and (max-width: 480px){td.blk_container, .blk_parent, .bmeLeftColumn, .bmeRightColumn, .bmeColumn1, .bmeColumn2, .bmeColumn3, .bmeBody{display:table !important;max-width:600px !important;width:100% !important;} }        
         @media only screen and (max-width: 480px){table.container-table, .bmeheadertext, .container-table { width: 95% !important; } }        
         @media only screen and (max-width: 480px){.mobile-footer, .mobile-footer a{ font-size: 13px !important; line-height: 18px !important; } .mobile-footer{ text-align: center !important; } table.share-tbl { padding-bottom: 15px; width: 100% !important; } table.share-tbl td { display: block !important; text-align: center !important; width: 100% !important; } }        
         @media only screen and (max-width: 480px){td.bmeShareTD, td.bmeSocialTD{width: 100% !important; } }        
         @media only screen and (max-width: 480px){td.tdBoxedTextBorder{width: auto !important;}}    
         @media only screen and (max-width: 480px){table.blk, table[name=tblText], .bmeHolder, .bmeHolder1, table[name=bmeMainColumn]{width:100% !important;} }    
         @media only screen and (max-width: 480px){.bmeImageCard table.bmeCaptionTable td[name=tblCell]{padding:0px 20px 20px 20px !important;} }    
         @media only screen and (max-width: 480px){.bmeImageCard table.bmeCaptionTable.bmeCaptionTableMobileTop td[name=tblCell]{padding:20px 20px 0 20px !important;} }    
         @media only screen and (max-width: 480px){table.bmeCaptionTable td[name=tblCell]{padding:10px !important;} }    
         @media only screen and (max-width: 480px){table[name=tblGtr]{ padding-bottom:20px !important;} }    
         @media only screen and (max-width: 480px){td.blk_container, .blk_parent, [name=bmeLeftColumn], [name=bmeRightColumn], [name=bmeColumn1], [name=bmeColumn2], [name=bmeColumn3], [name=bmeBody]{display:table !important;max-width:600px !important;width:100% !important;} }    
         @media only screen and (max-width: 480px){table[class=container-table], .bmeheadertext, .container-table { width: 95% !important; } }    
         @media only screen and (max-width: 480px){.mobile-footer, .mobile-footer a{ font-size: 13px !important; line-height: 18px !important; } .mobile-footer{ text-align: center !important; } table[class="share-tbl"] { padding-bottom: 15px; width: 100% !important; } table[class="share-tbl"] td { display: block !important; text-align: center !important; width: 100% !important; } }    
         @media only screen and (max-width: 480px){td[name=bmeShareTD], td[name=bmeSocialTD]{width: 100% !important; } }    
         @media only screen and (max-width: 480px){td[name=tdBoxedTextBorder]{width: auto !important;}}    
         @media only screen and (max-width: 480px){.bmeImageCard table.bmeImageTable{height: auto !important; width:100% !important; padding:20px !important;clear:both; float:left !important; border-collapse: separate;} }    
         @media only screen and (max-width: 480px){.bmeMblInline table.bmeImageTable{height: auto !important; width:100% !important; padding:10px !important;clear:both;} }    
         @media only screen and (max-width: 480px){.bmeMblInline table.bmeCaptionTable{width:100% !important; clear:both;} }    
         @media only screen and (max-width: 480px){table.bmeImageTable{height: auto !important; width:100% !important; padding:10px !important;clear:both; } }    
         @media only screen and (max-width: 480px){table.bmeCaptionTable{width:100% !important;  clear:both;} }    
         @media only screen and (max-width: 480px){table.bmeImageContainer{width:100% !important; clear:both; float:left !important;} }    
         @media only screen and (max-width: 480px){table.bmeImageTable td{padding:0px !important; height: auto; } }    
         @media only screen and (max-width: 480px){td.bmeImageContainerRow{padding:0px !important;}}    
         @media only screen and (max-width: 480px){img.mobile-img-large{width:100% !important; height:auto !important;} }    
         @media only screen and (max-width: 480px){img.bmeRSSImage{max-width:320px; height:auto !important;}}    
         @media only screen and (min-width: 640px){img.bmeRSSImage{max-width:600px !important; height:auto !important;} }    
         @media only screen and (max-width: 480px){.trMargin img{height:10px;} }    
         @media only screen and (max-width: 480px){div.bmefooter, div.bmeheader{ display:block !important;} }    
         @media only screen and (max-width: 480px){.tdPart{ width:100% !important; clear:both; float:left !important; } }    
         @media only screen and (max-width: 480px){table.blk_parent1, table.tblPart {width: 100% !important; } }    
         @media only screen and (max-width: 480px){.tblLine{min-width: 100% !important;}}     
         @media only screen and (max-width: 480px){.bmeMblCenter img { margin: 0 auto; } }       
         @media only screen and (max-width: 480px){.bmeMblCenter, .bmeMblCenter div, .bmeMblCenter span  { text-align: center !important; text-align: -webkit-center !important; } }    
         @media only screen and (max-width: 480px){.bmeNoBr br, .bmeImageGutterRow, .bmeMblStackCenter .bmeShareItem .tdMblHide { display: none !important; } }    
         @media only screen and (max-width: 480px){.bmeMblInline table.bmeImageTable, .bmeMblInline table.bmeCaptionTable, td.bmeMblInline { clear: none !important; width:50% !important; } }    
         @media only screen and (max-width: 480px){.bmeMblInlineHide, .bmeShareItem .trMargin { display: none !important; } }    
         @media only screen and (max-width: 480px){.bmeMblInline table.bmeImageTable img, .bmeMblShareCenter.tblContainer.mblSocialContain, .bmeMblFollowCenter.tblContainer.mblSocialContain{width: 100% !important; } }    
         @media only screen and (max-width: 480px){.bmeMblStack> .bmeShareItem{width: 100% !important; clear: both !important;} }    
         @media only screen and (max-width: 480px){.bmeShareItem{padding-top: 10px !important;} }    
         @media only screen and (max-width: 480px){.tdPart.bmeMblStackCenter, .bmeMblStackCenter .bmeFollowItemIcon {padding:0px !important; text-align: center !important;} }    
         @media only screen and (max-width: 480px){.bmeMblStackCenter> .bmeShareItem{width: 100% !important;} }    
         @media only screen and (max-width: 480px){ td.bmeMblCenter {border: 0 none transparent !important;}}    
         @media only screen and (max-width: 480px){.bmeLinkTable.tdPart td{padding-left:0px !important; padding-right:0px !important; border:0px none transparent !important;padding-bottom:15px !important;height: auto !important;}}    
         @media only screen and (max-width: 480px){.tdMblHide{width:10px !important;} }    
         @media only screen and (max-width: 480px){.bmeShareItemBtn{display:table !important;}}    
         @media only screen and (max-width: 480px){.bmeMblStack td {text-align: left !important;}}    
         @media only screen and (max-width: 480px){.bmeMblStack .bmeFollowItem{clear:both !important; padding-top: 10px !important;}}    
         @media only screen and (max-width: 480px){.bmeMblStackCenter .bmeFollowItemText{padding-left: 5px !important;}}    
         @media only screen and (max-width: 480px){.bmeMblStackCenter .bmeFollowItem{clear:both !important;align-self:center; float:none !important; padding-top:10px;margin: 0 auto;}}    
         @media only screen and (max-width: 480px){    
         .tdPart> table{width:100% !important;}    
         }    
         @media only screen and (max-width: 480px){.tdPart>table.bmeLinkContainer{ width:auto !important; }}    
         @media only screen and (max-width: 480px){.tdPart.mblStackCenter>table.bmeLinkContainer{ width:100% !important;}}     
         .blk_parent:first-child, .blk_parent{float:left;}    
         .blk_parent:last-child{float:right;}    
         /*** BMEMBF END ***/    
         table[name="bmeMainBody"], body {background-color:#2f2f2f;}    
         td[name="bmePreHeader"] {background-color:#00ae54;}    
         td[name="bmeHeader"] {background:#ffffff;background-color:#00ae54;}    
         td[name="bmeBody"], table[name="bmeBody"] {background-color:#ffffff;}    
         td[name="bmePreFooter"] {background-color:#ffffff;}    
         td[name="bmeFooter"] {background-color:#ffffff;}    
         td[name="tblCell"], .blk {font-family:initial;font-weight:normal;font-size:initial;}    
         table[name="blk_blank"] td[name="tblCell"] {font-family: "Lato", sans-serif;font-size:14px;}    
         [name=bmeMainContentParent] {border-color:#666666;border-width:0px;border-style:none;border-radius:0px;border-collapse:separate;border-spacing:0px;overflow:hidden;}    
         [name=bmeMainColumnParent] {border-color:transparent;border-width:0px;border-style:none;border-radius:0px;}    
         [name=bmeMainColumn] {border-color:transparent;border-width:0px;border-style:none;border-radius:0px;border-collapse:separate;border-spacing:0px;}    
         [name=bmeMainContent] {border-color:transparent;border-width:0px;border-style:none;border-radius:0px;border-collapse:separate;border-spacing:0px;}    
      </style>
   </head>
   <body marginheight=0 marginwidth=0 topmargin=0 leftmargin=0 style="height: 100% !important; margin: 0; padding: 0; width: 100% !important;min-width: 100%;">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" name="bmeMainBody" style="background-color: #eaeaea;" bgcolor="#eaeaea">
         <tbody>
            <tr>
               <td width="100%" valign="middle" align="center">
                  <table cellspacing="0" cellpadding="0" border="0" name="bmeMainColumnParentTable">
                     <tbody>
                        <tr>
                           <td name="bmeMainColumnParent" style="border: 0px none transparent; border-radius: 0px; border-collapse: separate;">
                              <table name="bmeMainColumn" class="bmeHolder bmeMainColumn" style="max-width: 600px; border-radius: 0px; border-collapse: separate; border-spacing: 0px; overflow: visible;" cellspacing="0" cellpadding="0" border="0" align="center">
                                 <tbody>
                                    <tr>
                                       <td width="100%" class="blk_container bmeHolder" name="bmePreHeader" valign="top" align="center" style="color: rgb(102, 102, 102); border: 0px none transparent; background-color: rgb(0, 174, 84);" bgcolor="#00ae54"></td>
                                    </tr>
                                    <tr>
                                       <td width="100%" class="bmeHolder" valign="top" align="center" name="bmeMainContentParent" style="border: 0px none rgb(102, 102, 102); border-radius: 0px; border-collapse: separate; border-spacing: 0px; overflow: hidden;">
                                          <table name="bmeMainContent" style="border-radius: 0px; border-collapse: separate; border-spacing: 0px; border: 0px none transparent;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                             <tbody>
                                                <tr>
                                                   <td width="100%" class="blk_container bmeHolder bmeBody" name="bmeBody" valign="top" align="center" style="color: rgb(56, 56, 56); border: 0px none transparent; background-color: rgb(255, 255, 255);" bgcolor="#ffffff">
                                                      <div id="dv_15" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table width="600" cellspacing="0" cellpadding="0" border="0" class="blk" name="blk_divider" style="">
                                                            <tbody>
                                                               <tr>
                                                                  <td class="tblCellMain" style="padding: 10px 0px;">
                                                                     <table class="tblLine" cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top-width: 0px; border-top-style: none; min-width: 1px;">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td><span></span></td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                      <div id="dv_11" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table width="600" cellspacing="0" cellpadding="0" border="0" class="blk" name="blk_image">
                                                            <tbody>
                                                               <tr>
                                                                  <td>
                                                                     <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td align="center" class="bmeImage" style="border-collapse: collapse; padding: 20px;"><a href="https://www.linqq.io"><img    
                                                                                 src="http://mobulous.in/mailer_linqq/logo.png" width="52" style="max-width: 150px; display: block; width: 150px;" alt="" border="0"></a></td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                      <div id="dv_17" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table width="600" cellspacing="0" cellpadding="0" border="0" class="blk" name="blk_text">
                                                            <tbody>
                                                               <tr>
                                                                  <td>
                                                                     <table cellpadding="0" cellspacing="0" border="0" width="100%" class="bmeContainerRow">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td class="tdPart" valign="top" align="center">
                                                                                 <table cellspacing="0" cellpadding="0" border="0" width="600" name="tblText" style="float:left; background-color:transparent;" align="left" class="tblText">
                                                                                    <tbody>
                                                                                       <tr>
                                                                                          <td valign="top" align="left" name="tblCell" style="padding: 10px 20px; font-family: "Lato", sans-serif; font-size: 14px; font-weight: 400; color: #3f81c7; text-align: left;" class="tblCell">
                                                                                             <div style="line-height: 150%; text-align: left;"><span style="font-size: 16px; font-family: "Lato", sans-serif; color: #3f81c7; line-height: 150%;"><strong style="color: #3f81c7;">Hello '.$user->name.'</strong></span><br><br>    
                                                                                                <br><span style="font-size: 14px; ont-family: "Lato", sans-serif; color: #3879bd; font-weight:bold; line-height: 150%;"><strong>'.Auth::user()->name.' has sent you connection request on LINQQ.</strong></span><br><br>
                                                                                                 <span style="font-size: 14px; font-family: "Lato", sans-serif; color: #464646; line-height: 150%;">Check out their interests and purpose to know more about them. Connect and meet to discover new oppertunites'.'</span>
                                                                                             </div>
                                                                                          </td>
                                                                                       </tr>
                                                                                    </tbody>
                                                                                 </table>
                                                                              </td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                      
                                                      
                                                      <div id="dv_8" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table width="900"  cellspacing="0" cellpadding="0" border="0" class="blk" name="blk_divider" style="
    margin: 10px auto;
    background: #f6f6f8;
    padding: 20px 10px; 
    overflow:hidden; 
    clear:both;
    width: 600px;">
                                                            <tbody>
                                                               <tr>
                                                                  <td class="tblCellMain" style="padding: 10px 20px;">
                                                                     <table class="tblLine" cellspacing="0" cellpadding="0" border="0" width="1000" style="border-top-width: 0px; border-top-style: none;  width: 561px;min-width: 1px;">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td valign="top" align="left" width="200" style="width:20%;" class="tdPart">
                                                                                  
                                                                                   <img src="'.$this->obj->is_filetemp("profile", Auth::user()->image).'" style="width:60px; height:60px;margin-right:20px;">
                                                                               </td>
                                                                              <td valign="middle"  width="500" style="width:80%;" class="tdPart">
                                                                                  <a href="javascript:void();" style="color: #3879bd !important;">
                                                                                  <span style="    display: block;
    color: #3879bd;
    font-weight: 600;font-family: "Lato", sans-serif;">'.Auth::user()->name.',</span>
    </a><br>
                                                                                  <span style="    display: block;
    color: #333;
    font-size: 14px;font-family: "Lato", sans-serif;">'.Auth::user()->location.'</span>
                                                                               </td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                      
                                                   </td>
                                                </tr>
                                               
                                                
                                                <tr>
                                                   <td width="100%" class="blk_container bmeHolder" name="bmePreFooter" valign="top" align="center" style="color: rgb(56, 56, 56); border: 0px none transparent; background-color: #fff;" bgcolor="#ffffff">
                                                      <div id="dv_5" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table width="600" cellspacing="0" cellpadding="0" border="0" class="blk" name="blk_text">
                                                            <tbody>
                                                               <tr>
                                                                  <td>
                                                                     <table cellpadding="0" cellspacing="0" border="0" width="100%" class="bmeContainerRow">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td class="tdPart" valign="top" align="center">
                                                                                 <table cellspacing="0" cellpadding="0" border="0" width="600" name="tblText" style="float:left; background-color:transparent; color: #3879bd  !important; font-weight:bold;" align="left" class="tblText">
                                                                                    <tbody>
                                                                                       <tr>
                                                                                          <td valign="top" align="left" name="tblCell" style="padding: 20px; font-family: "Lato", sans-serif; font-size: 14px; font-weight: 900; color: #3879bd  !important; font-weight:bold; text-align: left;" class="tblCell">
                                                                                             <div style="line-height: 200%; color: #3879bd  !important; font-weight:bold;"><span style="    font-size: 14px;
    ont-family: "Lato", sans-serif;
    color: #3879bd !important;
    line-height: 200%;
    display:inline-block;
    font-weight: 900;">Regards</span><br><span style="    font-size: 14px;
   font-family: "Lato", sans-serif;
    color: #3879bd  !important;
    line-height: 200%;
    display:inline-block;
    font-weight: 900;">Team LINQQ</span></div>
                                                                                          </td>
                                                                                       </tr>
                                                                                    </tbody>
                                                                                 </table>
                                                                              </td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                      <div id="dv_9" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table width="600" cellspacing="0" cellpadding="0" border="0" class="blk" name="blk_divider" style="">
                                                            <tbody>
                                                               <tr>
                                                                  <td class="tblCellMain" style="padding: 10px 20px;">
                                                                     <table class="tblLine" cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top-width: 0px; border-top-style: none; min-width: 1px;">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td><span></span></td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                      
                                                      
                                                      <div id="dv_13" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table cellspacing="0" cellpadding="0" border="0" name="blk_divider" width="600" class="blk">
                                                            <tbody>
                                                               <tr>
                                                                  <td style="padding: 10px 0px;" class="tblCellMain">
                                                                     <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-top-width: 0px; border-top-style: none; min-width: 1px;" class="tblLine">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td><span></span></td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                       
                                                 
                                                      
                                                      
                                                      
                                                       
                                                      <!----------------------------------------------------->
                                                      
                                                      <div id="dv_8" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table cellspacing="0" cellpadding="0" border="0" id="bllbbbb"  width="1000"  align="center" class="blk" name="blk_divider" style="background: #f6f6f8;padding: 20px 10px;    background: #f6f6f8;
    padding: 20px 10px;
    margin: 0 auto;
    width: 600px;">   
    
    
                                                            <tbody>
                                                               <tr>
                                                                  <td>
                                                                     <table cellpadding="0" cellspacing="0" border="0" class="bmeContainerRow">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td class="tdPart" valign="top" align="center">
                                                                                 <table cellspacing="0" cellpadding="0" border="0" width="1000" style="    background: #f6f6f8;
    padding: 20px 10px;
    margin: 0 auto;
    width: 600px;" name="tblText" style="" align="center" class="tblText">
                                                                                    <tbody>
                                                                                       <tr>
                                                                                          <td valign="top" align="center" name="tblCell" style="padding: 10px 20px; font-family: "Lato", sans-serif; font-size: 14px; font-weight: 400; color: rgb(56, 56, 56); text-align: center;" class="tblCell">
                                                                                             <div style="text-align: center;margin-bottom: 0px;">
                                                                                                 <span style="font-size: 16px; font-family: "Lato", sans-serif; color: #333; line-height: 150%;"><b><a href="https://www.linqq.io" style="display:inline-block; color:#333 !important;">www.linqq.io</a></b></span>  <br>
                                                                                                 <span style="font-size: 16px; font-family: "Lato", sans-serif; color: #333; line-height: 150%;"><b><a href="https://www.linqq.io/" style="display:inline-block; color:#333 !important;">Support@linqq.io</a></b></span> 
                                                                                                <br>
                                                                                             </div>
                                                                                          </td>
                                                                                       </tr>
                                                                                    </tbody>
                                                                                 </table>
                                                                              </td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                            <tbody>
                                                                           <tr>
                                                                              <td class="tdItemContainer" style="margin-top:20px">
                                                                                 <table cellspacing="0" cellpadding="0" border="0" class="mblSocialContain" style="table-layout: auto;     text-align: center;
    margin: auto;">
                                                                                    <tbody>
                                                                                       <tr>
                                                                                          <td valign="top" name="bmeSocialTD" class="bmeSocialTD">
                                                                                             <!--[if gte mso 6]>
                                                                                          </td>
                                                                                          <td align="left" valign="top">
                                                                                             <![endif]-->    
                                                                                             <table cellspacing="0" cellpadding="0" border="0" class="bmeFollowItem" type="facebook" style="float: left; display: block;" align="left">
                                                                                                <tbody>
                                                                                                   <tr>
                                                                                                      <td align="left" class="bmeFollowItemIcon" gutter="20" width="24" style="padding-right:20px;height:20px;">    
                                                                                                         <a href="https://www.facebook.com/Linqq.io/" target="_blank" style="display: inline-block;">
                                                                                                         <img src="'.url("/public/img/facebook.png").'" border="0" width="24" height="24"></a>
                                                                                                      </td>
                                                                                                   </tr>
                                                                                                </tbody>
                                                                                             </table>
                                                                                             <!--[if gte mso 6]>
                                                                                          </td>
                                                                                          <td align="left" valign="top">
                                                                                             <![endif]-->    
                                                                                             <table cellspacing="0" cellpadding="0" border="0" class="bmeFollowItem" type="twitter" style="float: left; display: block;" align="left">
                                                                                                <tbody>
                                                                                                   <tr>
                                                                                                      <td align="left" class="bmeFollowItemIcon" gutter="20" width="24" style="padding-right:20px;height:20px;">    
                                                                                                         <a href="https://www.linkedin.com/company/linqqapp/" target="_blank" style="display: inline-block;"><img src="'.url("/public/img/linkedin.png").'" border="0" width="24" height="24"></a>
                                                                                                      </td>
                                                                                                   </tr>
                                                                                                </tbody>
                                                                                             </table>
                                                                                             <!--[if gte mso 6]>
                                                                                          </td>
                                                                                          <td align="left" valign="top">
                                                                                             <![endif]-->    
                                                                                             <table cellspacing="0" cellpadding="0" border="0" class="bmeFollowItem" type="email" style="float: left; display: block; padding-right: 20px;
}" align="left">
                                                                                                <tbody>
                                                                                                   <tr>
                                                                                                      <td align="left" class="bmeFollowItemIcon" gutter="20" width="24" style="height:20px;">    
                                                                                                         <a href="https://www.instagram.com/linqq.io/" style="display: inline-block;" target="_blank" ><img src="'.url("/public/img/instagram.png").'" alt="Email" style="display: block; max-width: 114px; margin-right: 20px;" border="0" width="24" height="24"></a>
                                                                                                      </td>
                                                                                                   </tr>
                                                                                                </tbody>
                                                                                             </table>  
                                                                                             <table cellspacing="0" cellpadding="0" border="0" class="bmeFollowItem" type="email" style="float: left; display: block;" align="left">
                                                                                                <tbody>
                                                                                                   <tr>
                                                                                                      <td align="left" class="bmeFollowItemIcon" gutter="20" width="24" style="height:20px;">    
                                                                                                         <a href="https://twitter.com/Linqq_io" style="display: inline-block;" target="_blank"><img src="'.url("/public/img/twitter.png").'" alt="Email" style="display: block; max-width: 114px;" border="0" width="24" height="24"></a>
                                                                                                      </td>
                                                                                                   </tr>
                                                                                                </tbody>
                                                                                             </table>
                                                                                          </td>
                                                                                       </tr>
                                                                                    </tbody>
                                                                                 </table>
                                                                              </td>
                                                                           </tr>
                                                                        </tbody>
                                                         </table>
                                                      </div>
                                                      <!----------------------------------------------------->
                                                       
                                                       
                                                     
                                                      
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                    
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
   </body>
</html>';              
                     $to      =  $user->email; 
                    $subject = 'Connection Request';  
                    $message =  $mssg;  
                    $headers1 = "From:  info@mobulous.co.in \r\n"; 
                    $headers1 .= "X-Mailer: PHP/' . phpversion(); \r\n";
                    $headers1 .= "MIME-Version: 1.0 \r\n";
                    $headers1 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                    mail($to, $subject, $message, $headers1); 


//  $email = new \SendGrid\Mail\Mail(); 
// $email->setFrom("support@linqqapp.com", "Team Linqq");
// $email->setSubject("Connection Request");
// $email->addTo($to, "Team Linqq");
// $email->addContent(
//     "text/html", $message
// );
// $sendgrid = new \SendGrid("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");
// try {
//     $response = $sendgrid->send($email);
//    // print $response->statusCode() . "\n";
//     //print_r($response->headers());
//     //print $response->body() . "\n";
// } catch (Exception $e) {
//    // echo 'Caught exception: '. $e->getMessage() ."\n";
// }



        } 

                          $ucount = User::where('id',$user_id)->first();
                           User::where('id', $user_id)->update(['count' => $ucount->count+1]);  
                           
                          //if (!empty($request->message)) {

                                $message = new Message;
                                $message->message     = !empty($request->message)?$request->message:''; 
                                $message->receiver_id = $request->receiver_id;  
                                $message->status      = empty($request->message)?'Read':'Read';
                                $message->sender_id   = $user_id;  
                                $message->request_id  = $create->id;
                                $message->save();
                            //} 
                        
                            $not_id = $this->save_notification($user_id,$request->receiver_id,"send_request",Auth::user()->name . " sent you request."); 
                            
                            $batch['type']        =  "send_request";
                            $batch['sender_id']   =  $user_id;
                            $batch['receiver_id'] =  $request->receiver_id; 
                            $batch['message']     =  Auth::user()->name . " send you request.";  
                            $batch['not_id']      =  (string)$not_id;
                          
                           $ncount = UserNotification::where('receiver_id',$batch['receiver_id'])->where('status','Unread')->count();
                          
                      if ($user->email_push == "ON" && !empty($user->device_id)) {    
                     
                          if ($user->device_type == "android") {

                     $this->obj->android_push($user->device_id, $batch['message'], $batch['type'],$ncount,$batch);
                                } else {
                    $this->obj->iphone_push($user->device_id, $batch['message'], $batch['type'],$ncount,$batch);
                                }
                            }

                            $this->obj->set_data("200", "Sent successfully.", $request->path());
                       
                        }
                    } 

                } else { 
             
                    $this->obj->error_message('500', "No user found.", $request->path());
             
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }


   
    public function connection_list(Request $request)
    {  
         try {   

            $rules = array(

            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
           
            } else {

                 $user_id = Auth::user()->id;
                 $query   = "SELECT id AS request_id, sender_id, receiver_id, message FROM user_requests WHERE  receiver_id = $user_id AND connection='OFF' AND reject_status='0' ORDER BY id DESC";
                $get     = DB::select($query);
                if (count($get)) {
                    foreach ($get as $value) {
                        if ($value->sender_id != $user_id) {
                            $user = User::where('id', $value->sender_id)->first();
                        } else {
                            $user = User::where('id', $value->receiver_id)->first();
                        }
                        $value->name         = !empty($user->name) ? $user->name : "";
                        $value->designation  = !empty($user->designation) ? $user->designation : "";
                        $value->organisation = !empty($user->organisation) ? $user->organisation : "";
                        $image               = !empty($user->image) ? $user->image : "";
                        $value->image        = $this->obj->is_file('profile_resize', $image);
                    } 
                    
                     $this->obj->set_data("200", "List.", $request->path(), 'response', $get);
              
                } else { 

                      $this->obj->set_data("404", "No record found.", $request->path());
               }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
  }  


  public function send_request_list(Request $request)
    {  
         try {   

            $rules = array(

            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
           
            } else {

                 $user_id = Auth::user()->id;
                 $query   = "SELECT id AS request_id, sender_id, receiver_id, message FROM user_requests WHERE  sender_id = $user_id AND connection='OFF' AND reject_status='0' ORDER BY id DESC";
                $get     = DB::select($query);
                if (count($get)) {
                    foreach ($get as $value) {
                        if ($value->sender_id != $user_id) {
                            $user = User::where('id', $value->sender_id)->first();
                        } else {
                            $user = User::where('id', $value->receiver_id)->first();
                        }
                        $value->name         = !empty($user->name) ? $user->name : "";
                        $value->designation  = !empty($user->designation) ? $user->designation : "";
                        $value->organisation = !empty($user->organisation) ? $user->organisation : "";
                        $image               = !empty($user->image) ? $user->image : "";
                        $value->image        = $this->obj->is_file('profile_resize', $image);
                    } 
                    
                     $this->obj->set_data("200", "List.", $request->path(), 'response', $get);
              
                } else { 

                      $this->obj->set_data("404", "No record found.", $request->path());
               }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
  }  
  
  

   public function search_user(Request $request)
    { 
        try {

           $rules = array(
          
                'name'          => 'max:190',
                'interest_id'   => 'max:190',
                'networking_id' => 'max:190',
                'location'      => 'max:190',
            );

            if ($this->obj->validate_request($request, $rules)) {
                $user_id      = Auth::user()->id; 
                $fields       = " users.id AS user_id, users.name, users.location, users.designation, users.organisation, users.image ";
                $condition    = " users.id != $user_id AND users.is_admin = '0' ";
                $user_request = UserRequest::where('sender_id', $user_id)->where('connection','ON')->where('reject_status','0')->pluck('receiver_id');
                
                if (count($user_request)) {
                    $remove_id = implode(",", $user_request->toArray());
                    $condition .= " AND users.id NOT IN ($remove_id)";
                } 
               
                $user_requestt = UserRequest::where('receiver_id', $user_id)->where('connection','ON')->where('reject_status','0')->pluck('sender_id'); 
                if (count($user_requestt)) {
                    $remove_id = implode(",", $user_requestt->toArray());
                    $condition .= " AND users.id NOT IN ($remove_id)";  
                } 

                $user_wait = UserRequest::where('sender_id', $user_id)->where('connection','OFF')->where('reject_status','0')->pluck('receiver_id'); 
                if (count($user_wait)) {
                    $remove_id = implode(",", $user_wait->toArray());
                    $condition .= " AND users.id NOT IN ($remove_id)";   
                } 
                
                $user_rwait = UserRequest::where('receiver_id', $user_id)->where('connection','OFF')->where('reject_status','0')->pluck('sender_id'); 
                if (count($user_rwait)) {
                    $remove_id = implode(",", $user_rwait->toArray());
                    $condition .= " AND users.id NOT IN ($remove_id)";  
                } 
            
              $user_rej = UserRequest::where('sender_id', $user_id)->where('connection','OFF')->where('reject_status','1')->pluck('receiver_id'); 
                if (count($user_rej)) {
                    $remove_id = implode(",", $user_rej->toArray());
                    $condition .= " AND users.id NOT IN ($remove_id)";   
                } 
                 
                $user_rrej = UserRequest::where('receiver_id', $user_id)->where('connection','OFF')->where('reject_status','1')->pluck('sender_id'); 
                if (count($user_rrej)) {
                    $remove_id = implode(",", $user_rrej->toArray());
                    $condition .= " AND users.id NOT IN ($remove_id)";   
                } 
                
                  
             /*if (empty($request->interest_id)) {
                
                   $igg = UserInterest::where('user_id',$user_id)->pluck('interest_id')->toArray();
                   if(!empty($igg)){ 
                     
                      $interest_ids = UserInterest::whereIn('interest_id',$igg)->pluck('user_id')->toArray();
                      $int_ids = implode(',',$interest_ids);   
                      $condition .= " OR users.id IN ($int_ids)";

                    }  
              }*/ 
 
/*
                $user_ignore = IgnoreUser::where('user_id',$user_id)->pluck('ignore_id');
                if (count($user_ignore)) {
                    $remove_id    = implode(",", $user_ignore->toArray());
                    $condition   .=  " AND users.id NOT IN ($remove_id)"; 
                   
                } */  

                if (!empty($request->name)) {
                    $condition .= " AND ( users.name LIKE '%$request->name%' OR users.designation LIKE '%$request->name%' OR users.organisation LIKE '%$request->name%' ) ";
                }
                if (!empty($request->location)) {
                    $condition .= " AND users.location LIKE '%$request->location%' ";
                }
                
                if (!empty($request->networking_id)) {
                    $condition .= " AND user_networkings.networking_id =$request->networking_id";
                }

                
                if (!empty($request->interest_id)) {

                    $intarray = explode(',',$request->interest_id);
                    $icount   = count($intarray);
                   $quu = "SELECT user_id  FROM user_interests WHERE interest_id IN($request->interest_id) AND user_id != $user_id group by user_id having COUNT(interest_id) >= $icount"; 
                   $iusers = DB::select($quu);
                  // print_r($iusers);die;
                 if(!empty($iusers)){  

                        $resultArray = json_decode(json_encode($iusers), true);
                        foreach($resultArray as $v){
                            $final[]=$v['user_id'];
                        }
                        $idstr  = implode(',',$final);
                       
                          //$condition .= " AND user_interests.interest_id IN ('$request->interest_id') ";
                        $condition .= " AND users.id IN ($idstr)";
                    }
                    else
                    {
                        $condition .= " AND users.id IN ('')";
                    }
                }    
              


              
              // echo $condition;die;
                $query = "SELECT $fields FROM users
                LEFT JOIN user_interests ON users.id = user_interests.user_id  
                LEFT JOIN user_networkings ON users.id = user_networkings.user_id
                where $condition GROUP BY user_id LIMIT 20";     
                   

                //echo $query;die;
                $users = DB::select($query);   
               // print_r($users);die;
                if (count($users)) {

                    foreach ($users as $value) { 
                         
                         $value->image = $this->obj->is_file('profile_resize', $value->image);
                    }
                    $this->obj->set_data("200", "List.", $request->path(), 'response', $users);
                } else {
                    $this->obj->error_message('500', "No user found.", $request->path());
                } 
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }    

    public function notification_list(Request $request)
      { 


        
          try{ 
         
            $user_id       = Auth::user()->id;
            UserNotification::where('receiver_id', $user_id)->update(['status' =>'Read']);
            $notifications = UserNotification::where('receiver_id', $user_id)->orderBy('id', 'DESC')->get();
            if(count($notifications)) {
              //print_r($notifications);die;
                foreach ($notifications as $value) { 


                   
                    $user= User::where('id', $value->sender_id)->first();
                    if(!empty($user)){

                    $image                    = !empty($user->image) ? $user->image : "";
                    $value->image             = $this->obj->is_file('profile_resize', $image); 
                    $value->notification_time = $this->obj->time_string($value->created_at); 
                    if($value->type  == 'accept_request') {

                        $value->name = !empty($user->name)?$user->name:""; 
                        $checkRequest = UserRequest::where('sender_id',$user_id)->where('receiver_id',$value->sender_id)->where('connection', "ON")->first(); 
                        $value->request_id = !empty($checkRequest)?(string)$checkRequest->id:'';
                        $value->notification_status ='OFF';

                       }
                     
                    if($value->type  == 'send_request') {

                        $checkRequest1 = UserRequest::where('sender_id',$value->sender_id)->where('receiver_id',$user_id)->where('connection', "ON")->first();
                        $value->request_id = !empty($checkRequest1)?(string)$checkRequest1->id:'';
                         $value->name = !empty($user->name)?$user->name:""; 
                        $value->notification_status = !empty($checkRequest1)?(string)$checkRequest1->connection:'';        

                       }
                   }

                   
                     
                   
    
                  }                   
            
               $this->obj->set_data("200", "List.", $request->path(), 'response', $notifications);
            
            } else { 

                $this->obj->error_message('500', "No record found.", $request->path());
            } 

        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        } 
  

  } 
    

   public function accept_request(Request $request)
    {
        try { 
            
            $rules = array(
                'request_id' => 'required',
            );
            
          if ($this->obj->validate_request($request, $rules)) {
                $user_id      = Auth::user()->id;
                $user_request = UserRequest::where('id', $request->request_id)->first();
                if (!empty($user_request)) {
                    $user_request->connection = "ON";
                    
                     if($user_request->save()) {
                      
                       $not_id=$this->save_notification($user_request->receiver_id,$user_request->sender_id,"accept_request",Auth::user()->name . " accepted your request."); 
                      
                        $batch['type']        = "accept_request";
                        $batch['sender_id']   = $user_request->receiver_id;
                        $batch['receiver_id'] = $user_request->sender_id;
                        $batch['request_id']  = (string)$user_request->id;
                        $batch['message']     = Auth::user()->name . " accepted your request.";
                        $batch['not_id']      = (string)$not_id;
                        

                     $ncount = UserNotification::where('receiver_id',$batch['receiver_id'])->where('status','Unread')->count();
                        $user = User::where('id',$batch['receiver_id'])->first();
                        if (!empty($user)) { 
                            $batch['sender_name'] = Auth::user()->name;
                            $batch['sender_image'] = $this->obj->is_file('profile_resize',Auth::user()->image);
                            if ($user->email_push == "ON" && !empty($user->device_id)) {
                                

                   if ($user->device_type == "android") {

                  $this->obj->android_push($user->device_id, $batch['message'],$batch['type'],$ncount,$batch);
                                } else {
                  $this->obj->iphone_push($user->device_id, $batch['message'], $batch['type'],$ncount,$batch);
                                  
                                      }

                               }
                        }
                        $this->obj->set_data("200", "Accepted successfully.", $request->path());
                    } else {
                        $this->obj->error_message('500', "Internal error.", $request->path());
                    }
                } else {
                    $this->obj->error_message('500', "No request found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    } 

   

   public function reject_request(Request $request)
    {
        try {
          
           $rules = array(
                'request_id' => 'required', 
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id      = Auth::user()->id;
                $user_request = UserRequest::where('id', $request->request_id)->first();
                if (!empty($user_request)) {

                    $user_request->reject_status = '1';
                    
                    if ($user_request->save()) { 

                       $dnot = UserNotification::where('sender_id',$user_request->sender_id)->where('receiver_id',$user_request->receiver_id)->first();
                       
                       //User::where('id', $userId)->update(['count' => 0]); 
                       
                       if(!empty($dnot)){
                         
                            UserNotification::where('id',$dnot->id)->delete();
                            //$user_request->delete();
                         } 

                        $message = Message::where('request_id',$request->request_id)->first();
                        if (!empty($message)) {

                            $message->delete();
                        }

                        $this->obj->set_data("200", "Rejected successfully.", $request->path());
                    } else {
                        $this->obj->error_message('500', "Internal error.", $request->path());
                    }
                } else {
                    $this->obj->error_message('500', "No request found.", $request->path());
                }
            }
    
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
    
        }
 }    
 

   public function chat_list(Request $request)
    { 
        try {
            $user_id = Auth::user()->id;
            
            if (!empty($user_id)) {
                $query         = "SELECT id AS request_id, sender_id, receiver_id, message, created_at FROM user_requests WHERE (receiver_id = $user_id OR sender_id = $user_id) AND connection='ON' ORDER BY updated_at DESC "; 
                $getConnection = DB::select($query);
                
                if(!empty($getConnection)){

                foreach ($getConnection as $request22) {
                    $req_id[]=$request22->request_id;
                }

                $query2         = "SELECT request_id FROM messages WHERE ( receiver_id = $user_id OR sender_id = $user_id ) group by request_id"; 
                $getConnection2 = DB::select($query2);

                foreach ($getConnection2 as $request2) {
                    $req_id2[]=$request2->request_id;
                }
                $result=array_values(array_diff($req_id,$req_id2));
               
                $count_ids = count($result);
                if(!empty($result))
                {


                    for($i=0;$i<$count_ids;$i++)
                    {
                        $left_requests = UserRequest::where('id',$result[$i])->first();

                        $message = new Message;
                        $message->request_id = $result[$i];
                        $message->sender_id = $left_requests->sender_id;
                        $message->receiver_id = $left_requests->receiver_id;
                        $message->message = "Welcome to Linqq. Start SWIPING Right to connect Message me for any help regarding app. #HappyNetworking.";
                        $message->status="Read";
                        $message->created_at = date('Y-m-d h:i:s');
                        $message->updated_at = date('Y-m-d h:i:s');

                        if($message)
                        {
                            $message->save();
                        }
                    }

                }

                
            }




                //array_reverse($getConnection); 
                if (count($getConnection)) {
                     //print_r($getConnection);die;
                foreach ($getConnection as $message) {

                  //  print_r($message);
                   // $individualMsg = Message::select('id AS message_id', 'request_id', 'receiver_id', 'sender_id', 'message', 'created_at')->where('request_id', $message->request_id)->orderBy('id', 'DESC')->first();  

                  //if(!empty($individualMsg)){


                       if($request->update == "Yes") {

                          if($message->request_id == $request->request_id){
                           
                            Message::where('receiver_id', $user_id)->where('request_id',$request->request_id)->update(['status' =>'Read']);  
                        
                              }
                       } 

                       //echo $message->request_id.'-';
                       
                        $individualMsg = Message::select('id AS message_id', 'request_id', 'receiver_id', 'sender_id', 'message', 'created_at')->where('request_id', $message->request_id)->orderBy('id', 'DESC')->first(); 
                       // print_r($individualMsg);
                        if (!empty($individualMsg)) {
                            if ($individualMsg->receiver_id == $user_id) {
                                $message->receiver_id = (string) $individualMsg->sender_id;
                                $message->sender_id   = (string) $user_id;
                            } else {
                                $message->receiver_id = (string) $individualMsg->receiver_id;
                                $message->sender_id   = (string) $individualMsg->sender_id;
                            }
                            $message->message = $individualMsg->message;
                            $message->date    = $this->obj->time_string($individualMsg->created_at);
                            $message->msg_date = date("Y-m-d H:i:s",strtotime($individualMsg->created_at));
                        } else {
                            if ($message->receiver_id == $user_id) {
                                $message->receiver_id = (string) $message->sender_id;
                                $message->sender_id   = (string) $user_id;
                            } else {
                                $message->receiver_id = (string) $message->receiver_id;
                                $message->sender_id   = (string) $message->sender_id;
                            }

                            $message->date = $this->obj->time_string($message->created_at);
                        }
                        $user           = User::select('image', 'name')->where('id', $message->receiver_id)->first();
                        $message->name  = "";
                        $message->image = "";

                        if (!empty($user)) {
                            $message->name  = !empty($user->name) ? $user->name : "";
                            $message->image = $this->obj->is_file('profile_resize', $user->image);
                        } 
                        

                        $message->msg_date = date("Y-m-d H:i:s",strtotime($individualMsg->created_at)); 
                        $message->msg_id   = $individualMsg->message_id; 
                        $ccount = Message::where('request_id',$message->request_id)->where('receiver_id',$user_id)->where('status','Unread')->count(); 
                        $message->chat_badge   = $ccount; 
                    
                        //$final[]=$message; 
 
                  //}  
            }         

                if(!empty($getConnection)) {  
                     
                     usort($getConnection, function($a, $b) { return $b->msg_id - $a->msg_id; });
                     //print_r($getConnection);die; 
                  } else {

                     $final = array();
                  }  
                  
                 $this->obj->set_data("200", "List", $request->path(), 'response', $getConnection);
               
                } else {
                    $this->obj->error_message('404', "No message found.", $request->path());
                }
            } else {
                $this->obj->error_message('404', "No user found.", $request->path());
            }

        } catch (Exception $e) {

            $this->obj->error_message('500', "Internal error.", $request->path());
        
         }    
  }



   public function chat_countapi(Request $request) {    

            
            $user_id =  Auth::user()->id; 
            $ccount   =  Message::where('receiver_id',$user_id)->where('status','Unread')->count();
            
            
                $badge['total_chatcount'] = $ccount;
              
            $this->obj->set_data("200", "Total Chat Count.", $request->path(),'response', $badge);
 } 




  public function chat_history(Request $request)
   {
        try {
            $rules = array(
                'request_id' => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id  = Auth::user()->id;
                $messages = Message::where('request_id', $request->request_id)->orderBy('id', 'ASC')->get();
                Message::where('receiver_id', $user_id)->where('request_id',$request->request_id)->update(['status' =>'Read']); 
             
             if(count($messages)) {

                     
                     foreach ($messages as $message) {

                     if(!empty($message->message)){
                        $message->message_id     = (string) $message->id;
                        $message->date           = $this->obj->time_string($message->created_at);
                        $message->microtime      = date_timestamp_get($message->created_at);
                        $user                    = User::select('image', 'name')->where('id', $message->receiver_id)->first();
                        $message->receiver_name  = "";
                        $message->receiver_image = "";
                        
                        if(!empty($user)) { 

                            $message->receiver_name  = !empty($user->name) ? $user->name : "";
                            $message->receiver_image = $this->obj->is_file('profile_resize', $user->image);
                        }
                        $message->sender_name  = "";
                        $message->sender_image = "";
                        $user                  = Auth::user();
                        if (!empty($user)) {
                            $message->sender_name  = !empty($user->name) ? $user->name : "";
                            $message->sender_image = $this->obj->is_file('profile_resize', $user->image);
                          }
                        $final[]=$message;
                       } 

                   }
                  
                  if(!empty($final)){

                  $this->obj->set_data("200", "List", $request->path(), 'response', $final);
                
                } else {

                     $this->obj->error_message('404', "No message found.", $request->path());
                } 
                
                } else {
                    $this->obj->error_message('404', "No message found.", $request->path());
                }
            }
        } catch (Exception $e) {
         
            $this->obj->error_message('500', "Internal error.", $request->path());
         
         }
 }  



    public function send_message(Request $request)
    { 
      
       try {     

             $rules = array(
                'message'     => 'required',
                'receiver_id' => 'required|max:190',
                'request_id'  => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id = Auth::user()->id;
                if (!empty($user_id)) {
                    if ($request->receiver_id == $user_id) {
                        $this->obj->error_message('500', "Not a valid receiver.", $request->path());
                    } else {


                        $userRequest = UserRequest::where('id', $request->request_id)->first();
                        if (empty($userRequest)) {
                            $this->obj->error_message('500', "No request found.", $request->path());
                        }

                        $userRequest->save();
                        $setMessage              = new Message;
                        $setMessage->sender_id   = (string)$user_id;
                        $setMessage->message     = $request->message; 
                        $setMessage->receiver_id = (string)$request->receiver_id;
                        $setMessage->request_id  = (string)$request->request_id;
                        if ($setMessage->save()) {
                            $senderUser               = Auth::user();
                            $setMessage->sender_name  = !empty($senderUser->name) ? $senderUser->name : "";
                            $setMessage->sender_image = $this->obj->is_file("profile_resize", $senderUser->image);
                            $setMessage->message_time = $this->obj->time_string($setMessage->created_at);
                            $setMessage->microtime    = date_timestamp_get($setMessage->created_at);
                            $message                  = !empty($setMessage->message) ? $setMessage->message : "";
                            $type                     = "send_message";
                            $ccount = Message::where('receiver_id',$request->receiver_id)->where('status','Unread')->count();

                            $cpount = Message::where('receiver_id',$request->receiver_id)->where('status','Unread')->where('sender_id',$user_id)->count();   
                            $ncount = UserNotification::where('receiver_id',$request->receiver_id)->where('status','Unread')->count(); 
                            $tbad= $cpount+$ncount;
                            $setMessage->total_chatcount = $ccount;
                            $setMessage->specific_chatcount = $cpount; 
                            $user            = $this->obj->user_device_id($request->receiver_id);
                            //print_r($user);die;
                            //if ($user->notification == "ON") {  
                                if(!empty($user->device_id)){

                                if ($user->device_type == "android") { 


                                    $this->obj->android_pushh($user->device_id, $message, $type, $setMessage);
                                } else {
                                    $this->obj->iphone_pushh($user->device_id, $message, $type, $setMessage,$tbad,$content_available=1);
                                }
                           }   

                            //}
                            $this->obj->set_data("200", "Sent successfully", $request->path(), 'response', $setMessage); 
                        } else {
                            $this->obj->error_message('500', "Internal error.", $request->path());
                        }
                    }
                  
                 } else {
                    $this->obj->error_message('404', "No team found.", $request->path());
                 }
             } 

        } catch (Exception $e) {
        
            $this->obj->error_message('500', "Internal error.", $request->path());
        
        }
   }    
    

   public function send_report(Request $request)
   {
        try {
            $rules = array(
                'message' => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id = Auth::user()->id;
                if (!empty($user_id)) {
                    $setReport          = new UserReport;
                    $setReport->user_id = $user_id;
                    $setReport->message = $request->message;
                    if ($setReport->save()) {
                        $this->obj->set_data("200", "Sent successfully", $request->path());
                    } else {
                        $this->obj->error_message('500', "Internal error.", $request->path());
                    }
                } else {
                    $this->obj->error_message('404', "No team found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }
 

    public function notification_setting(Request $request)
    { 
        try {
            $rules = array(
                'status' => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id = Auth::user()->id;
                if (!empty($user_id)) {
                    $set = User::where('id', $user_id)->update(['notification' => $request->status]);
                    if ($set) {
                        $set         = new User;
                        $set->status = $request->status;
                        $this->obj->set_data("200", "Updated successfully", $request->path(), 'response', $set);
                    } else {
                        $this->obj->error_message('500', "Internal error.", $request->path());
                    }
                } else {
                    $this->obj->error_message('404', "No team found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
  }  


    public function email_push_setting(Request $request)
    
    {
        try { 
            $rules = array(
                'status' => 'required|max:190',
            );
            if ($this->obj->validate_request($request, $rules)) {
                $user_id = Auth::user()->id;
                if (!empty($user_id)) {
                    $set = User::where('id', $user_id)->update(['email_push' => $request->status]);
                    if ($set) {
                        $set         = new User;
                        $set->status = $request->status;
                        $this->obj->set_data("200", "Updated successfully", $request->path(), 'response', $set);
                    } else {
                        $this->obj->error_message('500', "Internal error.", $request->path());
                    }
                } else {
                    $this->obj->error_message('404', "No team found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
  }  

    public function my_info(Request $request)
    {
        try {
            $user_id = Auth::user()->id;
            if (!empty($user_id)) {
                $this->obj->set_data("200", "Data", $request->path(), 'response', Auth::user());
            } else {
                $this->obj->error_message('404', "No team found.", $request->path());
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    } 

    public function twilio($num = null, $code = null)
    {
        require base_path() . '/vendor' . '/twilio-php-master/Twilio/autoload.php';
        $sid     = 'AC2977cafdaf3cd1c8c4204af1b4007a92';
        $token   = 'cb9fe63be97fd536e6b82beffd958105';
        $message = "You are invited to join our team and team code is: $code";
        $client  = new Client($sid, $token);
        $client->messages->create(
            $num, //the number you'd like to send the message to
            array(
                'from' => '+18312176917', // A Twilio phone number you purchased at twilio.com/console
                'body' => $message, // the body of the text message you'd like to send
            )
         );
     }



    public function sinch($num = null, $code = null)
     {

        //$key          = "50f450dd-8c92-4394-953c-a05e5d59b3e5";
        $key          = "AIzaSyBq6yn2242IzEOu3buIG93ypgHHNRdr1_Y";
        $secret       = "2uNu+31Oa0WXnZzucDlw6A==";
        $phone_number = $num;
        $user         = "application\\" . $key . ":" . $secret;
        $message      = array("message" => "Teamtastico team code: " . $code);
        $data         = json_encode($message);

        $ch = curl_init('https://messagingapi.sinch.com/v1/sms/' . $phone_number);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_USERPWD, $user);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            //echo "Error";die;
            echo 'Curl error: ' . curl_error($ch);
        } else {
            //echo $result; die;
        }
        curl_close($ch);
    }

    public function message_details(Request $request)
    {
        try { 

            $rules = array(
                'chat_id' => 'max:190',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {
                $user_id    = Auth::user()->id;
                $getMessage = IndividualMessage::select('id AS message_id', 'chat_id', 'receiver_id', 'sender_id', 'image AS attachment', 'message', 'created_at')->where('chat_id', $request->chat_id)->orderBy('id', 'ASC')->get();
                $response   = new \stdClass();
                if (count($getMessage)) {
                    foreach ($getMessage as $key => $message) {
                        //$rMessage = IndividualMessageRemove::where('user_id',$user_id)->where('message_id',$message->message_id)->first();
                        if (1) {
                            $senderUser            = User::where('id', $message->sender_id)->first();
                            $message->sender_id    = $message->sender_id;
                            $message->sender_name  = "";
                            $message->sender_image = "";
                            if (!empty($senderUser)) {
                                $message->sender_name  = !empty($senderUser->name) ? $senderUser->name : "";
                                $message->sender_image = $this->obj->is_file("profile_resize", $senderUser->image);
                            }
                            $message->type = "message";
                            if (!empty($message->image)) {
                                $message->type = "image";
                            }
                            $message->attachment     = $this->obj->is_file("attachment", $message->attachment);
                            $receiverUser            = User::where('id', $message->receiver_id)->first();
                            $message->receiver_id    = $message->receiver_id;
                            $message->receiver_name  = "";
                            $message->receiver_image = "";
                            if (!empty($receiverUser)) {
                                $message->receiver_name  = !empty($receiverUser->name) ? $receiverUser->name : "";
                                $message->receiver_image = $this->obj->is_file("profile_resize", $receiverUser->image);
                            }
                            $message->message_id   = (string) $message->message_id;
                            $message->created_at   = $message->created_at;
                            $message->message_time = $this->obj->time_string($message->created_at);
                        } else {
                            //unset($getMessage[$key]);
                        }
                    }
                    $this->obj->set_data("200", "List", $request->path(), 'response', $getMessage);
                } else {
                    $this->obj->set_data('404', "No record.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }
    



    public function delete_message(Request $request)
    {
        try {
            $rules = array(
                'message_id' => 'required|max:190',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {
                $user_id = Auth::user()->id; 
                $message = IndividualMessage::where('id', $request->message_id)->first();
                if (!empty($message)) {
                    /*$rMessage = new IndividualMessageRemove;
                    $rMessage->user_id = $user_id;
                    $rMessage->message_id = $request->message_id;
                    $rMessage->save()*/
                    if (IndividualMessage::where('id', $request->message_id)->delete()) {
                        $this->obj->set_data("200", "Sent deleted", $request->path());
                    } else {
                        $this->obj->error_message('500', "Internal error.", $request->path());
                    }
                } else {
                    $this->obj->error_message('404', "No message found.", $request->path());
                }
            }
        } catch (Exception $e) {
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
    }   

    

      public function send_invite(Request $request)
     { 

         //echo "sada";die;
     
        try {
            $rules = array(
                'email' => 'required',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {
                $user_id = Auth::user()->id;  
                $user = User::where('id',$user_id)->first();
                $emails = explode(',',$request->email);
                 
                 User::where('id', $user_id)->update(['count' => 0]); 
                 User::where('id', $user_id)->update(['time_status' =>'no']); 

                foreach ($emails as $key) {
                                 
                    $inv              = new Invite;
                    $inv->user_id     = $user_id;
                    $inv->email       = $key;  
                    $inv->save();      
           /*$email   = $key;
                $newPassword = "12345";
                $subject = "Invite Link";
                $message = "Your new password ";*/ 
                
               /* Mail::send('emails.forgot_password', ['email'=> $email,'newPassword' => $newPassword], function ($message) use ($email,$subject) {
                    $message->to($email)->subject($subject);     
               });  */   
                 
                $mssg='<!DOCTYPE html>    
<html>
   <head>
      <title>Push Email</title>
      <link rel="shortcut icon" href="favicon.ico">
      <style type="text/css">
         table[name="blk_permission"], table[name="blk_footer"] {display:none;} 
      </style>
      <meta name="googlebot" content="noindex" />
      <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"/>
      <link rel="stylesheet" href="/style/dhtmlwindow.css" type="text/css" />
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900" type="text/css" />
      <script type="text/javascript" src="/script/dhtmlwindow.js">
         /***********************************************
         * DHTML Window Widget- © Dynamic Drive (www.dynamicdrive.com)
         * This notice must stay intact for legal use.
         * Visit www.dynamicdrive.com for full source code
         ***********************************************/
      </script>
      <link rel="stylesheet" href="/style/modal.css" type="text/css" />
      <script type="text/javascript" src="/script/modal.js"></script>
   
      <meta content="width=device-width, initial-scale=1.0" name="viewport">
      <style type="text/css">   
          @import url("https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900")
          body{font-family: "Lato", sans-serif !important;}
          
          .ii a[href] {
                color: #000000 !important;
            }

         /*** BMEMBF Start ***/    
         [name=bmeMainBody]{/* min-height:1000px; */}      
         @media only screen and (max-width: 480px){table.blk, table.tblText, .bmeHolder, .bmeHolder1, table.bmeMainColumn{width:100% !important;} }        
         @media only screen and (max-width: 480px){.bmeImageCard table.bmeCaptionTable td.tblCell{padding:0px 20px 20px 20px !important;} }        
         @media only screen and (max-width: 480px){.bmeImageCard table.bmeCaptionTable.bmeCaptionTableMobileTop td.tblCell{padding:20px 20px 0 20px !important;} }        
         @media only screen and (max-width: 480px){table.bmeCaptionTable td.tblCell{padding:10px !important;} }        
         @media only screen and (max-width: 480px){table.tblGtr{ padding-bottom:20px !important;} }        
         @media only screen and (max-width: 480px){td.blk_container, .blk_parent, .bmeLeftColumn, .bmeRightColumn, .bmeColumn1, .bmeColumn2, .bmeColumn3, .bmeBody{display:table !important;max-width:600px !important;width:100% !important;} }        
         @media only screen and (max-width: 480px){table.container-table, .bmeheadertext, .container-table { width: 95% !important; } }        
         @media only screen and (max-width: 480px){.mobile-footer, .mobile-footer a{ font-size: 13px !important; line-height: 18px !important; } .mobile-footer{ text-align: center !important; } table.share-tbl { padding-bottom: 15px; width: 100% !important; } table.share-tbl td { display: block !important; text-align: center !important; width: 100% !important; } }        
         @media only screen and (max-width: 480px){td.bmeShareTD, td.bmeSocialTD{width: 100% !important; } }        
         @media only screen and (max-width: 480px){td.tdBoxedTextBorder{width: auto !important;}}    
         @media only screen and (max-width: 480px){table.blk, table[name=tblText], .bmeHolder, .bmeHolder1, table[name=bmeMainColumn]{width:100% !important;} }    
         @media only screen and (max-width: 480px){.bmeImageCard table.bmeCaptionTable td[name=tblCell]{padding:0px 20px 20px 20px !important;} }    
         @media only screen and (max-width: 480px){.bmeImageCard table.bmeCaptionTable.bmeCaptionTableMobileTop td[name=tblCell]{padding:20px 20px 0 20px !important;} }    
         @media only screen and (max-width: 480px){table.bmeCaptionTable td[name=tblCell]{padding:10px !important;} }    
         @media only screen and (max-width: 480px){table[name=tblGtr]{ padding-bottom:20px !important;} }    
         @media only screen and (max-width: 480px){td.blk_container, .blk_parent, [name=bmeLeftColumn], [name=bmeRightColumn], [name=bmeColumn1], [name=bmeColumn2], [name=bmeColumn3], [name=bmeBody]{display:table !important;max-width:600px !important;width:100% !important;} }    
         @media only screen and (max-width: 480px){table[class=container-table], .bmeheadertext, .container-table { width: 95% !important; } }    
         @media only screen and (max-width: 480px){.mobile-footer, .mobile-footer a{ font-size: 13px !important; line-height: 18px !important; } .mobile-footer{ text-align: center !important; } table[class="share-tbl"] { padding-bottom: 15px; width: 100% !important; } table[class="share-tbl"] td { display: block !important; text-align: center !important; width: 100% !important; } }    
         @media only screen and (max-width: 480px){td[name=bmeShareTD], td[name=bmeSocialTD]{width: 100% !important; } }    
         @media only screen and (max-width: 480px){td[name=tdBoxedTextBorder]{width: auto !important;}}    
         @media only screen and (max-width: 480px){.bmeImageCard table.bmeImageTable{height: auto !important; width:100% !important; padding:20px !important;clear:both; float:left !important; border-collapse: separate;} }    
         @media only screen and (max-width: 480px){.bmeMblInline table.bmeImageTable{height: auto !important; width:100% !important; padding:10px !important;clear:both;} }    
         @media only screen and (max-width: 480px){.bmeMblInline table.bmeCaptionTable{width:100% !important; clear:both;} }    
         @media only screen and (max-width: 480px){table.bmeImageTable{height: auto !important; width:100% !important; padding:10px !important;clear:both; } }    
         @media only screen and (max-width: 480px){table.bmeCaptionTable{width:100% !important;  clear:both;} }    
         @media only screen and (max-width: 480px){table.bmeImageContainer{width:100% !important; clear:both; float:left !important;} }    
         @media only screen and (max-width: 480px){table.bmeImageTable td{padding:0px !important; height: auto; } }    
         @media only screen and (max-width: 480px){td.bmeImageContainerRow{padding:0px !important;}}    
         @media only screen and (max-width: 480px){img.mobile-img-large{width:100% !important; height:auto !important;} }    
         @media only screen and (max-width: 480px){img.bmeRSSImage{max-width:320px; height:auto !important;}}    
         @media only screen and (min-width: 640px){img.bmeRSSImage{max-width:600px !important; height:auto !important;} }    
         @media only screen and (max-width: 480px){.trMargin img{height:10px;} }    
         @media only screen and (max-width: 480px){div.bmefooter, div.bmeheader{ display:block !important;} }    
         @media only screen and (max-width: 480px){.tdPart{ width:100% !important; clear:both; float:left !important; } }    
         @media only screen and (max-width: 480px){table.blk_parent1, table.tblPart {width: 100% !important; } }    
         @media only screen and (max-width: 480px){.tblLine{min-width: 100% !important;}}     
         @media only screen and (max-width: 480px){.bmeMblCenter img { margin: 0 auto; } }       
         @media only screen and (max-width: 480px){.bmeMblCenter, .bmeMblCenter div, .bmeMblCenter span  { text-align: center !important; text-align: -webkit-center !important; } }    
         @media only screen and (max-width: 480px){.bmeNoBr br, .bmeImageGutterRow, .bmeMblStackCenter .bmeShareItem .tdMblHide { display: none !important; } }    
         @media only screen and (max-width: 480px){.bmeMblInline table.bmeImageTable, .bmeMblInline table.bmeCaptionTable, td.bmeMblInline { clear: none !important; width:50% !important; } }    
         @media only screen and (max-width: 480px){.bmeMblInlineHide, .bmeShareItem .trMargin { display: none !important; } }    
         @media only screen and (max-width: 480px){.bmeMblInline table.bmeImageTable img, .bmeMblShareCenter.tblContainer.mblSocialContain, .bmeMblFollowCenter.tblContainer.mblSocialContain{width: 100% !important; } }    
         @media only screen and (max-width: 480px){.bmeMblStack> .bmeShareItem{width: 100% !important; clear: both !important;} }    
         @media only screen and (max-width: 480px){.bmeShareItem{padding-top: 10px !important;} }    
         @media only screen and (max-width: 480px){.tdPart.bmeMblStackCenter, .bmeMblStackCenter .bmeFollowItemIcon {padding:0px !important; text-align: center !important;} }    
         @media only screen and (max-width: 480px){.bmeMblStackCenter> .bmeShareItem{width: 100% !important;} }    
         @media only screen and (max-width: 480px){ td.bmeMblCenter {border: 0 none transparent !important;}}    
         @media only screen and (max-width: 480px){.bmeLinkTable.tdPart td{padding-left:0px !important; padding-right:0px !important; border:0px none transparent !important;padding-bottom:15px !important;height: auto !important;}}    
         @media only screen and (max-width: 480px){.tdMblHide{width:10px !important;} }    
         @media only screen and (max-width: 480px){.bmeShareItemBtn{display:table !important;}}    
         @media only screen and (max-width: 480px){.bmeMblStack td {text-align: left !important;}}    
         @media only screen and (max-width: 480px){.bmeMblStack .bmeFollowItem{clear:both !important; padding-top: 10px !important;}}    
         @media only screen and (max-width: 480px){.bmeMblStackCenter .bmeFollowItemText{padding-left: 5px !important;}}    
         @media only screen and (max-width: 480px){.bmeMblStackCenter .bmeFollowItem{clear:both !important;align-self:center; float:none !important; padding-top:10px;margin: 0 auto;}}    
         @media only screen and (max-width: 480px){    
         .tdPart> table{width:100% !important;}    
         }    
         @media only screen and (max-width: 480px){.tdPart>table.bmeLinkContainer{ width:auto !important; }}    
         @media only screen and (max-width: 480px){.tdPart.mblStackCenter>table.bmeLinkContainer{ width:100% !important;}}     
         .blk_parent:first-child, .blk_parent{float:left;}    
         .blk_parent:last-child{float:right;}    
         /*** BMEMBF END ***/    
         table[name="bmeMainBody"], body {background-color:#2f2f2f;}    
         td[name="bmePreHeader"] {background-color:#00ae54;}    
         td[name="bmeHeader"] {background:#ffffff;background-color:#00ae54;}    
         td[name="bmeBody"], table[name="bmeBody"] {background-color:#ffffff;}    
         td[name="bmePreFooter"] {background-color:#ffffff;}    
         td[name="bmeFooter"] {background-color:#ffffff;}    
         td[name="tblCell"], .blk {font-family:initial;font-weight:normal;font-size:initial;}    
         table[name="blk_blank"] td[name="tblCell"] {font-family: "Lato", sans-serif;font-size:14px;}    
         [name=bmeMainContentParent] {border-color:#666666;border-width:0px;border-style:none;border-radius:0px;border-collapse:separate;border-spacing:0px;overflow:hidden;}    
         [name=bmeMainColumnParent] {border-color:transparent;border-width:0px;border-style:none;border-radius:0px;}    
         [name=bmeMainColumn] {border-color:transparent;border-width:0px;border-style:none;border-radius:0px;border-collapse:separate;border-spacing:0px;}    
         [name=bmeMainContent] {border-color:transparent;border-width:0px;border-style:none;border-radius:0px;border-collapse:separate;border-spacing:0px;}    
      </style>
   </head>
   <body marginheight=0 marginwidth=0 topmargin=0 leftmargin=0 style="height: 100% !important; margin: 0; padding: 0; width: 100% !important;min-width: 100%;">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" name="bmeMainBody" style="background-color: #eaeaea;" bgcolor="#eaeaea">
         <tbody>
            <tr>
               <td width="100%" valign="middle" align="center">
                  <table cellspacing="0" cellpadding="0" border="0" name="bmeMainColumnParentTable">
                     <tbody>
                        <tr>
                           <td name="bmeMainColumnParent" style="border: 0px none transparent; border-radius: 0px; border-collapse: separate;">
                              <table name="bmeMainColumn" class="bmeHolder bmeMainColumn" style="max-width: 600px; border-radius: 0px; border-collapse: separate; border-spacing: 0px; overflow: visible;" cellspacing="0" cellpadding="0" border="0" align="center">
                                 <tbody>
                                    <tr>
                                       <td width="100%" class="blk_container bmeHolder" name="bmePreHeader" valign="top" align="center" style="color: rgb(102, 102, 102); border: 0px none transparent; background-color: rgb(0, 174, 84);" bgcolor="#00ae54"></td>
                                    </tr>
                                    <tr>
                                       <td width="100%" class="bmeHolder" valign="top" align="center" name="bmeMainContentParent" style="border: 0px none rgb(102, 102, 102); border-radius: 0px; border-collapse: separate; border-spacing: 0px; overflow: hidden;">
                                          <table style="background-color: #fff; !important;" bgcolor="#fff" name="bmeMainContent" style="border-radius: 0px; border-collapse: separate; border-spacing: 0px; border: 0px none transparent;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                             <tbody>
                                                <tr>
                                                   <td width="100%" class="blk_container bmeHolder bmeBody" name="bmeBody" valign="top" align="center" style="color: rgb(56, 56, 56); border: 0px none transparent; background-color: rgb(255, 255, 255);" bgcolor="#ffffff">
                                                      <div id="dv_15" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table width="600" cellspacing="0" cellpadding="0" border="0" class="blk" name="blk_divider" style="">
                                                            <tbody>
                                                               <tr>
                                                                  <td class="tblCellMain" style="padding: 10px 0px;">
                                                                     <table class="tblLine" cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top-width: 0px; border-top-style: none; min-width: 1px;">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td><span></span></td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                      <div id="dv_11" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table width="600" cellspacing="0" cellpadding="0" border="0" class="blk" name="blk_image">
                                                            <tbody>
                                                               <tr>
                                                                  <td>
                                                                     <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td align="left" class="bmeImage" style="border-collapse: collapse; padding: 20px;"><img    
                                                                                 src="'.url("/public/img/logo_5.png").'" width="52" style="max-width: 150px; display: block; width: 150px;" alt="" border="0"></td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                      <div id="dv_17" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table style="background-color: #fff; !important;" width="600" cellspacing="0" cellpadding="0" border="0" class="blk" name="blk_text">
                                                            <tbody>
                                                               <tr>
                                                                  <td>
                                                                     <table cellpadding="0" cellspacing="0" border="0" width="100%" class="bmeContainerRow">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td class="tdPart" valign="top" align="center">
                                                                                 <table cellspacing="0" cellpadding="0" border="0" width="600" name="tblText" style="float:left; background-color:transparent;" align="left" class="tblText">
                                                                                    <tbody>
                                                                                       <tr>
                                                                                          <td valign="top" align="left" name="tblCell" style="padding: 10px 20px; font-family: "Lato", sans-serif; font-size: 14px; font-weight: 400; color: rgb(56, 56, 56); text-align: left;" class="tblCell">
                                                                                             <div style="line-height: 150%; text-align: left;"><span style="font-size: 16px; font-family: "Lato", sans-serif; color: #333; line-height: 150%;"><strong>'.$user->name.' invited you to connect on Linqq.</strong></span>    
                                                                                                <br><span style="font-size: 14px; ont-family: "Lato", sans-serif; color: #464646; line-height: 150%;">Linqq is a hyper local platform transforming the professional networking industry.</span><br><br>
                                                                                                 <span style="font-size: 14px; font-family: "Lato", sans-serif; color: #464646; line-height: 150%;">Join Linqq to discover new opportunities.</span>
                                                                                             </div>
                                                                                          </td>
                                                                                       </tr>
                                                                                    </tbody>
                                                                                 </table>
                                                                              </td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                      
                                                      
                                                      
                                                      
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td style="background-color: #fff; !important;" width="100%">
                                                      <table style="background-color: #fff; !important;" class="bmeHolder" name="bmeBody" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="color: rgb(56, 56, 56); border: 0px none transparent; background-color: rgb(255, 255, 255);" bgcolor="#ffffff">
                                                         <tbody>
                                                            <tr>
                                                               <td  style="background-color: #fff; !important;"width="100%" valign="top" align="center">
                                                                  <div>
                                                                     <table style="background-color: #fff; !important;" class="blk_parent1" cellspacing="0" cellpadding="0" style="max-width: 600px;" width="600" align="center">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td  style="background-color: #fff; !important;" valign="top" align="left" width="50%" class="tdPart">
                                                                                 <table style="background-color: #fff; !important;" cellspacing="0" cellpadding="0" border="0" width="100%" class="bmeHolder1" style="float:left;" align="left">
                                                                                    <tbody>
                                                                                       <tr>
                                                                                          <td  style="background-color: #fff; !important;" valign="top" align="left" class="blk_container bmeLeftColumn" name="bmeLeftColumn" style="  " bgcolor="">
                                                                                             <div style="background-color: #fff; !important;" id="dv_4" class="blk_wrapper" style="padding:0px 20px;">
                                                                                                <table style="background-color: #fff; !important;" width="300" cellspacing="0" cellpadding="0" border="0" class="blk" name="blk_button">
                                                                                                   <tbody>
                                                                                                      <tr>
                                                                                                         <td style="background-color: #fff; !important;" align="left">
                                                                                                            <table style="background-color: #fff; !important;" class="tblContainer" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                                               <tbody>
                                                                                                                  <tr>
                                                                                                                     <td height="20"></td>
                                                                                                                  </tr>
                                                                                                                  <tr>
                                                                                                                     <td align="center">
                                                                                                                        <table style="background-color: #fff; !important;" cellspacing="0" cellpadding="0" border="0" class="bmeButton" align="left" width="100%" style="border-collapse: separate;">
                                                                                                                           <tbody>
                                                                                                                              <tr>
                                                                                                                                 <td style="border: 3px solid #3879bd;
    text-align: center;
    font-family: "Lato", sans-serif;
    font-size: 14px;
    padding: 15px 30px;
    font-weight: bold;
    border-collapse: separate;
    background-color: #fff;
    border-radius: 100px;"><span style="font-family: "Lato", sans-serif; font-size: 14px; color: rgb(255, 255, 255);">    
    
     <a style="color: rgb(255, 255, 255);
    text-decoration: none;
    background: #3f81c7;
    padding: 18px 10px;
    display: inline-block;text-align: center;width: 100%;
    border-radius: 100px;
    font-size: 14px;" href="http://linqq.io/" target="_blank">Please Download the app here</a></span>
                                                                                                                                 </td>
                                                                                                                              </tr>
                                                                                                                           </tbody>
                                                                                                                        </table>
                                                                                                                     </td>
                                                                                                                  </tr>
                                                                                                                  <tr>
                                                                                                                     <td height="20"></td>
                                                                                                                  </tr>
                                                                                                               </tbody>
                                                                                                            </table>
                                                                                                         </td>
                                                                                                         <td width="20"></td>
                                                                                                      </tr>
                                                                                                   </tbody>
                                                                                                </table>
                                                                                             </div>
                                                                                              <div id="dv_8" class="blk_wrapper" style="padding:0px 20px;">
                                                                                                 <table width="600" cellspacing="0" cellpadding="0" border="0" class="blk" name="blk_divider" style="width: 100%;
                                            margin: 0px auto;
                                            padding: 0px 0px;"> 
                                                                                                    <tbody>
                                                                                                       <tr>
                                                                                                          <td class="tblCellMain" style="padding: 10px 20px;">
                                                                                                             <table class="tblLine" cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top-width: 0px; border-top-style: none; min-width: 1px;">
                                                                                                                <tbody>
                                                                                                                   <tr>
                                                                                                                      <td valign="top" align="left" width="20%" class="tdPart">
                                                                                                                          <img src="'.$this->obj->is_filetemp("profile", $user->image).'" style="width:60px; height:60px;">
                                                                                                                       </td>
                                                                                                                      <td valign="middle" align="left" width="75%" class="tdPart">
                                                                                                                          <a href="javascript:void();" style="color: #3879bd !important;"><span style="    display: block;
                                            font-weight: 600;font-family: "Lato", sans-serif"; display:inline-block; color: #3879bd !important; >'.$user->name.'</span></a><br>
                                                                                                                          <span style="    display: block;
                                            color: #333;
                                            font-size: 14px;font-family: "Lato", sans-serif;" display:inline-block;>'.$user->location.'</span>
                                                                                                                       </td>
                                                                                                                   </tr>
                                                                                                                </tbody>
                                                                                                             </table>
                                                                                                          </td>
                                                                                                       </tr>
                                                                                                    </tbody>
                                                                                                 </table>
                                                                                              </div>
                                                                                          </td>
                                                                                       </tr>
                                                                                    </tbody>
                                                                                 </table>
                                                                              </td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td width="100%" class="blk_container bmeHolder" name="bmePreFooter" valign="top" align="center" style="color: rgb(56, 56, 56); border: 0px none transparent; background-color: rgb(255, 255, 255);" bgcolor="#ffffff">
                                                     <br><br> <div id="dv_5" class="blk_wrapper" style="">
                                                         <table width="600" cellspacing="0" cellpadding="0" border="0" class="blk" name="blk_text">
                                                            <tbody>
                                                               <tr>
                                                                  <td>
                                                                     <table cellpadding="0" cellspacing="0" border="0" width="100%" class="bmeContainerRow">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td class="tdPart" valign="top" align="center">
                                                                                 <table cellspacing="0" cellpadding="0" border="0" width="600" name="tblText" style="float:left; background-color:transparent;" align="left" class="tblText">
                                                                                    <tbody>
                                                                                       <tr>
                                                                                          <td valign="top" align="left" name="tblCell" style="color: #3879bd !important; font-weight:bold; font-weight:bold; padding: 20px; font-family: "Lato", sans-serif; font-size: 14px; font-weight: 400; color: #3879bd !important; font-weight:bold; text-align: left;" class="tblCell">
                                                                                             <div style="line-height: 200%;" style="padding:0px 20px; color: #3879bd !important; font-weight:bold;"><span style="    font-size: 14px;
    ont-family: "Lato", sans-serif;
    color: #3879bd;
    line-height: 200%;
    display:inline-block;
    font-weight: 900;"><a href="javascript:void();" style="color: #3879bd; font-weight:bold; display:inline-block;">Regards</a></span><br><span style="    font-size: 14px;
   font-family: "Lato", sans-serif;
    color: #3879bd;
    line-height: 200%;
    display:inline-block;
    font-weight: 900;"><a href="javascript:void();" style="color: #3879bd; font-weight:bold; display:inline-block;">Team Linqq</a></span></div>
                                                                                          </td>
                                                                                       </tr>
                                                                                    </tbody>
                                                                                 </table>
                                                                              </td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                
                                                      
                                                  <!----------------------------------------------------->
                                                      
                                                      <div id="dv_8" class="blk_wrapper" style="padding:0px 20px;">
                                                         <table cellspacing="0" cellpadding="0" border="0" id="bllbbbb"  width="1000"  align="center" class="blk" name="blk_divider" style="background: #f6f6f8;padding: 20px 10px;    background: #f6f6f8;
    padding: 20px 10px;
    margin: 0 auto;
    width: 600px;">   
    
    
                                                            <tbody>
                                                               <tr>
                                                                  <td>
                                                                     <table cellpadding="0" cellspacing="0" border="0" class="bmeContainerRow">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td class="tdPart" valign="top" align="center">
                                                                                 <table cellspacing="0" cellpadding="0" border="0" width="1000" style="    background: #f6f6f8;
    padding: 20px 10px;
    margin: 0 auto;
    width: 600px;" name="tblText" style="" align="center" class="tblText">
                                                                                    <tbody>
                                                                                       <tr>
                                                                                          <td valign="top" align="center" name="tblCell" style="padding: 10px 20px; font-family: "Lato", sans-serif; font-size: 14px; font-weight: 400; color: rgb(56, 56, 56); text-align: center;" class="tblCell">
                                                                                             <div style="text-align: center;margin-bottom: 0px;">
                                                                                                 <span style="font-size: 16px; font-family: "Lato", sans-serif; color: #333; line-height: 150%;"><b><a href="javascript:void();" style="display:inline-block; color:#333 !important;">www.linqq.io</a></b></span>  <br>
                                                                                                 <span style="font-size: 16px; font-family: "Lato", sans-serif; color: #333; line-height: 150%;"><b><a href="javascript:void();" style="display:inline-block; color:#333 !important;">Support@linqq.io</a></b></span> 
                                                                                                <br>
                                                                                             </div>
                                                                                          </td>
                                                                                       </tr>
                                                                                    </tbody>
                                                                                 </table>
                                                                              </td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                            <tbody>
                                                                           <tr>
                                                                              <td class="tdItemContainer" style="margin-top:20px">
                                                                                 <table cellspacing="0" cellpadding="0" border="0" class="mblSocialContain" style="table-layout: auto;     text-align: center;
    margin: auto;">
                                                                                    <tbody>
                                                                                       <tr>
                                                                                          <td valign="top" name="bmeSocialTD" class="bmeSocialTD">
                                                                                             <!--[if gte mso 6]>
                                                                                          </td>
                                                                                          <td align="left" valign="top">
                                                                                             <![endif]-->    
                                                                                             <table cellspacing="0" cellpadding="0" border="0" class="bmeFollowItem" type="facebook" style="float: left; display: block;" align="left">
                                                                                                <tbody>
                                                                                                   <tr>
                                                                                                      <td align="left" class="bmeFollowItemIcon" gutter="20" width="24" style="padding-right:20px;height:20px;">    
                                                                                                         <a href="https://www.facebook.com/Linqq.io/" target="_blank" style="display: inline-block;">
                                                                                                         <img src="'.url("/public/img/facebook.png").'" border="0" width="24" height="24"></a>
                                                                                                      </td>
                                                                                                   </tr>
                                                                                                </tbody>
                                                                                             </table>
                                                                                             <!--[if gte mso 6]>
                                                                                          </td>
                                                                                          <td align="left" valign="top">
                                                                                             <![endif]-->    
                                                                                             <table cellspacing="0" cellpadding="0" border="0" class="bmeFollowItem" type="twitter" style="float: left; display: block;" align="left">
                                                                                                <tbody>
                                                                                                   <tr>
                                                                                                      <td align="left" class="bmeFollowItemIcon" gutter="20" width="24" style="padding-right:20px;height:20px;">    
                                                                                                         <a href="https://www.linkedin.com/company/linqqapp/" target="_blank" style="display: inline-block;"><img src="'.url("/public/img/linkedin.png").'" border="0" width="24" height="24"></a>
                                                                                                      </td>
                                                                                                   </tr>
                                                                                                </tbody>
                                                                                             </table>
                                                                                             <!--[if gte mso 6]>
                                                                                          </td>
                                                                                          <td align="left" valign="top">
                                                                                             <![endif]-->    
                                                                                             <table cellspacing="0" cellpadding="0" border="0" class="bmeFollowItem" type="email" style="float: left; display: block; padding-right: 20px;
}" align="left">
                                                                                                <tbody>
                                                                                                   <tr>
                                                                                                      <td align="left" class="bmeFollowItemIcon" gutter="20" width="24" style="height:20px;">    
                                                                                                         <a href="https://www.instagram.com/linqq.io/" style="display: inline-block;" target="_blank" ><img src="'.url("/public/img/instagram.png").'" alt="Email" style="display: block; max-width: 114px; margin-right: 20px;" border="0" width="24" height="24"></a>
                                                                                                      </td>
                                                                                                   </tr>
                                                                                                </tbody>
                                                                                             </table>  
                                                                                             <table cellspacing="0" cellpadding="0" border="0" class="bmeFollowItem" type="email" style="float: left; display: block;" align="left">
                                                                                                <tbody>
                                                                                                   <tr>
                                                                                                      <td align="left" class="bmeFollowItemIcon" gutter="20" width="24" style="height:20px;">    
                                                                                                         <a href="https://twitter.com/Linqq_io" style="display: inline-block;" target="_blank"><img src="'.url("/public/img/twitter.png").'" alt="Email" style="display: block; max-width: 114px;" border="0" width="24" height="24"></a>
                                                                                                      </td>
                                                                                                   </tr>
                                                                                                </tbody>
                                                                                             </table>
                                                                                          </td>
                                                                                       </tr>
                                                                                    </tbody>
                                                                                 </table>
                                                                              </td>
                                                                           </tr>
                                                                        </tbody>
                                                         </table>
                                                      </div>
                                                      <!----------------------------------------------------->
                                                       
                                                      
                                                       
                                                 
                                                      
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                    
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
   </body>
</html>
';    


  $message  = '<div bgcolor="#f4f8fc" marginwidth="0" marginheight="0" style="margin:0;padding:0;background:#f4f8fc">
<table style="background:#f4f8fc;width:100%!important" width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#f4f8fc"><tbody><tr><td>

 <table style="min-width:450px!important;max-width:620px!important;width:100%!important;margin:auto" class="m_-1255399300471218352m_-7251330376895672873m_3466839749643232769email-container" width="620" cellspacing="0" cellpadding="0" border="0" align="center">

    <tbody><tr>
    <td>
        
        <table style="width:100%!important;border-bottom:solid 2px #f1f3f5;background:#ffffff" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
          <tbody><tr>
            <td style="font-size:0;line-height:0" width="50%" height="30">&nbsp;</td>
            <td style="font-size:0;line-height:0" width="50%" height="30">&nbsp;</td>
          </tr>
          <tr>
            <td style="text-align:center" colspan="2" width="100%" valign="center">
                <center><a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://www.shapr.net&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNHwi8xTsaT8hvSLKHkXMDqSrj4uAg"><img src="'.url("/public/img/logo_5.png").'" alt="Shapr" style="display:block" width="84" height="32" border="0" class="CToWUd"></a></center>
            </td>
          </tr>
          <tr>
            <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
            <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
          </tr>
        </tbody></table>
        
    </td>
</tr> 
   
     <tr>
    <td>
        
       <table style="width:100%!important;background:#2f6ca6" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#2F6CA6"> 
            <tbody><tr>
                <td style="font-size:0;line-height:0" width="50%" height="30">&nbsp;</td>
                <td style="font-size:0;line-height:0" width="50%" height="30">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align:center;font-family:sans-serif;font-size:13px;color:#ffffff" width="100%" valign="center">

                    <table cellspacing="0" cellpadding="5" border="0" align="center">
                        <tbody><tr>
                            <td><img src="https://ci5.googleusercontent.com/proxy/v9TMbJ9GGvODy90kKGYzoHrESFEvhNpfdgDYnNuVT5WcPVQtfvWtqLklALR0pyU_wIBsNsIs7NxocXU-u5Xu=s0-d-e1-ft#http://ws.shapr.net/front/images/Chat.png" class="CToWUd"></td>
                            <td style="font-family:sans-serif;font-size:32px;color:#ffffff"><b>Hi, I think you should try Linqq, the app where you connect inspiring professionals every day. Get it here....</b></td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
            <tr>
                <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
                <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
            </tr>
        </tbody></table>
      </td>  
    </tr>
<tr>

<td>
    <table style="background:#12283d;width:100%!important" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#12283d">
      <tbody>
          
       <tr>
        <td style="font-size:0;line-height:0" height="40">&nbsp;</td>
      </tr>
      <tr>
        <td style="font-family:sans-serif;font-size:16px;color:#ffffff" valign="middle" align="center">
            <a href="https://play.google.com" target="_blank"><img src="https://www.mobulous.com/assets/img/feature-project/bg/google-play.png" style="    width: 75%;">  </a>     </td>
          <td style="font-family:sans-serif;font-size:16px;color:#ffffff" valign="middle" align="center">
            <a href="https://itunes.apple.com" target="_blank"><img src="https://www.mobulous.com/assets/img/feature-project/bg/app-store.png"  style="    width: 75%;">     </a>  </td>
          <td style="font-family:sans-serif;font-size:16px;color:#ffffff" valign="middle" align="center">
            <a href="http://linqqapp.com/"><img src="https://www.mobulous.com/assets/img/feature-project/bg/website.png"  style="    width: 75%;">      </a> </td>
      </tr>
      <tr> 
        <td style="font-size:0;line-height:0" height="40">&nbsp;</td>
      </tr>
</tbody></table>
<tr>
  <td>
    <table style="border-bottom:solid 1px #e2e9ef;background:#ffffff;width:100%!important" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
      <tbody><tr>
        <td colspan="3" style="font-size:0;line-height:0" height="50">&nbsp;</td>
      </tr>
      <tr> 
        <td style="text-align:left;padding-left:36px" class="m_-1255399300471218352m_-7251330376895672873m_3466839749643232769content-padded-left" valign="middle">
          <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://www.shapr.net&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNHwi8xTsaT8hvSLKHkXMDqSrj4uAg"><img src="'.url("/public/img/logo_5.png").'" alt="Shapr" width="84" height="32" border="0" class="CToWUd"></a>
        </td>
        <td style="text-align:left;font-size:12px;color:#606c76;font-family:sans-serif;white-space:nowrap" valign="middle">&nbsp;</td>
        <td style="text-align:right;padding-right:36px" class="m_-1255399300471218352m_-7251330376895672873m_3466839749643232769content-padded-right" valign="middle"> 
          
          <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://www.facebook.com/ShaprApp&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNH5JT4AAWMS_Gi3tk6N4o_eeNy9Mg"><img src="https://ci3.googleusercontent.com/proxy/46FKFGM-8-H6ucqfv1acrmZUWXrYTX6EIsmG5BZZzeHkE-q8zt1caX-Q4NDnZ-bqHoYSb72_WsA4wz1CVZXP7_sZ-sAJHSEWDIc=s0-d-e1-ft#http://ws.shapr.net/front/emails/social-facebook.png" alt="Facebook" width="32" height="32" border="0" class="CToWUd"></a>&nbsp;
          <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://twitter.com/weareshapr&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNGDiPxh00M54w_fnmHOJ65UUUr1PA"><img src="https://ci3.googleusercontent.com/proxy/o1cMaAilevsLR04C4mKjru0nXtNp2F1b2nTzUp74lG9dGe2s1rKxOzXse1VP5jxcWqPe26juTgYQLqm3nE97YbSgvf8kD_DGOw=s0-d-e1-ft#http://ws.shapr.net/front/emails/social-twitter.png" alt="Twitter" width="32" height="32" border="0" class="CToWUd"></a>&nbsp;
          <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://plus.google.com/%2BShaprNet&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNH8LQk6IW7qqCzsK5U4J-0bhKf12Q"><img src="https://ci4.googleusercontent.com/proxy/SMh2etWUm4-OEvKG7SW6H4QbH-VtuqTgb3taS15uHzEjVw9wWotK8h_-98DjdIG8jXUqHUjw5YmAJWV4HQ3VqvNswTnEm2I=s0-d-e1-ft#http://ws.shapr.net/front/emails/social-gplus.png" alt="Google Plus" width="32" height="32" border="0" class="CToWUd"></a>
        </td>
      </tr>
      <tr>
        <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
        <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
        <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
      </tr>
    </tbody></table>
    
  </td>
</tr>
      <tr>
        <td style="font-size:0;line-height:0" height="46">&nbsp;</td>
      </tr>
    </tbody></table>
  </td>
</tr>
  </tbody></table>
            </td></tr></tbody></table><div class="yj6qo"></div><div class="adL">
</div></div>'; 
                    
              try{
                    // //echo $key;die;
                    $to      = (string)$key;
                    $subject = 'Linqq App Invitation.'; 
                    $message =  $mssg; 
                    //$message  = "This is Inviation Link Content..";
                    //$headers   =  'From: info@mobulous.co.in' ."\r\n" . 'X-Mailer: PHP/' . phpversion(); 
                    $headers1  =  "From: info@mobulous.co.in \r\n"; 
                    //$headers1 .= "Reply-To: info@mobulous.co.in \r\n";
                    $headers1 .= "MIME-Version: 1.0 \r\n";
                    $headers1 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                    mail($to, $subject, $message, $headers1);
                }
                catch(\Exception $e)
                {

                }
                    



               
 
// $email = new \SendGrid\Mail\Mail(); 
// $email->setFrom("support@linqqapp.com", "Team Linqq");
// $email->setSubject("Linqq App Invitation");
// $email->addTo($to, "Team Linqq");
// $email->addContent(
//     "text/html", $mssg
// );
// $sendgrid = new \SendGrid("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");
// try {
//     $response = $sendgrid->send($email);
//    // print $response->statusCode() . "\n";
//     //print_r($response->headers());
//     //print $response->body() . "\n";
// } catch (Exception $e) {
//    // echo 'Caught exception: '. $e->getMessage() ."\n";
// }
                      

                       //$headers = 'From: info@mobulous.co.in' ."\r\n" . 'X-Mailer: PHP/' . phpversion(); 
                       /* $headers = "From: " . strip_tags($_POST['req-email']) . "\r\n"; 
                       $headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n"; 
                       $headers .= "CC: susan@example.com\r\n"; */
                       //$headers = "MIME-Version: 1.0\r\n";
                       //$headers  = 'X-Mailer: PHP/' . phpversion().'\r\n'; 
                       //$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                    
                    /*if(mail($to, $subject, $message, $headers1)) {
                    
                       echo "Sent";
                     
                    } else {
                         
                          echo "Not sent";
                    }

                 die;*/
                                   
           }     
          
         $this->obj->set_data("200", "Sent successfully .", $request->path());
     }      
 
   } catch (Exception $e) { 

 
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
  
  }     

 public function already_sent(Request $request)
   {
      try { 
                $user_id = Auth::user()->id; 
                $already = Invite::select('email')->where('user_id', $user_id)->get();
                $this->obj->set_data("200", "Email List.", $request->path(), 'response', $already);
        
         } catch (Exception $e) { 
       
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
  
   }  
 
 
  public function update_single(Request $request)
    
    {
    
       try {  

               $rules = array(
                'not_id' => 'required',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->formatValidationErrors($validator);
                $error = reset($error);
                $this->obj->error_message('403', $error[0], $request->path());
            } else {

              $user_id = Auth::user()->id; 
             
             
               //$already = Invite::select('email')->where('user_id', $user_id)->get();
               //$this->obj->set_data("200", "Email List.", $request->path(), 'response', $already);
              $ncount     = UserNotification::where('receiver_id', $user_id)->where('status','Unread')->count(); 
              if($ncount != 0 ){

                $badge['count'] = $ncount-1;
              
              } else {

                  $badge['count'] = 0;
              }

              UserNotification::where('id', $request->not_id)->update(['status' =>'Read']);
           
            $this->obj->set_data("200", "Status Updated.", $request->path(),'response', $badge);
          }
       
        } catch (Exception $e) { 
       
            $this->obj->error_message('500', "Internal error.", $request->path());
        }
  
    }   


  public function send_template(Request $request) {   

          $user_id = Auth::user()->id; 
   
          $user = User::where('id',$user_id)->first();
         if(!empty($user->email) && ($user->notification == "ON")) { 

           $mssg='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

                <html>
      <body><div bgcolor="#f4f8fc" marginwidth="0" marginheight="0" style="margin:0;padding:0;background:#f4f8fc">
<table style="background:#f4f8fc;width:100%!important" width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#f4f8fc"><tbody><tr><td>
  <table style="min-width:450px!important;max-width:620px!important;width:100%!important;margin:auto" class="m_-1255399300471218352m_-7251330376895672873m_3466839749643232769email-container" width="620" cellspacing="0" cellpadding="0" border="0" align="center">
   <tbody><tr>
 <td>
        <table style="width:100%!important;border-bottom:solid 2px #f1f3f5;background:#ffffff" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
          <tbody><tr>
            <td style="font-size:0;line-height:0" width="50%" height="30">&nbsp;</td>
            <td style="font-size:0;line-height:0" width="50%" height="30">&nbsp;</td>
          </tr>
          <tr>
            <td style="text-align:center" colspan="2" width="100%" valign="center">
                <center><a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://www.shapr.net&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNHwi8xTsaT8hvSLKHkXMDqSrj4uAg">
                <img src="'.url("/public/img/logo_5.png").'" alt="Linqq" style="display:block" width="126" height="30" border="0" class="CToWUd"></a></center>
            </td>
          </tr>
          <tr>
            <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
            <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
          </tr>
        </tbody></table>
        
    </td>
</tr>
    <tr>
    <td>
        <table style="width:100%!important;background:#2f6ca6" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#2F6CA6">
            <tbody><tr>
                <td style="font-size:0;line-height:0" width="50%" height="30">&nbsp;</td>
                <td style="font-size:0;line-height:0" width="50%" height="30">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align:center;font-family:sans-serif;font-size:13px;color:#ffffff" width="100%" valign="center">

              </td>
            </tr>
            <tr>
                <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
                <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
            </tr>
        </tbody></table>        
    </td>
    </tr>
    <tr> 
  <td> 
    <table style="background:#12283d;width:100%!important" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#12283d">
      <tbody><tr>
        <td style="font-size:0;line-height:0" height="46">&nbsp;</td>
      </tr>
      <tr>
      
          <td style="font-family:sans-serif;font-size:16px;color:#ffffff" valign="middle" align="center">
          Congratulations, Deepak!
          </td>
      </tr>
      <tr>
        <td style="font-size:0;line-height:0" height="12">&nbsp;</td>
      </tr>

      <tr> 
        <td style="font-size:0;line-height:0" height="40">&nbsp;</td>
      </tr>
      <tr><td width="50" valign="middle" align="center">
          
        <img alt="Userimage" src="'.$this->obj->is_filetemp("profile", Auth::user()->image).'" style="    border-radius: 100%;
    width: 100px;
    height: 100px;" class="CToWUd">
      </td>
      </tr><tr>
        <td style="font-size:0;line-height:0" height="20">&nbsp;</td>
      </tr>
        <tr>
        <td style="font-family:sans-serif;font-size:25px;color:#ffffff;padding-right:36px;padding-left:36px" valign="middle" align="center">'.Auth::user()->designation.'</td>  
      </tr>
      <tr> 
        <td style="font-size:0;line-height:0" height="10">&nbsp;</td>
      </tr> 
       <tr>
        <td style="font-size:0;line-height:0" height="20">&nbsp;</td>
      </tr>
      <tr> 
        <td style="font-size:0;line-height:0" height="46">&nbsp;</td>
      </tr>
    </tbody></table>
    
  </td>
</tr>
     <tr>
     
        <td>
             <table style="width:100%!important;background:#e5e5e5" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#E5E5E5">
            
               <tbody>
                     <tr>
                     <td style="font-size:0;line-height:0" width="50%" height="15">&nbsp;</td>
                     <td style="font-size:0;line-height:0" width="50%" height="15">&nbsp;</td>
                     </tr>
                <tr>
                    <td style="text-align:left;font-family:sans-serif;font-size:13px;color:#ffffff" width="100%" valign="center">

                        <table style="width:100%!important" width="100%" cellspacing="0" cellpadding="5" border="0">
                            <tbody><tr>
                                <td width="2%"></td>
                               
                                <td style="font-family:sans-serif;font-size:15px;color:#000">You have just joined a global community of professionals who believe that the craziest life opportunities come from meeting new people.<p>&nbsp;</p>

At Shapr, our goal is to spark meaningful, mutually beneficial conversations that evolve into long-term relationships. I truly believe you will be inspired through your conversations if you approach networking with an open mind, a curious attitude, and an eagerness to help others learn and succeed.
<p>&nbsp;</p>
Welcome to Linqq, Sunil!
</td>

                                <td width="2%"></td>
                            </tr>
                        </tbody></table>
              </td>
            </tr>
                <tr>
                    <td style="font-size:0;line-height:0" height="15">&nbsp;</td>
                    <td style="font-size:0;line-height:0" height="15">&nbsp;</td>
                </tr>
            </tbody></table>
            

        </td>
    </tr>


            <tr>
  <td>
    
    <table style="border-bottom:solid 1px #e2e9ef;background:#ffffff;width:100%!important" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
      <tbody><tr>
        <td colspan="3" style="font-size:0;line-height:0" height="50">&nbsp;</td>
      </tr>
      <tr>
        <td style="text-align:left;padding-left:36px" class="m_-1255399300471218352m_-7251330376895672873m_3466839749643232769content-padded-left" valign="middle">
          <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://www.shapr.net&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNHwi8xTsaT8hvSLKHkXMDqSrj4uAg"><img src="'.url("/public/img/logo_5.png").'" alt="Linqq" width="84" height="25" border="0" class="CToWUd"></a>
        </td>

        <td style="text-align:left;font-size:12px;color:#606c76;font-family:sans-serif;white-space:nowrap" valign="middle">&nbsp;</td>
        <td style="text-align:right;padding-right:36px" class="m_-1255399300471218352m_-7251330376895672873m_3466839749643232769content-padded-right" valign="middle">
          <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://itunes.apple.com/us/app/shapr/id859091569?mt%3D8&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNGZz6HUgvOTwHST7283NsGT3JV9DQ"><img src="https://ci6.googleusercontent.com/proxy/0NZeXk4LPNZRWvbEqWEP8ad9Ze3p4l8rjev4cx-KQLkJ7YBZPmcis6JVkC2PbB-OWsd4Fbkb26nJmKJEinWwGLVQx7CBuUk=s0-d-e1-ft#http://ws.shapr.net/front/emails/social-apple.png" alt="Apple" width="32" height="32" border="0" class="CToWUd"></a>&nbsp;
          <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://play.google.com/store/apps/details?id%3Dcom.shapr&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNGQATgICekqfBhimlyyS9us2poyCw"><img src="https://ci3.googleusercontent.com/proxy/Cmj9Gqo1YmIHxxwdGqHTYQqdu_kmZkIpnp9XKlKXFG2r0dCYwBIpJTgq5ReZuLxPa1B5WDlagqb50R8Dxl3CYQFrJCYbQ1cDww=s0-d-e1-ft#http://ws.shapr.net/front/emails/social-android.png" alt="Android" width="32" height="32" border="0" class="CToWUd"></a>&nbsp;
          <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://www.facebook.com/LinqqApp&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNH5JT4AAWMS_Gi3tk6N4o_eeNy9Mg"><img src="https://ci3.googleusercontent.com/proxy/46FKFGM-8-H6ucqfv1acrmZUWXrYTX6EIsmG5BZZzeHkE-q8zt1caX-Q4NDnZ-bqHoYSb72_WsA4wz1CVZXP7_sZ-sAJHSEWDIc=s0-d-e1-ft#http://ws.shapr.net/front/emails/social-facebook.png" alt="Facebook" width="32" height="32" border="0" class="CToWUd"></a>&nbsp;
          <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://twitter.com/weareshapr&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNGDiPxh00M54w_fnmHOJ65UUUr1PA"><img src="https://ci3.googleusercontent.com/proxy/o1cMaAilevsLR04C4mKjru0nXtNp2F1b2nTzUp74lG9dGe2s1rKxOzXse1VP5jxcWqPe26juTgYQLqm3nE97YbSgvf8kD_DGOw=s0-d-e1-ft#http://ws.shapr.net/front/emails/social-twitter.png" alt="Twitter" width="32" height="32" border="0" class="CToWUd"></a>&nbsp;
          <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://plus.google.com/%2LinqqNet&amp;source=gmail&amp;ust=1530003060432000&amp;usg=AFQjCNH8LQk6IW7qqCzsK5U4J-0bhKf12Q"><img src="https://ci4.googleusercontent.com/proxy/SMh2etWUm4-OEvKG7SW6H4QbH-VtuqTgb3taS15uHzEjVw9wWotK8h_-98DjdIG8jXUqHUjw5YmAJWV4HQ3VqvNswTnEm2I=s0-d-e1-ft#http://ws.shapr.net/front/emails/social-gplus.png" alt="Google Plus" width="32" height="32" border="0" class="CToWUd"></a>
        </td>
      </tr>
      <tr>
        <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
        <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
        <td style="font-size:0;line-height:0" height="30">&nbsp;</td>
      </tr>
    </tbody></table>
    
  </td>
</tr>
        

      </tbody></table>  
  
            </td></tr></tbody></table><div class="yj6qo"></div><div class="adL">
</div></div></body></html>';   
                      
                    $to      = "deepak.chand@mobulous.com"; 
                    $subject = 'Welcome User';  
                    $message =  $mssg;  
                    $headers1 = "From:  info@mobulous.co.in \r\n"; 
                    //$headers1 .= "Reply-To: info@mobulous.co.in \r\n";
                    $headers1 .= "MIME-Version: 1.0 \r\n";
                    $headers1 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                    mail($to, $subject, $message, $headers1);  


// $email = new \SendGrid\Mail\Mail(); 
// $email->setFrom("support@linqqapp.com", "Team Linqq");
// $email->setSubject("Linqq App Invitation");
// $email->addTo($to, "Team Linqq");
// $email->addContent(
//     "text/html", $mssg
// );
// $sendgrid = new \SendGrid("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");
// try {
//     $response = $sendgrid->send($email);
//    // print $response->statusCode() . "\n";
//     //print_r($response->headers());
//     //print $response->body() . "\n";
// } catch (Exception $e) {
//     echo 'Caught exception: '. $e->getMessage() ."\n";
// }


        } 
   
     }





public function check_mail()
{

ini_set('SMTP','smtp.zoho.com');
ini_set('smtp_port',465);
ini_set('sendmail_from', 'support@linqq.io');

//define the receiver of the email
$to = 'manishsolanki1989@gmail.com';
//define the subject of the email
$subject = 'Test for title'; 
//define the message to be sent. Each line should be separated with \n
$message = 'Message to send'; 

//define the headers we want passed. Note that they are separated with \r\n
$headers = 'From: support@linqq.io\r\nReply-To:support@linqq.io';

//send the email
$mail_sent = mail($to, $subject, $message, $headers);
mail($to, $subject, $message, $headers);

//if the message is sent successfully print "Mail sent correctly". Otherwise print "Mail failed" 
echo $mail_sent ? "Mail sent" : "Mail failed";
}




 }  



