<?php

return [
  'gcm' => [
      'priority' => 'normal',
      'dry_run' => true,
      'apiKey' => 'AIzaSyBQzgxWDDe2u_UEk8VKbC5Bg6CUfrVTIUY',
  ],
  'fcm' => [
        'priority' => 'normal',
        'dry_run' => true,
        'apiKey' => 'AIzaSyBQzgxWDDe2u_UEk8VKbC5Bg6CUfrVTIUY',
  ],
  'apn' => [
      'certificate' => public_path() . '/pem/pushcert.pem',
      'passPhrase' => '123456', //Optional
      'passFile' => '', //Optional
      'dry_run' => true
  ]
];