@extends('layouts.admins') 
@section('title', 'Player List')
@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Player List
                </h3>
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Date of account creation
                            </th>
                            <th>
                                Team name
                            </th>
                            <th>
                                Team code
                            </th>
                            <th>
                                Position
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Profile Image
                            </th>
                            <th>
                                Action
                            </th>
                        </tr>
                    </thead>
                    @if(!empty($users))
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>
                                {{++$i}}
                            </td>
                            <td>
                                {{$user->name}}
                            </td>
                            <td>
                                {{$user->created_at}}
                            </td>
                            <td>
                                {{$user->team_name}}
                            </td>
                            <td>
                                {{$user->team_code}}
                            </td>
                            <td>
                                {{$user->position}}
                            </td>
                            <td>
                                {{$user->email}}
                            </td>
                            <td>
                                <img alt="Avatar" class="img-circle" src="{{$user->image}}" style="height:50px">
                                </img>
                            </td>
                            <td>
                                <a class="action_an" href="javascript::void(0)" onclick="delete_user({{$user->id}})">
                                    <span class="dlt_icon">
                                        <img class="img-responsive" src="{{url('/public')}}/img/delete-button.png"/>
                                    </span>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
                {!! $users->links('admins.pagination') !!}
            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
</div>
<script type="text/javascript">
    function delete_user(id){
        if (confirm('Are you sure you want to delete.') == true) {
            $.ajax({
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'common_delete',
                datatType : 'json',
                type: 'POST',
                data: {
                    id:id,
                    table:'users'
                },
                cache: false,
                //contentType: false,
                //processData: false,
                success:function(response) {
                    if (response) {
                        location.reload();
                    }
                }
            });
        }else{
            return false;
        }       
    }
</script>
@endsection