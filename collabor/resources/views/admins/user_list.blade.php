@extends('layouts.admins') 
@section('title', 'User List')
@section('content')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
<div class="row">
</div>
    <div class="col-md-12">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    User List
                </h3>
            </div>
            <div class="panel-body">

            <button style="float: right;" onclick="get_card_count()" class="btn btn-warning" data-toggle="modal" data-target="#myModal3"><i class="glyphicon glyphicon-plus"></i>Card Count</button>

                 <button style="float: right;" onclick="get_radius_count()" class="btn btn-warning" data-toggle="modal" data-target="#myModal2"><i class="glyphicon glyphicon-plus"></i>Radius range</button>
                 
                <table class="table table-bordered" id="data">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Name
                            </th>
                             <th>
                                Email.
                            </th> 
                            <th>
                                Phone No.
                            </th>
                            <th>
                                Gender
                            </th>
                            <th>
                                Designation
                            </th>
                            <th>
                                Organisation
                            </th>
                            <th>
                                Created On
                            </th>
                            <th>
                                Profile Image
                            </th>
                            <th>
                                Action
                            </th>
                        </tr>
                    </thead>
                    @if(!empty($users))
                    <tbody>
                        <?php $i=0;?>
                        @foreach($users as $user)
                        <tr>
                            <td>
                                {{ ++$i }}
                            </td>
                            <td>
                                {{$user->name}}
                            </td>

                             <td>
                                 {{$user->email}}
                             </td> 
                            <td>
                                {{$user->phone}}
                            </td>
                            <td>
                                {{$user->gender}}
                            </td>
                            <td>
                                {{$user->designation}}
                            </td>
                            <td>
                                {{$user->organisation}}
                            </td>
                             <td>
                                {{$user->created_on}}
                            </td>
                            <td>
                                <img alt="Avatar" class="img-circle" src="{{$user->image}}" style="height:50px">
                                </img>
                            </td>
                            <td>
                                <a class="action_an" href="{{url('admin/view_user')}}/{{Crypt::encrypt($user->id)}}">
                                    <span class="dlt_icon">
                                        <img class="img-responsive" src="{{url('/public')}}/img/eye.png"/>
                                    </span>
                                </a>
                                <a class="action_an" href="javascript::void(0)" onclick="delete_user({{$user->id}})">
                                    <span class="dlt_icon">
                                        <img class="img-responsive" src="{{url('/public')}}/img/delete-button.png"/>
                                    </span>
                                </a>
                                
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
       
        <!-- END BORDERED TABLE -->
    </div>
</div>


     <!-- Bootstrap modal -->
  <div class="modal fade" id="myModal2" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" id="btnClose" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Radius range</h3>
      </div>
      <form action="{{url('admin/add_radius')}}" method="post" id="form" class="form-horizontal" enctype="multipart/form-data">
        <div class="modal-body form">
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">Radius range</label>
              <div class="col-md-9">
                {{ csrf_field() }}
                <input name="count" id="ads_count" placeholder="Ads Count" class="form-control" type="number" value="{{old('count')}}">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave"  class="btn btn-primary">Save</button>
            <button type="button" id="btnCancle" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
</div>
    <!-- END BORDERED TABLE -->

<div class="modal fade" id="myModal3" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" id="btnClose" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Card Count</h3>
      </div>
      <form action="{{url('admin/add_count')}}" method="post" id="form" class="form-horizontal" enctype="multipart/form-data">
        <div class="modal-body form">
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">Card Count</label>
              <div class="col-md-9">
                {{ csrf_field() }}
                <input name="c_count" id="cards_count" placeholder="Card Count" class="form-control" type="number" value="{{old('count')}}">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave"  class="btn btn-primary">Save</button>
            <button type="button" id="btnCancle" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
</div>


 
</div>
    <!-- END BORDERED TABLE -->

<script type="text/javascript">

    function delete_user(id){
        if (confirm('Are you sure you want to delete.') == true) {
            $.ajax({
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'common_delete',
                datatType : 'json',
                type: 'POST',
                data: {
                    id:id,
                    table:'users'
                },
                cache: false,
                success:function(response) {
                    if (response) {
                        location.reload();
                    }
                }
            });
        }else{
            return false;
        }       
    }

    function get_radius_count(){
        
            $.ajax({
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'common_get',
                datatType : 'json',
                type: 'POST',
                data: {
                    id:1,
                    table:'location_radii'
                },
                cache: false,
                success:function(response) {
                    if (response) { 
                        response = jQuery.parseJSON(response);
                        $("#ads_count").val(response.radius);
                    }
                }
            });
        
    }

function get_card_count() { 
        
            $.ajax({
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'card_get', 
                datatType : 'json',
                type: 'POST',
                data: {
                    id:1,
                    table:'cards'
                },
                cache: false,
                success:function(response) {

                    if (response) {  
                        response = jQuery.parseJSON(response);
                        $("#cards_count").val(response.count);
                    }
               } 
          });
    }


    $('#data').dataTable({
     "processing": true,
     dom: 'Bfrtip',
        buttons: [
            
            {
                extend: 'excelHtml5',
               exportOptions: {
                    columns: [ 0,1,2,3,4,5,6 ]
                }
            },
          
            'colvis'
        ]

        });
</script>
@endsection