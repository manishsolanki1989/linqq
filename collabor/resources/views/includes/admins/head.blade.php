 <link href="{{ asset('public/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"></link>
<link href="{{ asset('public/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"></link>
<link href="{{ asset('public/vendor/linearicons/style.css') }}" rel="stylesheet"></link>
<link href="{{ asset('public/css/main.css') }}" rel="stylesheet"></link>
<link href="{{ asset('public/css/demo.css') }}" rel="stylesheet"></link>
<link href="{{ asset('public/DataTables/datatables.min.css') }}" rel="stylesheet"></link>

<script type="text/javascript" src="{{ asset('public/vendor/jquery/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/vendor/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/scripts/klorofil-common.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/DataTables/datatables.min.js') }}"></script>
