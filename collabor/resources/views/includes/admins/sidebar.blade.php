<style type="text/css">
    .sidebar-scroll{overflow-y: scroll;
    height: 500px;
}




/* width */
.sidebar-scroll::-webkit-scrollbar {
    width: 10px;
}

/* Track */
.sidebar-scroll::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px grey; 
    border-radius: 10px;
}

/* Handle */
.sidebar-scroll::-webkit-scrollbar-thumb {
    background: #484848; 
    border-radius: 10px;
}


</style>

<!-- LEFT SIDEBAR -->
        <div id="sidebar-nav" class="sidebar">
            <div class="sidebar-scroll">
                <nav>
                    <?php $url=Request::url(); ?>
                    <ul class="nav">
                        
                        <li>
                            <a href="{{ url('admin/user_list') }}" <?php if(strpos($url, "admin/user_list")): ?> class="active"<?php  endif ?>><i class="lnr lnr-dice"></i> <span>User List</span></a>
                        </li>
                        <li>
                            <a href="{{ url('admin/interest_list')}}" <?php if(strpos($url, "admin/interest_list")): ?> class="active"<?php  endif ?>><i class="lnr lnr-dice"></i> <span>Interest List</span></a>
                        </li>
                         <li>
                            <a href="{{ url('admin/degree_list')}}" <?php if(strpos($url, "admin/degree_list")): ?> class="active"<?php  endif ?> ><i class="lnr lnr-dice"></i> <span>Degree List</span></a>
                        </li>
                        <li>
                            <a href="{{ url('admin/industry_list')}}" <?php if(strpos($url, "admin/industry_list")): ?> class="active"<?php  endif ?>><i class="lnr lnr-dice"></i> <span>Industry List</span></a>
                        </li>
                         <li>
                            <a href="{{ url('admin/company_list')}}" <?php if(strpos($url, "admin/company_list")): ?> class="active"<?php  endif ?>><i class="lnr lnr-dice"></i> <span>Company List</span></a>
                        </li>
                         <li>
                            <a href="{{ url('admin/designation_list')}}" <?php if(strpos($url, "admin/designation_list")): ?> class="active"<?php  endif ?>><i class="lnr lnr-dice"></i> <span>Designation List</span></a>
                        </li>
                        <li>
                            <a href="{{ url('admin/advertisement_list')}}" <?php if(strpos($url, "admin/advertisement_list")): ?> class="active"<?php  endif ?>><i class="lnr lnr-dice"></i> <span>Advertisement List</span></a>
                        </li>
                        <li>
                            <a href="{{ url('admin/message_template_list')}}" <?php if(strpos($url, "admin/message_template_list")): ?> class="active"<?php  endif ?>><i class="lnr lnr-dice"></i> <span>Message Tempalate List</span></a>
                        </li>
                        <li>
                            <a href="{{ url('admin/request_template_list')}}" <?php if(strpos($url, "admin/request_template_list")): ?> class="active"<?php  endif ?>><i class="lnr lnr-dice"></i> <span>Request Tempalate List</span></a>
                        </li>
                        <li>
                            <a href="{{ url('admin/user_report_list')}}" <?php if(strpos($url, "admin/user_report_list")): ?> class="active"<?php  endif ?>><i class="lnr lnr-dice"></i> <span>User Report List</span></a>
                        </li>
                        <li>
                            <a href="{{ url('admin/data_list') }}" <?php if(strpos($url, "admin/data_list")): ?> class="active"<?php  endif ?>><i class="lnr lnr-dice"></i> <span>User Analytical Report</span></a>
                        </li>

                         <li>
                            <a href="{{ url('admin/data_list_notification') }}" <?php if(strpos($url, "admin/data_list_notification")): ?> class="active"<?php  endif ?>><i class="lnr lnr-dice"></i> <span>Send Notification</span></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
<!-- END LEFT SIDEBAR -->

