
<!DOCTYPE html>
<html lang="en">
<head>
  <title>::My Doctor::</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
    
    <!--main style sheet-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<style type="text/css">
  @import url('https://fonts.googleapis.com/css?family=Poppins:200,300,300i,400,500,600,700,800,900');
@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800');

*{
    margin: 0px;
    padding: 0px;
}

body{
    overflow-x: hidden;
    font-family: 'Open Sans', sans-serif;
}


h1,
h2,
h3,
h4,
h5,
h6 {}

ul {
    margin: 0;
    padding: 0;
    list-style: none;
}

li {
    display: inline-block;
}



a:hover {
    text-decoration: none;
    -webkit-transition: .2s;
    transition: .2s;
    text-decoration: none;
}

h5 {
    color: #000;
    font-weight: 600;
    font-size: 17px;
    margin: 23px 0 9px 0;
}

h6 {
    font-weight: 600;
    text-transform: uppercase;
    font-size: 13px;
    text-decoration: underline;
}

ul li ul{
  padding-left:20px;
  list-style-type:disc;
}

.aboutus h1 {
    font-size: 6vh;
}


</style>    
</head>
<body>

<div class="col-md-12 aboutus">
  <div class="container">
      <div class="desc">
        <h1>About US</h1>
      <p>
      Linqq is a professional networking app, whose prior inducement is to connect people who are
      driven with a purpose. We profoundly focus on the three P’s, which are; Productivity, Possibility
      and Personality. Unlike other platforms, we are not entitled on showing the number of
      connections, rather we perceive meaningful professional connections, which would help your
      business to grow immensely.
      </p>

      <p>With Linqq, you can explore varied opportunities like; meeting new people who are a specialist
      in their respective fields, discuss the job or freelance opportunities, obtain a venture capital
      financing, examine business strategies, find people with different startup ideas, and so much
      more!</p>

      <p>The pre-eminent motive of Linqq is to cater to the needs of professionals and helping them find
      possibilities which could be massively favourable to them. When your ideas counterparts with
      someone else, your ‘could be’ automatically becomes your ‘would definitely be’.</p>

      <p>We aspire and intend to make Linqq an extensive global network, which plays a prominent part
      in making professionals meet. The ideation behind Linqq is to get professionals acquainted with
      each other, which are spread across heterogeneous geographical regions. At Linqq, we
      prominently focus on building a network of diverse people. We aim at procuring different and
      affirmative professionals at the right time and place, henceforth enabling brighter insights to
      your requirements.</p>
        </div>
    
    <div class="feature">
      <h5>Our chief features:</h5>
      <ul>
        <li>
          <ul>
            <li>
              <h6>PERSONALIZATION</h6>
              <p>Create your profile, and share informative things about yourself and your work experiences.</p>
            </li>
            
            <li>
              <h6>SUITABILITY</h6>
              <p>We find matches according to your location and interests, thus eliminating the wastage of people’s time.</p>
            </li>
            
            <li>
              <h6>OBJECTIVITY</h6>
              <p>Find people who are looking quintessentially for you, to turn your ‘can happen’ to ‘will definitely happen’.</p>
            </li>
            
            <li>
              <h6>SELECTION</h6>
              <p>Express yourself by choosing the automated options, or simply type whatever is in your mind.</p>
            </li>
            
            <li>
              <h6>SPECIFICATION</h6>
              <p>You will be specifically searched according to your skills, and interests, and therewith connecting to the right professional match.</p>
              <p>At Linqq, we want to inspire people from the inspired, and thus accelerating their growth. So, Linqq it up to discover new opportunities.</p>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</div>

   
</body>
</html>
