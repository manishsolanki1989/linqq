<!DOCTYPE html>
<html lang="en">
<head>
  <title>Travolong Privacy Policy</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <style>
        body{
            font-family: 'Lato', sans-serif;
        }
        
        .sec-onby-on-termscondition h1{
            font-size: 26px;
            margin: 0px;
        }
        
        .sec-onby-on-termscondition h4{
            margin-top: 20px;
            margin-bottom: 5px;
            font-weight: 700;
        }
    </style>
</head>
<body>


<div class="col-xs-12 col-md-12 col-lg-12">
  <div class="container">
    <div class="terms-and-condition">
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h1>Travolong Privacy Policy</h1>
            <p>Travolong (“Travolong,” “we,” and “us”) respects the privacy of its users (“you”) and has developed this Privacy Policy to demonstrate its commitment to protecting your privacy. This Privacy Policy describes the information we collect, how that information may be used, with whom it may be shared, and your choices about such uses and disclosures. We encourage you to read this Privacy Policy carefully when using our application or services or transacting business with us. By using our website, application or other online services (our “Service”), you are accepting the practices described in this Privacy Policy.</p>
            <p>If you have any questions about our privacy practices, please refer to the end of this Privacy Policy for information on how to contact us.</p>
            <P>You will also be asked to allow Travolong to collect your location information from your device when you download or use the Service. In addition, we may collect and store any personal information you provide while using our Service or in some other manner. This may include identifying information, such as your name, address, email address and telephone number, and, if you transact business with us, financial information. You may also provide us photos, a personal description and information about your gender and preferences for recommendations, such as hobbies, search location. </P>
            <P>Use of technologies to collect information. We use various technologies to collect information from your device and about your activities on our Service.</P>
            <P>Information collected automatically. We automatically collect information from your browser or device when you visit our Service. This information could include your IP address, device ID and type, your browser type and language, the operating system used by your device, access times, your mobile device’s geographic location while our application is actively running, and the referring website address.</P>
            <P>Cookies and use of cookie and similar data. When you visit our Service, we may assign your device one or more cookies or other technologies that facilitate personalization to facilitate access to our Service and to personalize your experience. Through the use of a cookie, we also may automatically collect information about your activity on our Service, such as the pages you visit, the time and date of your visits and the links you click. If we advertise, we (or third parties) may use certain data collected on our Service to show you travolong advertisements on other sites or applications.</P>
            <P>Mobile device IDs. If you're using our app, we use mobile device IDs (the unique identifier assigned to a device by the manufacturer), or Advertising IDs (for iOS 6 and later), instead of cookies, to recognize you. We do this to store your preferences and track your use of our app. Unlike cookies, device IDs cannot be deleted, but Advertising IDs can be reset in “Settings” on your iPhone. Ad companies also use device IDs or Advertising IDs to track your use of the app, track the number of ads displayed, measure ad performance and display ads that are more relevant to you. Analytics companies use device IDs to track information about app usage.</P>
            <P>Information collected by third-parties for advertising purposes. We may allow service providers, advertising companies and ad networks, and other third parties to display advertisements on our Service and elsewhere. These companies may use tracking technologies, such as cookies or web beacons, to collect information about users who view or interact with their advertisements. We do not provide any non-masked or non-obscured personal information to third parties. Some of these third-party advertising companies may be advertising networks that are members of the Network Advertising Initiative, which offers a single location to opt out of ad targeting from member companies (www.networkadvertising.org). Opting out will not decrease the number of advertisements you see. To opt-out of cookies that may be set by third party data or advertising partners, please go to <a href="http://www.aboutads.info/choices/.">http://www.aboutads.info/choices/.</a></P>
           
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>How we use the information we collect</h4>
            <p>In General. We may use information that we collect about you to:</p>
            <p>deliver and improve our products and services, and manage our business;</p>
            <p>manage your account and provide you with customer support;</p>
            <p>perform research and analysis about your use of, or interest in, our or others’ products, services, or content;</p>
            <p>communicate with you by email, postal mail, telephone and/or mobile devices about products or services that may be of interest to you either from us or other third parties;</p>
            <p>develop, display, and track content and advertising tailored to your interests on our Service and other sites, including providing our advertisements to you when you visit other sites;</p>
            <p>website or mobile application analytics;</p>
            <p>verify your eligibility and deliver prizes in connection with contests and sweepstakes;</p>
            <p>enforce or exercise any rights in our Terms of Use; and</p>
            <p>perform functions or services as otherwise described to you at the time of collection.</p>
            
       </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Do Not Track disclosure</h4>
            <p>Do Not Track (“DNT”) is a privacy preference that users can set in their web browsers. DNT is a way for users to inform websites and services that they do not want certain information about their webpage visits collected over time and across websites or online services. We are committed to providing you with meaningful choices about the information we collect and that is why we provide the opt-out links above. However, we do not recognize or respond to any DNT signals, as the Internet industry works toward defining exactly what DNT means, what it means to comply with DNT, and a common approach to responding to DNT.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>With whom we share your information</h4>
            <p>Information Shared with Other Users.When you register as a user of travolong, your travolong profile will be viewable by other users of the Service. Other users (and in the case of any sharing features available on travolong, the individuals or apps with whom a travolong user may choose to share you with) will be able to view information you have provided to us directly or through Facebook or other social netwoking sites such as your photos, any additional imagery you upload, your name, age, your personal description, and information you have in common with the person viewing your profile.</p>
            <p>Personal information. We do not share your personal information with others except as indicated in this Privacy Policy or when we inform you and give you an opportunity to opt out of having your personal information shared. We may share personal information with:</p>
            <p>Service providers. We may share information, including personal and financial information, with third parties that perform certain services on our behalf. These services may include fulfilling orders, providing customer service and marketing assistance, performing business and sales analysis, ad tracking and analytics. These service providers may have access to personal information needed to perform their functions but are not permitted to share or use such information for any other purposes.</p>
            <p>Other Situations. We may disclose your information, including personal information:</p>
            <p>In response to an investigation or as directed by a court order, or a request for cooperation from a law enforcement or other government agency; to establish or exercise our legal rights; to defend against legal claims; or as otherwise required by law. In such cases, we may raise or waive any legal objection or right available to us.</p>
            <p>When we believe disclosure is appropriate in connection with efforts to investigate, prevent, or take other action regarding illegal activity, suspected fraud or other wrongdoing; to protect and defend the rights, property or safety of our company, our users, our employees, or others; to comply with applicable law or cooperate with law enforcement; or to enforce our Terms of Use or other agreements or policies.</p>
            <p>In connection with a substantial corporate transaction, such as the sale of our business, a divestiture, merger, consolidation, or asset sale, or in the unlikely event of bankruptcy.</p>
            <p>Aggregated and/or non-personal information. We may use and share non-personal information we collect under any of the above circumstances.</p>
            <p>We may also share it with third parties to develop and deliver targeted advertising on our Service and on websites or applications of third parties, and to analyze and report on advertising you see. We may combine non-personal information we collect with additional non-personal information collected from other sources. We also may share aggregated, non-personal information, or personal information in hashed, non-human readable form, with third parties, including advisors, advertisers and investors, for the purpose of conducting general business analysis, advertising, marketing, or other business purposes. For example, we may engage a data provider who may collect web log data from you (including IP address and information about your browser or operating system), or place or recognize a unique cookie on your browser to enable you to receive customized ads or content. The cookies may re?ect de-identified demographic or other data linked to data you voluntarily have submitted to us (such as your email address), that we may share with a data provider solely in hashed, non-human readable form. To opt out of the sharing of your geolocation information, please discontinue use of the travolong application.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Do Not Track disclosure</h4>
            <p>Do Not Track (“DNT”) is a privacy preference that users can set in their web browsers. DNT is a way for users to inform websites and services that they do not want certain information about their webpage visits collected over time and across websites or online services. We are committed to providing you with meaningful choices about the information we collect and that is why we provide the opt-out links above. However, we do not recognize or respond to any DNT signals, as the Internet industry works toward defining exactly what DNT means, what it means to comply with DNT, and a common approach to responding to DNT.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Third-party websites</h4>
            <p>There are a number of places on our Service where you may click on a link to access other websites that do not operate under this Privacy Policy. For example, if you click on an advertisement on our Service, you may be taken to a website that we do not control. These third- party websites may independently solicit and collect information, including personal information, from you and, in some instances, provide us with information about your activities on those websites. We recommend that you consult the privacy statements of all third-party websites you visit by clicking on the “privacy” link typically located at the bottom of the webpage you are visiting.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>How you can access and correct your information</h4>
            <p>If you have a travolong account, you have the ability to review and update your personal information within the Service by opening your account and going to settings. Applicable privacy laws may allow you the right to access and/or request the correction of errors or omissions in your personal information that is in our custody or under our control. Our Privacy Officer will assist you with the access request. This includes:</p>
            <ul>
                <li>Identification of personal information under our custody or control; and</li>
                <li>Information about how personal information under our control may be or has been used by us.</li>
                <li>We will respond to requests within the time allowed by all applicable privacy laws and will make every effort to respond as accurately and completely as possible. Any corrections made to personal information will be promptly sent to any organization to which it was disclosed.</li>
                <li>In certain exceptional circumstances, we may not be able to provide access to certain personal information we hold. For security purposes, not all personal information is accessible and amendable by the Privacy Officer. If access or corrections cannot be provided, we will notify the individual making the request within 30 days, in writing, of the reasons for the refusal.</li>
            </ul>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Data retention</h4>
            <p>We keep your information only as long as we need it for legitimate business purposes and as permitted by applicable legal requirements. If you close your account, we will retain certain data for analytical purposes and recordkeeping integrity, as well as to prevent fraud, enforce our Terms of Use, take actions we deem necessary to protect the integrity of our Service or our users, or take other actions otherwise permitted by law. In addition, if certain information has already been provided to third parties as described in this Privacy Policy, retention of that information will be subject to those third parties’ policies.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Your choices about collection and use of your information</h4>
            <p>You can choose not to provide us with certain information, but that may result in you being unable to use certain features of our Service because such information may be required in order for you to register as user; purchase products or services; participate in a contest, promotion, survey, or sweepstakes; ask a question; or initiate other transactions.</p>
            <p>Our Service may also deliver notifications to your phone or mobile device. You can disable these notifications by going into “App Settings” on the app or by changing the settings on your mobile device.</p>
            <p>You can also control information collected by cookies. You can delete or decline cookies by changing your browser settings. Click “help” in the toolbar of most browsers for instructions.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>How we protect your personal information</h4>
            <p>We take security measures to help safeguard your personal information from unauthorized access and disclosure. However, no system can be completely secure. Therefore, although we take steps to secure your information, we do not promise, and you should not expect, that your personal information, chats, or other communications will always remain secure. Users should also take care with how they handle and disclose their personal information and should avoid sending personal information through insecure email.</p>
            <p>You agree that we may communicate with you electronically regarding security, privacy, and administrative issues, such as security breaches. We may post a notice on our Service if a security breach occurs. We may also send an email to you at the email address you have provided to us. You may have a legal right to receive this notice in writing. To receive free written notice of a security breach (or to withdraw your consent from receiving electronic notice), please notify us at privacy@travolong.com.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Information you provide about yourself while using our Service</h4>
            <p>We provide areas on our Service where you can post information about yourself and others and communicate with others. Such postings are governed by our Terms of Use. Also, whenever you voluntarily disclose personal information on publicly-viewable pages, that information will be publicly available and can be collected and used by others. For example, if you post your email address, you may receive unsolicited messages. We cannot control who reads your posting or what other users may do with the information you voluntarily post, so we encourage you to exercise discretion and caution with respect to your personal information.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Children's privacy </h4>
            <p>Although our Service is a general audience Service, we restrict the use of our service to individuals age 18 and above. We do not knowingly collect, maintain, or use personal information from children under the age of 18.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Data Processing </h4>
            <p>We have developed data practices designed to assure information is appropriately protected but we cannot always know where personal information may be accessed or processed. We may employ other companies and individuals to perform functions on our behalf. If we disclose personal information to a third party or to our employees outside of the United States, we will seek assurances that any information we may provide to them is safeguarded adequately and in accordance with this Privacy Policy and the requirements of applicable privacy laws.</p>
            <p>By providing your personal information, you consent to any transfer and processing in accordance with this Policy.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>No Rights of Third Parties</h4>
            <p>This Privacy Policy does not create rights enforceable by third parties or require disclosure of any personal information relating to users</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Changes to this Privacy Policy </h4>
            <p>We will occasionally update this Privacy Policy to reflect changes in the law, our data collection and use practices, the features of our Service, or advances in technology. When we post changes to this Privacy Policy, we will revise the “last updated” date at the top of this Privacy Policy. Please review the changes carefully. Your continued use of the Services following the posting of changes to this policy will mean you consent to and accept those changes. If you do not consent to such changes you can delete your account by following the instructions under Settings.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>How to contact us</h4>
            <p>If you have any questions about this Privacy Policy, please contact us by email as follows:<a href="mailto:support@travolong.com">support@travolong.com</a></p>
        </div>
        
        
    </div>
  </div>
</div>

</body>
</html>