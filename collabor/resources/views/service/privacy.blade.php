
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Privacy Policy</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <style>
        body{
            font-family: 'Lato', sans-serif;
        }
        
        .sec-onby-on-termscondition h1{
            font-size: 26px;
            margin: 6px 0px;
        }
        
        .sec-onby-on-termscondition h4{
            margin-top: 20px;
            margin-bottom: 5px;
            font-weight: 700;
        }
        
        ol li{
            padding-bottom: 10px;
        }
    </style>
</head>
<body>


<div class="col-xs-12 col-md-12 col-lg-12">
  <div class="container">
    <div class="terms-and-condition">
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h1>Privacy Policy</h1>
            <h4>Effective as of may 25, 2018</h4>
            <p>Linqq ™ and its affiliates ("Epton Applications Pvt Ltd." or the "Company") are committed to protecting the privacy of individuals who visit the Company's Web sites and individuals who register to use the Linqq™ Services, as defined below. This Linqq™ Member and Visitor Privacy Statement describes the Company's privacy practices in relation to the use of the Company's Web sites and the related applications and services offered by the Company under the "Linqq™" brand (the "Linqq™ Services").</p>
            
            <h4>Web Sites Covered</h4>
            <p>This Privacy Statement covers the information practices of Web sites that link to this Privacy Statement, (referred to as "the Company's Web sites").
The Company's Web site may contain links to other Web sites. The information practices or the content of such other Web sites are governed by the privacy statements of such other Web sites. The Company encourages you to review the privacy statements of other Web sites to understand their information practices.</p>
          
            <h4>Information Collected</h4>
            <p>The Company collects individually-identifiable information from individuals who visit the Company's Web site WWW.linqqapp.com ("Web Site Visitors") and individuals who register to use the linqq™ Services, including Linqq™ Members ("Customers"). Important note: registering to use the Linqq™ Services, or becoming a Linqq™ Member, does not automatically add your contact information to the Linqq™.</p>
            <p>When expressing an interest in obtaining additional information about the Linqq™ Services or registering to use the Linqq™ Services, the Company requires you to provide the Company with contact information, such as name, company name, address, phone number, and email address ("Required Contact Information"). When purchasing the Linqq™ Services, you may require to provide certain details to the respective payment gateway portal and the terms and conditions of the respective payment gateway portal shall be applicable. The Company may also ask you to provide, or otherwise allow you to share additional information, which you may choose to share in your registration for or use of the Linqq™ Services ("Optional Information"). Required Contact Information, Billing Information, and Optional Information are referred to collectively as "Data About Customers."</p>
			<p>As you navigate the Company's Web site or use the linqq™ Services, the Company may also collect information through the use of commonly-used information-gathering tools, such as cookies and Web beacons ("Web Site Navigational Information"). Web Site Navigational Information includes standard information from your Web browser (such as browser type and browser language), your Internet Protocol ("IP") address, HTTP header information and the actions you take on the Company's Web site (such as the Web pages viewed, and the links clicked). </p>
		  
            <h4>Use of Information Collected</h4>
            <p>The Company uses Data About Customers to perform the services requested. For example, if you fill out a "Contact Me" Web form, the Company will use the information provided to contact you about your interest in the linqq™ Services.</p>
            <p>The Company may also use Data About Customers for marketing purposes. For example, the Company may use information you provide to contact you to further discuss your interest in the linqq™ Services and to send you information regarding the Company, its affiliates and its partners, such as information about promotions or events.</p>
            <p>The Company uses Web Site Navigational Information to operate and improve the Company's Web sites. The Company may also use Web Site Navigational Information alone or in combination with Data About Customers to provide personalized information about the Company and the Linqq™ Services. For additional information about the use of Web Site Navigational Information, please view the Web Site Navigational Information section of this Privacy Statement.</p>
            <p>The Company may use both Data About Customers and Web Site Navigational Information to enforce the Company's Web site policies, to protect the Company's rights and intellectual property, or the rights of others using the Company's Web sites or the Linqq™ Services.</p>
          
            <h4>Web Site Navigational Information</h4>
            <p>The Company uses commonly-used information-gathering tools, such as cookies and Web beacons, to collect information from the computers or mobile devices of linqq™ Members, Customers and Web Site Visitors as they navigate the Company's Web site, interact with emails sent by the Company or on its behalf, or use the Linqq™ Services ("Web Site Navigational Information"). This section describes the types of Web Site Navigational Information used on the Company's Web sites and in connection with the Linqq™ Services, and how this information may be used.</p>
          
            <h4>Cookies</h4>
            <p>The Company uses cookies and JavaScript to make interactions with the Company's Web site easy and meaningful. When you visit the Company's Web site, or use the Linqq™ Services online, the Company's servers or an authorized third party may send a cookie to your computer. Cookies allow the Company to track overall Web Site usage, determine areas that Visitors prefer, and to make your visit to the Company’s Web site easier by recognizing you and providing you with a customized experience. Standing alone, cookies do not personally identify you; they merely recognize your Web browser. Unless you choose to identify yourself to the Company, such as by responding to a promotional offer, becoming a Linqq™ Member, opening an account, or filling out a Web form (such as a "Contact Me" form), you remain anonymous to the Company. For a more thorough explanation of what cookies are and how they operate, including how to see which cookies have been set and how to manage or delete them, please visit www.aboutcookies.org or www.allaboutcookies.org.</p>
			<p>The Company uses cookies that are session-based and persistent-based. Session cookies exist only during one session, i.e., only while you are reading or navigating the Company's Web Site. They disappear from your computer when you close your browser software or turn off your computer. Persistent cookies remain on your computer after you close your browser or turn off your computer. Please note that if you disable your Web browser's ability to accept cookies, you will be able to navigate the Company's Web site, but the functionality and features of the Company's Web site and Linqq™ Services may be affected, and you may not be able to successfully use the Company's Web site and Linqq™ Services. Similarly, if you disable JavaScript, some features of the Company’s Web site may not function properly, and some areas of the Company’s Web site may not function at all.</p>
			<p>The following sets out how the Company may use different categories of cookies and your options for managing cookies' settings:</p>
		  
            <h4>Type of Cookies: Required cookies.</h4>
            <p>Description: Required cookies enable you to navigate the Company's Web sites and use its features, such as accessing secure areas of the Company's Web sites and using the Linqq™ Services. If you have chosen to identify yourself to the Company, the Company uses cookies containing encrypted information to allow the Company to uniquely identify you. Each time you log into the Linqq™ Services, a cookie containing an encrypted, unique identifier that is tied to your account is placed on your browser. These cookies allow the Company to uniquely identify you when you are logged into the Linqq™ Services and to process your online transactions and requests. Managing Settings: Because required cookies are essential to operate the Company's Web sites and the Linqq™ Services, there is no option to opt out of these cookies.</p>
 
          
            <h4>Type of Cookies: Performance cookies.</h4>
            <p><strong>Description</strong>: These cookies collect information about how Web Site Visitors use the Company's Web site, including which pages visitors go to most often and if they receive error messages from certain pages. These cookies do not collect information that individually identifies a Web Site Visitor. All information these cookies collect is aggregated and anonymous. It is only used to improve how the Company's Web site functions and performs. From time-to-time, the Company engages third parties to track and analyze usage and volume statistical information from individuals who visit the Company's Web sites. The Company may also utilize Flash cookies for these purposes. Flash cookies are different from browser cookies because of the amount of, type of, and how data is stored.</p>
            <p><strong>Managing Settings</strong>: To learn how to opt out of performance cookies using your browser settings click here. To learn how to manage privacy and storage settings for Flash cookies click here.</p>
          
            <h4>Type of Cookies: Functionality cookies.</h4>
            <p><strong>Description</strong>: Functionality cookies allow the Company's Web site to remember information you have entered or choices you make (such as your username, language, or your region) and provide enhanced, more personal features. These cookies also enable you to optimize your use of the Linqq™ Services after logging in. These cookies can also be used to remember changes you have made to text size, fonts and other parts of web pages that you can customize. The Company may use local shared objects, also known as Flash cookies, to store your preferences or display content based upon what you view on the Company's Web sites to personalize your visit.</p>
			<p><strong>Managing Settings</strong>: To learn how to opt out of functionality cookies using your browser settings click here. Note that opting out may impact the functionality you receive when visiting the Company's Web site. To learn how to manage privacy and storage settings for Flash cookies click here. </p>
		  
            <h4>Type of Cookies: Analytics, Targeting and Advertising cookies.</h4>
            <p><strong>Description</strong>: From time-to-time, the Company may engage third parties to track and analyze usage and volume statistical information from individuals who visit the Company's Web sites.
The Company may use cookies delivered by third parties to track the performance of Company advertisements. For example, these cookies remember which browsers have visited the Company's Web site. The information provided to third parties does not include personal information, but this information may be re-associated with personal information after the Company receives it. The Company may also contract with third-party advertising networks that collect IP addresses and other information from the Company's Web site, from emails, and on third-party Web sites. Ad networks follow your online activities over time by collecting Web Site Navigational Information through automated means, including through the use of cookies. They use this information to provide advertisements about products and services tailored to your interests. You may see these advertisements on other Web sites. This process also helps the Company manage and track the effectiveness of its marketing efforts. Third parties, with whom the Company may partner to provide certain features on our Company Web site or to display advertising based upon your Web browsing activity, may use Flash cookies to collect and store information.
</p>
<p><strong>Managing Settings</strong>: To learn more about advertising networks and their opt out instructions, click here and here. To learn how to opt out of these cookies using your browser settings click here, and to learn how to manage privacy and storage settings for Flash cookies click here.</p>
<p>Cookies that are used as part of the Linqq™ Services (in the proxy) may include cookies of a third-party Web site that is being co-browsed, for example, in order to deliver the Web site as intended by the third party Web site operator. The cookie practices of such Web sites are governed by their own respective privacy statements. The Company encourages you to review the privacy statements of other Web sites to understand their information practices.</p>		
		
            <h4>Web Beacons</h4>
            <p>The Company may use Web beacons alone or in conjunction with cookies to compile information about Linqq™ Members, Customers and Web Site Visitors' usage of the Company's Web site and interaction with emails from the Company. Web beacons are clear electronic images that can recognize certain types of information on your computer, such as cookies, when you viewed a particular Web site tied to the Web beacon, and a description of a Web site tied to the Web beacon. For example, the Company may place Web beacons in marketing emails that notify the Company when you open the email or when you click on a link in the email that directs you to one of the Company's Web sites. The Company may use Web beacons to operate and improve the Company's Web site and email communications.</p>
            <p>The Company may use information from Web beacons in combination with Data About Customers to provide you with information about the Company and the Linqq™ Services.</p>
			
            <h4>IP Addresses and URLs</h4>
            <p>Due to Internet communications standards, when you visit or use the Company's Web site, the Company automatically receives the URL of the Web site from which you came and the Web site to which you go when you leave the Company's Web site. The Company also receives your computer's Internet Protocol (IP) address (or the proxy server you use to access the World Wide Web), your computer operating system, the type of Web browser you are using, and your Internet Service Provider (ISP). This information is used to analyze overall trends to help us improve the Linqq™ Service and to track and aggregate non-personal information. For example, the Company uses IP addresses to monitor the regions from which Linqq™ Members, Customers and Web Site Visitors navigate the Company's Web site.</p>
            <p>The Company may collect aggregated data regarding use of the Linqq™ Services and the Company's Web site, including, without limitation, number of Web Site Visitors to the Company's Web site, frequency and patterns of use, tag usage, feedback request trends, etc. ("Aggregated Data"). The Company uses Aggregated Data as a statistical measure and not in a manner that would identify you personally. Aggregated Data enables the Company to determine how often certain parts of the Company's Web site or the Linqq™ Services are used so that we can improve them. The Company may make use of, or make such Aggregated Data available to, third parties, in any manner in our sole discretion.</p>
			
            <h4>Social Media Features</h4>
            <p>The Company’s Web sites may use social media features, such as the Facebook ‘like’ button (“Social Media Features”). These features may collect your IP address and which page you are visiting on the Company’s Web site and may set a cookie to enable the feature to function properly. You may be given the option by such Social Media Features to post information about your activities on the Company’s Web site to a profile page of yours that is provided by a third party Social Media network in order to share with others within your network. Social Media Features are either hosted by a third party or hosted directly on the Company’s Web site. Your interactions with these features are governed by the privacy policy of the company providing the relevant Social Media Features.</p>
			
            <h4>Do Not Track</h4>
            <p>Currently, various browsers – including Internet Explorer, Firefox, and Safari – offer a “do not track” or “DNT” option that relies on a technology known as a DNT header, which sends a signal to Web sites' visited by the user about the user's browser DNT preference setting. www.linqqapp.in does not currently commit to responding to browsers' DNT signals with respect to the Company's Web sites, in part, because no common industry standard for DNT has been adopted by industry groups, technology companies or regulators, including no consistent standard of interpreting user intent. Linqqapp.com takes privacy and meaningful choice seriously and will make efforts to continue to monitor developments around DNT browser technology and the implementation of a standard.</p>
          
			
            <h4>ustomer Data</h4>
            <p>As part of using the Linqq Services, Customers may submit electronic data or information to the Linqq™ Services ("Customer Data"). Any uses of Customer Data by the Company are done so pursuant to the Company’s Terms of Use agreement in place between the Company and the relevant Customer, which exclusively govern the Company’s treatment of Customer Data and supersedes this Privacy Statement in case of conflict, or as may be required by law.</p>
          
			
            <h4>Correcting and Updating Your Information</h4>
            <p>Members may access or update their registration information using the "settings" area of the Company's Web site. Members and Customers may also submit such requests to support@linqqapp.com. For having your registration information deleted, please contact support@linqqapp.com To discontinue your account, you can using the “settings” area of the Web site or alternatively, you may send a request to support@linqqapp.com Requests to access, change, or delete your information will be handled within 30 days.</p>
          
			
            <h4>Communications Preferences</h4>
            <p>The Company offers Linqq™ Members, Customers and Web Site Visitors who provide contact information a means to choose how the Company uses the information provided. Members and Customers may manage your receipt of marketing and non-transactional communications by sending a request specifying your communications preferences to Linqq™ Support e-mail id support@linqqapp.com Individuals cannot opt out of receiving transactional emails related to their business relationship with the Company.</p>
          
			
            <h4>Sharing of Information Collected</h4>
            <p><strong>Business Partners</strong> : rom time to time, the Company may partner with other companies to jointly offer products or services. If you purchase or specifically express interest in a jointly-offered product or service from the Company, the Company may share Data About Customers collected in connection with your purchase or expression of interest with our joint promotion partner(s). The Company does not control its business partners' use of the Data About Customers, and their use of the information will be in accordance with their own privacy policies. If you do not wish for your information to be shared in this manner, you may opt not to purchase or specifically express interest in a jointly offered product or service.</p>
            
			<h4>Service Providers</h4>
			<p>The Company may share Data About Customers with the Company's contracted service providers, such as payment processors, so that these service providers can provide services on the Company's behalf. The Company may also share Data About Customers with the Company's service providers to ensure the quality of information provided. Unless described in this Privacy Statement, the Company does not share, sell, rent, or trade Data About Customers with third parties for their promotional purposes.</p>
          
			
            <h4>Affiliates</h4>
            <p>The Company may share Data About Customers with other members and affiliates in order to work with them. For example, the Company may need to share Data About Customers for customer relationship management purposes.</p>
			
            <h4>Billing</h4>
            <p>The company may use a third party service provider to manage credit card/debit card/net banking processing. this service provider is not permitted to store, retain ,or use billing information expect for the sole purpose of payment processing on the company’s behalf.</p>
			
            <h4>Third Parties</h4>
            <p>This Privacy Statement sets forth the information www.linqqapp.com / mobile application collects on the Company’s Web sites and the information we share with third parties. linqq™ does not authorize the collection of personal information by third parties through advertising technologies deployed on the Company's Web sites, nor do we share personal information with any third parties collected from the Company's Web sites, except as provided in this Privacy Statement. Section 4 of this Privacy Statement, Web Site Navigational Information, specifically addresses the information we collect through cookies and web beacons, and how you can control cookies through your Web browsers.</p>
			
            <h4>Compelled Disclosure</h4>
            <p>The Company reserves the right to use or disclose information provided if required by law or if the Company reasonably believes that use or disclosure is necessary to protect the Company's rights and/or to comply with a judicial proceeding, court order, or legal process.</p>
			
            <h4>Public Forums and Customer Testimonials</h4>
            <p>The Company may provide bulletin boards, blogs, or chat rooms on the Company's Web site. Any personal information you choose to submit in such a forum may be read, collected, or used by others who visit these forums, and may be used to send you unsolicited messages. The Company is not responsible for the personal information you choose to submit in these forums.
Customers and Web Site Visitors may elect to use the Company's referral program to inform friends about the Company's Web sites and the linqq™ Services. The Company does not store this information.

The Company may post a list of customer testimonials on the Company's Web site that contain information such as names and titles of customer personnel. The Company obtains the consent of each customer prior to posting any information on such a list or posting testimonials.</p>
			
            <h4>International Transfer of Information Collected</h4>
            <p>To facilitate the Company's global operations, the Company may transfer and access Data About Customers from around the world, including the United States. This Linqq™ Member and Web Site Visitor Privacy Statement shall apply even if the Company transfers Data About Customers to other countries.</p>
			
            <h4>Security</h4>
            <p>The Company takes precautions, including appropriate administrative, technical and physical measures, to protect Data About Customers against loss, theft and misuse, as well as unauthorized access, disclosure, alteration and destruction. When we collect or transmit credit card numbers, we use industry standard methods to protect such information. Customers are solely responsible for protecting their passwords, limiting access to their computers, and signing out of the Linqq™ Services after their sessions.
</p>
			
            <h4>Changes to this Privacy Statement</h4>
            <p>The Company reserves the right to change this Linqq™ Member and Web Site Visitor Privacy Statement. The Company will provide notification of material changes to these Privacy Statements through the Company's Web site, or by other means, to provide you the opportunity to review the changes before they become effective. </p>
			
            <h4>Contacting Us</h4>
            <p>Questions regarding this Linqq™ Member and Web Site Visitor Privacy Statement or the information practices of the Company should be directed to the Company by contacting Linqq™ Support at support@linqqapp.com or by mailing Linqq </p>
			<p>Building No. 13</p>
			<p>Street No.78</p>
			<p>Punjabi Bagh(W) </p>
			<p>New Delhi, </p>
			<p>INDIA </p>
			<p>Pin Code- 110026</p>
			<br>
			<p style="text-align:center">Copyright © 2018 Epton Applications Pvt. Ltd. All Rights Reserved. </p>
          
            
            
            
            
            
        
        </div>
        
        
    </div>
  </div>
</div>

</body>
</html>
