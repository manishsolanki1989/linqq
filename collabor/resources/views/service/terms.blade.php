<!DOCTYPE html>
<html lang="en">
<head>
  <title>TERMS OF USE</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <style>
        body{
            font-family: 'Lato', sans-serif;
        }
        
        .sec-onby-on-termscondition h1{
            font-size: 26px;
            margin: 0px;
        }
        
        .sec-onby-on-termscondition h4{
            margin-top: 20px;
            margin-bottom: 5px;
            font-weight: 700;
        }
    </style>
</head>
<body>


<div class="col-xs-12 col-md-12 col-lg-12">
  <div class="container">
    <div class="terms-and-condition">
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h1>TERMS OF USE</h1>  
            <p>Welcome to Travolong, operated by Pingala Innovations Private Limited (“us,” “we,” the “Company” or “Travolong”).</p>
            <h4>Acceptance of Terms of Use Agreement.</h4>
            <p>By creating a Travolong account or by using the Travolong chat app, whether through a mobile device, mobile application or computer (collectively, the “Service”) you agree to be bound by (i) these Terms of Use, (ii) our Privacy Policy and Safety Tips, each of which is incorporated by reference into this Agreement, and (iii) any terms disclosed and agreed to by you if you purchase additional features, products or services we offer on the Service (collectively, this “Agreement”). If you do not accept and agree to be bound by all of the terms of this Agreement, please do not use the Service.</p>
            <p>We update or make changes to this Agreement and to the Services from time to time. We may do this for a variety of reasons including an update in the service setting, an update on the additional features that are added in the travolong and also the business practices. If you continue to use the Services after the  changes become effective, then you agree to the revised Agreement.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Eligibility.</h4>
            <p>You must be at least 18 years of age to create an account on Travolong and use the Service. By creating an account and using the Service, you represent and warrant that:</p>
            <p>you can form a binding contract with Travolong,</p>
            <p>you are not a person who is barred from using the Service under the laws of India any other applicable jurisdiction,
    you will comply with this Agreement and all applicable local, state, national and international laws, rules and regulations, and</p>
            <p>you have never been convicted on any charges of any nature by courts and is not listed as a offender</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Your Account.</h4>
            <p>You are responsible for maintaining the confidentiality of your login credentials you use to sign up for Travolong, and you are solely responsible for all activities that occur under those credentials. If you think someone has gained access to your account, please immediately contact help@Travolong.com.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Modifying the Services and Termination.</h4>
            <p>Travolong is always striving to improve the Services and bring you additional functionality that you will find engaging and useful. This means we may add new product features or enhancements from time to time as well as remove some features, and if these actions do not materially affect your rights or obligations, we may not provide you with notice before taking them. We may even suspend the Services entirely, in which event we will notify you in advance unless extenuating circumstances, such as safety or security concerns, prevent us from doing so.</p>
            <p>You may terminate your account at any time, for any reason, by following the instructions in “Settings” in the Service, however you will need to manage your in app purchases through your mobile device platform. Travolong may terminate your account at any time without notice if it believes that you have violated this Agreement. Upon such termination, you will not be entitled to any refund for purchases. After your account is terminated, this Agreement will terminate, except that the following provisions will still apply to you and Travolong: Section 4, Section 6, and Sections 12 through 19.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Safety; Your Interactions with Other Users.</h4>
            <p>Though Travolong strives to encourage a respectful user experience through "First Time Reply Option" where in two users cannot chat further till the time both of them do not opt in to chat.Travolong is not responsible for the conduct of any user on or off of the Services. You agree to use caution in all interactions with other users, particularly if you decide to communicate off the Service or meet in person. You should not provide any personal infromation which is very sensitive in anture like your financial information (for example, your credit card or bank account information), or wire or otherwise send money, to other users.</p>
            <p>YOU ARE SOLELY RESPONSIBLE FOR YOUR INTERACTIONS WITH OTHER USERS. YOU UNDERSTAND THAT Travolong DOES NOT CONDUCT CRIMINAL BACKGROUND CHECKS ON the USERS of ITS PLATFORM.Travolong MAKES NO REPRESENTATIONS OR WARRANTIES AS TO THE CONDUCT OF USERS. Travolong RESERVES THE RIGHT TO CONDUCT ANY CRIMINAL BACKGROUND CHECK OR OTHER SCREENINGS AT ANY TIME USING AVAILABLE PUBLIC RECORDS.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Rights Travolong Grants You.</h4>
            <p>Travolong grants you a personal, worldwide, royalty-free, non-assignable, nonexclusive, revocable, and non-sublicensable license to access and use the Services. This license is for the sole purpose of letting you use and enjoy the Services’ benefits as intended by Travolong and permitted by this Agreement. Therefore, you agree not to:</p>
            <p>use the Service or any content contained in the Service for any commercial purposes without our written consent.
                copy, modify, transmit, create any derivative works from, make use of, or reproduce in any way any copyrighted material, images, trademarks,     trade names, service marks, or other intellectual property, content or proprietary information accessible through the Service without            Travolong’s prior written consent.
                express or imply that any statements you make are endorsed by Travolong.
                use any robot, bot, spider, crawler, scraper, site search/retrieval application, proxy or other manual or automatic device, method or process     to access, retrieve, index, “data mine,” or in any way reproduce or circumvent the navigational structure or presentation of the Service or       its contents. use the Services in any way that could interfere with, disrupt or negatively affect the Service or the servers or networks         connected to the service.
                upload viruses or other malicious code or otherwise compromise the security of the Services.
                forge headers or otherwise manipulate identifiers in order to disguise the origin of any information transmitted to or through the Service.
                “frame” or “mirror” any part of the Service without Travolong’s prior written authorization.
                use meta tags or code or other devices containing any reference to Travolong or the Service (or any trademark, trade name, service mark, logo     or slogan of Travolong) to direct any person to any other website for any purpose.
                modify, adapt, sublicense, translate, sell, reverse engineer, decipher, decompile or otherwise disassemble any portion of the Service, or         cause others to do so.
                use or develop any third-party applications that interact with the Services or other users’ Content or information without our written         consent.
                use, access, or publish the Travolong application programming interface without our written consent.
                probe, scan or test the vulnerability of our Services or any system or network.
                encourage or promote any activity that violates this Agreement.

            The Company may investigate and take any available legal action in response to illegal and/ or unauthorized uses of the Service, including termination of your account.

            Any software that we provide you may automatically download and install upgrades, updates, or other new features. You may be able to adjust these automatic downloads through your device’s settings.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Rights you Grant Travolong.</h4>
            <p>By creating an account, you grant to Travolong a worldwide, transferable, sub-licensable, royalty-free, right and license to host, store, use, copy, display, reproduce, adapt, edit, publish, modify and distribute information you authorize us to access, as well as any information you post, upload, display or otherwise make available (collectively, “post”) on the Service or transmit to other users (collectively, “Content”). Our license to your Content is subject to your rights under applicable law  and is for the limited purpose of operating, developing, providing, and improving the Service and researching and developing new ones. You agree that any Content you place or that you authorize us to place on the Service may be viewed by other users and may be viewed by any person visiting or participating in the Service (such as individuals who may receive shared Content from other Travolong users).</p>
            <p>You agree that all information that you submit upon creation of your account is accurate and truthful and you have the right to post the Content on the Service and grant the license to Travolong above.</p>
            <p>You understand and agree that we may monitor or review any Content you post as part of a Service. We may delete any Content, in whole or in part, that in our sole judgment violates this Agreement or may harm the reputation of the Service.</p>
            <p>When communicating with our customer care representatives, you agree to be respectful and kind. If we feel that your behavior towards any of our customer care representatives or other employees is at any time threatening or offensive, we reserve the right to immediately terminate your account.</p>
            <p>In consideration for Travolong allowing you to use the Services, you agree that we, our affiliates, and our third-party partners may place advertising on the Services. By submitting suggestions or feedback to Travolong regarding our Services, you agree that Travolong may use and share such feedback for any purpose without compensating you.</p>
            <p>You agree that Travolong may access, preserve and disclose your account information and Content if required to do so by law or in a good faith belief that such access, preservation or disclosure is reasonably necessary, such as to: (i) comply with legal process; (ii) enforce this Agreement; (iii) respond to claims that any Content violates the rights of third parties; (iv) respond to your requests for customer service; or (v) protect the rights, property or personal safety of the Company or any other person.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Community Rules.</h4>
            <p>By using the Services, you agree that you will not:

    use the Service for any purpose that is illegal or prohibited by this Agreement.
    spam, solicit money from or defraud any users.
    impersonate any person or entity or post any images of another person without his or her permission.
    bully, “stalk,” intimidate, harass or defame any person.
    post any Content that violates or infringes anyone’s rights, including rights of publicity, privacy, copyright, trademark or other     intellectual property or contract right.
    post any Content that is hate speech, threatening, sexually explicit or pornographic; incites violence; or contains nudity or graphic or     gratuitous violence.
    post any Content that promotes racism, bigotry, hatred or physical harm of any kind against any group or individual.
    solicit passwords for any purpose, or personal identifying information for commercial or unlawful purposes from other users or disseminate     another person’s personal information without his or her permission.
    use another user’s account.
    create another account if we have already terminated your account, unless you have our permission.

Travolong reserves the right to investigate and/ or terminate your account without a refund of any purchases if you have misused the Service or behaved in a way that Travolong regards as inappropriate or unlawful, including actions or communications that occur off the Service but involve users you meet through the Service.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Other Users’ Content.</h4>
            <p>Although Travolong reserves the right to review and remove Content that violates this Agreement, such Content is the sole responsibility of the user who posts it, and Travolong cannot guarantee that all Content will comply with this Agreement. If you see Content on the Services that violates this Agreement, please report it by mail it to <a href="mailti:support@Travolong.com">support@Travolong.com</a>.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Purchases.</h4>
            <p>In App Purchases. From time to time, Travolong may offer products and services for purchase (“in app purchases”) through different travel portals like Make My Trip, Expedia and other Online Travel portals authorized by Travolong. These travel Portals may charge you sales tax, depending on where you live. </p>
            <p>If you choose to make a purchase through Travolong in app purchase, you agree to pay Travolong all charges at the prices displayed to you for the services you’ve selected as well as any sales or similar taxes that may be imposed on your payments, and you authorize Travolong to charge your chosen payment provider (your “Payment Method”). Travolong may correct any billing errors or mistakes that it makes even if it has already requested or received payment. If you initiate a chargeback or otherwise reverse a payment made with your Payment Method, Travolong may terminate your account immediately in its sole discretion.</p>
            <p>You may edit your Payment Method information by visiting Travolong Online and going to “My Profile.” If a payment is not successfully settled, due to expiration, insufficient funds, or otherwise, and you do not edit your Payment Method information or cancel your subscription, you remain responsible for any uncollected amounts and authorize us to continue billing the Payment Method, as it may be updated.  </p>
            <p>This may result in a change to your payment billing dates. In addition, you authorize us to obtain updated or replacement expiration dates and card numbers for your credit or debit card as provided by your credit or debit card issuer. The terms of your payment will be based on your Payment Method and may be determined by agreements between you and the financial institution, credit card issuer or other provider of your chosen Payment Method. </p>
            <p>Refunds - Refunds can happen if any user wishes to cancel his bookings done through any Online travel portal using travolong platform. However Cancellation and Refund policy is the sole discretion of Online Tavel Portal as per their policy. Travolong is not responsible for any actions taken by Online Travel Portals on refunds and cancellation.</p>
          
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Notice and Procedure for Making Claims of Copyright Infringement.</h4>
            <p>If you believe that your work has been copied and posted on the Service in a way that constitutes copyright infringement, please provide our Copyright Agent with the following information:</p>
            <p>an electronic or physical signature of the person authorized to act on behalf of the owner of the copyright interest;
            a description of the copyrighted work that you claim has been infringed;
            a description of where the material that you claim is infringing is located on the Service (and such description must be reasonably     sufficient to enable us to find the alleged infringing material);
            your contact information, including address, telephone number and email address;
            a written statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or     the law; and a statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the         copyright owner or authorized to act on the copyright owner’s behalf.

            Notice of claims of copyright infringement should be provided to the Company’s Copyright Agent at contact@travolong.com

            Travolong will terminate the accounts of repeat infringers.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Disclaimers.</h4>
            <p>Travolong PROVIDES THE SERVICE ON AN “AS IS” AND “AS AVAILABLE” BASIS AND TO THE EXTENT PERMITTED BY APPLICABLE LAW, GRANTS NO WARRANTIES OF ANY KIND, WHETHER EXPRESS, IMPLIED, STATUTORY OR OTHERWISE WITH RESPECT TO THE SERVICE (INCLUDING ALL CONTENT CONTAINED THEREIN), INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTIES OF SATISFACTORY QUALITY, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT. Travolong DOES NOT REPRESENT OR WARRANT THAT (A) THE SERVICE WILL BE UNINTERRUPTED, SECURE OR ERROR FREE, (B) ANY DEFECTS OR ERRORS IN THE SERVICE WILL BE CORRECTED, OR (C) THAT ANY CONTENT OR INFORMATION YOU OBTAIN ON OR THROUGH THE SERVICES WILL BE ACCURATE.</p>
            <p>Travolong TAKES NO RESPONSIBILITY FOR ANY CONTENT THAT YOU OR ANOTHER USER OR THIRD PARTY POSTS, SENDS OR RECEIVES THROUGH THE SERVICES. ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE SERVICE IS ACCESSED AT YOUR OWN DISCRETION AND RISK.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Third Party Services.</h4>
            <p>The Service may contain advertisements and promotions offered by third parties and links to other web sites or resources. Travolong is not responsible for the availability (or lack of availability) of such external websites or resources. If you choose to interact with the third parties made available through our Service, such party’s terms will govern their relationship with you. Travolong is not responsible or liable for such third parties’ terms or actions.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Limitation of Liability.</h4>
            <p>TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT WILL Travolong, ITS AFFILIATES, EMPLOYEES, LICENSORS OR SERVICE PROVIDERS BE LIABLE FOR ANY INDIRECT, CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, SPECIAL OR PUNITIVE DAMAGES, INCLUDING, WITHOUT LIMITATION, LOSS OF PROFITS, WHETHER INCURRED DIRECTLY OR INDIRECTLY, OR ANY LOSS OF DATA, USE, GOODWILL, OR OTHER INTANGIBLE LOSSES, RESULTING FROM: (I) YOUR ACCESS TO OR USE OF OR INABILITY TO ACCESS OR USE THE SERVICES, (II) THE CONDUCT OR CONTENT OF OTHER USERS OR THIRD PARTIES ON, THROUGH, OR FOLLOWING USE OF THE SERVICES; OR (III) UNAUTHORIZED ACCESS, USE OR ALTERATION OF YOUR CONTENT, EVEN IF Travolong HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. IN NO EVENT WILL Travolong’S AGGREGATE LIABILITY TO YOU FOR ALL CLAIMS RELATING TO THE SERVICE EXCEED THE AMOUNT PAID, IF ANY, BY YOU TO Travolong FOR THE SERVICE WHILE YOU HAVE AN ACCOUNT.</p>
            <p>SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF CERTAIN DAMAGES, SO SOME OR ALL OF THE EXCLUSIONS AND LIMITATIONS IN THIS SECTION MAY NOT APPLY TO YOU.</p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Arbitration and Governing Law</h4>
            <p>The venue of the arbitration shall be Mumbai (India). The Arbitration proceeding shall be governed by the Arbitration and Conciliation Act, 1996, amended from time to time. The proceedings of arbitration shall be in English language.  </p>
            <p>By using the Service in any manner, you agree to the arbitration agreement. This Agreement shall be governed and construed in accordance with the laws of India. Subject to the provisions of this agreement, the Courts having the jurisdiction under the provisions of the Arbitration and Conciliation Act, 1996, to determine all matters which the Courts are entitled to determine under the Act, including, without limitation, provisions of interim reliefs under the provisions of Section 9 of the Arbitration and Conciliation Act, 1996 shall exclusively be courts of Delhi, India. It is agreed that the Parties shall bear all costs and expenses of the arbitration equally. </p>
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Indemnity by You.</h4>
            <p>You agree, to the extent permitted under applicable law, to indemnify, defend and hold harmless Travolong, our affiliates, and their and our respective officers, directors, agents, and employees from and against any and all complaints, demands, claims, damages, losses, costs, liabilities and expenses, including attorney’s fees, due to, arising out of, or relating in any way to your access to or use of the Services, your Content, or your breach of this Agreement.</p>
 
        </div>
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h4>Entire Agreement; Other.</h4>
            <p>This Agreement, with the Privacy Policy and any terms disclosed and agreed to by you if you purchase additional features,products or services we offer on the Service, contains the entire agreement between you and Travolong regarding the use of the Service. If any provision of this Agreement is held invalid, the remainder of this Agreement shall continue in full force and effect. The failure of the Company to exercise or enforce any right or provision of this Agreement shall not constitute a waiver of such right or provision. You agree that your Travolong account is non-transferable and all of your rights to your account and its Content terminate upon your death. No agency, partnership, joint venture or employment is created as a result of this Agreement and you may not make any representations or bind Travolong in any manner.</p>
            <p>Contact Privacy Terms </p>
        </div>
    </div>
  </div>
</div>

</body>
</html>