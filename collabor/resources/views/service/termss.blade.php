<!DOCTYPE html>
<html lang="en">
<head>
  <title>Terms and Conditions</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <style>
        body{
            font-family: 'Lato', sans-serif;
        }
        
        .sec-onby-on-termscondition h1{
            font-size: 26px;
            margin: 6px 0px;
        }
        
        .sec-onby-on-termscondition h4{
            margin-top: 20px;
            margin-bottom: 5px;
            font-weight: 700;
        }
        
        ol li{
            padding-bottom: 10px;
        }
    </style>
</head>
<body>


<div class="col-xs-12 col-md-12 col-lg-12">
  <div class="container">
    <div class="terms-and-condition">
        <div class="sec-terms-condition sec-onby-on-termscondition">
            <h1>Terms Of Use</h1>
            <h4>INTRODUCTION</h4>
            <p>Linqq™, a brand and/or registered trademark of Epton Applications Pvt Ltd, (hereafter referred to as Linqq™) welcomes You. “You” or “Your” means the individual, company, or entity who entered into this Agreement. Please carefully review these Terms of Use (“TOU” or “Agreement”), as they contain important information regarding Your legal rights, remedies, and obligations. The TOU apply if You visit, view, use, or access (collectively, “access”) Linqq’s websites and/or Linqq mobile applications. (collectively, the “WWW.LINQQAPP.COM”), and/or (hereinafter, “or”) if, through any Internet­ enabled mechanism, You access data, information, products, services, or applications (collectively, “Services”) made available by or from www.linqqapp.com or its mobile applications.</p>
            
            <h4>LEGALLY BINDING AGREEMENT</h4>
            <p>By accessing the Website or Services, You represent and warrant that You have read and understood, and agree to be bound by these TOU and that You acknowledge the adequacy of consideration of this Agreement. Please review this document carefully, as it is a legally binding document between You and linqq™. BY ACCEPTING THIS AGREEMENT, EITHER THROUGH CLICKING ‘PROCEED’ OR CLICKING A BOX INDICATING ACCEPTANCE, YOU AGREE TO THE TERMS OF THIS AGREEMENT. IF YOU ARE ENTERING INTO THIS AGREEMENT ON BEHALF OF A COMPANY OR OTHER LEGAL ENTITY, YOU REPRESENT THAT YOU HAVE THE AUTHORITY TO BIND SUCH ENTITY AND ITS AFFILIATES TO THESE TERMS AND CONDITIONS, IN WHICH CASE THE TERMS "YOU" OR "YOUR" SHALL REFER TO SUCH ENTITY AND ITS AFFILIATES. IF YOU DO NOT HAVE SUCH AUTHORITY, OR IF YOU DO NOT AGREE WITH THESE TERMS AND CONDITIONS, YOU MUST NOT EXECUTE AN ORDER FORM OR OTHERWISE ACCEPT THIS AGREEMENT AND YOU MAY NOT USE THE Linqq™ SERVICES. IF YOU DO NOT AGREE TO THESE TOU, YOU ARE PROHIBITED FROM ACCESSING THE WEBSITE AND THE SERVICES AND MUST EXIT IMMEDIATELY. To the maximum extent permitted and enforceable under applicable law, Epton Applications Pvt Ltd. reserves the right to modify the terms and conditions of this Agreement at any time, effective upon the posting of an updated version of this Agreement. You are responsible for regularly reviewing this Agreement. Your continued use of the Services after any such changes shall constitute Your consent to such changes. Updates to the TOU will be indicated by the “Effective Date” in the Introduction section of this Agreement. If You do not agree to such modifications and updates, please exit the Website and Services immediately.</p>
        
		
            <h4>ELIGIBILITY</h4>
            <p>The Website and Services are not targeted to, and should not be used by, persons under the age of 18. BY ACCESSING THE WEBSITE OR SERVICES, YOU REPRESENT AND WARRANT THAT YOU ARE AT LEAST 18 YEARS OLD, ARE LEGALLY QUALIFIED TO ENTER INTO AND FORM CONTRACTS UNDER APPLICABLE LAW and are not barred from accessing the website or services under the laws of any applicable jurisdiction.</p>
        
		
            <h4>PRIVACY POLICY</h4>
            <p>4.1 Privacy Statement: By accessing the Website, You consent to the collection and use of certain information about You, as specified in the Linqq™ Member and Web Site Visitor Privacy Statement (“Privacy Statement”), incorporated hereto. Linqq™ encourages users of the Website to frequently check the Privacy Statement. By accessing the Website, You represent and warrant that You have read and understood, and agree to be bound by, the Privacy Statement. IF YOU DO NOT UNDERSTAND OR DO NOT AGREE TO BE BOUND BY THE PRIVACY STATEMENT, YOU MUST IMMEDIATELY LEAVE THE WEBSITE.</p>
            <p>4.2 Modification to Privacy Statement. Epton Applications Pvt Ltd. RESERVES THE RIGHT TO CHANGE THIS AGREEMENT AND/OR THE PRIVACY STATEMENT AT ANY TIME. To the maximum extent permitted and enforceable under applicable law, EAPL reserves the right to modify the terms and conditions of the Privacy Statement at any time, effective upon the posting of an updated version of the Privacy Statement. You are responsible for regularly reviewing the Privacy Statement and continued use of the Services after any such changes shall constitute Your consent to such changes. Notice of any such change will be given by the posting of an updated Privacy Statement or a change notice on the Website. It is Your responsibility to review the Privacy Statement periodically. If at any time You find anything unacceptable, You must immediately leave the Website and refrain from using the Website and Services.</p>
        
		
            <h4>MEMBER REGISTRATION</h4>
            <p>5.1 By accessing the Website, You are not automatically a Linqq™ Member; You are a “Website Visitor”. In order to become a Linqq™ Member, You are required to complete a registration process. You certify, represent and warrant that the information You provide therein is true, accurate, complete, current, and that it belongs to You. You shall keep Your information complete and up to ­date at all times. Failure to maintain Your registration information may cause Your access to the Services to be interrupted, suspended, or terminated. You are responsible for monitoring Your account, changing Your password periodically and notifying Linqq™ immediately of any unauthorized use or breach of security of Your password or other information.</p>
        
		
            <h4>LICENSE</h4>
            <p>Subject to Your compliance with the terms and conditions of this Agreement, and any other agreement between You and Linqq™, Linqq™ grants You a non­exclusive, non-­sublicensable, non-­assignable, revocable, non-­transferable license to access the Website, Mobile Application and other Services. Except as expressly set forth herein, this Agreement grants You no rights in or to the intellectual property of Linqq™, or any other party. In the event that You breach any provision of this Agreement, Your rights under this paragraph will immediately get terminated. By accepting this license, You agree that all information contained within Linqq™ is the proprietary, confidential information of Linqq™, that You will safeguard and protect such information, and that You will use the information in accordance with this Agreement’s Code of Conduct. Your obligations set forth in this Agreement shall survive termination of this Agreement. To the extent You elect to submit contact or company information (including portions thereof) to Linqq™ , You hereby grant Linqq™ a royalty free, worldwide, irrevocable, perpetual license to use and incorporate into the Website and Services any such submitted information (including any portions thereof submitted by You) and distribute such information through the Website and Services.</p>
        
		
            <h4>CODE OF CONDUCT</h4>
            <p>This section sets forth the Linqq™ Code of Conduct (the “Code”). All Members and Website Visitors must fully comply with this Code at all times. You certify, represent and warrant that You will not violate this Code.</p>
			<p>(A) Restrictions on Inputting Information. You shall not enter illegal or improper information into the Website or in or through the Services, including, without limitation, the following:</p>
			<ol>
				<li>Information subject to confidentiality, non­disclosure, non­competition, trade secret or proprietary rights, limitations or restrictions;</li>
				<li>Information that is false, inaccurate, incorrect, incomplete, inexact, outdated or otherwise wrong;</li>
				<li>Information that is sexually explicit, profane, pornographic, immoral, obscene, vulgar, offensive, inflammatory, violent, dangerous, harmful, threatening, abusive, harassing, hateful, discriminatory or racially, ethnically or otherwise objectionable, or which may solicit information from anyone under the age of 18;</li>
				<li>Information that infringes the copyrights or intellectual property rights of others;</li>
				<li>Information that You do not have the necessary rights and/or licenses to enter.</li>
				<li>Information that is defamatory, libellous, fraudulent, knowingly incorrect, or invasive of privacy or publicity rights of others;</li>
				<li>Information that is personal or non-business related, including, but not limited to, home addresses, Social Security numbers, credit card numbers, mobile or cellular telephone numbers;</li>
				<li>Information that advocates or encourages conduct that could constitute a criminal offense;</li>
				<li>Information that is actionable or may subject Linqq™ to legal action or liability of any kind;</li>
				<li>Information that violates any provision of this Agreement or any other agreement or policy set forth by Linqq™.</li>
				<li>Information that violates any applicable local, state, national or international law, regulation, or convention; or</li>
				<li></li>
			</ol>
			<p>Use of Information Obtained via Website/Services. Linqq™ has no actual control over Your use of information outside the Website. You shall not access the Website or Services for any purposes or in any manner that is illegal or improper, including, without limitation, the following:</p>
				<ol>
					<li>To communicate with anyone using language or in any manner that is sexually explicit, profane, pornographic, immoral, obscene, vulgar, offensive, violent, dangerous, harmful, threatening, abusive, harassing, hateful, discriminatory, or racially, ethnically or otherwise objectionable;</li>
					<li>In violation of any local, state, national or international laws, regulations or conventions;</li>
					<li>To illegally ‘spam’ anyone or to sell, give, make available or otherwise distribute information to a spammer or for the purpose of spamming;</li>
					<li>In connection with any individual credit, employment, or insurance decision;</li>
					<li>To associate, attribute, collect, store, distribute or otherwise process any non­business information related to anyone;</li>
					<li>For unethical marketing activities;</li>
					<li>To prepare or compile information that is distributed in any manner or form to any third-party;</li>
					<li>To enhance, verify, supplement, append, confirm, or modify any compilation of information that is thereinafter distributed in any manner or form to a third-party;</li>
					<li>For any purpose, activity or in any manner that is criminal, illegal or actionable;</li>
					<li>To disclose or solicit the private, non­business, information of any person;</li>
					<li>For sale, re­sale, sub­license, commercial use, or redistribution of any kind, without Linqq™’s express, prior consent.</li>
				</ol>
		<p>Acts against the Website/Services. You shall not attempt to or engage in potentially harmful acts that are directed against the Website or Services including, without limitation, the following:</p>
			<ol>
				<li>Causing, allowing or assisting machines, bots or automated services to access or use the Website or Services without the express written permission of Linqq™;</li>
				<li>Causing, allowing or assisting any other person to use Your account or impersonate You;</li>
				<li>Falsely stating or otherwise misrepresenting Your affiliation with any person or entity;</li>
				<li>Logging onto a server or account that You are not authorized to access;</li>
				<li>Forging screen names manipulating identifiers, or otherwise impersonating any other person or misrepresenting Your identity or affiliation with any person or entity;</li>
				<li>Emulating or faking usage of the Website or Services;</li>
				<li>Attempting to probe, scan, or test the vulnerability of the Website, or any associated system or network, or breach any security or authentication measures;</li>
				<li>Using manual or automated software, devices, scripts robots, other means or processes to access, “scrape,” “crawl” or “spider” any pages contained in the Website;</li>
				<li>Using the Website or Services in contravention of any other agreement to which You are a party, including without limitation any employment agreement to which You may be a party;</li>
				<li>Introducing viruses, worms, software, Trojan horses or other similar harmful code into the Website or Services;</li>
				<li>Interfering or attempting to interfere with the use of the Website by any other user, host or network, including, without limitation by means of submitting a virus, overloading, “flooding,” “spamming,” “mailbombing,” or “crashing” the Website;</li>
				<li>Sharing Your password or login with any other person;</li>
				<li>Tampering with the operation, functionality or the security of the Website or Services;</li>
				<li>Attempting to override or circumvent any security or usage rules embedded into the Website or Services that permit digital materials to be protected;</li>
				<li>Engaging in “framing,” “mirroring,” or otherwise simulating the appearance or function of the Website;</li>
				<li>Misusing, tricking, disrupting or otherwise interfering with the functioning of the Website or Services;</li>
				<li>Harvesting or collecting email addresses or other contact information of other users from the Website by electronic or other means;</li>
				<li>Reverse engineering, decompiling, disassembling, deciphering or otherwise attempting to derive the source code for any underlying intellectual property used to provide the Website or Services; </li>
				<li>Violating or attempting to violate any security features of the Website;</li>
				<li>Uploading, posting, transmitting, sharing, storing or otherwise making available any content that Linqq™ deems to be harmful, threatening, unlawful, defamatory, infringing, abusive, inflammatory, harassing, vulgar, obscene, fraudulent, invasive of privacy or publicity rights, hateful, or racially, ethnically or otherwise objectionable; and</li>
				<li>Without Linqq™’s prior, express consent, advertising or selling any products, services or otherwise (whether or not for profit) or soliciting others or using the Website for commercial purposes of any kind.</li>
			</ol>
			<p>Suspected Misuse and Penalties. Linqq™ may monitor the Website and Services for misuse of the Code. Linqq™ shall be the sole and final arbiter of suspected Code violations, misuse and penalty. For suspected violations, and without limiting any of its other remedies, Linqq™ reserves the right to immediately and without notice:</p>
			<ol>
				<li>Delete or modify content;</li>
				<li>Identify You to necessary law agencies ;</li>
				<li>Take legal action.</li>
				<li>Suspend Your account;</li>
				<li>Terminate Your account;</li>
			</ol>
			<p>Linqq™ intends to cooperate fully with any law enforcement officials or agencies in the investigation of any violation of this Agreement or of any applicable laws. Linqq™ reserves the right to seek civil, criminal or injunctive relief, at its sole discretion and without obligation, to enforce this Code.</p>
			
            <h4>INTELLECTUAL PROPERTY</h4>
            <p>The Website/ Mobile application and all content and materials located thereon, including without limitation any Linqq™ names and logos (the “Linqq™ Marks”), the Services, Directory, designs, text, graphics and other files, and the selection, arrangement and organization thereof, are the intellectual property of Linqq™ or its licensors. Except as explicitly provided, neither the Website nor this Agreement grant You any right, title or interest in or to any such content or materials. The Linqq™ Marks are trademarks or registered trademarks of Linqq™. Other trademarks, service marks, graphics, logos and domain names appearing on the Website may be the trademarks of third parties. The Website is Copyright © 2018, Epton  Applications Pvt Ltd., ALL RIGHTS RESERVED. Moreover, except as expressly stated herein, or as expressly granted by Linqq™ in a signed writing, You have no intellectual property or other rights in the information You contribute to the Directory. As Linqq™ asks others to respect its intellectual property rights, Linqq™ respects the intellectual property rights of others. You agree that You shall not remove, obscure, or alter any proprietary rights notices (including copyright and trade mark notices) which may be affixed to or contained within the Services. Likewise, if You have evidence, know, or have a good faith belief that Your rights or the rights of a third party have been violated and You want Linqq™ to delete, edit, or disable the material in question, You must provide Linqq™ with all of the following information:</p>
			<ol>
				<li>(a) physical or electronic signature of a person authorized to act on behalf of the owner of the exclusive right that is allegedly infringed;</li>
				<li>identification of the material that is claimed to be infringed or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit Linqq™ to locate the material;</li>
				<li>identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works are covered by a single notification, a representative list of such works;</li>
				<li>information reasonably sufficient to permit Linqq™ to contact You, such as an address, telephone number, and if available, an electronic mail address at which You may be contacted;</li>
				<li>(a) statement that the information in the notification is accurate, and under penalty of perjury, that You are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed. For this notification to be effective, You must provide it to Linqq™’s designated representative at:</li>
			</ol>
		
		
		
            <p>Linqq™ Representative</p>
            <p>Epton Applications Pvt Ltd</p>
            <p>Building No.13</p>
            <p>Street No. 78</p>
            <p>Punjabi Bagh(W)</p>
            <p>New Delhi,</p>
            <p>INDIA, </p>
            <p>Pin Code-110026 </p>
        <br>
		
		<p>6. (a) statement that You have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; and</p>
		
            <h4> COPYRIGHT INFRINGEMENT POLICY</h4>
            <p>Linqq™ respects the intellectual property rights of others. If you believe that your work has been copied in a way that constitutes copyright infringement, please provide Linqq™'s Copyright Agent at the e­mail address support@linqqapp.com with the information specified below in the form of a "Notification of Alleged Infringement." It is Linqq™'s policy to respond to clear Notifications of Alleged Infringement, and our policy is designed to make submitting Notifications of Alleged Infringement as straightforward as possible while reducing the number of Notifications that we receive that are fraudulent or difficult to understand or verify. If you are a User or subscriber and concerned about the removal of or blocked access to your content, please provide Linqq™'s Copyright Agent with the written information specified below in the form of a "Counter­ Notification." The forms specified below are consistent with the forms suggested by The Copyright Act, 1957 as amended up to 2012 (the text of which can be found at the Indian Copyright Office Website at http://www.copyright.gov.in ).</p>
			<p>To be considered effective, a Notification of Alleged Infringement must be submitted in writing and include the following information:</p>
			
			<ol>
				<li>Physical or electronic signature of the owner, or a person authorized to act on behalf of the owner, of an exclusive copyright that has allegedly been infringed</li>
				<li>Identification of the material that is claimed to be infringing or to be the subject of infringing activity that is to be removed or access to which is to be disabled</li>
				<li>Identification of the copyrighted material claimed to have been infringed </li>
				<li>Information reasonably sufficient to permit Linqq™ to contact person submitting the Notification, such as a physical address, email address, and telephone number</li>
				<li>Information reasonably sufficient to permit Linqq™ to locate the material that is claimed to be infringing or to be the subject of infringing activity</li>
				<li>A statement that the information in the Notification is accurate, and under penalty of perjury, that the person submitting the Notification is authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.</li>
				<li>A statement that the person submitting the Notification has a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law. </li>
			</ol>
		
            <h4>THIRD PARTY WEBSITE</h4>
            <p>On Linqq™’s Website, You may find links to websites operated by third parties (“Third-party Sites”). Linqq™ does not endorse or control Third-party Sites, each of which may be governed by its own terms of service and privacy policy. Linqq™ disclaims, and You hereby agree to assume, all responsibility and liability for any damages or other harm, whether to You or third parties, resulting from Your use of Third-party Sites. Please take all protections necessary to protect Yourself and Your computer system when accessing Third-party Sites, particularly when downloading or purchasing anything therefrom. </p>
        
		
            <h4>INDEMNIFICATION</h4>
            <p>You agree to indemnify and hold Linqq™, its directors, officers, employees and agents, and its suppliers, licensors, and service providers, harmless from and against any loss, liability, claim, demand, damages, costs and expenses, including reasonable attorneys' fees (collectively, “Claims”), arising out of or in connection with: (1) Your use of the Website or Services; (2) any violation of this Agreement (including, without limitation, the Code) ; (3) Your violation of any applicable law or the intellectual property rights of any third party. Linqq™ will have the right, but not the obligation, to participate through counsel of its choice in any defence by You of any Claims as to which You are required to defend, indemnify, or hard harmless Linqq™. You may not settle any Claims without the prior written consent of the concerned Linqq™ person or persons.</p>
        
		
            <h4>DISCLAIMERS / LIMITATION OF LIABILITY</h4>
            <p>12.1 YOUR RESPONSIBILITY FOR YOUR ACTIONS YOU AGREE AND UNDERSTAND THAT YOU MAY BE HELD LEGALLY RESPONSIBLE FOR DAMAGES SUFFERED BY OTHER WEBSITE VISITORS OR THIRD PARTIES AS THE RESULT OF YOUR REMARKS, INFORMATION, FEEDBACK OR OTHER CONTENT POSTED OR MADE AVAILABLE ON THE WEBSITE OR THROUGH THE SERVICES THAT IS DEEMED DEFAMATORY OR OTHERWISE LEGALLY ACTIONABLE.</p>
			<p>12.2 YOUR RESPONSIBILITY FOR DAMAGE YOU AGREE THAT YOUR USE OF THE WEBSITE OR SERVICES IS AT YOUR SOLE RISK. YOU WILL NOT HOLD Linqq™ OR ITS LICENSORS OR SUPPLIERS, AS APPLICABLE, RESPONSIBLE FOR ANY DAMAGE OR LOSS THAT RESULTS FROM YOUR USE OF THE WEBSITE OR SERVICES, INCLUDING WITHOUT LIMITATION ANY DAMAGE TO ANY OF YOUR COMPUTERS OR DATA. THE WEBSITE OR SERVICES MAY CONTAIN BUGS, ERRORS, PROBLEMS OR OTHER LIMITATIONS.</p>
			<p>12.3 THE LIABILITY OF Linqq™, ITS DIRECTORS, OFFICERS, EMPLOYEES, 
AND AGENTS, AND ITS LICENSORS AND SUPPLIERS, IS LIMITED TO THE MAXIMUM EXTENT PERMITTED BY LAW, IN NO EVENT SHALL Linqq™, ITS DIRECTORS, OFFICERS, EMPLOYEES, AND AGENTS, OR ITS LICENSORS OR SUPPLIERS BE LIABLE FOR ANY SPECIAL, INCIDENTAL, PUNITIVE OR CONSEQUENTIAL DAMAGES (INCLUDING WITHOUT LIMITATION LOST PROFITS, LOST DATA OR CONFIDENTIAL OR OTHER INFORMATION, LOSS OF PRIVACY, FAILURE TO MEET ANY DUTY, INCLUDING WITHOUT LIMITATION OF GOOD FAITH OR OF REASONABLE CARE, NEGLIGENCE, OR OTHERWISE, REGARDLESS OF THE FORESEEABILITY OF THOSE DAMAGES OR OF ANY ADVICE OR NOTICE GIVEN TO Linqq™, ITS DIRECTORS, OFFICERS, EMPLOYEES, AND AGENTS, OR ITS LICENSORS AND SUPPLIERS) ARISING OUT OF OR RELATING TO YOUR USE OF THE WEBSITE OR SERVICES. THIS LIMITATION SHALL APPLY REGARDLESS OF WHETHER THE DAMAGES ARISE OUT OF BREACH OF CONTRACT, OR ANY OTHER LEGAL THEORY OR FORM OF ACTION. ADDITIONALLY, THE MAXIMUM LIABILITY OF Linqq™, ITS DIRECTORS, OFFICERS, EMPLOYEES, AND AGENTS, AND ITS LICENSORS AND SUPPLIERS TO YOU UNDER ALL CIRCUMSTANCES WILL NOT EXCEED THE GREATER OF INR. 5000 OR THE AMOUNT if any that YOU HAVE PAID US IN THE LAST 12 MONTHS </p>
			<p>12.4 LIMITATION OF LIABILITY YOU AGREE THAT NEITHER Linqq™, ITS DIRECTORS, OFFICERS, EMPLOYEES, AND AGENTS, NOR ITS LICENSORS OR SUPPLIERS, HAVE ANY LIABILITY WHATSOEVER IN CONNECTION WITH YOUR ACCESS OF THE WEBSITE OR THE SERVICES. Linqq™ IS NOT RESPONSIBLE FOR THE ACTIONS OF THIRD­ PARTIES (INCLUDING OTHER WEBSITE VISITORS OR MEMBERS), AND YOU RELEASE Linqq™, ITS DIRECTORS, OFFICERS, EMPLOYEES AND AGENTS FROM ANY CLAIMS AND DAMAGES, KNOWN OR UNKNOWN, ARISING OUT OF OR IN ANY WAY CONNECTED WITH ANY CLAIM YOU HAVE AGAINST ANY SUCH THIRD ­PARTIES. IN THIS REGARD.</p>
			<p>12.5 NO WARRANTIES ALTHOUGH Linqq™ TAKES REASONABLE MEASURES TO KEEP THE WEBSITE AND SERVICES ERROR­FREE AND SAFE, YOU ACCESS THEM AT YOUR OWN RISK. Linqq™, ON BEHALF OF ITSELF AND ITS LICENSORS AND SUPPLIERS, HEREBY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, RELATING TO THE WEBSITE AND THE SERVICES, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT. THE WEBSITE AND SERVICES ARE PROVIDED “AS IS” AND “AS AVAILABLE.” NEITHER Linqq™ NOR ITS LICENSORS OR SUPPLIERS WARRANT THAT THE WEBSITE OR THE SERVICES WILL MEET YOUR REQUIREMENTS OR THAT THE OPERATION OF THE WEBSITE OR THE SERVICES WILL BE UNINTERRUPTED, ERROR­FREE, SAFE OR SECURE.</p>
			<p>12.6 APPLICATION THE ABOVE DISCLAIMERS, WAIVERS AND LIMITATIONS DO NOT IN ANY WAY LIMIT ANY OTHER DISCLAIMER OF WARRANTIES OR ANY OTHER LIMITATIONS OF LIABILITY BETWEEN YOU AND Linqq™ OR BETWEEN YOU AND ANY OF Linqq™’S DIRECTORS, OFFICERS, EMPLOYEES, AGENTS, LICENSORS OR SUPPLIERS. SOME JURISDICTIONS MAY NOT ALLOW THE EXCLUSION OF CERTAIN IMPLIED WARRANTIES OR THE LIMITATION OF CERTAIN DAMAGES, SO SOME OF THE ABOVE DISCLAIMERS, WAIVERS AND LIMITATIONS OF LIABILITY MAY NOT APPLY TO YOU. UNLESS LIMITED OR MODIFIED BY APPLICABLE LAW, THE FOREGOING DISCLAIMERS, WAIVERS AND LIMITATIONS SHALL APPLY TO THE MAXIMUM EXTENT PERMITTED, EVEN IF ANY REMEDY FAILS ITS ESSENTIAL PURPOSE. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU THROUGH THE WEBSITE, SERVICES, OR OTHERWISE SHALL ALTER ANY OF THE DISCLAIMERS OR LIMITATIONS STATED IN THIS SECTION.</p>
		
            <h4>TERMINATION</h4>
            <p>13.1 Without altering any other provision of this Agreement, Linqq™ may terminate
this Agreement without notice or liability. Linqq™ may also deny, suspend, cancel, or</p>
<p>terminate Your use of the Website or Services, including without limitation, for breach
or suspected breach of this Agreement or of any applicable law or regulation.</p>
       <p>13.2 Account Deletion. You can terminate this contract by deleting your Account at any time by going to the 'Account Settings' page when you are logged in and clicking on the 'Delete account' link. Your Account will be deleted with immediate effect.
Deleting your account means permanently deleting your profile and removing access to all your information on Linqq™.</p>
		
            <h4>ARBITRATION</h4>
            <p>EXCEPT AS MAY OTHERWISE BE PROVIDED BELOW OR IN A SIGNED/ ACCEPTED AGREEMENT BETWEEN YOU AND Linqq™, THE SOLE AND EXCLUSIVE FORUM AND REMEDY FOR ANY AND ALL DISPUTES AND CLAIMS RELATING IN ANY WAY TO OR ARISING OUT OF THIS AGREEMENT OR YOUR ACCESS OF THE WEBSITE OR SERVICES SHALL BE FINAL AND BINDING ARBITRATION, except that: (a) to the extent that either of You or Linqq™ has in any manner infringed upon or violated or threatened to infringe upon or violate the other party's patent, copyright, trademark or trade secret rights, or You have otherwise violated the Code set forth above, then You and Linqq™ acknowledge that arbitration is not an adequate remedy at law and that injunctive or other appropriate relief may be sought; and (b) no disputes or claims relating to any transactions You enter into with a third party through the Website may be arbitrated.</p>
			<p>Arbitration under this Agreement shall be conducted in accordance with The Indian Arbitration and Conciliation Act, 1996. The location of the arbitration shall be in New Delhi, India, and the allocation of costs and fees for such arbitration shall be borne equally by both the parties. The arbitrator's award shall be binding and may be entered as a judgment in any court of competent jurisdiction.</p>
		
		
		
            <h4>GOVERNING LAW</h4>
            <p>This Agreement, including without limitation its construction and enforcement, shall be treated as though it were executed and performed in New Delhi, India, and shall be governed by and construed in accordance with the laws of India without regard to its conflict of law principles.</p>
			<p>To the extent arbitration is not required under this Agreement, THE PROPER VENUE FOR ANY JUDICIAL ACTION ARISING OUT OF OR RELATING TO THIS AGREEMENT OR THE WEBSITE OR SERVICES WILL BE THE HON’BLE COURTS AT New Delhi, INDIA. IN CONNECTION WITH THIS SECTION, THE PARTIES HEREBY STIPULATE TO, AND AGREE TO WAIVE ANY OBJECTION TO, THE PERSONAL JURISDICTION AND VENUE OF SUCH COURTS, AND FURTHER EXPRESSLY SUBMIT TO EXTRATERRITORIAL SERVICE OF PROCESS.</p>
			<p>To the fullest extent permitted by applicable law, NO ARBITRATION OR CLAIM ARISING OUT OF OR RELATING TO THIS AGREEMENT OR THE WEBSITE OR SERVICES SHALL BE JOINED TO ANY OTHER ARBITRATION OR CLAIM, INCLUDING ANY ARBITRATION OR CLAIM INVOLVING ANY OTHER CURRENT OR FORMER WEBSITE VISITORS, AND NO CLASS ARBITRATION PROCEEDINGS SHALL BE PERMITTED. In no event shall any claim, action or proceeding by You related in any way to this agreement, the Website or Services be instituted more than one (1) year after the cause of action arose.</p>
		
		
		
		
            <h4>MISCELLANEOUS</h4>
            <p>- Linqq™’s failure to exercise or enforce any right or provision of this Agreement shall not constitute a waiver of such right or provision.</p>
            <p>- If any provision of this Agreement shall be deemed unlawful, void, or for any reason unenforceable, then that provision shall be deemed severable from the Agreement and shall not affect the validity and enforceability of any remaining provisions.</p>
			
			<p>Product Website</p>
			<p>About us</p>
			<p>Interviews</p>
			<p>Blogs and Media</p>
			<p>Contact us</p>
		
		
		    <p>- Linqq™ may assign its rights and duties under this Agreement to any party at any time without notice to You. Your rights and duties under this Agreement are not assignable by You without written consent of Linqq™.</p>
			<p>- Headings are used for convenience only and are not to be used for meaning or intent.</p>
			<p>- This Agreement,and any other signed writing between You and Linqq™ regarding the Services, constitutes the entire understanding between You and Linqq™ and supersedes any prior or contemporaneous communications or provisions on the subject matter. This Agreement cannot be modified, unless in a writing labelled “Modification to TOU”, executed by both You and an officer of Linqq™.</p>
			<p>- Linqq™ values Your enthusiasm of its Website and Services. If You have any questions, comments, suggestions, or concerns, or if You would like to provide any notice under this Agreement, please contact us via certified mail at:</p>
		
			<h6>Epton Applications Pvt Ltd</h5>
			<h5>Building No. 13</h5>
			<h5>Street No.78</h5>
			<h5>Punjabi Bagh(W)</h5>
			<h5>New Delhi, </h5>
			<h5>INDIA </h5>
			<h5>Pin Code-110026  </h5>
		
		
		
		
            
        
        </div>
        
        
    </div>
  </div>
</div>

</body>
</html>
