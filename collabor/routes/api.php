<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('check_mail', 'API\UserController@check_mail');


Route::post('testPush', 'API\UserController@testPush');
Route::post('geocode_by_address', 'API\UserController@geocode_by_address');


Route::post('company_and_desig_list', 'API\UserController@company_and_desig_list');

Route::post('login', 'API\UserController@login');
Route::post('unique_user', 'API\UserController@unique_user');
Route::post('forgot_password', 'API\UserController@forgot_password');
Route::post('signup', 'API\UserController@signup');
Route::post('linkdin_signup', 'API\UserController@linkdin_signup');
Route::post('join_team', 'API\UserController@join_team');
Route::post('send_team_request', 'API\UserController@send_team_request');
Route::post('unique_user_otp', 'API\UserController@unique_user_otp');

Route::group(['middleware' => 'auth:api'], function(){

	Route::post('logout', 'API\UserController@logout');
	Route::post('profile', 'API\UserController@profile');
	Route::post('edit_profile', 'API\UserController@edit_profile');
	Route::post('change_password', 'API\UserController@change_password');
	Route::post('delete_message', 'API\UserController@delete_message');
	Route::post('user_message_template', 'API\UserController@user_message_template');
	Route::post('add_message_template', 'API\UserController@add_message_template');
	Route::post('edit_message_template', 'API\UserController@edit_message_template');
	Route::post('delete_message_template', 'API\UserController@delete_message_template');
	Route::post('add_organisations', 'API\UserController@add_organisations');
	Route::post('edit_organisations', 'API\UserController@edit_organisations');
	Route::post('delete_organisations', 'API\UserController@delete_organisations');
	Route::post('user_organisations', 'API\UserController@user_organisations');
	Route::post('add_education', 'API\UserController@add_education');
	Route::post('edit_education', 'API\UserController@edit_education');
	Route::post('delete_education', 'API\UserController@delete_education');
	Route::post('delete_networking', 'API\UserController@delete_networking');
	Route::post('user_networking', 'API\UserController@user_networking');
	Route::post('networking_list', 'API\UserController@networking_list');
	Route::post('add_networking', 'API\UserController@add_networking');
	Route::post('user_education', 'API\UserController@user_education');
	Route::post('add_favourite', 'API\UserController@add_favourite'); 
	Route::post('user_favourite', 'API\UserController@user_favourite');
	Route::post('delete_favourite', 'API\UserController@delete_favourite');
	Route::post('add_interest', 'API\UserController@add_interest');
	Route::post('user_interest', 'API\UserController@user_interest');
	Route::post('delete_interest', 'API\UserController@delete_interest');
	Route::post('interest_list', 'API\UserController@interest_list');
	
	Route::post('industry_list', 'API\UserController@industry_list');
	Route::post('all_user_profile', 'API\UserController@all_user_profile');
	Route::post('send_message', 'API\UserController@send_message');
	Route::post('chat_list', 'API\UserController@chat_list');
	Route::post('get_ads', 'API\UserController@get_ads');
	Route::post('send_request', 'API\UserController@send_request');
	Route::post('connection_list', 'API\UserController@connection_list');
	Route::post('send_request_list', 'API\UserController@send_request_list');

	Route::post('search_user', 'API\UserController@search_user');
	Route::post('view_profile', 'API\UserController@view_profile');
	Route::post('notification_list', 'API\UserController@notification_list');
	Route::post('accept_request', 'API\UserController@accept_request');
	Route::post('reject_request', 'API\UserController@reject_request');
	Route::post('chat_history', 'API\UserController@chat_history');
	Route::post('send_report', 'API\UserController@send_report');
	Route::post('user_request_template', 'API\UserController@user_request_template');
	Route::post('notification_setting', 'API\UserController@notification_setting');
	Route::post('email_push_setting', 'API\UserController@email_push_setting');
	Route::post('my_info', 'API\UserController@my_info');
	Route::post('card_count', 'API\UserController@card_count');
	Route::post('degree_list', 'API\UserController@degree_list');
	Route::post('ignore_user', 'API\UserController@ignore_user');
	Route::post('update_location', 'API\UserController@update_location');
	Route::post('send_invite', 'API\UserController@send_invite');
    Route::post('already_sent', 'API\UserController@already_sent'); 
    Route::post('all_user_profiletest', 'API\UserController@all_user_profiletest');
    Route::post('all_user_profilet', 'API\UserController@all_user_profilet');

    
    //Route::post('save_autobid', 'API\UserController@save_autobid');
    Route::post('update_single', 'API\UserController@update_single');
    Route::post('all_user_profilet', 'API\UsertestController@all_user_profilet');
    Route::post('timer_timeleft', 'API\UserController@timer_timeleft'); 
    Route::post('chat_listr', 'API\UsertestController@chat_listr');
    Route::post('chat_countapi', 'API\UserController@chat_countapi');
    Route::post('send_template', 'API\UserController@send_template');
     Route::post('feedback', 'API\UserController@feedback');  


});

