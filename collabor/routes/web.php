<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    


    Route::post('add_profile_add', 'AdminController@add_profile_add');
    Route::post('send_notification', 'AdminController@send_notification');
    Route::get('/', 'AdminController@login')->name('admins.login');
    Route::get('login', 'AdminController@login')->name('admins.login');
    Route::post('login', 'AdminController@login')->name('admins.login');
    Route::get('logout', 'AdminController@logout');
    Route::get('user_list', 'AdminController@user_list')->name('admins.user_list');
    Route::get('player_list', 'AdminController@player_list')->name('admins.player_list');
    Route::get('edit_profile', 'AdminController@edit_profile');
    Route::post('common_delete', 'AdminController@common_delete');
    Route::post('common_get', 'AdminController@common_get');
    Route::post('card_get', 'AdminController@card_get');
    Route::match(array('GET','POST'),'forgot_password', 'AdminController@forgot_password');
    Route::match(array('GET','POST'),'profile', 'AdminController@profile');
    Route::get('interest_list', 'AdminController@interest_list');
    Route::get('industry_list', 'AdminController@industry_list');

    Route::post('add_company', 'AdminController@add_company');
    Route::get('company_list', 'AdminController@company_list');

     Route::post('add_designation', 'AdminController@add_designation');
    Route::get('designation_list', 'AdminController@designation_list');
    Route::post('upload_designation', 'AdminController@upload_designation');

    Route::post('add_interest', 'AdminController@add_interest');
    Route::post('add_industry', 'AdminController@add_industry');
    
    Route::post('upload_company', 'AdminController@upload_company');
    Route::post('upload_interest', 'AdminController@upload_interest');
    Route::post('upload_industry', 'AdminController@upload_industry');
    Route::post('upload_degree', 'AdminController@upload_degree');
    Route::get('advertisement_list', 'AdminController@advertisement_list');
    Route::post('add_advertisement', 'AdminController@add_advertisement');
    Route::get('degree_list', 'AdminController@degree_list');
    Route::post('add_degree', 'AdminController@add_degree');
    Route::post('add_ads_count', 'AdminController@add_ads_count');
    Route::get('message_template_list', 'AdminController@message_template_list');
    Route::post('add_message_template', 'AdminController@add_message_template');
    Route::get('user_report_list', 'AdminController@user_report_list');
    Route::get('request_template_list', 'AdminController@request_template_list');
    Route::post('add_request_template', 'AdminController@add_request_template');
    Route::post('add_radius', 'AdminController@add_radius');
    Route::post('add_count', 'AdminController@add_count');
    Route::get('view_user/{id}', 'AdminController@view_user');
    Route::get('data_list/{name?}/{location?}/{networking_id?}/{interest_id?}/{favourite_id?}/{gender?}', 'AdminController@data_list')->name('admins.data_list');
    Route::get('data_list_notification/{name?}/{location?}/{networking_id?}/{interest_id?}/{favourite_id?}/{gender?}', 'AdminController@data_list_notification')->name('admins.data_list_notification');
   
    
});

Auth::routes();

Route::get('service/service_info', function () {
    return view('service.service_info');
}); 

Route::get('about', function () {
    return view('service.about');
}); 

Route::get('support', function () {
    return view('service.support');
}); 


Route::get('service/termss', function () {
    return view('service/termss');
}); 

Route::get('service/privacy', function () { 
    return view('service/privacy');
});

Route::get('service/redirect/{id}', function () {
    return redirect('https://play.google.com/store/apps/details?id=com.linqq');
});

