@extends('layouts.admins')
@section('title', 'Advertisement List')
@section('content')

<div class="row">
</div>
    <div class="col-md-12">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                  Advertisement List
                </h3>
            </div>
            <div class="panel-body">
                <button style="float: right;" onclick="get_ads_count()" class="btn btn-warning" data-toggle="modal" data-target="#myModal2"><i class="glyphicon glyphicon-plus"></i> Advertisement Count</button>

                <button style="float: right;" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-plus"></i> Add Advertisement</button>
                <table class="table table-bordered" id="data">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Title
                            </th>
                            <th>
                                Description
                            </th>
                            <th>
                                URL
                            </th>
                            <th>
                                Image
                            </th>
                            <th>
                                Action
                            </th>
                        </tr>
                    </thead>
                    @if(!empty($ads))
                    <tbody>
                        <?php $i = 0;?>
                        @foreach($ads as $value)
                        <tr>
                            <td>
                                {{ ++$i }}
                            </td>
                            <td>
                                {{$value->title}}
                            </td>
                            <td>
                                {{$value->description}}
                            </td>
                            <td>
                                {{$value->url}}
                            </td>
                            <td>
                             <img src='{{url("public/ads/$value->image")}}' class="img-circle" style="height:50px"></img> 
                            </td>
                            <td>
                                <a class="action_an" href="javascript:void(0);" data-toggle="modal" data-target="#myModal" onclick="get_ads({{$value->id}})">
                                    <span class="dlt_icon">
                                        <img class="img-responsive" src="{{url('/public')}}/img/edit.png"/>
                                    </span>
                                </a>
                                <a class="action_an" href="javascript:void(0);" onclick="delete_user({{$value->id}})">
                                    <span class="dlt_icon">
                                        <img class="img-responsive" src="{{url('/public')}}/img/delete-button.png"/>
                                    </span>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
 
    <!-- Bootstrap modal -->
  <div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" id="btnClose" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Advertisement</h3>
      </div>
      <form action="{{url('admin/add_advertisement')}}" method="post" id="form" class="form-horizontal" enctype="multipart/form-data">
        <div class="modal-body form">
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">Title</label>
              <div class="col-md-9">
                {{ csrf_field() }}
                <input type="hidden" name="id" id="id">
                <input name="title" id="title" placeholder="Title" class="form-control" type="text" value="{{old('title')}}">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Description</label>
              <div class="col-md-9">
                <input name="description" id="description" placeholder="Description" class="form-control" type="text" value="{{old('description')}}">
              </div>
            </div>
             <div class="form-group">
              <label class="control-label col-md-3">URL</label>
              <div class="col-md-9">
                <input name="url" id="url" placeholder="URL" class="form-control" type="text" value="{{old('url')}}">
              </div>
            </div>
           <div class="form-group">
              <label class="control-label col-md-3">Advertisement Image</label>
              <div class="col-md-9">
                <input name="image" placeholder="Image" class="form-control" type="file" id="profile-img">
              </div>
            </div>
            <div class="image_change_edit">
                 <img src="{{url('public/img/ads.jpeg')}}" class="img-responsive center-block" id="profile-img-tag" width="150px">
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave"  class="btn btn-primary">Save</button>
            <button type="button" id="btnCancle" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
</div>
    <!-- END BORDERED TABLE -->


      <!-- Bootstrap modal -->
  <div class="modal fade" id="myModal2" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" id="btnClose" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Advertisement Count</h3>
      </div>
      <form action="{{url('admin/add_ads_count')}}" method="post" id="form" class="form-horizontal" enctype="multipart/form-data">
        <div class="modal-body form">
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">Ads Count</label>
              <div class="col-md-9">
                {{ csrf_field() }}
                <input name="count" id="ads_count" placeholder="Ads Count" class="form-control" type="text" value="{{old('count')}}" >
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave"  class="btn btn-primary">Save</button>
            <button type="button" id="btnCancle" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
</div>
    <!-- END BORDERED TABLE -->

    
</div>
<script type="text/javascript">


    function delete_user(id){
        if (confirm('Are you sure you want to delete.') == true) {
            $.ajax({
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'common_delete',
                datatType : 'json',
                type: 'POST',
                data: {
                    id:id,
                    table:'advertisements'
                },
                cache: false,
                success:function(response) {
                    if (response) {
                        location.reload();
                    }
                }
            });
        }else{
            return false;
        }
    }

    function get_ads(id){
        
            $.ajax({
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'common_get',
                datatType : 'json',
                type: 'POST',
                data: {
                    id:id,
                    table:'advertisements'
                },
                cache: false,
                success:function(response) {
                    if (response) {
                        response = jQuery.parseJSON(response);
                        $("#title").val(response.title);
                        $("#description").val(response.description);
                        $("#url").val(response.url);
                        $("#id").val(response.id);
                        var img = response.image;
                        $('#profile-img-tag').attr("src",img);
                    }
                }
            });
        
    }

     function get_ads_count(){
        
            $.ajax({
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'common_get',
                datatType : 'json',
                type: 'POST',
                data: {
                    id:1,
                    table:'advertisement_counts'
                },
                cache: false,
                success:function(response) {
                    if (response) { 
                        response = jQuery.parseJSON(response);
                        $("#ads_count").val(response.count);
                    }
                }
            });
        
    }
    

    $(document).ready(function(){
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profile-img").change(function(){
            readURL(this);
        });

        $("#btnCancle").on("click",function(){
            window.location.reload();
        });

        $("#btnClose").on("click",function(){
            window.location.reload();
        });
    });
</script>
@endsection