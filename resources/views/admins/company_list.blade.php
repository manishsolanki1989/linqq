@extends('layouts.admins')
@section('title', 'Company List')
@section('content')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
  
  <style type="text/css">
    button.multiselect.dropdown-toggle.btn.btn-default {
    width: 288px;
    overflow: hidden;
    text-overflow: ellipsis;
    background-color: #5ac0de;
    border: 1px solid #cacaca;
    color: #000;
}

.add_companys{padding: 5px;
    margin-right: 4px;
    margin-left: 7px;
    padding-left: 22px;
    padding-right: 22px;}

    .add_mesg{color: #0e8812;}
  </style>

<div class="row">
</div>
    <div class="col-md-12">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                   Company List
                </h3>
            </div>
           <!--  <form  action="{{url('admin/upload_industry')}}" method="POST" enctype="multipart/form-data">
                <input type="file" name="upload" >
                {{ csrf_field() }}
                <input type="submit" name="submit" value="submit" style="margin-top: 7px;">
            </form> -->
            <div class="panel-body">
            <span class="add_mesg" id="com_added_msg"></span>
              <form action="" method="post" name="company_form" id="framework_form">
                <select style="float: right;" name="company[]" id="common_add" multiple class="form-control common_add1" >
                   @if(!empty($company_add))
                      @foreach($company_add as $com_add)
                        <option value="{{$com_add->name}}">{{$com_add->name}}</option>
                      @endforeach
                   @endif

                </select>

                <input type="submit" name="add_companys" value="Submit" class="add_companys">
                </form>
                <button style="float: right;" class="btn btn-warning" data-toggle="modal" data-target="#myModal2"><i class="glyphicon glyphicon-plus"></i>  Import  Company </button>
                <button style="float: right;" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-plus"></i> Add Company</button>
                <table class="table table-bordered" id="data">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Action
                            </th>
                        </tr>
                    </thead>
                    @if(!empty($company))
                    <tbody>
                        <?php $i = 0;?>
                        @foreach($company as $company)
                        <tr>
                            <td>
                                {{ ++$i }}
                            </td>
                            <td>
                                {{$company->name}}
                            </td>
                            <td>
                                <a class="action_an" href="javascript::void(0)" onclick="delete_user({{$company->id}})">
                                    <span class="dlt_icon">
                                        <img class="img-responsive" src="{{url('/public')}}/img/delete-button.png"/>
                                    </span>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>


    <!-- Bootstrap modal -->
  <div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Add Company</h3>
      </div>
      <form action="{{url('admin/add_company')}}" method="post" id="form" class="form-horizontal">
        <div class="modal-body form">
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">Company</label>
              <div class="col-md-9">
                {{ csrf_field() }}
                <input name="name" placeholder="company" class="form-control" type="text">
              </div>
            </div>
          </div>

          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave"  class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->



      <!-- Bootstrap modal -->
  <div class="modal fade" id="myModal2" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title"> Import  Company List</h3>
      </div>
      <form action="{{url('admin/upload_company')}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
        <div class="modal-body form">
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3"> Import  Company </label>
              <div class="col-md-9">
                {{ csrf_field() }}
                <input type="file" name="upload" class="form-control">
              </div>
            </div>
          </div>

          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave"  class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->





        <!-- END BORDERED TABLE -->
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#common_add').multiselect({
    nonSelectedText: 'Select Company',
    enableFiltering: true,
    enableCaseInsensitiveFiltering: true,
    buttonWidth:'300px'
  });


  $('#framework_form').on('submit', function(event){
    event.preventDefault();
   // var form_data = $(this).serialize();
    var type ="company";
    
     var ids = [];
            $("#common_add > option").each(function() {
                ids.push(this.value);
            });

            var ids_string = ids.toString();  
           
    $.ajax({
      headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
      url:"{{url('admin/add_profile_add')}}",
      method:"POST",
      data:{company:ids_string,type:type},
      success:function(data)
      {
        response = data.trim();
        if(response=='1')
        {
          $('#com_added_msg').text('Company addedd successfully.');
        }
      }
    });
  });

  });
  
    function delete_user(id){
        if (confirm('Are you sure you want to delete.') == true) {
            $.ajax({
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'common_delete',
                datatType : 'json',
                type: 'POST',
                data: {
                    id:id,
                    table:'companies'
                },
                cache: false,
                //contentType: false,
                //processData: false,
                success:function(response) {
                    if (response) {
                        location.reload();
                    }
                }
            });
        }else{
            return false;
        }
    }

    $('#data').dataTable({
     "processing": true,
     dom: 'Bfrtip',
        buttons: [
            
            {
                extend: 'excelHtml5',
               exportOptions: {
                    columns: [ 0,1 ]
                }
            },
          
            'colvis'
        ]

        });
</script>
@endsection