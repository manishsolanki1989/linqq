@extends('layouts.admins') 
@section('title', 'Notification Report')
@section('content')
<head>
  <style>
 .panel-body select{
    height: 34px;
    width: 220px;
    margin-bottom: 10px;
    margin-right: 25px;
    background: #2B333E;
    color: #fff;
    border: 1px solid #fff; 
 }
 .panel-body select:hover{
  background: #222;
 }

 .panel-body select:hover{
  border: 1px solid #fff;
 }
.place a{
  background: #cccccc;
  padding: 10px 20px;
  color: #222;
  text-decoration: none;
}
.place a:hover{
  text-decoration: none;
  color: #fff;
  background: #828282;
}
.color{
  background: #fff;
  color: #222;
}
.place input{
  margin-right: 25px;
  height: 35px;
  width: 410px;
  margin-bottom: 10px;
  background: #ccc;
}
.search-btn{
  background: #cccccc;
  padding: 9px 20px;
  color: #222;
  text-decoration: none;
}
.dataTables_filter, .dataTables_info { display: none; }

.cust-progred{
  margin: 10px 10px;
}
.leftdiv button{
  float: right;
    padding: 4px;
    border: none;
    background-color: #13d3ff;
    margin-left: 10px;
    padding-left: 10px;
    padding-right: 10px;
}

.leftdiv{float: right;}
</style>
</head>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
<div class="row">
</div>
    <div class="col-md-12">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                   Send Notification
                </h3>
            </div>
                <div class="panel-body">
                                    
                                     
                     <form class="example" action="{{url('admin/data_list_notification')}}">
                         <select name="interest_id">
                          <option value="" >Select Interest</option>
                          @foreach($interests as $value)
                            <option class="color"  value="{{$value->id}}" <?php if (app('request')->input('interest_id')==$value->id): ?>
                              selected
                            <?php endif ?> >{{$value->name}}</option>
                          @endforeach
                         </select>

                         <select name="networking_id">
                          <option value="" >Select Networking</option>
                           @foreach($networkings as $value)
                            <option class="color" value="{{$value->id}}" <?php if (app('request')->input('networking_id')==$value->id): ?>
                              selected
                            <?php endif ?> >{{$value->name}}</option>
                          @endforeach
                         </select>

                         <select name="favourite_id">
                          <option value="" >Select Favourite Way To Meet</option>
                          @foreach($favourites as $value)
                            <option class="color" value="{{$value->id}}" <?php if (app('request')->input('favourite_id')==$value->id): ?>
                              selected
                            <?php endif ?>>{{$value->name}}</option>
                          @endforeach
                         </select>

                         <select name="industry_id">
                          <option  value="">Select Industry</option>
                          @foreach($industries as $value)
                           <option class="color" value="{{$value->id}}" <?php if (app('request')->input('industry_id')==$value->id): ?>
                              selected
                            <?php endif ?> >{{$value->name}}</option>
                           @endforeach
                         </select>

                          <select name="degree">
                            <option  value="">Select Degree</option>
                           @foreach($degrees as $value)
                           <option class="color" value="{{$value->name}}" <?php if (app('request')->input('degree')==$value->name): ?>
                              selected
                            <?php endif ?> >{{$value->name}}</option>
                           @endforeach
                         </select>

                          <select name="gender">
                           <option  value="">Select Gender</option>
                           <option class="color" value="Male" <?php if (app('request')->input('gender')=="Male"): ?>
                              selected
                            <?php endif ?> >Male</option>
                           <option class="color" value="Female" <?php if (app('request')->input('gender')=="Female"): ?>
                              selected
                            <?php endif ?> >Female</option>
                         </select>
                            <div class="place">
                              <input type="text" name="name" placeholder="Name" value="{{app('request')->input('name')}}">
                              <input type="text" name="location" placeholder="Location" value="{{app('request')->input('location')}}">
                              <button class="search-btn" type="submit">Search</button>
                           </div>
                        </form>
                        
                        <!-- @if(!empty($count_interest))
                        <div class="col-xs-12 col-sm-3 col-md-3 cust-progred">
                          <label>Interest</label>
                          <div class="progress " >
                            <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="70" style="width:{{$count_interest}}%"> {{$count_interest}}% 
                            </div> 
                          </div> 
                         </div> 
                        @endif -->
                         
                        @if(isset($_GET['name']) OR isset($_GET['interest_id']) OR isset($_GET['networking_id']) OR isset($_GET['favourite_id']) OR isset($_GET['industry_id']) OR isset($_GET['degree']) OR isset($_GET['gender']) OR isset($_GET['name']) OR isset($_GET['location']))
                        <div class="col-xs-12 col-sm-3 col-md-3 cust-progred" style="width: 70%"> 
                          <label>{{ $label }}</label>
                          <div class="progress" >
                            <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="70" style="width:{{$total_user}}%"> {{ !empty($total_user)?$total_user:0}}% 
                            </div> 
                          </div>
                        </div>
                        @endif

                       <!--  @if(!empty($count_favourite))
                       <div class="col-xs-12 col-sm-3 col-md-3 cust-progred">
                          <label>Favourite Way To Meet</label>
                         <div class="progress " >
                           <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="70" style="width:{{$count_favourite}}%"> {{$count_favourite}}% 
                           </div> 
                         </div>
                       </div> 
                       @endif
                       
                       @if(!empty($count_industry))
                       <div class="col-xs-12 col-sm-3 col-md-3 cust-progred">
                         <label>Industry</label>
                         <div class="progress">
                           <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="70" style="width:{{$count_industry}}%"> {{$count_industry}}% 
                           </div> 
                         </div>
                       </div>
                       @endif
                       
                       @if(!empty($count_degree))
                       <div class="col-xs-12 col-sm-3 col-md-3 cust-progred">
                         <label>Degree</label>
                         <div class="progress ">
                           <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="70" style="width:{{$count_degree}}%"> {{$count_degree}}% 
                           </div> 
                         </div>
                       </div>
                       @endif
                       
                       @if(!empty($count_gender))
                       <div class="col-xs-12 col-sm-3 col-md-3 cust-progred">
                          <label>Gender</label>
                         <div class="progress" >
                           <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="70" style="width:{{$count_gender}}%"> {{$count_gender}}% 
                           </div> 
                         </div>
                       </div>
                       @endif
                       
                       @if(!empty($count_name))
                       <div class="col-xs-12 col-sm-3 col-md-3 cust-progred">
                         <label>Name</label>
                         <div class="progress" >
                           <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="70" style="width:{{$count_name}}%"> {{$count_name}}% 
                           </div> 
                         </div>
                       </div>
                       @endif
                       
                       @if(!empty($count_location))
                       <div class="col-xs-12 col-sm-3 col-md-3 cust-progred">
                         <label>Location</label>
                         <div class="progress" >
                           <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="70" style="width:{{$count_location}}%"> {{$count_location}}% 
                           </div> 
                         </div>
                       </div>
                       @endif
                        -->
                        <div class="leftdiv">
                        <button class="deleteTriger">Send Selected User</button>
                        <button class="" data-toggle="modal" data-target="#myModal1">Send Notification To All Registered User</button>
                                     </div>
                <table class="table table-bordered" id="data">
                    <thead>
                        <tr>
                            <td><input type="checkbox" id="bulkDelete" /></td>
                            <th> # </th>
                            <th> Name </th>
                            <th> Phone No. </th>
                            <th> Gender </th>
                            <th> Designation </th>
                            <th> Organisation </th>
                            <th> Location </th>
                            <th> Created On </th>
                            <th> Profile Image </th>
                            <!-- <th> Action </th> -->
                        </tr>
                    </thead>
                    @if(!empty($users))
                    <tbody>
                        <?php $i=0;?>
                        @foreach($users as $user)
                        <tr>
                            <td><input type='checkbox'  class='deleteRow' value={{$user->user_id}}  /></td>
                            <td> {{ ++$i }} </td>
                            <td> {{$user->name}} </td>
                            <td> {{$user->phone}} </td>
                            <td> {{$user->gender}} </td>
                            <td> {{$user->designation}} </td>
                            <td> {{$user->organisation}} </td>
                            <td> {{$user->location}} </td>
                             <td> {{$user->created_on}} </td>
                            <td>
                                <img alt="Avatar" class="img-circle" src="{{$user->image}}" style="height:50px">
                                </img>
                            </td>
                           <!--  <td>
                               <a class="action_an" href="{{url('admin/view_user')}}/{{Crypt::encrypt($user->user_id)}}">
                                    <span class="dlt_icon">
                                        <img class="img-responsive" src="{{url('/public')}}/img/eye.png"/>
                                    </span>
                                </a>
                                <a class="action_an" href="javascript::void(0)" onclick="delete_user({{$user->user_id}})">
                                    <span class="dlt_icon">
                                        <img class="img-responsive" src="{{url('/public')}}/img/delete-button.png"/>
                                    </span>
                                </a>
                            </td> -->
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
       
        <!-- END BORDERED TABLE -->
    </div>
</div>


</div>
    <!-- END BORDERED TABLE -->
    
     <!-- Bootstrap modal -->
  <div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Send Notification</h3>
      </div>
      <form action="{{url('admin/send_notification')}}" method="post" id="form" class="form-horizontal">
        <div class="modal-body form">
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">Message</label>
              <div class="col-md-9">
                {{ csrf_field() }}
                <textarea name="message" placeholder="message" class="form-control message" maxlength="200" value="" required></textarea>
                <input name="userids" placeholder="" class="form-control uids" type="hidden" value="">

              </div>
              
              </div>
          </div>

          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave"  class="btn btn-primary">Send</button>

            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
    
    
       <!-- Bootstrap modal -->
  <div class="modal fade" id="myModal1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Send Notification</h3>
      </div>
      <form action="{{url('admin/send_notification')}}" method="post" id="form" class="form-horizontal">
        <div class="modal-body form">
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">Message</label>
              <div class="col-md-9">
                {{ csrf_field() }}
                <textarea name="message" placeholder="message" class="form-control message" value="" maxlength="200" required></textarea>

              </div>
              
              </div>
          </div>

          </div>
          <div class="modal-footer">
             <button type="submit" id="btnSave"  class="btn btn-primary">Send</button>

            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<script type="text/javascript">

    function delete_user(id){
        if (confirm('Are you sure you want to delete.') == true) {
            $.ajax({
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'common_delete',
                datatType : 'json',
                type: 'POST',
                data: {
                    id:id,
                    table:'users'
                },
                cache: false,
                success:function(response) {
                    if (response) {
                        location.reload();
                    }
                }
            });
        }else{
            return false;
        }       
    }

    

    $('#data').dataTable({
     "processing": true,
      
        buttons: [
            
            {
                extend: 'excelHtml5',
               exportOptions: {
                    columns: [ 0,1,2,3,4,5,6 ]
                }
            },
          
            'colvis'
        ],
        aLengthMenu: [
        [10,25, 50, 100, 200, -1],
        [10,25, 50, 100, 200, "All"]
    ],

        });
      
        
</script>
<script>
    
$("#bulkDelete").on('click',function() { // bulk checked
        var status = this.checked;
        $(".deleteRow").each( function() {
            $(this).prop("checked",status);
        });
    });
     
    $('.deleteTriger').on("click", function(event){ // triggering delete one by one
        if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
        
            $("#myModal").modal('show');  /// showing form

            var ids = [];
            $('.deleteRow').each(function(){
                if($(this).is(':checked')) { 
                    ids.push($(this).val());
                }
            });
            
             var ids_string = ids.toString();  // array to string conversion 
             var message = $('textarea.message').val();
             var button = $('.buttonid').attr('btn');
            
            
             //alert(message);
              
            $('.uids').val(ids_string);
            
            // if (message) {
            //     alert('test');
            // $.ajax({
            //       headers: {
            //           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     },
            //     type: "POST",
            //     url: "{{url('admin/send_notification')}}",
            //     data: {userids:ids_string,message:message},
            //     success: function(result) {
            //       alert(result);
            //     },
            //     async:false
            // });
            
            // }
        }
        else
        {
            alert('Please first select at least one user to send notification.');
        }
    }); 

</script>

@endsection