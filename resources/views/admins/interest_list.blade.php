@extends('layouts.admins')
@section('title', 'Interest List')
@section('content')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
<div class="row">
</div>
    <div class="col-md-12">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                  Interest List
                </h3>
            </div>
           <!--  <form  action="{{url('admin/upload_interest')}}" method="POST" enctype="multipart/form-data">
                <input type="file" name="upload" >
                {{ csrf_field() }}
                <input type="submit" name="submit" value="submit" style="margin-top: 7px;" >
            </form> -->
            <div class="panel-body">
                <button style="float: right;" class="btn btn-warning" data-toggle="modal" data-target="#myModal2"><i class="glyphicon glyphicon-plus"></i>  Import  Interest </button>
                <button style="float: right;" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-plus"></i> Add Interest</button>
                <table class="table table-bordered" id="data">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Action
                            </th>
                        </tr>
                    </thead>
                    @if(!empty($interests))
                    <tbody>
                        <?php $i = 0;?>
                        @foreach($interests as $interest)
                        <tr>
                            <td>
                                {{ ++$i }}
                            </td>
                            <td>
                                {{$interest->name}}
                            </td>
                            <td>
                                <a class="action_an" href="javascript::void(0)" onclick="delete_user({{$interest->id}})">
                                    <span class="dlt_icon">
                                        <img class="img-responsive" src="{{url('/public')}}/img/delete-button.png"/>
                                    </span>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>


    <!-- Bootstrap modal -->
  <div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Add Interest</h3>
      </div>
      <form action="{{url('admin/add_interest')}}" method="post" id="form" class="form-horizontal">
        <div class="modal-body form">
          
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">Interest</label>
              <div class="col-md-9">
                {{ csrf_field() }}
                <input name="name" placeholder="Interest" class="form-control" type="text">
              </div>
            </div>
          </div>

          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave"  class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->


    <!-- Bootstrap modal -->
  <div class="modal fade" id="myModal2" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title"> Import  Interest List</h3>
      </div>
      <form action="{{url('admin/upload_interest')}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
        <div class="modal-body form">
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3"> Import  Interest </label>
              <div class="col-md-9">
                {{ csrf_field() }}
                <input type="file" name="upload" class="form-control">
              </div>
            </div>
          </div>

          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave"  class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->






        <!-- END BORDERED TABLE -->
    </div>
</div>
<script type="text/javascript">


    function delete_user(id){
        if (confirm('Are you sure you want to delete.') == true) {
            $.ajax({
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'common_delete',
                datatType : 'json',
                type: 'POST',
                data: {
                    id:id,
                    table:'interests'
                },
                cache: false,
                success:function(response) {
                    if (response) {
                        location.reload();
                    }
                }
            });
        }else{
            return false;
        }
    }

    $('#data').dataTable({
     "processing": true,
     dom: 'Bfrtip',
        buttons: [
            
            {
                extend: 'excelHtml5',
               exportOptions: {
                    columns: [ 0,1 ]
                }
            },
          
            'colvis'
        ]

        });
   


    
</script>
@endsection