@extends('layouts.admins') 
@section('title', 'View User')
@section('content')

	<style type="text/css">
		.profile-wrapper{
			text-align: center;
			padding-top: 30px;
			padding-bottom: 50px;

		}
		.profile-wrapper h2{
			margin-bottom: 20px;
		}
		.headline-wrapper .line{
			border: 1px solid #ccc;
			border-radius: 0px;
			margin-bottom: 10px;
			padding:5px 5px;
		}
        .headline-wrapper .line h3{
		color:  #676a6d;
		font-size: 18px;
       font-weight: 700;
	}
	.profile-wrapper .prof{
		margin-right: 50px;
		background: none;
		padding: 30px 0px;
		color: #222;
		border: 2px solid #222;
	}
	.profile-wrapper .prof img{
		border-radius: 100%; 
		height: 180px;
		width: 180px;
		border: 2px solid #fff;
	}

	h3 {
    	margin-top: 10px;
    	margin-bottom: 10px;
	}

	.toggle-list{
       display:none;
    }

    table, th, td {
   border: 1px solid #ccc;
	}

.no_msg{
	    padding: 20px;
    text-align: center;
    width: 307px;
    position: absolute;
   
    font-size: 18px;
    color: #000;
}


	</style>

<section class="profile-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
			<div class="prof">
			<h3>{{ucfirst($user->name)}}</h3>
			<div>
				<img src="{{$user->image}}" >
			</div>
			<div>
				
				<h4>{{$user->designation}}</h4>
				<h5>{{$user->organisation}}</h5>
				<h5>{{$user->phone}}</h5>
				<h5>{{$user->location}}</h5>
				<h5>{{$user->email}}</h5>
              </div>
			</div>
		</div>
	</div></div>
</section>


@if(empty($user->headline) && empty($user->networkings) && empty($user->networkings) && empty($user->favourites) && empty($user->industry) && empty($user->organisations) && empty($user->educations && (!empty($user->linkdin_link) || !empty($user->website_link) || !empty($user->twitter_link) || !empty($user->instagram_link)) ))
<span class="no_msg">Not yet completed profile.</span>
@else
<section class="headline-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-11">
				@if(!empty($user->headline))
				<div class="line">
					<h3>Headline <a href="javascript::void(0);"></a></h3>
					<p>{{$user->headline}}</p>
				</div>
				@endif

				@if(!empty($user->networkings))
				<div class="line">
					<h3>Networking purpose <a href="javascript::void(0);"><i class="fa fa-plus pull-right toggle-networking-plus" aria-hidden="true"></i></a></h3>
					<div class="toggle-list toggle-networking" >
						<ol>
							@foreach($user->networkings as $value)
                          		<li>{{$value->name}}</li>
                          	@endforeach
                        </ol> 
					</div>
				</div>
				@endif

				@if(!empty($user->interests))
				<div class="line">
					<h3>Interest <a href="javascript::void(0);"><i class="fa fa-plus pull-right toggle-interest-plus" aria-hidden="true"></i></a></h3>
					<div class="toggle-list toggle-interest">
						<ol>
                         	@foreach($user->interests as $value)
                          		<li>{{$value->name}}</li>
                          	@endforeach
                        </ol> 
					</div>
				</div>
				@endif

				@if(!empty($user->favourites))
				<div class="line">
					<h3>Favourite ways to meet <a href="javascript::void(0);"><i class="fa fa-plus pull-right toggle-favourite-plus" aria-hidden="true"></i></a></h3>
					<div class="toggle-list toggle-favourite">
						<ol>
                         	@foreach($user->favourites as $value)
                          		<li>{{$value->name}}</li>
                          	@endforeach
                        </ol> 
					</div>
				</div>
				@endif

				@if(!empty($user->industry))
				<div class="line">
					<h3>My Industry <a href="javascript::void(0);"><i class="fa fa-plus pull-right toggle-industry-plus" aria-hidden="true"></i></a></h3>
					<div class="toggle-list toggle-industry">
						<ol>
                         	@foreach($user->industry as $value)
                          		<li>{{$value->name}}</li>
                          	@endforeach
                        </ol> 
					</div>
				</div>
				@endif

				@if(!empty($user->organisations))
				<div class="line">
					<h3>Previous Organisation <a href="javascript::void(0);"><i class="fa fa-plus pull-right toggle-organisation-plus" aria-hidden="true"></i></a></h3>
					<div class="toggle-list toggle-organisation">
						<ol>
                         	@foreach($user->organisations as $value)
                          		<li><b>{{$value->name}}</b>, {{$value->title}}</li>
                          	@endforeach
                        </ol> 
					</div>
				</div>
				@endif

				@if(!empty($user->educations))
				<div class="line">
					<h3>Education <a href="javascript::void(0);"><i class="fa fa-plus pull-right toggle-education-plus" aria-hidden="true"></i></a></h3>
					<div class="toggle-list toggle-education">
						<table>
							<tr>
								<th>Sr.</th>
								<th>School</th>
								
								<th>Degree</th>
							</tr>
							<?php $i=1; ?> 
                         	@foreach($user->educations as $value)
                          		<tr>
                          			<td>{{$i++}}</td>
                          			<td>{{$value->school}}</td>
                          			
                          			<td>{{$value->degree}}</td>
                          		</tr>
                          	@endforeach
                        </table> 
					</div>
				</div>
				@endif



				@if(!empty($user->linkdin_link) || !empty($user->website_link) || !empty($user->twitter_link) || !empty($user->instagram_link))
				<div class="line">
					<h3>Links <a href="javascript::void(0);"><i class="fa fa-plus pull-right toggle-links-plus" aria-hidden="true"></i></a></h3>
					<div class="toggle-list toggle-links">
						<ol>

							@if(!empty($user->linkdin_link))
							<td>LinkedIn - {{$user->linkdin_link}}<br></td>
							@endif

							@if(!empty($user->instagram_link))
							<td>Instagram - {{$user->instagram_link}}<br></td>
							@endif

							@if(!empty($user->twitter_link))
							<td>Twiiter - {{$user->twitter_link}}<br></td>
							@endif

							@if(!empty($user->website_link))
							<td>Website - {{$user->website_link}}<br></td>
							@endif

                        </ol> 
					</div>
				</div>
				@endif

			</div>
			<div class="col-md-1">
				<div class="icon">
				
				</div>
			</div>
		</div>
	</div>
</section>
@endif




<script type="text/javascript">
	$(".toggle-links-plus").on("click",function(){
		$(".toggle-links").toggle();
	});

	$(".toggle-link-plus").on("click",function(){
		$(".toggle-links").toggle();
	});

	$(".toggle-education-plus").on("click",function(){
		$(".toggle-education").toggle();
	});
	$(".toggle-organisation-plus").on("click",function(){
		$(".toggle-organisation").toggle();
	});
	$(".toggle-industry-plus").on("click",function(){
		$(".toggle-industry").toggle();
	});
	$(".toggle-favourite-plus").on("click",function(){
		$(".toggle-favourite").toggle();
	});
	$(".toggle-interest-plus").on("click",function(){
		$(".toggle-interest").toggle();
	});
	$(".toggle-networking-plus").on("click",function(){
		$(".toggle-networking").toggle();
	});
</script>

@endsection