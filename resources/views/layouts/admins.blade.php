<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="{{ asset('public/img/logo.png') }}" rel="shortcut icon" type="image/x-icon"></link>
        <title>@yield('title') | Linqq</title>
         @yield('styles')
         @include('includes.admins.head')
    </head>
    <body>
        <div id="wrapper">
            @include('includes.admins.sidebar')
            <div class="main" id="page-wrapper">
                @include('includes.admins.header')
                 <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg)) <?php //$msg = 'success';?>
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                        @endif
                    @endforeach
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </p>
                            @endforeach
                        </ul>
                    </div>  
                @endif
                <div class="wrapper wrapper-content animated fadeInRight">
                    {{ csrf_field() }}
                    @yield('content')
                </div>
            </div>
        </div>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#data').DataTable();
                } );
            </script>
            @section('scripts')
    </body>
</html>