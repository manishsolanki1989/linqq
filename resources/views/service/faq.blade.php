<!DOCTYPE html>
<html lang="en">
<head>
  <title>faq</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,600i,700,700i" rel="stylesheet">
    
    <style>
        body{
            font-family: 'Open Sans', sans-serif;
        }
        
        .trablons_question_ec h4{
            font-size: 16px;
            font-weight: 600;
            color: #333;
            margin: 0;
        }
        
        .trablons_question_ec p {
            font-size: 14px;
            line-height: 24px;
            color: #595959;
            margin: 0px;
        }

        
        .trablons_question_ec{
            margin-bottom: 30px;
        }
    </style>
    
    
</head>
<body>


    
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="container">
            <div class="travlon_faq">
                <div class="trablons_question_ec">
                    <h4>What is travolong?</h4>
                    <p>Travolong is one of the world most fastest growing online travel chat community. We are a platform which helps you to plan your travel better. We let you meet traveller at your place of travel. You can chat with the other traveller and can know info about the place you are planning to travel. Hey it is much better then searhing the web for the info or asking you freind/family members who must have been to that place years back. It ia alaways much better to know whats happening right now at your planned destination. </p>
                    <p>Welcome to Travolong - Type in your destination and let the magic happen.</p>
                </div>
                <div class="trablons_question_ec">
                    <h4>How Can I use Travolong?</h4>
                    <p>you can download the Travolong app for ios and Android from Apple & Google Playstore.</p>
                </div>
                <div class="trablons_question_ec">
                    <h4>Is this app available everywhere?</h4>
                    <p>Yes, travolong is launched globally and is avaialbe to all travellers aroud the globe. You can download the Travolong app for ios and Android from Apple & Google Playstore.</p>
                </div>
                <div class="trablons_question_ec">
                    <h4>Is Travolong Free?</h4>
                    <p>Yes Travolong is free to download application and is available in gogle and apple playstore. </p>
                </div>
                <div class="trablons_question_ec">
                    <h4>What is minimum age requirment?</h4>
                    <p>The minimum age requirment for Travolong is 18 years old.If you are under 18 and have an account in travolong, it will not be deleted but it will be disabled.</p>
                    <p>I dont want my status to be online/available.</p>
                    <p>You can always go to settings and change your status from online mode to Offline mode.</p>
                </div>
                <div class="trablons_question_ec">
                    <h4>Can I block,mute or delete other travller?</h4>
                    <p>Yes you can always block, mute and delete other traveller by going to the setings option or keep pressing for the options to pop up at the upper right side of the homescreen or any chat screen.</p>
                </div>
                <div class="trablons_question_ec">
                    <h4>Can I send messages to blocked traveller?</h4>
                    <p>No you cannot send message to other traveller who has blocked you. You can send message to toher traveller only if the other traveller has not blocked you.</p>
                </div>
                <div class="trablons_question_ec">
                    <h4>Can I share picture and videos?</h4>
                    <p>Yes travolong gives you the freedom of sending pictures, videos, documents and your location to other traveller.</p>
                </div>
                <div class="trablons_question_ec">
                    <h4>what if any other support?</h4>
                    <p>We are happy to help you,all you need to so is send a mail to support@travolong.com</p>
                </div>
                
            </div>
        </div>
    </div>
    
    
    
    
    
    
    
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
</body>
</html>