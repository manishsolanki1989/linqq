<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Collabor | Services</title>
<link rel="shortcut icon" href="{{ asset('totrr_favicon.ico') }}"/>
<style>
.content {
    width: 60%;
    margin: 50px auto;
    padding: 20px;
  }
  .content h1 {
    font-weight: 400;
    text-transform: uppercase;
    margin: 0;
  }
  .content h2 {
    font-weight: 400;
    text-transform: uppercase;
    color: #333;
    margin: 0 0 20px;
  }
  .content p {
    font-size: 1em;
    font-weight: 300;
    line-height: 1.5em;
    margin: 0 0 20px;
  }
  .content p:last-child {
    margin: 0;
  }
  .content a.button {
    display: inline-block;
    padding: 10px 20px;
    background: #ff0;
    color: #000;
    text-decoration: none;
  }
  .content a.button:hover {
    background: #000;
    color: #ff0;
  }
  .content.title {
    position: relative;
    background: none;
    border: 2px dashed #333;
  }
  .content.title h1 span.demo {
    display: inline-block;
    font-size: .5em;
    padding: 5px 10px;
    background: #000;
    color: #fff;
    vertical-align: top;
    margin: 7px 0 0;
  }
  .content.title .back-to-article {
    position: absolute;
    bottom: -20px;
    left: 20px;
  }
  .content.title .back-to-article a {
    padding: 10px 20px;
    background: #f60;
    color: #fff;
    text-decoration: none;
  }
  .content.title .back-to-article a:hover {
    background: #f90;
  }
  .content.title .back-to-article a i {
    margin-left: 5px;
  }
  .content.white {
    background: #fff;
    box-shadow: 0 0 10px #999;
  }
  .content.black {
    background: #000;
  }
  .content.black p {
    color: #999;
  }
  .content.black p a {
    color: #08c;
  }

  .accordion-container {
    width: 100%;
    margin: 0 0 20px;
    clear: both;
  }
  .accordion-toggle {
    margin-bottom: 15px;
    position: relative;
    display: block;
    padding: 15px;
    font-size: 1.2em;
    font-weight: 300;
    background: #BBB;
    color: #fff;
    text-decoration: none;
    font-family: Bree serif !important;
  }
  .accordion-toggle.open {
    background: #1e2e65;
    color: #fff;
  }
  .accordion-toggle:hover {
    background:rgb(237,156,40);
    color: #fff!important;
  }
  .accordion-toggle span.toggle-icon {
    position: absolute;
    top: 17px;
    left: 20px;
    font-size: 1.5em;
  }
  .accordion-content {
    display: none;
    padding: 20px;
    overflow: auto;
    font-family: Bree serif;
  }
  .accordion-content img {
    display: block;
    float: left;
    margin: 0 15px 10px 0;
    max-width: 100%;
    height: auto;
  }

  /* media query for mobile */
  @media (max-width: 767px) {
    .content {
      width: auto;
    }
    .accordion-content {
      padding: 10px 0;
      overflow: inherit;
    }
  }
</style>
<div class="container pages">
    <div class="row text-left" style="padding:10px;">
      <h2 style="text-align:center;"></h2>
        <div class="page-header"><h3>Service Listing</h3></div>
        <div class="page-header">Service Url : http://mobulous.co.in/collabor/api/</div><br/>
<div class="page-header"><h3>Status Code</h3></div>
        <pre>
200 OK
201 Created
304 Not Modified
400 Bad Request
401 Unauthorized
403 Forbidden
404 Not Found
422 Unprocessable Entity
500 Internal Server Error
        </pre>  
<h3>Header parameter</h3>
<pre>
Accept:application/json
Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJ...
</pre>
<?php $i=1; ?>

<!-- *************************unique_user*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) unique_user</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
  'email'       => 'required|email|max:190|unique:users',
  'phone'       => 'required|numeric|max:190|unique:users',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>requestKey: </h5>unique_user</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "No record.",
    "requestKey": "api/unique_user"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************logout*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) logout</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>logout</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Logout successfully",
    "requestKey": "api/logout"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>

<!-- *************************signup*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) signup</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'name'         => 'required|max:190',
'email'        => 'required|email|max:190|unique:users',
'phone'        => 'required|numeric|unique:users',
'dob'          => 'required|max:190',
'gender'       => 'required|max:190',
'designation'  => 'required|max:190',
'organisation' => 'required|max:190',
'password'     => 'required|max:190',
'device_id'    => 'required|max:190',
'device_type'  => 'required|max:190',
'image'        => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>requestKey: </h5>signup</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": {
        "email": "aksit@gmail.com",
        "phone": "123456789",
        "dob": "1996-1-10",
        "gender": "male",
        "designation": "dev",
        "organisation": "devntu",
        "device_id": "234sdiiwfn",
        "device_type": "android",
        "name": "Akshit",
        "image": "20180504075024223123.jpg",
        "updated_at": "2018-05-04 07:50:25",
        "created_at": "2018-05-04 07:50:25",
        "id": 6,
        "user_id": "6",
        "token": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjY1MzNjNTcwZjVmZjlkZmJkZWEyODFmM2NiODY4NmViYTc4YmZjYTk3MjVjZmQ5NzJjYzdhNDIxM2RjODkzMDk0NmZlODkxNmYxZDQ4ZmQ5In0.eyJhdWQiOiIxIiwianRpIjoiNjUzM2M1NzBmNWZmOWRmYmRlYTI4MWYzY2I4Njg2ZWJhNzhiZmNhOTcyNWNmZDk3MmNjN2E0MjEzZGM4OTMwOTQ2ZmU4OTE2ZjFkNDhmZDkiLCJpYXQiOjE1MjU0MjAyMjYsIm5iZiI6MTUyNTQyMDIyNiwiZXhwIjoxNTU2OTU2MjI1LCJzdWIiOiI2Iiwic2NvcGVzIjpbXX0.bebHW2UpfjrRJWLXlQmHeVoGbVcEQfpZ5ryC3y2gLmu4TABRupmOx7uPHvA78iK3NqxZcLmy1TPOHeLfUhEnQnZP4KuSQuzl7fgrQd1vRw-BpVPgQAIeSkk6IVQxYbvsHsPVp-c3d1xHtvTgwC3adpBCb_XC-pTWoUzGMuV9t30ippi6kHPHbNrsjjDe8PJY9mMi7ReJcEp0fp6xqFYd6UCmmJpXcOY0Tn6VYfeM-1cA_JNfU2q-3faJfelTFsQH9GG2HAj8QbnZB0dUTqYUEZmV6q004KRJ4E8Kv86hEXfqzkKaKizPf2gQQdkyruIhgcQxboCMwZw94VhXfiGtNlKQ7kDWU4EF4fv5LWg0zfLdb0hC_CI1cfdT9nqH2iF1wywvDWFs7djSDqad_GS-Wv61qYyvejF4y3z81C071Nu96QkpUXBUI-mUhq0J-MMl0uQFy1YovFv6kGnWyNZfksHf8mrCjcL5R605vXCNC725_bNeyZjef8PklhOg3P25ABFqTfu_e_ZE_e-WJSV8YCtoDQuVdxtrHTjOQu4MgAVfRJ7Sa96x2wSJwIw1SRmjIGr4bdIKqC2T8apsMajpq9C-OixP6sc3B-XpMT7bu0S7aMM51iQljv8HXE-ojDAasDRuDQUU_FvWtk0bR6adzHT-70chqIbehRPrHYETdvo"
    },
    "message": "Signup successfully",
    "requestKey": "api/signup"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************login*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) login</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'email'       => 'required|email|max:190',
'password'    => 'required|max:190',
'device_id'   => 'required|max:190',
'device_type' => 'required|max:190',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          
          <p> <h5>requestKey: </h5>login</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": {
        "id": 6,
        "name": "Akshit",
        "email": "aksit@gmail.com",
        "phone": "123456789",
        "dob": "1996-01-10",
        "gender": "male",
        "designation": "dev",
        "organisation": "devntu",
        "image": "http://mobulous.co.in/collabor/public/profile/20180504075024223123.jpg",
        "device_id": "123123123",
        "device_type": "ios",
        "is_admin": "0",
        "created_at": "2018-05-04 07:50:25",
        "updated_at": "2018-05-04 09:39:11",
        "user_id": "6",
        "token": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU4Mjk5ZjcxMTYzYzU2YzQyNGZkZmFiYzk5YWFjYmM1MDgzMTRhNzNmOTIzZDc0MDAzMzJlMTk5NWJhNjYyNzE2OGQzN2U5YTVlYjIyYWUyIn0.eyJhdWQiOiIxIiwianRpIjoiNTgyOTlmNzExNjNjNTZjNDI0ZmRmYWJjOTlhYWNiYzUwODMxNGE3M2Y5MjNkNzQwMDMzMmUxOTk1YmE2NjI3MTY4ZDM3ZTlhNWViMjJhZTIiLCJpYXQiOjE1MjU0MjY3NTEsIm5iZiI6MTUyNTQyNjc1MSwiZXhwIjoxNTU2OTYyNzUxLCJzdWIiOiI2Iiwic2NvcGVzIjpbXX0.po3x36IKJJWaMBAofCO3xHylWSuUQxo13CMS6SCHfz0ojsLmXmetrm0T6g4BMzMw7JWX3TM4Gr9X0KvW2e-O1mi3YPDaCRHLoXObhh9z_sGbomcEFRNTYUYsqClk_sT6EbwW2pfCR4tE62vQ5bU4swB9KUh63bvYD_r0QD1m5nzr-IoOq7BD_xmiDvxwGqXSv8LjsOXNTV6wRiX8ble2JBlJHJ8oG12XPobDh3_Vb0Dasluqscs7ewmUOp0kmxhopay28-xJwRQsSHjFN57HtF2W1aL_jHFCIMtf7VpxKhn2hTXxykzLkv5tdkvOH6AUAPZw3nOj9_ojx5hZXDJU0a6ZijtRkFtPgLgnklBY9oj6eJfPXqm9zh6eMyHfzrM_lQR8IRcGSmNsBbnBsJL1Tov7fl9vHoqFeIA6AqFD8DG895GZlG_DUHHewfEmYso4_t5qY3dBHoO7nnMo85TA0SYbBMMr9EEph6PPXz5w2Ifjy9xETQuT8opnQrLcNk99D4JZUzBSYMZbS1toSjnoFp2vacMG51CoaYv6m72TjHyGi_jne_mBNwNFzxaT2wpncR87W5LbDtIyuCzYjFLlgqX4tYslkVmNnYZ082rJK6OJmeIpDJ3FlfXufxyBkUHY-vXc5UP0ZJ_JQy-RyZSoeFjhbd-0R9bSmoTKhTBUwFY"
    },
    "message": "Login successfully",
    "requestKey": "api/login"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


  <!-- *************************forgot_password*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) forgot_password</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'phone'    => 'required|numeric',
'password' => 'required|max:190',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          
          <p> <h5>requestKey: </h5>forgot_password</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Changed successfully",
    "requestKey": "api/forgot_password"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>

<!-- *************************change_password*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) change_password</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'old_password' => 'required|max:190',
'new_password' => 'required|max:190',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>change_password</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Changed successfully",
    "requestKey": "api/change_password"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


  <!-- *************************user_message_template*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) user_message_template</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>user_message_template</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": [
        {
            "template_id": "10",
            "message": "message1"
        },
        {
            "template_id": "11",
            "message": "message2"
        },
        {
            "template_id": "12",
            "message": "message3"
        },
        {
            "template_id": "13",
            "message": "message4"
        }
    ],
    "message": "User template.",
    "requestKey": "api/user_message_template"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************user_request_template*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) user_request_template</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>user_request_template</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": [
        {
            "template_id": "2",
            "message": "Oye",
            "request_template_id": ""
        },
        {
            "request_template_id": "1",
            "message": "Hey, praveenThakur would love to know more about what you do at company.",
            "template_id": ""
        },
        {
            "request_template_id": "2",
            "message": "Hi praveenThakur, seem like we have a lot in common like common interest lets meet over coffee.",
            "template_id": ""
        },
        {
            "request_template_id": "3",
            "message": "Hey praveenThakur, I'm looking for a mentor to guide me with my current venture, hoping to connect with you for the same.",
            "template_id": ""
        },
        {
            "request_template_id": "4",
            "message": "Hi praveenThakur, I'm looking to raise funds for my venture. Would like to discuss in detail about the same.",
            "template_id": ""
        },
        {
            "request_template_id": "6",
            "message": "Hey praveenThakur, I'm looking for a job opportunity, would be great if you can help out in that regard.",
            "template_id": ""
        }
    ],
    "message": "User template.",
    "requestKey": "api/user_request_template"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************add_message_template*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) add_message_template</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'message' => 'required|max:190',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>add_message_template</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Changed successfully",
    "requestKey": "api/add_message_template"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************edit_message_template*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) edit_message_template</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'message' => 'required|max:190',
'template_id' => 'required|max:190',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>edit_message_template</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Updated successfully",
    "requestKey": "api/edit_message_template"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************delete_message_template*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) delete_message_template</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'template_id' => 'required|max:190',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>delete_message_template</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Deleted successfully",
    "requestKey": "api/delete_message_template"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************add_organisations*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) add_organisations</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'title' => 'required|max:190',
'name' => 'required|max:190',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>add_organisations</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Saved successfully",
    "requestKey": "api/add_organisations"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************edit_organisations*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) edit_organisations</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'organisation_id' => 'required|max:190',
'name'            => 'required|max:190',
'title'           => 'required|max:190',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>edit_organisations</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Updated successfully",
    "requestKey": "api/edit_organisations"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************delete_organisations*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) delete_organisations</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'organisation_id' => 'required|max:190',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>delete_organisations</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Deleted successfully",
    "requestKey": "api/delete_organisations"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************user_organisations*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) user_organisations</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>user_organisations</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": [
        {
            "organisation_id": "1",
            "title": "PG",
            "name": "Graphic Ero University"
        }
    ],
    "message": "User template.",
    "requestKey": "api/user_organisations"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>

<!-- *************************user_education*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) user_education</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>user_education</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": [
        {
            "id": 2,
            "user_id": "38",
            "degree": "MCA",
            "start_year": "2012",
            "end_year": "2015",
            "school": "Graphic Era University",
            "created_at": "2018-05-09 07:50:11",
            "updated_at": "2018-05-18 14:29:14",
            "education_id": "2"
        },
        {
            "id": 3,
            "user_id": "38",
            "degree": "MCA",
            "start_year": "2105",
            "end_year": "2150",
            "school": "DIT",
            "created_at": "2018-05-15 10:57:02",
            "updated_at": "2018-05-18 14:29:04",
            "education_id": "3"
        }
    ],
    "message": "List.",
    "requestKey": "api/user_education"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


      <!-- *************************delete_education*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) delete_education</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'education_id' => 'required|max:190',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>delete_education</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Deleted successfully",
    "requestKey": "api/delete_education"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************add_education*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) add_education</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'degree'     => 'required|max:190',
'start_year' => 'required|max:190|date_format:Y',
'end_year'   => 'required|max:190|date_format:Y',
'school'     => 'required|max:190',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>add_education</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Saved successfully",
    "requestKey": "api/add_education"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************edit_education*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) edit_education</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'degree'       => 'required|max:190',
'start_year'   => 'required|max:190|date_format:Y',
'end_year'     => 'required|max:190|date_format:Y',
'school'       => 'required|max:190',
'education_id' => 'required|max:190',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>edit_education</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Updated successfully",
    "requestKey": "api/edit_education"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************user_networking*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) user_networking</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>user_networking</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": [
        {
            "networking_id": "1"
        }
    ],
    "message": "List.",
    "requestKey": "api/user_networking"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


  <!-- *************************delete_networking*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) delete_networking</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'networking_id' => 'required|max:190',

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>delete_networking</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Deleted successfully",
    "requestKey": "api/delete_networking"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>

<!-- *************************networking_list*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) networking_list</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'


                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>networking_list</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": [
        {
            "networking_id": "1",
            "name": "Funding"
        },
        {
            "networking_id": "2",
            "name": "Collaboration"
        },
        {
            "networking_id": "3",
            "name": "Jobs"
        }
    ],
    "message": "List.",
    "requestKey": "api/networking_list"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************add_networking*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) add_networking</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'networking_id' => 'required|max:190',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>add_networking</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Saved successfully",
    "requestKey": "api/add_networking"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************edit_profile*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) edit_profile</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'name'           => 'max:190',
'gender'         => 'max:190',
'designation'    => 'max:190',
'organisation'   => 'max:190',
'headline'       => 'max:190',
'industry'       => 'max:190',
'linkdin_link'   => 'max:190',
'website_link'   => 'max:190',
'twitter_link'   => 'max:190',
'instagram_link' => 'max:190',
'location'       => 'max:190',
'image'          => 'max:10240',
'cover_image'    => 'max:10240',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>edit_profile</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Saved successfully.",
    "requestKey": "api/edit_profile"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>

<!-- *************************profile*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) profile</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>profile</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": {
        "id": 12,
        "name": "Hi, I Am Fine.",
        "email": "ram@gmail.com",
        "phone": "123123111",
        "dob": "1996-01-10",
        "gender": "male",
        "designation": "dev",
        "organisation": "devntu",
        "image": "",
        "headline": "",
        "industry": "",
        "device_id": "123123123",
        "device_type": "ios",
        "is_admin": "0",
        "linkdin_link": "",
        "website_link": "",
        "twitter_link": "",
        "instagram_link": "",
        "created_at": "2018-05-08 12:21:15",
        "updated_at": "2018-05-18 13:56:09",
        "degree": "MCA",
        "school": "DIT",
        "organisation_name": "Graphic Ero University",
        "organisation_title": "PG"
    },
    "message": "User profile.",
    "requestKey": "api/profile"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************add_favourite*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) add_favourite</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'favourite_id' => 'required|max:190',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>add_favourite</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Saved successfully",
    "requestKey": "api/add_favourite"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************user_favourite*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) user_favourite</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'favourite_id' => 'required|max:190',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>user_favourite</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": [
        {
            "favourite_id": "1"
        }
    ],
    "message": "List.",
    "requestKey": "api/user_favourite"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************delete_favourite*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) delete_favourite</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'favourite_id' => 'required|max:190',
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>delete_favourite</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Deleted successfully",
    "requestKey": "api/delete_favourite"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


  <!-- *************************user_interest*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) user_interest</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>user_interest</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": [
        {
            "interest_id": "1",
            "name": "n1"
        }
    ],
    "message": "List.",
    "requestKey": "api/user_interest"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************add_interest*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) add_interest</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'interest_id'     => 'required|max:190',
                 </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>add_interest</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Saved successfully",
    "requestKey": "api/add_interest"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************delete_interest*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) delete_interest</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'interest_id'     => 'required|max:190',
                 </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>delete_interest</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Deleted successfully",
    "requestKey": "api/delete_interest"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************interest_list*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) interest_list</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
                 </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>interest_list</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": [
        {
            "interest_id": "1",
            "name": "n1"
        },
        {
            "interest_id": "2",
            "name": "n2"
        }
    ],
    "message": "List.",
    "requestKey": "api/interest_list"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************industry_list*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) industry_list</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'key' => 'required|max:190',
              </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>industry_list</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": [
        {
            "industry_id": "34",
            "name": "Education Management"
        },
        {
            "industry_id": "54",
            "name": "Higher Education"
        },
        {
            "industry_id": "111",
            "name": "Primary/Secondary Education"
        },
        {
            "industry_id": "157",
            "name": "EDUCATION AND TRAINING"
        }
    ],
    "message": "List.",
    "requestKey": "api/industry_list"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


  <!-- *************************all_user_profile*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) all_user_profile</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'offset' => 'required',
              </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>all_user_profile</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": [
        {
            "id": "53",
            "name": "Get Right",
            "email": "9110424394",
            "phone": "9110424394",
            "dob": "0000-00-00",
            "gender": "Male",
            "designation": "Quality Analyst",
            "organisation": "Mobulous Technologies",
            "industry": "",
            "image": "http://mobulous.co.in/collabor/public/profile/20180522072837874865.png",
            "cover_image": "",
            "headline": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took",
            "location": "",
            "remember_token": "",
            "device_id": "123456",
            "device_type": "iphone",
            "is_admin": "",
            "linkdin_link": "",
            "website_link": "",
            "twitter_link": "",
            "instagram_link": "",
            "created_at": "2018-05-22 07:28:39",
            "updated_at": "2018-05-22 07:31:11",
            "degree": "",
            "school": "",
            "organisation_name": "",
            "organisation_title": "",
            "favourites": "",
            "networkings": [
                {
                    "name": "To Hire Talent",
                    "image": "http://mobulous.co.in/collabor/public/apps/n9.png",
                    "networking_id": "9"
                },
                {
                    "name": "Looking for Co-founder",
                    "image": "http://mobulous.co.in/collabor/public/apps/n1.png",
                    "networking_id": "1"
                },
                {
                    "name": " Looking for a Job",
                    "image": "http://mobulous.co.in/collabor/public/apps/n5.png",
                    "networking_id": "5"
                }
            ],
            "interests": ""
        }
    ],
    "message": "User profile.",
    "requestKey": "api/all_user_profile"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************linkdin_signup*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) linkdin_signup</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
 'name'         => 'required|max:190',
  'linkedin_id'  => 'required|max:190',
  'device_id'    => 'required|max:190',
  'device_type'  => 'required|max:190',
  'designation'  => 'max:190',
  'organisation' => 'max:190',
  'headline'     => 'max:190',
  'location'     => 'max:190',
  'image'        => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
              </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>linkdin_signup</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": {
        "designation": "dev",
        "organisation": "devntu",
        "device_id": "234sdiiwfn",
        "device_type": "android",
        "linkedin_id": "12spoii",
        "name": "Akshit",
        "industry_id": 179,
        "updated_at": "2018-05-24 12:34:12",
        "created_at": "2018-05-24 12:34:12",
        "id": 74,
        "user_id": "74",
        "token": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijk2MTExOGFlMWFhYTZhZGU1ODhhMGUwMWE2MzYxNDI1N2Y0MjkxNmY4YmJjYmU2MTkwMmFkNzFhMDEwMTlmMWQzYTIzMTRhMDExYjQ3ODdjIn0.eyJhdWQiOiIxIiwianRpIjoiOTYxMTE4YWUxYWFhNmFkZTU4OGEwZTAxYTYzNjE0MjU3ZjQyOTE2ZjhiYmNiZTYxOTAyYWQ3MWEwMTAxOWYxZDNhMjMxNGEwMTFiNDc4N2MiLCJpYXQiOjE1MjcxNjUyNTIsIm5iZiI6MTUyNzE2NTI1MiwiZXhwIjoxNTU4NzAxMjUyLCJzdWIiOiI3NCIsInNjb3BlcyI6W119.ib8GSn3lBCJEgRLuNAeASEgIjCAKvLrIZeg3rs-DWWkRSGmL5nsx4zJMoNW8INy7ZVWnb1E11t3t3YiIVoqNVJvY05xN-skE6l4_WsVPLKIR0UM_f6gDuB53Rd6uqOV7aBDc3pZkjVm5kMhgcoSVcSjh-37ng8BeaSUPmDsGOqI29qcod8By2NJ6chtxhO-K8PK6IaS3ot-CKGErhm6iMjVfpDIylTCQYM2jg_-vOzgiCIhQNFIxwCwEHORaZ01v9rlULeWIMHa06xGIe33f68bys14Tt2gfGNnzmM6rK6ndsdzbwZ4qn6sQ0N3gMS5b8TwD6Wdk_p4QCXTAT5kJXieUqEoZa3i0u4QCEmjAtgcS936djbD-GzpqFJDi7k7e1-OLBkwykPGJj8D32qNlBpJON6WR0GQE0pW-9icoU0pD3E8gcEJNYcLkDufeESnqi1RnR3ZH7MGziCNOvRckR8-KgZfi08mJa0nZhEdNNy0jNx81-v5urJr5S17afVOM7v81iJWrUklGy2Vzs1fgH0dkBUne40ONfKEkbvraK62I2kTifLD7_xmZuEHhaT6efBS2JNxaORkDCrlyrFhWRqZ6yPnqZA4KCpomMu8q9lZ_QgMOs5LV51uDrivIrz2C5Ez5MmyStt9UTX91_aIDKB9yejlfbp-ul3AU8NFTIOI",
        "image": ""
    },
    "message": "Signup successfully",
    "requestKey": "api/linkdin_signup"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************get_ads*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) get_ads</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
 header
       </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>get_ads</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": {
        "id": 4,
        "title": "asdfa",
        "description": "asdf",
        "image": "http://mobulous.co.in/collabor/public/ads/20180528093448845790.jpg",
        "url": "https://www.google.com",
        "created_at": "2018-05-28 08:36:38",
        "updated_at": "2018-05-29 06:34:25"
    },
    "message": "List",
    "requestKey": "api/get_ads"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


  <!-- *************************send_request*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) send_request</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
 header
 'receiver_id' => 'required|max:190',
 'message'=>''
       </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>send_request</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Sent successfully.",
    "requestKey": "api/send_request"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


 <!-- *************************connection_list*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) connection_list</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
 header
 
       </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>connection_list</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": [
        {
            "request_id": "1",
            "sender_id": "146",
            "receiver_id": "149",
            "name": "PraveenThakur",
            "designation": "Android Developer",
            "organisation": "Mobulous Technologies Pvt Ltd",
            "image": "http://mobulous.co.in/collabor/public/profile_resize/20180607080433401314.jpg"
        },
        {
            "request_id": "2",
            "sender_id": "172",
            "receiver_id": "146",
            "name": "Get Right",
            "designation": "Project Manager",
            "organisation": "Mobulous Technologies",
            "image": "http://mobulous.co.in/collabor/public/profile_resize/20180606035920647524.jpg"
        }
    ],
    "message": "List.",
    "requestKey": "api/connection_list"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


 <!-- *************************search_user*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) search_user</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
 header
  'name'          => 'max:190',
  'interest_id'   => 'max:190',
  'networking_id' => 'max:190',
  'location'      => 'max:190',
       </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>search_user</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": [
        {
            "user_id": "137",
            "name": "HartejSingh",
            "designation": "Director",
            "organisation": "EAppPLtd",
            "image": "http://mobulous.co.in/collabor/public/profile_resize/20180531100930204839.png"
        },
        {
            "user_id": "178",
            "name": "HartejSingh",
            "designation": "Director",
            "organisation": "EAppPLtd",
            "image": "http://mobulous.co.in/collabor/public/profile_resize/20180607055655918539.jpg"
        }
    ],
    "message": "List.",
    "requestKey": "api/search_user"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


 <!-- *************************view_profile*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) view_profile</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
 header
  'user_id' => 'required|max:190'
       </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>view_profile</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": {
        "id": 149,
        "linkedin_id": "CxTOKrdrZc",
        "industry_id": "",
        "name": "PraveenThakur",
        "email": "",
        "phone": "",
        "gender": "",
        "designation": "Android Developer",
        "organisation": "Mobulous Technologies Pvt Ltd",
        "image": "http://mobulous.co.in/collabor/public/profile/20180607150246403678.jpg",
        "cover_image": "",
        "headline": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the indujjscjncjncncjncn",
        "location": "Noida Services, Sector 110, Noida, Uttar Pradesh, India",
        "device_id": "e0N9MnNEN0U:APA91bEJrSTl363zEo6xumKulKFD2xtX8zO5dAcJ4W0TkxwUKBnQfrnq8E9NgiHwnz3g2WTwA2NgpHOkf3SyWDEvBni6SD-xn-R9C8DETjzVpjbbndSUNPDalXI8HJajyGwdlO4PoJYt",
        "device_type": "android",
        "is_admin": "0",
        "linkdin_link": "",
        "website_link": "",
        "twitter_link": "",
        "instagram_link": "",
        "notification": "ON",
        "created_at": "2018-05-31 13:31:22",
        "updated_at": "2018-06-08 07:44:21",
        "degree": "",
        "school": "",
        "education": [],
        "organisation_name": "polyinfosoft pvt ltd",
        "organisation_title": "Android Developer",
        "industry": "",
        "favourites": [],
        "networkings": [],
        "interests": []
    },
    "message": "User profile.",
    "requestKey": "api/view_profile"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************notification_list*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) notification_list</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
 header
  
       </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>notification_list</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": [
        {
            "id": 15,
            "user_id": "146",
            "type": "send_request",
            "message": "PraveenThakur send you request.",
            "created_at": "2018-06-08 12:56:56",
            "updated_at": "2018-06-08 12:56:56"
        }
    ],
    "message": "List.",
    "requestKey": "api/notification_list"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************accept_request*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) accept_request</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
 header
  request_id' => 'required',
       </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>accept_request</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Accepted successfully.",
    "requestKey": "api/accept_request"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************reject_request*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) reject_request</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
 header
  request_id' => 'required',
       </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>reject_request</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Rejected successfully.",
    "requestKey": "api/reject_request"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************chat_list*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) chat_list</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
 header
 
       </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>chat_list</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": [
        {
            "request_id": "13",
            "sender_id": "146",
            "receiver_id": "161",
            "message": "HI",
            "created_at": "2018-06-11 09:12:27",
            "date": "21 minutes ago",
            "name": "Christopher",
            "image": "http://mobulous.co.in/collabor/public/profile_resize/20180604155133601011.jpg"
        }
    ],
    "message": "List",
    "requestKey": "api/chat_list"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>

<!-- *************************send_message*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) send_message</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
 header
  'message'     => 'required|max:190',
  'receiver_id' => 'required|max:190',
  'request_id'     => 'required|max:190',

       </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>send_message</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": {
        "sender_id": "146",
        "message": "I am good for good",
        "receiver_id": "161",
        "request_id": "13",
        "updated_at": "2018-06-11 09:42:40",
        "created_at": "2018-06-11 09:42:40",
        "id": 3,
        "sender_name": "Ravi Kant",
        "sender_image": "http://mobulous.co.in/collabor/public/profile_resize/20180530134206763958.png",
        "message_time": "just now"
    },
    "message": "Sent successfully",
    "requestKey": "api/send_message"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>




<!-- *************************chat_history*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) chat_history</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'request_id' => 'required|max:190',
              </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>chat_history</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": [
        {
            "id": 1,
            "request_id": "13",
            "sender_id": "146",
            "receiver_id": "161",
            "message": "I am good for good",
            "created_at": "2018-06-11 09:50:38",
            "updated_at": "2018-06-11 09:50:38",
            "message_id": "1",
            "date": "1 hour ago",
            "receiver_name": "Christopher",
            "receiver_image": "http://mobulous.co.in/collabor/public/profile_resize/20180604155133601011.jpg",
            "sender_name": "Ravi Kant",
            "sender_image": "http://mobulous.co.in/collabor/public/profile_resize/20180530134206763958.png"
        }
    ],
    "message": "List",
    "requestKey": "api/chat_history"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************send_report*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) send_report</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'message' => 'required|max:190',
              </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>send_report</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Sent successfully",
    "requestKey": "api/send_report"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************notification_setting*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) notification_setting</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'status' => (ON/OFF)'required|max:190',
              </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>notification_setting</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Updated successfully",
    "requestKey": "api/notification_setting"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>

<!-- *************************email_push_setting*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) email_push_setting</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'status' => (ON/OFF)'required|max:190',
              </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>email_push_setting</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Updated successfully",
    "requestKey": "api/email_push_setting"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>

<!-- *************************my_info*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) my_info</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'

              </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>my_info</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": {
        "id": 146,
        "linkedin_id": "",
        "industry_id": "3",
        "name": "Ravi Kant",
        "email": "",
        "phone": "8881999369",
        "gender": "Male",
        "designation": "Android Developers",
        "organisation": "Mobulous Technologies",
        "image": "20180530134206763958.png",
        "cover_image": "20180530134207783854.png",
        "headline": "Christopher Alesund, known by his alias GeT_RiGhT, is a Swedish professional Counter-Strike series player. He is currently a member of Ninjas in Pyjamas. He is considered one of the best Counter-Strike players in the history of the series. In his comeback",
        "location": "Noida",
        "device_id": "123123123",
        "device_type": "ios",
        "is_admin": "0",
        "linkdin_link": "",
        "website_link": "",
        "twitter_link": "",
        "instagram_link": "tanusingh291",
        "notification": "OFF",
        "email_push": "ON",
        "created_at": "2018-05-30 13:40:41",
        "updated_at": "2018-06-13 07:47:31"
    },
    "message": "Data",
    "requestKey": "api/my_info"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************card_count*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) card_count</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'

              </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>card_count</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "request": {
        "count": "1"
    },
    "message": "Phone number is in record.",
    "requestKey": "api/card_count"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************degree_list*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) degree_list</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'

              </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>degree_list</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "response": [
        {
            "degree_id": "1",
            "name": "MCA"
        },
        {
            "degree_id": "2",
            "name": "BSc"
        },
        {
            "degree_id": "8",
            "name": "MBA"
        },
        {
            "degree_id": "29",
            "name": "Bachelor Of Computer Science (B.Sc. (computer Science))"
        },
        {
            "degree_id": "28",
            "name": "Bachelor Of Computer Application (B.C.A.)"
        },
        {
            "degree_id": "27",
            "name": "Bachelor Of Commerce (B.Com)"
        },
        {
            "degree_id": "26",
            "name": "Bachelor Of Business Administration (B.B.A)"
        },
        {
            "degree_id": "25",
            "name": "Bachelor Of Ayurvedic Medicine & Surgery (B.A.M.S)"
        },
        {
            "degree_id": "24",
            "name": "Bachelor Of Arts (B.A.)"
        },
        {
            "degree_id": "23",
            "name": "Bachelor Of Architecture (B.Arch)"
        },
        {
            "degree_id": "22",
            "name": "Bachelor Of Agriculture (B.sc (Agriculture))"
        },
        {
            "degree_id": "30",
            "name": "Bachelor Of Dental Surgery (B.D.S)"
        },
        {
            "degree_id": "31",
            "name": "Bachelor Of Design (B.Des.)"
        },
        {
            "degree_id": "32",
            "name": "Bachelor Of Education (B.Ed)"
        },
        {
            "degree_id": "33",
            "name": "Bachelor Of Laws (L.L.B.) "
        },
        {
            "degree_id": "34",
            "name": "Bachelor Of Pharmacy (B.Pharm.)"
        },
        {
            "degree_id": "35",
            "name": "Bachelor Of Physical Education (B.P.Ed.)"
        },
        {
            "degree_id": "36",
            "name": "Bachelor Of Science (B.Sc.)"
        }
    ],
    "message": "List.",
    "requestKey": "api/degree_list"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>



<!-- *************************ignore_user*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) ignore_user</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'user_id' => 'required|max:190',

              </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>ignore_user</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Save successfully.",
    "requestKey": "api/ignore_user"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************update_location*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;"><?= $i++ ?>) update_location</span>
      </a>
      <div class="accordion-content">
          <p>
              <h5>Parameters: </h5>
              <pre>
'header'
'lat' => 'required|max:190',
'log' => 'required|max:190',

              </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>device_type: android,ios</p>
          <p> <h5>requestKey: </h5>update_location</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status_code": "200",
    "status": "SUCCESS",
    "message": "Save successfully.",
    "requestKey": "api/update_location"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


  </div>
</div>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
      $('.accordion-toggle').on('click', function(event){
        event.preventDefault();

        // create accordion variables
        var accordion = $(this);
        var accordionContent = accordion.next('.accordion-content');
        var accordionToggleIcon = $(this).children('.toggle-icon');

        // toggle accordion link open class
        accordion.toggleClass("open");

        // toggle accordion content
        accordionContent.slideToggle(250);

        // change plus/minus icon
        if (accordion.hasClass("open")) {
          accordionToggleIcon.html("<i class='fa fa-minus-circle'></i>");
        } else {
          accordionToggleIcon.html("<i class='fa fa-plus-circle'></i>");
        }
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function () {
      $('ul.nav a').each(function(){
        if(location.href === this.href){
          $(this).addClass('active');
          $('ul.nav a').not(this).removeClass('active');
          return false;
        }
      });
    });
    </script>
</body>
</html>
