

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Help Us</title>
<link href="/favicon.ico" type="image/x-icon" rel="icon" />

<link href="http://verproapp.com/css/style.css" rel="stylesheet" type="text/css"/>
<link href="http://verproapp.com/css/animate.css" rel="stylesheet" type="text/css"/>
<link href="http://verproapp.com/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

</head>
<style type="text/css">
  .logo img {
    width: 130px;
}
</style>
<body>
<!-- Header -->


<header class="header headerBg">
	<div class="container">
    	<div class="row">
        	<div class="col-md-3 col-sm-3">
            	<div class="logo">
                	<a href="/"><img src="http://mobulous.co.in/collabor/public/img/logo_5.png" /></a>
                </div>
            </div>
            <div class="col-md-9 col-sm-9">
            	
            </div>
        </div>
    </div>
</header>

<section>
	<div class="container">
    	<div class="helpContent">
            
            <div class="helpContact">
            	<h2>Contact Information</h2>
                <form method="post">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Contact Name</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Contact Name" name="email" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Contact Number</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Contact Number" name="number" required>
                  </div>
                  <div class="form-group">
                    <label>Description</label>
                    <textarea rows="4" class="form-control" placeholder="Description"  name="description" required></textarea>
                  </div>
                  
                  
                  <input type="submit" class="btn btn-default" value="Submit">
                </form>
            </div>
        </div>
    </div>
</section>




</body>
</html>